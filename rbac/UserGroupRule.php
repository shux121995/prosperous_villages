<?php
/**
 * Created by PhpStorm.
 * User: Muxtorov Ulugbek
 * Date: 12.09.2019
 * Time: 18:37
 */

namespace app\rbac;

use Yii;
use yii\rbac\Rule;

class UserGroupRule extends Rule
{
    public $name = 'userGroup';

    public function execute($user, $item, $params)
    {
        if (!\Yii::$app->user->isGuest) {
            $group = \Yii::$app->user->identity->username;
            if ($item->name === 'admin') {
                return $group == 'admin';
            } elseif ($item->name === 'region') {
                return $group == 'admin' || $group == 'region';
            } elseif ($item->name === 'author') {
                return $group == 'admin' || $group == 'author';
            } elseif ($item->name === 'viewer') {
                return $group == 'admin' || $group == 'viewer';
            }
        }
        return true;
    }
}