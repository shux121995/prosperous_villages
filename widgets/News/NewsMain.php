<?php
namespace app\widgets\News;
use app\models\News;
use app\models\Language;

class NewsMain extends \yii\bootstrap\Widget
{
    public function init(){}

    public function run() {
        
        $query = News::find()
                ->where(['news_language'=>(new \app\components\LangHelper())->check_lang(),'news_show'=>1])
                ->orderBy('news_date DESC')
                ->limit(2);
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->pagination = false;
        return $this->render('view', [
            'news' => $dataProvider->models,
        ]);
    }
}