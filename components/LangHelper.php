<?php
namespace app\components;

use Yii;
use yii\base\Component;

class LangHelper extends Component {

    /**
     * @return string
     * public lang
     */
    public function check_lang()
    {

        if(isset($_GET['language']))
        {
            $lang =  $_GET['language'];
            //if(!isset($_SESSION['lang']))
            $_SESSION['lang'] = $lang;
        }
        else
        {
            if(isset($_SESSION['lang'])) {
                $lang = $_SESSION['lang'];
            }else{
                $lang = 'en';
                $_SESSION['lang'] = $lang;
            }

        }
        return $lang;
    }

    /**
     * @return string
     * static lang
     */
    public static function lang()
    {
        return (new LangHelper())->check_lang();
    }

}
