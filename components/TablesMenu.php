<?php
/**
 * Created by PhpStorm.
 * User: Muxtorov Ulugbek
 * Date: 15.04.2019
 * Time: 17:05
 */

namespace app\components;


use app\models\Tables;
use yii\base\Component;

class TablesMenu extends Component
{
    /**
     * Menu для пользователей
     * @param string $link
     * @param array $menu
     * @return array
     */
    public static function userMenu($link = 'facts/index', $menu = [])
    {
        $tables = Tables::find()->All();
        foreach ($tables as $table) {
            $menu[] = [
                'label' => $table->title,
                'url' => [$link, 'table' => $table->id],
                'linkOptions' => ['title' => '(' . $table->excel_name . ') ' . $table->excel_description]
            ];
        }
        $qurulish_title = \app\models\Qurulish::TableNameShort;
        $bozor_title = \app\models\BozorInfratuzilmasi::TableNameShort;
        $qurulish_full_title = \app\models\Qurulish::TableName;
        $bozor_full_title = \app\models\BozorInfratuzilmasi::TableName;
        if($link === 'plans/index') {
            $menu[] = ['label' => $qurulish_title, 'url' => ['/qurilish/index'], 'linkOptions'=>['title'=>$qurulish_full_title]];
            $menu[] = ['label' => $bozor_title, 'url'=>['/bozor-infratuzilmasi/index'], 'linkOptions'=>['title'=>$bozor_full_title]];
        }
        elseif ($link === 'facts/index') {
            $menu[] = ['label' => $qurulish_title, 'url' => ['/qurilish-amalda/index'], 'linkOptions'=>['title'=>$qurulish_full_title]];
            $menu[] = ['label' => $bozor_title, 'url'=>['/bozor-infratuzilmasi-amalda/index'], 'linkOptions'=>['title'=>$bozor_full_title]];
        }
        elseif($link === 'respublika/index') {
            $menu[] = ['label' => $qurulish_title, 'url' => ['/results/respublika/qurilish'], 'linkOptions'=>['title'=>$qurulish_full_title]];
            $menu[] = ['label' => $bozor_title, 'url'=>['/results/respublika/bozor-infratuzilmasi'], 'linkOptions'=>['title'=>$bozor_full_title]];
        }
        elseif($link === 'region/index') {
            $menu[] = ['label' => $qurulish_title, 'url' => ['/results/region/qurilish'], 'linkOptions'=>['title'=>$qurulish_full_title]];
            $menu[] = ['label' => $bozor_title, 'url'=>['/results/region/bozor-infratuzilmasi'], 'linkOptions'=>['title'=>$bozor_full_title]];
        }
        return $menu;
    }

    /**
     * Menu для пользователей с годами
     * @param string $link
     * @param array $menu
     * @return array
     */
    public static function userMenuYears($menu = [])
    {
        $years = \Yii::$app->params['years'];
        $tables = Tables::find()->All();
        foreach ($years as $year) {
            $submenu = [];
            foreach ($tables as $table) {
                $submenu[] = [
                    'label' => $table->title . ' (' . $table->excel_name . ')',
                    'url' => ['/tables/list', 'year' => $year, 'table' => $table->id],
                    'linkOptions' => ['title' => $table->excel_description]
                ];
            }

            $menu[] = ['label' => $year,// 'url' => ['#'],
                'submenuOptions' => ['class' => 'dropdown-menu'],
                'items' => $submenu,
            ];
        }

        return $menu;
    }
}