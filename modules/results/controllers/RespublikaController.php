<?php

namespace app\modules\results\controllers;

use app\models\Region;
use app\models\Tables;
use app\modules\results\models\GenerateTable;
use app\models\Qurulish;
use yii\helpers\Url;

class RespublikaController extends \yii\web\Controller
{
    public function actionIndex($table=null)
    {
        //Вернем к главную
        if($table==null) return $this->redirect(Url::to('/results'));
        $locals = Region::find()->all();

        return $this->render('index',
            [
                'table'     => Tables::findOne($table),
                'header'    => GenerateTable::genTable($table),
                'locals'    => $locals
            ]
        );
    }

    public function actionQurilish()
    {
        $locals = Region::find()->all();
        return $this->render('qurilish', [
            'header'=>GenerateTable::genTableQurulish(),
            'locals'    => $locals
            ]);
    }

    public function actionBozorInfratuzilmasi()
    {
        $locals = Region::find()->all();
        return $this->render('bozor', [
            'header'=>GenerateTable::genTableBozor(),
            'locals' => $locals,
        ]);
    }
}
