<?php

namespace app\modules\results\controllers;

use app\models\District;
use app\models\Locality;
use app\models\Tables;
use app\modules\results\models\GenerateTable;

class DistrictController extends \yii\web\Controller
{
    /**
     * View results for Localities of district
     * @param null $table
     * @param null $district
     * @return string|\yii\web\Response
     */
    public function actionIndex($table=null,$district=null)
    {
        //Вернем к главную
        if($table==null && $district==null) return $this->redirect(Url::to('/results'));
        $locals = Locality::find()->where(['district_id'=>$district])->all();


        return $this->render('index',
            [
                'table'     => Tables::findOne($table),
                'header'    => GenerateTable::genTable($table),
                'locals'    => $locals,
                'district'  => District::findOne($district),

            ]
        );
    }

    /**
     * Export for Excel
     * @param null $table
     * @param null $district
     * @return \yii\web\Response
     */
    public function actionExcelOut($table=null,$district=null)
    {
        //Вернем к главную
        if($table==null && $district==null) return $this->redirect(Url::to('/results'));
        $locals = Locality::find()->where(['district_id'=>$district])->all();
        $filename = 'result_table_'.$table.'_district_'.$district.'.xls';
        $district = District::findOne($district);
        $header = GenerateTable::genTable($table);
        $table = Tables::findOne($table);

        header('Content-type: application/excel');
        header('Content-Disposition: attachment; filename='.$filename);

        $data = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">
<head>
    <!--[if gte mso 9]>
    <xml>
        <x:ExcelWorkbook>
            <x:ExcelWorksheets>
                <x:ExcelWorksheet>
                    <x:Name>Sheet 1</x:Name>
                    <x:WorksheetOptions>
                        <x:Print>
                            <x:ValidPrinterInfo/>
                        </x:Print>
                    </x:WorksheetOptions>
                </x:ExcelWorksheet>
            </x:ExcelWorksheets>
        </x:ExcelWorkbook>
    </xml>
    <![endif]-->
</head>

<body>';
        $html = '
<table class="table table-bordered table-hover">
    <thead>
    <tr><td colspan="16">'.$district->title .' / '. $district->region->title .' / '. $table->title .' / '. $table->excel_name.'</td></tr>
    <tr><td colspan="16">'.$table->excel_description.'</td></tr>
    '.$header.'
    </thead>
    <tbody>';

    /** @var $local \app\models\Locality*/
    $i = 1;
    foreach ($locals as $local){
        $html .= '<tr>
            <td>'.$i.'</td>
            <td>'.$local->title.'</td>';
            $excels = GenerateTable::getExcelInputs($table->id);
            foreach ($excels as $excel) {
                $html .= '<td>';
                //get sum and show results
                $html .= GenerateTable::getSum($excel->input_id,$excel->section_id,$local->id,'locality_id');
                $html .='</td>';
            }
        $html .= '</tr>';
        $i++;
    }
    $html .= '</tbody>
</table>';
        $data .= $html;
        $data .= '</body></html>';
        echo $data;
        exit();
    }

    public function actionQurilish($district)
    {
        if($district==null) return $this->redirect(Url::to('/results'));
        $locals = Locality::find()->where(['district_id'=>$district])->all();
        return $this->render('qurilish',[
            'header' => GenerateTable::genTableQurulish(),
            'locals' => $locals,
            'district'  => District::findOne($district),
        ]);
    }

    public function actionBozorInfratuzilmasi($district)
    {
        if($district==null) return $this->redirect(Url::to('/results'));
        $locals = Locality::find()->where(['district_id'=>$district])->all();
        return $this->render('bozor',[
            'header' => GenerateTable::genTableBozor(),
            'locals' => $locals,
            'district'  => District::findOne($district),
        ]);
    }
}
