<?php

namespace app\modules\results\controllers;

use app\models\District;
use app\models\Region;
use app\models\Tables;
use app\modules\results\models\GenerateTable;
use yii\helpers\Url;

class RegionController extends \yii\web\Controller
{
    public function actionIndex($table=null,$region=null)
    {
        //Вернем к главную
        if($table==null && $region==null) return $this->redirect(Url::to('/results'));
        $locals = District::find()->where(['region_id'=>$region])->all();

        return $this->render('index',
            [
                'table'     => Tables::findOne($table),
                'header'    => GenerateTable::genTable($table),
                'locals'    => $locals,
                'region'    => Region::findOne($region),
            ]
        );
    }

    public function actionQurilish($region)
    {
        if($region==null) return $this->redirect(Url::to('/results'));
        $locals = District::find()->where(['region_id'=>$region])->all();

        return $this->render('qurilish',[
            'header' => GenerateTable::genTableQurulish(),
            'locals' => $locals,
            'region'    => Region::findOne($region),
        ]);
    }

    public function actionBozorInfratuzilmasi($region)
    {
        if($region==null) return $this->redirect(Url::to('/results'));
        $locals = District::find()->where(['region_id'=>$region])->all();
        return $this->render('bozor',[
            'header' => GenerateTable::genTableBozor(),
            'locals' => $locals,
            'region' => Region::findOne($region),
        ]);
    }
}
