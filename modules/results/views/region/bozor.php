<?php
/* @var $this yii\web\View */
use app\modules\results\models\GenerateTable;
use app\models\MoliyaManbai;
$this->title = $region->title .' / '. \app\models\BozorInfratuzilmasi::TableNameShort;
?>
<h2><?= $this->title?></h2>
<h3><?= \app\models\BozorInfratuzilmasi::TableName?></h3>

<table class="table table-bordered">
    <thead>
    <?=$header?>
    </thead>
    <tbody>
    <?php
    /** @var $local \app\models\Region*/
    $i = 1;
    foreach ($locals as $local){ ?>
        <tr>
            <td><?=$i?></td>
            <td><?= \yii\helpers\Html::a($local->title,\yii\helpers\Url::toRoute(['/results/district/bozor-infratuzilmasi', 'district'=>$local->id]))?></td>
            <?php
            echo '<td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>'.GenerateTable::genBozorSum($local->id, 'bozor', 'district_id', 'quvvat_ish_chiq_hajm').'</td>
                    <td>'.GenerateTable::genBozorSum($local->id, 'bozor', 'district_id', 'yil_eksport_hajm').'</td>
                    <td>'.GenerateTable::genBozorSum($local->id, 'bozor', 'district_id', 'yil_byudjetga_tushum_hajm').'</td>
                    <td>'.GenerateTable::genBozorSum($local->id, 'bozor', 'district_id', 'loyiha_qiy_reja').'</td>
                    <td>'.GenerateTable::genBozorSum($local->id, 'bozor_amalda', 'district_id', 'loyiha_qiy_amalda').'</td>';
            $moliya_manbai = MoliyaManbai::find()->all();
            foreach($moliya_manbai as $item){
                echo '<td>'.GenerateTable::genBozorSum($local->id, 'reja', 'district_id', '', $item->id).'</td>
                            <td>'.GenerateTable::genBozorSum($local->id, 'amalda', 'district_id', '', $item->id).'</td>';
            }
            echo '<td></td>
                        <td></td>
                        <td></td>
                        <td>'.GenerateTable::genBozorSum($local->id, 'bozor', 'district_id', 'ish_urin_reja').'</td>
                        <td>'.GenerateTable::genBozorSum($local->id, 'bozor_amalda', 'district_id', 'ish_urin_amalda').'</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>';
            ?>
        </tr>
        <?php
        $i++;
    }
    ?>
    </tbody>
</table>
