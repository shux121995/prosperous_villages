<?php
/* @var $this yii\web\View */
use app\modules\results\models\GenerateTable;

$this->title = $region->title .' / '. \app\models\Qurulish::TableNameShort;
?>
<h2><?= $this->title?></h2>
<h3><?= \app\models\Qurulish::TableName?></h3>
<table class="table table-bordered">
    <thead>
        <?=$header?>
    </thead>
    <tbody>
    <?php
    /** @var $local \app\models\Region*/
    $i = 1;
    foreach ($locals as $local){ ?>
        <tr>
            <td><?=$i?></td>
            <td><?= \yii\helpers\Html::a($local->title,\yii\helpers\Url::toRoute(['/results/district/qurilish', 'district'=>$local->id]))?></td>
            <?php
            echo '<td></td>
                    <td></td>
                    <td></td>
                    <td>'.GenerateTable::genQurulishSum($local->id, 'reja','district_id').'</td>
                    <td></td>
                    <td>'.GenerateTable::genQurulishSum($local->id, 'qurulish','district_id', 'qiy_molled').'</td>
                    <td>'.GenerateTable::genQurulishSum($local->id, 'amalda','district_id').'</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>';
            ?>
        </tr>
        <?php
        $i++;
    }
    ?>
    </tbody>
</table>
