<?php
use app\models\Qurulish;
use app\models\BozorInfratuzilmasi;
?>
<div class="results-default-index">
    <h1>Килинган ишлар буйича натижалар:</h1>
    <h2>Жой номини танланг:</h2>
    <table style="width: 80%; margin: auto;" class="table table-bordered table-responsive table-hover">
        <thead>
        <th>#</th>
        <th>Республика кесимида</th>
        <th>Вилоят номи</th>
        <th>Туман номи</th>
        </thead>
    <?php
    $locals = \app\models\District::find()->orderBy('region_id')->all();
    $lcs = [];
    /** @var $local \app\models\District*/
    foreach ($locals as $key=>$local){
        $lcs[$key]['region']['id'] = $local->region_id;
        $lcs[$key]['region']['title'] = $local->region->title;
        $lcs[$key]['locality']['id'] = $local->id;
        $lcs[$key]['locality']['title'] = $local->title;
    }

    $i = 1;
    $region = 0;
    foreach ($lcs as $lcl){
        echo '<tr>';
        echo '<td>'.$i.'</td>';
        if($i==1) echo '<td rowspan="'.count($locals).'" class="pointer" data-toggle="modal" data-target="#resp" style="vertical-align:top; text-align:center;" ><a href="#">Республика кечимида</a></td>';
        if ($region != $lcl['region']['id']){
            $lcl['region']['rowspan'] = \app\models\District::find()->where(['region_id'=>$lcl['region']['id']])->count();
            echo '<td class="pointer" data-toggle="modal" data-target="#reg_'.$lcl['region']['id'].'" style="vertical-align:middle; text-align:center;" rowspan="'.($lcl['region']['rowspan']).'"><strong><a href="#" onclick="return false;">'.$lcl['region']['title'].'</a></strong></td>';
        }
        $region = $lcl['region']['id'];
        echo '<td class="pointer" data-toggle="modal" data-target="#dis_'.$lcl['locality']['id'].'"><a href="#" onclick="return false;">'.$lcl['locality']['title'].'</a></td>';
        echo '</tr>';
        $i++;
    }
    ?>
    </table>
</div>
<?php $regions = \app\models\Region::find()->all(); ?>
<?php foreach ($regions as $region){?>
<!-- Modal -->
<div class="modal fade" id="reg_<?=$region->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><?=$region->title?>:<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button></h5>
            </div>
            <div class="modal-body">
                <ul>
                <?php
                $tables = \app\models\Tables::find()->All();
                foreach ($tables as $table) {
                    echo '<li>'.\yii\helpers\Html::a($table->title,
                        \yii\helpers\Url::toRoute(['/results/region','table'=>$table->id,'region'=>$region->id]),
                        ['title' => '(' . $table->excel_name . ') ' . $table->excel_description]
                    ).'</li>';
                }
                ?>
                    <li><a href="/results/region/qurilish?region=<?=$region->id?>" title='<?=Qurulish::TableName?>'><?=Qurulish::TableNameShort?></a></li>
                    <li><a href="/results/region/bozor-infratuzilmasi?region=<?=$region->id?>" title='<?=BozorInfratuzilmasi::TableName?>'><?=BozorInfratuzilmasi::TableNameShort?></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<?php $districts = \app\models\District::find()->all(); ?>
<?php foreach ($districts as $district){?>
<!-- Modal -->
<div class="modal fade" id="dis_<?=$district->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><?=$district->title?>:<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button></h5>
            </div>
            <div class="modal-body">
                <ul>
                <?php
                $tables = \app\models\Tables::find()->All();
                foreach ($tables as $table) {
                    echo '<li>'.\yii\helpers\Html::a($table->title,
                        \yii\helpers\Url::toRoute(['/results/district','table'=>$table->id,'district'=>$district->id]),
                        ['title' => '(' . $table->excel_name . ') ' . $table->excel_description]
                    ).'</li>';
                }
                ?>
                    <li><a href="/results/district/qurilish?district=<?=$district->id?>" title='<?=Qurulish::TableName?>'><?=Qurulish::TableNameShort?></a></li>
                    <li><a href="/results/district/bozor-infratuzilmasi?district=<?=$district->id?>" title='<?=BozorInfratuzilmasi::TableName?>'><?=BozorInfratuzilmasi::TableNameShort?></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<!-- Modal -->
<div class="modal fade" id="resp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Республика кесимида:<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button></h5>
            </div>
            <div class="modal-body">
                <ul>
                    <?php
                    $tables = \app\models\Tables::find()->All();
                    foreach ($tables as $table) {
                        echo '<li>'.\yii\helpers\Html::a($table->title,
                                \yii\helpers\Url::toRoute(['/results/respublika/index','table'=>$table->id]),
                                ['title' => '(' . $table->excel_name . ') ' . $table->excel_description]
                            ).'</li>';
                    }
                    ?>
                    <li style="display: none;"><a href="/results/district/qurilish?district=<?=$district->id?>" title='<?=Qurulish::TableName?>'><?=Qurulish::TableNameShort?></a></li>
                    <li style="display: none;"><a href="/results/district/bozor-infratuzilmasi?district=<?=$district->id?>" title='<?=BozorInfratuzilmasi::TableName?>'><?=BozorInfratuzilmasi::TableNameShort?></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
