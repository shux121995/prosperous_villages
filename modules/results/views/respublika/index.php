<?php
use app\modules\results\models\GenerateTable;
/* @var $this yii\web\View */
/** @var $table \app\models\Tables */

$this->title = $table->title .' / '. $table->excel_name .' / '. $table->excel_description;
?>
<h2><?= $table->title .' / '. $table->excel_name?></h2>
<h3><?= $table->excel_description?></h3>
<table class="table table-bordered table-hover">
    <thead>
        <?=$header?>
    </thead>
    <tbody>
        <?php
        /** @var $local \app\models\Region*/
        $i = 1;
        foreach ($locals as $local){ ?>
            <tr>
                <td><?=$i?></td>
                <td><?= \yii\helpers\Html::a($local->title,\yii\helpers\Url::toRoute(['/results/region','table'=>$table->id,'region'=>$local->id,]))?></td>
                <?php
                $excels = GenerateTable::getExcelInputs($table->id);
                foreach ($excels as $excel) {
                    echo '<td class="sum">';
                    //get sum and show results
                    echo GenerateTable::getSum($excel->input_id,$excel->section_id,$local->id,'region_id');
                    echo '</td>';
                }
                ?>
            </tr>
        <?php
            $i++;
        }
        $excels = GenerateTable::getExcelInputs($table->id);
        echo '<td></td>';
        echo '<td class="all"><b>Жами:</b></td>';
        foreach ($excels as $excel) {
            echo '<td class="total"></td>';
        }
        ?>
    </tbody>
</table>
<?php
$js = <<<JS
$(function()
{
    function tally (selector) 
    {
      $(selector).each(function () 
      {
          var total = 0,
          column = $(this).siblings(selector).addBack().index(this);
          $(this).parents().prevUntil(':has(' + selector + ')').each(function () 
          {
            total += parseFloat($('td.sum:eq(' + column + ')', this).html()) || 0;
          })
          $(this).html(total);
     });
    }
  //tally('td.subtotal');
  tally('td.total');
});
JS;
$this->registerJs($js, \yii\web\View::POS_END);
