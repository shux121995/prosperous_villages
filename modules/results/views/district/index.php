<?php
use app\modules\results\models\GenerateTable;
/* @var $this yii\web\View */
/** @var $table \app\models\Tables */

$this->title = $district->title .' / '. $district->region->title .' / '. $table->title .' / '. $table->excel_name .' / '. $table->excel_description;
?>
<h2><?=\yii\helpers\Html::a('Export XLS',['excel-out','table'=>$table->id,'district'=>$district->id],['class'=>'btn btn-danger pull-right'])?> <?= $district->title .' / '. $district->region->title .' / '. $table->title .' / '. $table->excel_name?></h2>
<h3><?= $table->excel_description?></h3>
<table class="table table-bordered table-hover">
    <thead>
    <?=$header?>
    </thead>
    <tbody>
    <?php
    /** @var $local \app\models\Locality*/
    $i = 1;
    foreach ($locals as $local){ ?>
        <tr>
            <td><?=$i?></td>
            <td><?= $local->title//\yii\helpers\Html::a($local->title,\yii\helpers\Url::toRoute(['/results/locality','table'=>$table->id,'locality'=>$local->id,]))?></td>

            <?php
            $excels = GenerateTable::getExcelInputs($table->id);
            foreach ($excels as $excel) {
                echo '<td class="sum">';
                //get sum and show results
                echo GenerateTable::getSum($excel->input_id,$excel->section_id,$local->id,'locality_id');
                echo '</td>';
            }
            ?>
        </tr>
        <?php
        $i++;
    }
    $excels = GenerateTable::getExcelInputs($table->id);
    echo '<td></td>';
    echo '<td class="all"><b>Жами:</b></td>';
    foreach ($excels as $excel) {
        echo '<td class="total"></td>';
    }
    ?>
    </tbody>
</table>
<?php
$js = <<<JS
$(function()
{
    function tally (selector) 
    {
      $(selector).each(function () 
      {
          var total = 0,
          column = $(this).siblings(selector).addBack().index(this);
          $(this).parents().prevUntil(':has(' + selector + ')').each(function () 
          {
            total += parseFloat($('td.sum:eq(' + column + ')', this).html()) || 0;
          })
          $(this).html(total);
     });
    }
  //tally('td.subtotal');
  tally('td.total');
});
JS;
$this->registerJs($js, \yii\web\View::POS_END);
