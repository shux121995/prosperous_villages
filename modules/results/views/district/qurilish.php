<?php
/* @var $this yii\web\View */
use app\modules\results\models\GenerateTable;
use app\models\Qurulish;

$this->title = $district->title .' / '. $district->region->title .' / '. \app\models\Qurulish::TableNameShort;
?>
<h2><?= $this->title?></h2>
<h3><?= \app\models\Qurulish::TableName?></h3>
<table class="table table-bordered">
    <thead>
    <?=$header?>
    </thead>
    <tbody>
    <?php
    /** @var $local \app\models\Region*/
    $i = 1;
    foreach ($locals as $local){ ?>
        <?php
        $locality_plan = \app\models\LocalityPlans::find()->where(['locality_id'=>$local->id])->one();
        $qurulish = \app\models\Qurulish::find()->where(['plan_id'=>$locality_plan->id])->all();
        $moliya_manbai = \app\models\MoliyaManbai::find()->all();
        $count_qurulish = count($qurulish);

        ?>
        <?php foreach($qurulish as $key => $value):
            $qurulish_amalda = \app\models\QurulishAmalda::find()->where(['qurulish_id'=>$value->id])->one();
            $values_plan = \app\models\ValuesPlan::find()->where(['qurulish_id'=>$value->id])->one();
            $values_fact = \app\models\ValuesFact::find()->where(['plan_value_id'=>$values_plan->id])->one();
            $rowspan = '';
            if($count_qurulish>1 and $local->title != null) {
                $rowspan = 'rowspan="'.$count_qurulish.'"';
            }
            if(empty($value->maktab_raqam)) {
                $value->maktab_raqam = '-';
            }
            if(empty($value->mtm_raqam)) {
                $value->mtm_raqam = '-';
                $value->mtm_mulkchilik_shakli = '-';
            }
            if(empty($value->ssm)) {
                $value->ssm = '-';
            }
        ?>
        <tr>
            <?php if($local->title != null): ?>
                <td <?=$rowspan?> ><?=$i?></td>
                <td <?=$rowspan?> > <?=$local->title;?></td>
            <?php endif; ?>
            <?php if($count_qurulish>1){
                $local->title = null; $i = null;$i++;
            }
            ?>
            <?php
            echo '<td>'.$value->title.'</td>
                    <td>'.Qurulish::types()[$value->type].'</td>
                    <td>'.Qurulish::workTypes()[$value->baj_ish_turi].'</td>
                    <td>'.$values_plan->reja.'</td>
                    <td>';
                    foreach($moliya_manbai as $item):
                        echo $item->title.'<br/>';
                    endforeach;
                   echo '</td><td>'.$value->qiy_molled.'</td>';
                    if($values_fact != null) {
                        echo '<td>'.$values_fact->fact.'</td>';
                    }
                    else {
                        echo '<td></td>';
                    }
                     echo '<td>'.$value->ishga_tushish_reja.'</td>
                          <td>'.$value->asos_akt.'</td>
                          <td>'.$value->asos_date.'</td>
                          <td>'.Qurulish::exist_project_document()[$value->loyiha_smeta_hujjat].'</td>
                          <td>'.Qurulish::exist_tarmoq_jadvali()[$value->tarmoq_jadvali].'</td>
                          <td>'.$value->loyihachi_tashkilot.'</td>
                          <td>'.$value->pudratchi_tashkilot.'</td>
                          <td>'.$value->maktab_raqam.'</td>
                          <td>'.$value->mtm_raqam.'</td>
                          <td>'.$value->mtm_mulkchilik_shakli.'</td>
                          <td>'.$value->ssm.'</td>';
                    if($qurulish_amalda != null) {
                        echo '<td>'.$qurulish_amalda->oh_yer.'</td>
                        <td>'.$qurulish_amalda->oh_poydevor.'</td>
                        <td>'.$qurulish_amalda->oh_gisht_terish.'</td>
                        <td>'.$qurulish_amalda->oh_tom_yopish.'</td>
                        <td>'.$qurulish_amalda->oh_demontaj.'</td>
                        <td>'.$qurulish_amalda->oh_oraliq_devor.'</td>
                        <td>'.$qurulish_amalda->oh_eshik_deraza.'</td>
                        <td>'.$qurulish_amalda->oh_ichki_pardozlash.'</td>
                        <td>'.$qurulish_amalda->oh_tashqi_pardozlash.'</td>';
                    }
                    else {
                        echo '<td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>';
                    }

            ?>
        </tr>
        <?php endforeach; ?>
        <?php
        $i++;
    }
    ?>
    </tbody>
</table>
