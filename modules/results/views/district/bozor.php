<?php
/* @var $this yii\web\View */
use app\modules\results\models\GenerateTable;
use app\models\MoliyaManbai;
use app\models\BozorInfratuzilmasi;
$this->title = $district->title .' / '. $district->region->title .' / '. \app\models\BozorInfratuzilmasi::TableNameShort;
?>
<h2><?= $this->title?></h2>
<h3><?= \app\models\BozorInfratuzilmasi::TableName?></h3>

<table class="table table-bordered">
    <thead>
    <?=$header?>
    </thead>
    <tbody>
    <?php
    /** @var $local \app\models\Region*/
    $i = 1;
    foreach ($locals as $local){ ?>
        <?php
        $locality_plan = \app\models\LocalityPlans::find()->where(['locality_id'=>$local->id])->one();
        $bozor = BozorInfratuzilmasi::find()->where(['plan_id'=>$locality_plan->id])->all();
        $count_bozor = count($bozor);
        ?>
        <?php foreach($bozor as $key => $value):
            $bozor_amalda = \app\models\BozorInfratuzilmasiAmalda::find()->where(['bozor_id'=>$value->id])->one();
            $values_plan = \app\models\ValuesPlan::find()->where(['bozor_id'=>$value->id])->one();
            $values_fact = \app\models\ValuesFact::find()->where(['plan_value_id'=>$values_plan->id])->one();
            $rowspan = '';
            if($count_bozor>1 and $local->title != null){
                $rowspan = 'rowspan="'.$count_bozor.'"';
            }
            ?>
        <tr>
            <?php if($local->title != null): ?>
                <td <?=$rowspan?> ><?=$i?></td>
                <td <?=$rowspan?> > <?=$local->title;?></td>
            <?php endif; ?>
            <?php if($count_bozor>1){
                $local->title = null; $i = null; $i++;
            }
            ?>
            <?php
            echo '<td>'.$value->title.'</td>
                    <td>'.$value->loyiha_tashabbuskori.'</td>
                    <td>'.BozorInfratuzilmasi::types()[$value->loyiha_yunalishi].'</td>
                    <td>'.$value->quvvat_ulchov_birlik.'</td>
                    <td>'.$value->quvvat_natural_qiy.'</td>
                    <td>'.$value->quvvat_ish_chiq_hajm.'</td>
                    <td>'.$value->yil_eksport_hajm.'</td>
                    <td>'.$value->yil_byudjetga_tushum_hajm.'</td>
                    <td>'.$value->loyiha_qiy_reja.'</td>';
                if($bozor_amalda != null) {
                    echo '<td>'.$bozor_amalda->loyiha_qiy_amalda.'</td>';
                }
                else {
                    echo '<td></td>';
                }

                $values_manba_plan = \app\models\ValuesManbaPlan::find()->where(['values_plan_id'=>$values_plan->id])->all();
                if($values_fact != null) {
                    $values_manba_fact = \app\models\ValuesManbaFact::find()->where(['values_fact_id'=>$values_fact->id])->all();
                }
                $moliya_manbai = \app\models\MoliyaManbai::find()->all();
                $count_moliya_manbai = count($moliya_manbai);
                $j=0;
                for($n=1; $n<=$count_moliya_manbai; $n++){
                    echo '<td>'.$values_manba_plan[$j]['reja'].'</td>';
                    if($values_fact != null) {
                        echo '<td>'.$values_manba_fact[$j]['fact'].'</td>';
                    }
                    else {
                        echo '<td></td>';
                    }
                $j++;
                }
                echo '<td>'.$value->xorij_hamkor.'</td>
                    <td>'.$value->horij_davlat.'</td>
                    <td>'.$value->xiz_kursatuvchi_bank.'</td>
                    <td>'.$value->ish_urin_reja.'</td>';
                if($bozor_amalda != null) {
                      echo '<td>'.$bozor_amalda->ish_urin_amalda.'</td>';
                }
                else {
                    echo '<td></td>';
                }

                echo '<td>'.$value->ishga_tush_mud_reja.'</td>';
                if($bozor_amalda != null) {
                    echo '<td>'.$bozor_amalda->ishga_tush_mud_amalda.'</td>';
                }
                else {
                    echo '<td></td>';
                }
                echo '<td>'.$value->ishga_tush_mud_asos.'</td>
                    <td>'.BozorInfratuzilmasi::project_status1()[$value->loyiha_muhan_yer_holati].'</td>
                    <td>'.BozorInfratuzilmasi::project_status2()[$value->loyiha_muhan_elektr_holati].'</td>
                    <td>'.BozorInfratuzilmasi::project_status2()[$value->loyiha_muhan_gaz_holati].'</td>
                    <td>'.$value->loyiha_kredit_jadval_topshirish.'</td>
                    <td>'.$value->loyiha_kredit_haq_topshirish.'</td>
                    <td>'.$value->loyiha_kredit_jadval_ajratish.'</td>
                    <td>'.$value->loyiha_kredit_haq_ajratish.'</td>';
            ?>
        </tr>
        <?php endforeach; ?>
        <?php
        $i++;
    }
    ?>
    </tbody>
</table>
