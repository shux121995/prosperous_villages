<?php

namespace app\modules\results;

/**
 * results module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $layout = '//results';
    public $controllerNamespace = 'app\modules\results\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        \Yii::$app->setHomeUrl('/results');
        if(!isset($_SESSION['year'])) $_SESSION['year'] = date('Y');
        parent::init();

        // custom initialization code goes here
    }
}
