<?php
/**
 * Created by PhpStorm.
 * User: Muxtorov Ulugbek
 * Date: 16.04.2019
 * Time: 14:17
 */

namespace app\modules\results\models;

use Yii;
use yii\base\Component;
use app\models\Sections;
use app\models\Tables;
use app\models\TablesExcel;
use app\models\LocalityPlans;
use app\models\Inputs;
use app\models\ValuesPlan;
use app\models\ValuesFact;
use app\models\ValuesManbaPlan;
use app\models\ValuesManbaFact;
use app\models\Qurulish;
use app\models\QurulishAmalda;
use app\models\BozorInfratuzilmasi;
use app\models\BozorInfratuzilmasiAmalda;

class GenerateTable extends Component
{
    public static function genTable($table_id,$type=TablesExcel::TYPE_SECTION_INPUT,$title='Жой номи')
    {
        return self::genExcelTables($table_id,$type,$title);
    }

    /**
     * Генератор секции для header таблицы
     * @param $sections
     * @return string
     */
    public static function genExcelTables($table_id,$type,$title)
    {
        $table = Tables::findOne($table_id);
        $i=1;
        $html = '';
        while ($i<=$table->levels):
            $excels = self::getExcelInputs($table_id,$i,$type);
            $html .= '<tr>';
            if($i==1){
                $html .= '<th rowspan="'.($table->levels+1).'">';
                $html .= '<label>№</label>';
                $html .= '</th>';
                $html .= '<th rowspan="'.($table->levels+1).'">';
                $html .= '<label>'.$title.'</label>';
                $html .= '</th>';
            }

            /** @var $excel TablesExcel */
            foreach ($excels as $excel) {
                $section = TablesExcel::eSection($excel->section_id);
                $colspan = ($excel->colspan>1)?'colspan="'.$excel->colspan.'"':'';
                $rowspan = ($section->rows>1)?'rowspan="'.$section->rows.'"':'';

                if($excel->is_empty==null){
                    $html .= '<th '.$rowspan.' '.$colspan.'>';
                    $html .= '<label>' . $section->title . '</label>';
                    $html .= '</th>';
                }else{ /**$html .= '<th>'; $html .= '</th>';*/ } //Навсиякий случи
            }
            $html .= '</tr>';
            $i++;
        endwhile;

        if($type!=TablesExcel::TYPE_ONLY_SECTION){
            $html .= '<tr>';
            $excels = self::getExcelInputs($table_id,null,$type);
            /** @var $excel TablesExcel */
            foreach ($excels as $excel) {
                $input = TablesExcel::eInput($excel->input_id);
                $html .= '<th>';
                $html .= '<label>' . $input->title . '</label>';
                $html .= '</th>';

            }
            $html .= '</tr>';
        }
        return $html;
    }

    /**
     * @param $table_id
     * @param null $level
     * @param int $type
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getExcelInputs($table_id,$level=null,$type=TablesExcel::TYPE_SECTION_INPUT)
    {
        return TablesExcel::find()
            ->where(['table_id'=>$table_id,'level'=>$level,'type'=>$type])
            ->all();
    }

    /**
     * @param $inputId
     * @param $sectionId
     * @param $lpId //Это для таблице LocalityPlans reg,dis,local id
     * @param string $colName //Это колонка в таблице LocalityPlans
     * @return float|null
     */
    public static function getSum($inputId,$sectionId,$lpId,$colName='region_id')
    {
        $input = Inputs::findOne($inputId);

        //get plan ids for region
        $plans = LocalityPlans::find()->select('id')->where([$colName=>$lpId, 'year'=>$_SESSION['year']]);
        if($input->name=='moliya_manbai_id'){
            $sum = null;
        }elseif ($input->name=='fact'){
            //get plan values ids
            $planValues = ValuesPlan::find()
                ->select('id')
                ->where(['in','plan_id',$plans])
                ->andWhere(['section_id'=>$sectionId,'inputs_id'=>$inputId]);

            $sum = ValuesFact::find()
                ->where(['in','plan_value_id',$planValues])
                ->andWhere(['inputs_id'=>$inputId])
                ->sum('fact');
        }else{
            $sum = ValuesPlan::find()
                ->where(['in','plan_id',$plans])
                ->andWhere(['section_id'=>$sectionId,'inputs_id'=>$inputId])
                ->sum($input->name);
        }

        //show results
        return $sum>0?round($sum):null;
    }

    /**
     * Генератор секции для header таблицы
     * @param $sections
     * @return string
     */
    public static function genSections($sections,$table_id)
    {
        $table = Tables::findOne($table_id);
        $i=1;
        $html = '';
        while ($i<=$table->levels):
            $sections = Sections::find()->where(['level'=>$i,'tables_id'=>$table_id])->all();
            $html .= '<tr>';
                $html .= '<th>';
                $html .= '<label>'.$i.'</label>';
                $html .= '</th>';

            foreach ($sections as $sectioned) {
                if($sectioned->is_group){
                    $rowspan = count($sectioned->sections);
                }else{
                    $rowspan = $sectioned->cols;
                }

                $html .= '<th colspan="'.$rowspan.'">';
                $html .= '<label>' . $sectioned->title . ' ' . $sectioned->is_group . ' </br>level: ' . $sectioned->level . ' </br>cols: ' . $sectioned->cols . '</label>';
                $html .= '</th>';
            }

            $html .= '</tr>';
            $i++;
        endwhile;
        return $html;
        $html = '<tr>';
        foreach ($sections as $sectioned) {
            $html .= '<td>';
            $subSections = Sections::findOne($sectioned->id)->sections;
            $html .= '<label>' . $sectioned->title . ' ' . $sectioned->is_group . ' </br>level: ' . $sectioned->level . ' </br>cols: ' . $sectioned->cols . '</label>';
            $html .= '</td>';
        }
        $html .= '</tr>';
        if ($subSections != null) {
            //$html .= self::genSections($subSections);
        }
        return $html;
    }

    /**
     * @return string
     */
    public static function genTableQurulish()
    {
        $table = new Qurulish();
        $table2 = new QurulishAmalda();
        $html = '';
        $html .= '<tr><td rowspan="2"><label>№</label></td><td rowspan="2"><label>Жой номи</label></td>';
        $html .= '<td rowspan="2"><label>'.$table->getAttributeLabel('title').'</label></td><td rowspan="2"><label>'.$table->getAttributeLabel('type').'</label></td>
            <td rowspan="2"><label>'.$table->getAttributeLabel('baj_ish_turi').'</label></td><td colspan="4"><label>Қиймати (млн.сўм)</label></td>
            <td colspan="3"><label>Ишга тушиш муддати</label></td>
            <td rowspan="2"><label>'.$table->getAttributeLabel('loyiha_smeta_hujjat').'</label></td>
            <td rowspan="2"><label>'.$table->getAttributeLabel('tarmoq_jadvali').'</label></td><td rowspan="2"><label>'.$table->getAttributeLabel('loyihachi_tashkilot').'</label></td>
            <td rowspan="2"><label>'.$table->getAttributeLabel('pudratchi_tashkilot').'</label></td>
            <td rowspan="2"><label>'.$table->getAttributeLabel('maktab_raqam').'</label></td>
            <td rowspan="2"><label>'.$table->getAttributeLabel('mtm_raqam').'</label></td>
            <td rowspan="2"><label>'.$table->getAttributeLabel('mtm_mulkchilik_shakli').'</label></td>
            <td rowspan="2"><label>'.$table->getAttributeLabel('ssm').'</label></td>
            <td colspan="9"><label>Объект холати (%)</label></td>
            </tr>
            <tr>
            <td><label>Режа</label></td>
            <td><label>Молиялаштириш манбаи</label></td>
            <td><label>Молиялаштирилди</label></td>
            <td><label>Ўзлаштирилди</label></td>
            <td><label>Режа</label></td>
            <td><label>Асос (АКТ рақами)</label></td>
            <td><label>Асос муддати</label></td>
            <td><label>Ер ишлари</label></td>
            <td><label>Пойдевор ишлари</label></td><td><label>Ғишт териш</label></td>
            <td><label>Том ёпмаси</label></td><td><label>Демонтаж ишлари</label></td>
            <td><label>Оралиқ девор ишлари</label></td><td><label>Эшик ва дераза ўрнатиш</label></td>
            <td><label>Ички пардозлаш</label></td><td><label>Ташқи пардозлаш</label></td>
            </tr>';

        return $html;
    }

    public static function genQurulishSum($lpId, $type='reja', $colName='region_id', $column='')
    {

        //get plan ids for region
        $plans = LocalityPlans::find()->select('id')->where([$colName=>$lpId, 'year'=>$_SESSION['year']]);
        $qurulishIds = Qurulish::find()->select('id')->where(['in', 'plan_id', $plans]);
        if($type == 'qurulish') {
            $sum = Qurulish::find()
                ->where(['in','plan_id',$plans])
                ->sum($column);
        }
        elseif ($type=='amalda'){
            //get plan values ids
            $planValues = ValuesPlan::find()
                ->select('id')
                ->where(['in','plan_id',$plans])
                ->andWhere(['in', 'qurulish_id', $qurulishIds]);

            $sum = ValuesFact::find()
                ->where(['in','plan_value_id',$planValues])
                ->sum('fact');
        }elseif($type=='reja'){
            $sum = ValuesPlan::find()
                ->where(['in','plan_id',$plans])
                ->andWhere(['in','qurulish_id', $qurulishIds])
                ->sum('reja');
        }
        //show results
        return $sum>0?round($sum):null;
    }


    /**
     * @return string
     */
    public static function genTableBozor()
    {
        $table = new BozorInfratuzilmasi();
        $table2 = new BozorInfratuzilmasiAmalda();
        $moliya_manbai = \app\models\MoliyaManbai::find()->all();
        $moliya_manbai_sum = count($moliya_manbai);
        $moliya_manbai_parent_colspan =  count($moliya_manbai)*2;
        $html = '';

        $html .= '<tr><td rowspan="3"><label>№</label></td>
            <td rowspan="3"><label>Жой номи	</label></td>
            <td rowspan="3"><label>'.$table->getAttributeLabel('title').'</label></td>
            <td rowspan="3"><label>'.$table->getAttributeLabel('loyiha_tashabbuskori').'</label></td>
            <td rowspan="3"><label>'.$table->getAttributeLabel('loyiha_yunalishi').'</label></td>
            <td colspan="3"><label>Йиллик ишлаб чиқариш қуввати</label></td>
            <td rowspan="3"><label>'.$table->getAttributeLabel('yil_eksport_hajm').'</label></td>
            <td rowspan="3"><label>'.$table->getAttributeLabel('yil_byudjetga_tushum_hajm').'</label></td>
            <td rowspan="2" colspan="2"><label>Лойиҳа қиймати, млн сўмда</label></td>
            <td colspan="'.$moliya_manbai_parent_colspan.'"><label>Шу жумладан, молиялаштириш манбалари</label></td>
            <td colspan="2"><label>Хорижий инвестор</label></td>
            <td rowspan="3"><label>'.$table->getAttributeLabel('xiz_kursatuvchi_bank').'</label></td>
            <td rowspan="2" colspan="2"><label>Иш ўринлари</label></td>
            <td rowspan="2" colspan="3"><label>Ишга тушиш муддати</label></td>
            <td colspan="3"><label>Лойиҳага ер ажратиш ҳамда муҳандислик коммуникация тармоқларига уланиш ҳолати</label></td>
            <td colspan="4"><label>Лойиҳага кредит ажратиш ҳолати</label></td>
            </tr>
            <tr><td  rowspan="2"><label>Ўлчов бирлиги</label></td><td  rowspan="2"><label>Натурал қийматда</label></td>
            <td  rowspan="2"><label>Ишлаб чиқариш ҳажми, млн. сўм</label></td>';
            foreach($moliya_manbai as $item) {
                $html .= '<td colspan="2"><label>'.$item->title.'</label></td>';
            }
        $html .= '<td rowspan="2"><label>Хорижий хамкор номи</label></td>
            <td rowspan="2"><label>Давлати</label></td>
            <td rowspan="2"><label>Ер ажратиш ҳолати</label></td>
            <td rowspan="2"><label>Электр тармоқларига уланиш ҳолати</label></td>
            <td rowspan="2"><label>Газ тармоқларига уланиш ҳолати</label></td>
            <td rowspan="2"><label>Тармоқ жадвалига мувофиқ тижорат банкига кредит олиш учун ҳужжат топшириш санаси</label></td>
            <td rowspan="2"><label>Ҳақиқатда тижорат банкига кредит олиш учун ҳужжат топширилган сана</label></td>
            <td rowspan="2"><label>Тармоқ жадвалига мувофиқ тижорат банки томонидан кредит ажратиш санаси</label></td>
            <td rowspan="2"><label>Ҳақиқатда тижорат банки томонидан кредит ажратилган сана</label></td>
            </tr>
            <tr><td><label>Режа</label></td><td><label>Амалда</label></td>';
            $i=1;
            while($i<=$moliya_manbai_sum){
                $html .= '<td><label>Режа</label></td>
                          <td><label>Амалда</label></td>';
                $i++;
            }
            $html .= '<td><label>Режа</label></td>
                      <td><label>Амалда</label></td>
                      <td><label>Режа</label></td>
                      <td><label>Амалда</label></td>
                      <td><label>Асос</label></td></tr>';

        return $html;
    }

    public static function genBozorSum($lpId, $type='bozor', $colName='region_id', $column='', $moliya_manbai_id='')
    {

        //get plan ids for region
        $plans = LocalityPlans::find()->select('id')->where([$colName=>$lpId, 'year'=>$_SESSION['year']]);
        $bozorIds = BozorInfratuzilmasi::find()->select('id')->where(['in', 'plan_id', $plans]);
            if($type == 'bozor') {
                $sum = BozorInfratuzilmasi::find()
                    ->where(['in','plan_id',$plans])
                    ->sum($column);
            }
            elseif($type == 'bozor_amalda') {
                $sum = BozorInfratuzilmasiAmalda::find()
                    ->where(['in','bozor_id',$bozorIds])
                    ->sum($column);
            }
            elseif($type == 'amalda') {
                //get plan values ids
                $planValues = ValuesPlan::find()
                    ->select('id')
                    ->where(['in', 'plan_id', $plans])
                    ->andWhere(['in', 'bozor_id', $bozorIds]);

                $factValues = ValuesFact::find()
                    ->select('id')
                    ->where(['in', 'plan_value_id', $planValues]);

                $sum = ValuesManbaFact::find()
                    ->where(['in', 'values_fact_id', $factValues])
                    ->andWhere(['in', 'moliya_manba_id', $moliya_manbai_id])
                    ->sum('fact');
            }
            elseif($type == 'reja') {
                $planValues = ValuesPlan::find()
                    ->select('id')
                    ->where(['in','plan_id',$plans])
                    ->andWhere(['in','bozor_id', $bozorIds]);

                $sum = ValuesManbaPlan::find()
                    ->where(['in', 'values_plan_id', $planValues])
                    ->andWhere(['in', 'moliya_manbai_id', $moliya_manbai_id])
                    ->sum('reja');
            }


        //show results
        return $sum>0?round($sum):null;
    }
}