<?php


namespace app\modules\wbadmin\models;


use app\models\Documents;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class SearchDocuments extends Documents
{
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['title', 'title_en', 'file', 'file_en', 'url'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Documents::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'title_en', $this->title_en])
            ->andFilterWhere(['like', 'file', $this->file])
            ->andFilterWhere(['like', 'file_en', $this->file])
            ->andFilterWhere(['like', 'url', $this->url]);
        return $dataProvider;
    }
}