<?php


namespace app\modules\wbadmin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\WbInvestment;

class SearchInvestment extends WbInvestment
{
    public function rules()
    {
        return [
            [['id', 'region_id', 'district_id', 'locality_id', 'project_id', 'project_year', 'project_investment'], 'integer'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    public function search($params)
    {
        $query = WbInvestment::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'region_id' => $this->region_id,
            'district_id' => $this->district_id,
            'locality_id' => $this->locality_id,
            'project_id' => $this->project_id,
            'project_year' => $this->project_year,
            'project_investment' => $this->project_investment,
        ]);

        return $dataProvider;
    }
}