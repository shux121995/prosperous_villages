<?php
/**
 * Created by PhpStorm.
 * User: Ulugbek
 * Date: 29.10.2019
 * Time: 0:18
 */

namespace app\modules\wbadmin\models;

use app\models\WbFiles;
use yii\base\Model;
use yii\helpers\Inflector;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile[]
     */
    public $type;
    public $project_id;
    public $iFiles;

    public function rules()
    {
        return [
            [['project_id', 'type'], 'required'],
            [['iFiles'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, mp4, mpeg, mpg', 'maxFiles' => 4],
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            /** @var $file UploadedFile*/
            foreach ($this->iFiles as $file) {
                $filename = Inflector::slug($file->baseName, $replacement = '-', true);
                $path = 'uploads/' . $filename. '.' . $file->extension;
                $file->saveAs($path);

                $model = new WbFiles();
                $model->project_id = $this->project_id;
                $model->type = $this->type;
                $model->filename = $filename . '.' . $file->extension;
                $model->path = $path;
                $model->filesize = $file->size;
                $model->extension = $file->extension;
                $model->save();
            }
            return true;
        } else {
            return false;
        }
    }
}