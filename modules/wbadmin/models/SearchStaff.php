<?php


namespace app\modules\wbadmin\models;


use app\models\PIUstaff;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class SearchStaff extends PIUstaff
{
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['full_name', 'position', 'position_uz', 'cv', 'cv_uz', 'photo', 'email'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = PIUstaff::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'full_name', $this->full_name])
            ->andFilterWhere(['like', 'position', $this->position])
            ->andFilterWhere(['like', 'position_uz', $this->position_uz])
            ->andFilterWhere(['like', 'cv', $this->cv])
            ->andFilterWhere(['like', 'cv_uz', $this->cv_uz])
            ->andFilterWhere(['like', 'photo', $this->photo])
            ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
}