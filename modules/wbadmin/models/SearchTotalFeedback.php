<?php


namespace app\modules\wbadmin\models;


use app\models\TotalFeedback;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class SearchTotalFeedback extends TotalFeedback
{
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['region_id', 'feedback_year', 'feedback_quantity'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = TotalFeedback::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'region_id', $this->region_id])
            ->andFilterWhere(['like', 'feedback_year', $this->feedback_year])
            ->andFilterWhere(['like', 'feedback_quantity', $this->feedback_quantity]);
        return $dataProvider;
    }
}