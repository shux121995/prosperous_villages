<?php


namespace app\modules\wbadmin\models;

use app\models\forms\Form5_14;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class SearchForm_5_14 extends Form5_14
{
    public function rules()
    {
        return [
            [['id', 'village_id', 'sub_id', 'total_value', 'total_undis'], 'integer'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    public function search($params)
    {
        $query = Form5_14::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'village_id' => $this->village_id,
            'sub_id' => $this->sub_id,
            'cont_name' => $this->cont_name,
            'cont_number' => $this->cont_number,
            'contract_date' => $this->contract_date,
            'total_value' => $this->total_value,
            'total_undis' => $this->total_undis,
            'status_of_sub' => $this->status_of_sub,
        ]);

        return $dataProvider;
    }
}