<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FeedbackFiles */

$this->title = Yii::t('app', 'Create Feedback Files');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Feedback Files'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="feedback-files-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
