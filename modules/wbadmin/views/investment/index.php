<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\wbadmin\models\SearchInvestment */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'WB Investment');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="investment-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Add WB Investment'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'region_id',
            'district_id',
            'locality_id',
            'project_id',
            'project_year',
            'project_investment',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>