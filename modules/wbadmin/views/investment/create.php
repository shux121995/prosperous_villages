<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\WbInvestment */

$this->title = Yii::t('app', 'Add WB Investment');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'WB Investment'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>