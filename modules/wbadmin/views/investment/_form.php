<?php

use app\models\WbTypes;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\WbInvestment */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="wb-staff-form">

    <?php  $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'region_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Region::find()->all(), 'id', 'title'),
        [
            'prompt' => 'Select region',
            'onchange'=> '
            $.get( "' . \yii\helpers\Url::toRoute('districts') . '", { id: $(this).val() } )
                                .done(function( data ) {
                                    $( "#selected_district" ).html( data );
                                    }
                            );'
        ])->label(false) ?>
    <?= $form->field($model, 'district_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\District::find()->where(['region_id'=>$model->region_id])->all(), 'id', 'title'),
        ['prompt' => Yii::t('app', 'Select district'),
            'id'=>'selected_district',
            'onchange' => '
                            $.get( "' . \yii\helpers\Url::toRoute('localities') . '", { id: $(this).val() } )
                                .done(function( data ) {
                                    $( "#selected_village" ).html( data );                        
                                }
                            );
                            '])
        ->label(false)?>

    <?= $form->field($model, 'locality_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Locality::find()->where(['district_id'=>$model->district_id,'type'=>'wb'])->all(), 'id', 'title'),
        ['prompt' => Yii::t('app', 'Select village'),
            'id'=>'selected_village',
            ])
        ->label(false)?>

    <?= $form->field($model, 'project_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\WbTypes::find()->all(), 'id', 'title_en'),
        ['prompt' => Yii::t('app', 'Select subproject'),
            'id'=>'selected_village',
        ])
        ->label(false)?>

    <?= $form->field($model, 'project_year')->textInput(['maxlength' => 4]) ?>

    <?= $form->field($model, 'project_investment')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>