<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\WbPlans */

$this->title = Yii::t('app', 'Create Wb Plans');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Wb Plans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wb-plans-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
