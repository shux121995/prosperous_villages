<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\WbPlans */
/* @var $form yii\widgets\ActiveForm */

//Получить список уже имющие насленние пункты
$sub = \app\models\WbPlans::find()
    ->select('locality_id')
    ->where(['year' => $model->year])
    ->groupBy('locality_id')
    ->asArray();

//Получить список насленние пункты который нет в планы на текушие год
$locality = \app\models\Locality::find()->select(['id', 'title']);
if (!empty($model->district_id)) {
    $locality->where(['district_id' => $model->district_id]);
} else {
    $locality->where(['not in', 'id', $sub]);
}
$locality = $locality->all();

//Получить регионы
$region = \app\models\Region::find()->select(['id', 'title'])->all();

//Получить районы
$district = \app\models\District::find()->select(['id', 'title']);
if (!empty($model->region_id)) $district->where(['region_id' => $model->region_id]);
$district = $district->all();
?>

<div class="wb-plans-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'year')->dropDownList(Yii::$app->params['years'], ['prompt' => 'Йилни танланг']) ?>

    <?= $form->field($model, 'region_id')->dropDownList(ArrayHelper::map($region, 'id', 'title'),
        [
            'prompt' => 'Худудни танланг',
            'onchange' => '
                $.get( "' . Url::to('districts') . '", { year: $(\'#' . Html::getInputId($model, 'year') . '\').val(), id: $(this).val() } )
                    .done(function( data ) {
                        $( "#' . Html::getInputId($model, 'district_id') . '" ).html( data );                        
                    }
                );
                $.get( "' . Url::to('locals') . '", { year: $(\'#' . Html::getInputId($model, 'year') . '\').val(), id: null, region_id: $(this).val() } )
                    .done(function( data ) {
                        $( "#' . Html::getInputId($model, 'locality_id') . '" ).html( data );
                    }
                );
            '
        ]
    ) ?>

    <?= $form->field($model, 'district_id')->dropDownList(ArrayHelper::map($district, 'id', 'title'),
        [
            'prompt' => 'Туман ёки шахарни танланг',
            'onchange' => '
                $.get( "' . Url::to('locals') . '", { year: $(\'#' . Html::getInputId($model, 'year') . '\').val(), id: $(this).val() } )
                    .done(function( data ) {
                        $( "#' . Html::getInputId($model, 'locality_id') . '" ).html( data );
                    }
                );
            '
        ]
    ) ?>

    <?= $form->field($model, 'locality_id')->dropDownList(ArrayHelper::map($locality, 'id', 'title'), ['prompt' => 'Жойни танланг']) ?>

    <div class="row">
        <div class="col-md-9"><?= $form->field($model, 'dev_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(\app\models\WbDevelopers::find()->all(), 'id', 'title'),
                'language' => 'en',
                'options' => ['placeholder' => 'Пудратчини танланг'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?></div>
        <div class="col-md-3"><div><label for="">&nbsp;</label></div>рўйхатда топилмаса:
            <?=Html::button(Yii::t('app','қўшиш'),['class'=>'btn btn-success devadd'])?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$this->registerJsFile(
    'js/wbadmin.js',
    ['depends'=>'app\assets\AppAsset']
);
?>

<!-- Modal "Add developer" -->
<div class="modal fade" id="devadd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-body"></div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->