<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Locality */
/* @var $form yii\widgets\ActiveForm */
?>
    <style>
        .ui-autocomplete {
            background-color: white;
            width: 300px;
            border: 1px solid #cfcfcf;
            list-style-type: none;
            padding-left: 0px;
        }
    </style>
<div class="locality-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group field-locality-district_id has-success">
        <label class="control-label" for="locality-district_id">Region ID</label>
        <?= Html::dropDownList('a',null,\yii\helpers\ArrayHelper::map(\app\models\Region::find()->all(),'id','title'),
            [
                'class' => 'form-control',
                'prompt' => Yii::t('app', 'Select region'),
                'onchange' => '
                $.get( "' . \yii\helpers\Url::to('districts') . '", { id: $(this).val() } )
                    .done(function( data ) {
                        $( "#' . Html::getInputId($model, 'district_id') . '" ).html( data );                        
                    }
                );'
            ]) ?>
        <div class="help-block"></div>
    </div>

    <?= $form->field($model, 'district_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\District::find()->all(),'id','title')) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title_en')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'type')->dropDownList([ 'qishloq' => 'Qishloq', 'mahalla' => 'Mahalla', 'wb' => 'World bank' ], ['prompt' => '']) ?>

    <?= $form->field($model, 'sector_reg')->dropDownList([ '1' => '1', '2' => '2', '3' => '3', '4' => '4' ], ['prompt' => '']) ?>

    <?= $form->field($model, 'sector_dis')->dropDownList([ '1' => '1', '2' => '2', '3' => '3', '4' => '4' ], ['prompt' => '']) ?>

    <label>Адрес для поиска: </label><input id="address" style="width:600px;" type="text"/>
    <div id="map_canvas" style="width:100%; height:350px"></div><br/>

    <?= $form->field($model, 'map_latitude')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'map_longitude')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
if($model->map_latitude!=null AND $model->map_longitude!=null){
    $lat = $model->map_latitude;
    $lng = $model->map_longitude;
}else{
    $lat = 41.2994958;
    $lng = 69.24007340000003;
}
$script = <<< JS
    
var geocoder;
var map;
var marker;
    
function initialize(){
//Определение карты
  var latlng = new google.maps.LatLng($lat,$lng);
  var options = {
    zoom: 15,
    center: latlng,
    mapTypeId: google.maps.MapTypeId.SATELLITE
  };
        
  map = new google.maps.Map(document.getElementById("map_canvas"), options);
        
  //Определение геокодера
  geocoder = new google.maps.Geocoder();
        
  marker = new google.maps.Marker({
    map: map,
    draggable: true
  });
				
}
		
$(document).ready(function() { 
         
  initialize();
    var location = new google.maps.LatLng($lat, $lng);
    marker.setPosition(location);	  
  $(function() {
    $("#address").autocomplete({
      //Определяем значение для адреса при геокодировании
      source: function(request, response) {
        return geocoder.geocode( {'address': request.term}, function(results, status) {
          response($.map(results, function(item) {
            return {
              label:  item.formatted_address,
              value: item.formatted_address,
              latitude: item.geometry.location.lat(),
              longitude: item.geometry.location.lng()
            }
          })
          );
        })
      },
      //Выполняется при выборе конкретного адреса
        select: function(event, ui) {
            console.log(ui);
            $("#locality-map_latitude").val(ui.item.latitude);
            $("#locality-map_longitude").val(ui.item.longitude);
            var location = new google.maps.LatLng(ui.item.latitude, ui.item.longitude);
            marker.setPosition(location);
            map.setCenter(location);
        }
    });
  });
	
  //Добавляем слушателя события обратного геокодирования для маркера при его перемещении  
  google.maps.event.addListener(marker, 'drag', function() {
    geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[0]) {
          $('#address').val(results[0].formatted_address);
          $('#locality-map_latitude').val(marker.getPosition().lat());
          $('#locality-map_longitude').val(marker.getPosition().lng());
        }
      }
    });
  });
  
});
	
JS;

$this->registerJs($script, \yii\web\View::POS_READY);
$map_api = Yii::$app->request->hostName=='obodqishloq.uz'?Yii::$app->params['map_api_real']:Yii::$app->params['map_api_real'];
$this->registerJsFile('https://maps.googleapis.com/maps/api/js?key='.$map_api, ['depends'=>'yii\web\JqueryAsset'], \yii\web\View::POS_READY);
?>