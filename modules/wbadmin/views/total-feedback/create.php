<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TotalFeedback */

$this->title = Yii::t('app', 'Add Total Feedback');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Wb Total Feedback'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wb-staff-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>