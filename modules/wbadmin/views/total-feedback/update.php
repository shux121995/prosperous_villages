<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TotalFeedback */

$this->title = Yii::t('app', 'Update Wb PIU Staff: ' . $model->region_id);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Wb PIU Staff'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->region_id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="wb-staff-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>