<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\WbSubTypes */

$this->title = Yii::t('app', 'Create Wb Sub Types');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Wb Sub Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wb-sub-types-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
