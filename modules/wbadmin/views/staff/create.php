<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PIUstaff */

$this->title = Yii::t('app', 'Create PIU Staff Member');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'PIU Staff Members'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wb-staff-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>