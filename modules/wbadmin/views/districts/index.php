<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\wbadmin\models\DistrictSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Districts');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="district-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create District'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],

        //'id',
        'region.title',
        'title',
        [
            'class' => 'kartik\grid\EditableColumn',
            'attribute' => 'title_en',
            'editableOptions'=> [
                'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                'formOptions' => ['action' => ['edits']]
            ],
            //'pageSummary' => 'Page Total',
            /*'vAlign'=>'middle',
            'headerOptions'=>['class'=>'kv-sticky-column'],
            'contentOptions'=>['class'=>'kv-sticky-column'],
            'editableOptions'=>['header'=>'Order', 'size'=>'md']*/
        ],
        'type',
        'order',
        'map_latitude',
        'map_longitude',

        ['class' => 'yii\grid\ActionColumn'],
    ];
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
        'pjax' => true,
        'bordered' => true,
        'striped' => false,
        'condensed' => false,
        'responsive' => false,
        'hover' => true,
    ]); ?>
</div>
