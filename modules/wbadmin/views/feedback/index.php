<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\SearchFeedback */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Feedbacks');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="feedback-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name',
            'phone',
            'address',
            //'locality_id',
            //'table_id',
            //'section_id',
            [
                'attribute' => 'type',
                'value' => function($model){
                    return $model->activeTypeLabel;
                },
                'filter' => \app\models\Feedback::typeLables(),
                'filterInputOptions' => ['class' => 'form-control form-control-sm']
            ],
            'text:ntext',
            //'created_at',
            //'updated_at',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function($model){
                    $btn_class = '';
                    if($model->status==1)//Answered
                        $btn_class = 'btn btn-success iadd';
                    else if($model->status==2)//New
                        $btn_class = 'btn btn-primary iadd';
                    if($model->status==3)//Canceled
                        $btn_class = 'btn btn-danger iadd';
                    return Html::button($model->activeStatusLabel,
                        [
                            'data-href'=>\yii\helpers\Url::to(['answer', 'feedback_id'=>$model->id]),
                            'class'=>$btn_class
                        ]
                    );;
                },
                'filter' => \app\models\Feedback::statusLabels(),
                'filterInputOptions' => ['class' => 'form-control form-control-sm']
            ],
            [
                'attribute' => 'created_at',
                'value' => function($model){
                    return date('d-m-Y H:i',$model->created_at);
                },
                //'filter' => \app\models\Feedback::statusLabels(),
                //'filterInputOptions' => ['class' => 'form-control form-control-sm']
            ],

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
<?php
$this->registerJsFile(
    'js/wbadmin.js',
    ['depends'=>'app\assets\AppAsset']
);
?>
<!-- Modal "Add developer" -->
<div class="modal fade" id="iadd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-body"></div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->