<?php
/**
 * Created by PhpStorm.
 * User: Ulugbek
 * Date: 29.10.2019
 * Time: 0:23
 */
use yii\widgets\ActiveForm;

$feedback = \app\models\Feedback::findOne($model->feedback_id);
if($feedback==null){
    echo '<h2>Уппс! хатолик!</h2>';
} else { ?>
    <div>
        <h4><?=$feedback->name?>, <?=$feedback->phone?>:</h4>
        <div><p><?=$feedback->text?></p></div>
    </div>
    <?php
    $form = ActiveForm::begin([
        'action' => \yii\helpers\Url::to(['answersubmit', 'feedback_id' => $model->feedback_id]),
        'options' => ['class' => 'iadd-form', 'enctype' => 'multipart/form-data']
    ]) ?>

    <?= $form->field($model, 'text')->textarea() ?>

    <?= $form->field($model, 'status')->radioList(\app\models\FeedbackAnswers::statusLabels()) ?>

    <button>Жавоб бериш!</button>

    <?php ActiveForm::end();
}