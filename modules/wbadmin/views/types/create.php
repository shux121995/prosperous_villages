<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\WbTypes */

$this->title = Yii::t('app', 'Create WB Project');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'WB Projects'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wb-types-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
