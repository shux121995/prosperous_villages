<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\wbadmin\models\SearchStaff */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'WB Documents');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wb-documents-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Add Document'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'title_en',
            'file',
            'file_en',
            'url',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>