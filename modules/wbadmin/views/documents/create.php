<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Documents */

$this->title = Yii::t('app', 'Add document');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'WB documents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wb-staff-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>