<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\WbTasks */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="wb-tasks-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'project_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(\app\models\WbProjects::find()->all(), 'id', 'title'),
        'language' => 'en',
        'options' => ['placeholder' => 'Режани танланг'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <?= $form->field($model, 'sub_type_id')->dropDownList(ArrayHelper::map(\app\models\WbSubTypes::find()->all(), 'id', 'title'), ['prompt' => 'Суб турни танланг']) ?>

    <?= $form->field($model, 'date_start')->textInput() ?>

    <?= $form->field($model, 'date_end')->textInput() ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'price_usd')->textInput() ?>

    <?= $form->field($model, 'status')->checkbox() ?>



    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
