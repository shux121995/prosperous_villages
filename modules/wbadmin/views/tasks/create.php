<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\WbTasks */

$this->title = Yii::t('app', 'Create Wb Tasks');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Wb Tasks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wb-tasks-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
