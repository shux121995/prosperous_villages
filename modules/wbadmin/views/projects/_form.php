<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\WbProjects */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="wb-projects-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'plan_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(\app\models\WbPlans::find()->all(), 'id', 'yearplan'),
        'language' => 'en',
        'options' => ['placeholder' => 'Режани танланг'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <?= $form->field($model, 'type_id')->dropDownList(ArrayHelper::map(\app\models\WbTypes::find()->all(), 'id', 'title'), ['prompt' => 'Турни танланг']) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'price_usd')->textInput() ?>

    <div class="row">
        <div class="col-md-9"><?= $form->field($model, 'table_file')->textInput(['id'=>'img-input', 'maxlength' => true, 'placeholder'=>'фақат PDF файл']) ?></div>
        <div class="col-md-3">
            <div class="form-group">
                <label class="control-label" for="img-input">&nbsp;</label>
                <button type="button" class="form-control btn btn-primary select-img-btn" data-toggle="modal" data-target="#fileModal">
                    Select
                </button>
            </div>
        </div>
    </div>

    <?php //= $form->field($model, 'status')->dropDownList(\app\models\WbProjects::statusLabels(), ['prompt' => 'Статусни танланг']) ?>

    <?php //= $form->field($model, 'created_at')->textInput() ?>

    <?php //= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?=$this->render('//layouts/_imagemanager')?>