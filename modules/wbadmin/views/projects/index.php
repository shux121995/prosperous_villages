<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\wbadmin\models\SearchProjects */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Wb Projects');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wb-projects-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Wb Projects'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'plan_id',
            'type_id',
            'price',
            'price_usd',
            'table_file',
            [
                'label' => '',
                'format' => 'raw',
                'value' => function($model){
                    return Html::button(Yii::t('app','Файл қўшиш'),
                        [
                            'data-href'=>\yii\helpers\Url::to(['fileaddsubmit', 'project_id'=>$model->id]),
                            'class'=>'btn btn-success iadd'
                        ]
                    );
                }
            ],
            //'status',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
<?php
$this->registerJsFile(
    'js/wbadmin.js',
    ['depends'=>'app\assets\AppAsset']
);
?>
<!-- Modal "Add developer" -->
<div class="modal fade" id="iadd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-body"></div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->