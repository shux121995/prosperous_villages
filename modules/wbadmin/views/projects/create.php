<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\WbProjects */

$this->title = Yii::t('app', 'Create Wb Projects');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Wb Projects'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wb-projects-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
