<?php
/**
 * Created by PhpStorm.
 * User: Ulugbek
 * Date: 29.10.2019
 * Time: 0:23
 */
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin([
    'action'=>\yii\helpers\Url::to(['fileaddsubmit', 'project_id'=>$model->project_id]),
    'options' => ['class'=>'iadd-form', 'enctype' => 'multipart/form-data']
]) ?>

    <?= $form->field($model, 'iFiles[]')->fileInput(['multiple' => true, 'accept' => 'image/*, video/*']) ?>
    <?= $form->field($model, 'project_id')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'type')->radioList(\app\models\WbFiles::typeLabels()) ?>

    <button>Юклаш!</button>

<?php ActiveForm::end() ?>