<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\wbadmin\models\SearchFiles */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Wb Files';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wb-files-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Wb Files', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'project_id',
            'filename',
            'type',
            'path',
            //'extension',
            //'filesize',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
