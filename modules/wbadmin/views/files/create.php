<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\WbFiles */

$this->title = 'Create Wb Files';
$this->params['breadcrumbs'][] = ['label' => 'Wb Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wb-files-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
