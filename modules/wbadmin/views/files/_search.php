<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\wbadmin\models\SearchFiles */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="wb-files-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'project_id') ?>

    <?= $form->field($model, 'filename') ?>

    <?= $form->field($model, 'type') ?>

    <?= $form->field($model, 'path') ?>

    <?php // echo $form->field($model, 'extension') ?>

    <?php // echo $form->field($model, 'filesize') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
