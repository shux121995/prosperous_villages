<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\wbadmin\models\SearchForm_5_14 */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'WB Finance Form 5.14');
$this->params['breadcrumbs'][] = $this->title;
?>
<div>

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Add Finance Form 5.14'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'village_id',
            'sub_id',
            'type_of_work',
            'cont_name',
            'cont_number',
            'contract_date',
            'total_value',
            'total_undis',
            'status_of_sub',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>