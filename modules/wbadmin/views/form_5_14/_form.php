<?php

use app\models\WbTypes;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\forms\Form5_14 */
/* @var $form yii\widgets\ActiveForm */


$rows = \app\models\WbInvestment::find()->where(['locality_id' => $model->village_id])->all();


if(count($rows)>0){
    foreach($rows as $row){
        $project_name = WbTypes::find()->where(['id'=>$row->project_id])->one()->title;
        $subprojects[$row->id] = $project_name;
    }
}else
    $subprojects = [];

?>

<div class="wb-staff-form">

    <?php  $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'village_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Locality::find()->where(['type'=>'wb'])->all(), 'id', 'title'),
        [
            'prompt' => 'Select village',
            'id'=>'selected_village',
            'onchange'=> '
            $.get( "' . \yii\helpers\Url::toRoute('subprojects') . '", { id: $(this).val() } )
                                .done(function( data ) {
                                    $( "#subproject" ).html( data );
                                    }
                            );'
        ]) ?>

    <?= $form->field($model, 'sub_id')->dropDownList($subprojects,
        [
            'id' => 'subproject',
            'prompt' => 'Select Project Investment',
            'onchange'=> '
            $.get( "' . \yii\helpers\Url::toRoute('investment') . '", { vil_id: $(selected_village).val(), id: $(this).val() } )
                                .done(function( data ) {
                                    $( "#total_investment" ).val( data );
                                    
                                }
                            );'
    ]) ?>

    <?= $form->field($model, 'type_of_work')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\WbSubTypes::find()->all(), 'id', 'title_en'), ['prompt'=>'Select type of work']) ?>

    <?= $form->field($model, 'cont_name')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\WbDevelopers::find()->all(), 'id', 'title_en'), ['prompt'=>'Select developer']) ?>

    <?= $form->field($model, 'cont_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'contract_date')->widget(\kartik\date\DatePicker::className(),['pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'autoclose' => true,
        'todayHighlight' => true,
    ]])?>

    <?= $form->field($model, 'total_value')->textInput(['id'=>'total_investment', 'readonly' => true, 'value' => null]) ?>

    <?= $form->field($model, 'total_undis')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status_of_sub')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\WbStatus::find()->all(), 'id', 'title_en'), ['prompt'=>'Select status of project']) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>