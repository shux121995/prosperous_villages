<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\forms\Form5_14 */

$this->title = Yii::t('app', 'Add Form 5.14');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Finance Form 5.14'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="locality-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>