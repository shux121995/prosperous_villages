<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$form = ActiveForm::begin();
?>
<div class="form-group" style='margin-top: 15px;'>
    
    <?= $form->field($model, 'news_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'news_image')->fileInput() ?>
    
    <?= $form->field($model, 'news_alias')->textInput(['maxlength' => true]) ?>
    
    <?=
    $form->field($model, 'news_date')->widget(\kartik\date\DatePicker::classname(), [
        'name' => 'dp_1',
        'type' => \kartik\date\DatePicker::TYPE_INPUT,
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy-mm-dd'
        ]
    ]);
    ?>
    
    <?=
    $form->field($model, 'news_text')->widget(\vova07\imperavi\Widget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'paragraphize' => false,
            'cleanOnPaste' => false,
            'replaceDivs' => false,
            'linebreaks' => false,
            'allowedTags' => ['p','div','a','img', 'iframe', 'b', 'br', 'ul', 'li',
                'details', 'summary', 'span', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6',
                'label', 'ol', 'strong', 'italic', 'em', 'del', 'figure'],
            'plugins' => [
                'fullscreen',
                'video',
                'imagemanager',
            ],
            'imageUpload' => Url::to(['/admin/news/image-upload']),
            'imageManagerJson' => Url::to(['/admin/news/images-get']),
        ]
    ])->hint('Для втавки html-кода необходимо включить режим "Код". Разрешенные теги "p", "div", "a", "img", "iframe", "b", "br", "ul", "li",
                "details", "summary", "span", "h1", "h2", "h3", "h4", "h5", "h6","label", "ol", "strong", "figure"');

    ?>

    <?= $form->field($model, 'news_language')->dropDownList(Yii::$app->params['langs']) ?>


    <?= $form->field($model, 'news_keywords')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'news_description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'news_show')->checkbox([], false) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>



