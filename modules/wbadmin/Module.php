<?php

namespace app\modules\wbadmin;

/**
 * wbadmin module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\wbadmin\controllers';
    public $layout = '//wbadmin';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        \Yii::$app->setHomeUrl('/wbadmin');
        parent::init();

        // custom initialization code goes here
    }
}
