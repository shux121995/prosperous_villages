<?php


namespace app\modules\wbadmin\controllers;

use app\models\Documents;
use app\modules\wbadmin\models\SearchDocuments;
use yii\filters\VerbFilter;
use yii\web\Controller;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class DocumentsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    public function actionIndex()
    {
        $searchModel = new SearchDocuments();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    public function actionCreate()
    {
        $model = new Documents();
        //choose file
        if ($model->load(Yii::$app->request->post())) {

            $model->file = UploadedFile::getInstance($model, 'file');
            $model->file_en = UploadedFile::getInstance($model, 'file_en');

            if ($model->save()) {
                if($model->file != null || $model->file_en != null)
                {
                    $model->file->saveAs($_SERVER['DOCUMENT_ROOT'] . "/wb/documents/" . $model->file->baseName . "." . $model->file->extension);
                    $model->file_en->saveAs($_SERVER['DOCUMENT_ROOT'] . "/wb/documents/" . $model->file_en->baseName . "." . $model->file_en->extension);
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        //choose file
        if ($model->load(Yii::$app->request->post())) {

            $model->file = $model->id . '_' .'uz'.'_'.UploadedFile::getInstance($model, 'file');
            $model->file_en = $model->id . '_' .'en'.'_'.UploadedFile::getInstance($model, 'file_en');

            if ($model->save()) {
                if($model->file != null || $model->file_en != null)
                {
                    $model->file->saveAs($_SERVER['DOCUMENT_ROOT'] . "/wb/documents/" . $model->file->baseName . "." . $model->file->extension);
                    $model->file_en->saveAs($_SERVER['DOCUMENT_ROOT'] . "/wb/documents/" . $model->file_en->baseName . "." . $model->file_en->extension);
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }




            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        //Delete files in that location
        unlink($_SERVER['DOCUMENT_ROOT'].'/wb/documents/' . $this->findModel($id)->file);
        unlink($_SERVER['DOCUMENT_ROOT'].'/wb/documents/' . $this->findModel($id)->file_en);
        //delete record from database
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Documents::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}