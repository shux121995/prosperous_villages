<?php


namespace app\modules\wbadmin\controllers;


use app\models\District;
use app\models\Locality;
use app\models\WbInvestment;
use app\modules\wbadmin\models\SearchInvestment;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class InvestmentController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new SearchInvestment();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new WbInvestment();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    protected function findModel($id)
    {
        if (($model = WbInvestment::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionDistricts($id){
        $rows = \app\models\District::find()->where(['region_id' => $id])->all();

        echo "<option value=''>".Yii::t('app', 'Select district')."</option>";

        if(count($rows)>0){
            /** @var $row District*/
            foreach($rows as $row){
                if($row->id == 24 || $row->id == 19 ||$row->id == 23 ||$row->id == 28 ||$row->id == 25 ||$row->id == 82 ||
                    $row->id == 83 ||$row->id == 74 ||$row->id == 84 ||$row->id == 78 ||$row->id == 148 ||$row->id == 149 ||
                    $row->id == 142 ||$row->id == 147 ||$row->id == 112 ||$row->id == 116 ||$row->id == 119 ||$row->id == 46 ||
                    $row->id == 51 ||$row->id == 52 ||$row->id == 42)
                    echo "<option value='$row->id'>".$row->getTitleLang(Yii::$app->lang->check_lang())."</option>";

            }
        }
        die();
    }

    public function actionLocalities($id){
        $rows = \app\models\Locality::find()->where(['type'=>Locality::TYPE_WB, 'district_id' => $id])->all();

        echo "<option value=''>".Yii::t('app', 'Select village')."</option>";

        if(count($rows)>0){
            /** @var $row District*/
            foreach($rows as $row){
                echo "<option value='$row->id'>".$row->getTitleLang(Yii::$app->lang->check_lang())."</option>";
            }
        }
        die();
    }
}