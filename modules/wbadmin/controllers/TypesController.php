<?php

namespace app\modules\wbadmin\controllers;

use Yii;
use app\models\WbTypes;
use app\modules\wbadmin\models\SearchTypes;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * TypesController implements the CRUD actions for WbTypes model.
 */
class TypesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all WbTypes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchTypes();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single WbTypes model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new WbTypes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new WbTypes();

        if ($model->load(Yii::$app->request->post())) {

            $model->icon = UploadedFile::getInstance($model, 'icon');

            if($model->save()) {
                if($model->icon != null) {
                    $model->icon->saveAs($_SERVER['DOCUMENT_ROOT'] . "/wb/images/main_page_icons/". $model->icon->baseName . "." . $model->icon->extension);
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing WbTypes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            $model->icon = UploadedFile::getInstance($model, 'icon');

            if($model->save()) {

                if($model->icon != null) {
                    $model->icon->saveAs($_SERVER['DOCUMENT_ROOT'] . "/wb/images/main_page_icons/" . $model->icon->baseName . "." . $model->icon->extension);
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing WbTypes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        //unlink(Yii::$app->$_SERVER['DOCUMENT_ROOT'] . "/wb/images/main_page_icons/" . $this->findModel($id)->icon);
        unlink($_SERVER['DOCUMENT_ROOT'].'/wb/images/main_page_icons/' . $this->findModel($id)->icon);
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the WbTypes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return WbTypes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = WbTypes::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
