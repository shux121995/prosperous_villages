<?php

namespace app\modules\wbadmin\controllers;

use Yii;
use app\models\WbPlans;
use app\modules\wbadmin\models\SearchPlans;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PlansController implements the CRUD actions for WbPlans model.
 */
class PlansController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all WbPlans models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchPlans();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single WbPlans model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new WbPlans model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new WbPlans();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing WbPlans model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing WbPlans model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the WbPlans model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return WbPlans the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = WbPlans::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * @param $id
     */
    public function actionDistricts($id){
        $rows = \app\models\District::find()->where(['region_id' => $id])->all();

        echo "<option value=''>Туман ёки шаҳарни танланг</option>";

        if(count($rows)>0){
            foreach($rows as $row){
                echo "<option value='$row->id'>$row->title</option>";
            }
        }

    }

    /**
     * @param $id
     */
    public function actionLocals($year = 2019, $id=null,$region_id=null){
        $rows = \app\models\Locality::find();
        if($id!=null) {
            $rows->where(['district_id' => $id]);
            $subQuery = WbPlans::find()->select('locality_id')->where(['year'=>$year, 'district_id' => $id]);
            $rows->andWhere(['not in','id', $subQuery]);
        }elseif ($region_id!=null) {
            $sub = District::find()->select('id')->where(['region_id'=>$region_id]);
            $rows->where(['in','district_id', $sub]);
        }
        $rows = $rows->all();

        echo "<option value=''>Жойни танланг</option>";

        if(count($rows)>0){
            foreach($rows as $row){
                echo "<option value='$row->id'>$row->title</option>";
            }
        }

    }

    /**
     * @param $id
     */
    public function actionDevs(){
        $rows = \app\models\WbDevelopers::find();
        $rows = $rows->all();

        echo "<option value=''>Пудратчини танланг</option>";

        if(count($rows)>0){
            foreach($rows as $row){
                echo "<option value='$row->id'>$row->title</option>";
            }
        }
        return false;
    }
}
