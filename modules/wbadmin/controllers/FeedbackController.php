<?php

namespace app\modules\wbadmin\controllers;

use app\models\FeedbackAnswers;
use Yii;
use app\models\Feedback;
use app\modules\wbadmin\models\SearchFeedback;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FeedbackController implements the CRUD actions for Feedback model.
 */
class FeedbackController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Feedback models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchFeedback();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Feedback model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Feedback model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Feedback();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Feedback model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Feedback model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Feedback model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Feedback the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Feedback::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }



    /**
     * Ajax form
     * @return string
     */
    public function actionAnswer($feedback_id)
    {
        if(Yii::$app->request->isAjax) $this->layout = '//clean';
        $model = new FeedbackAnswers();
        $model->feedback_id = $feedback_id;

        if (Yii::$app->request->isPost AND $model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                // file is uploaded successfully
                return;
            }else{
                return $this->render('answer', ['model' => $model]);
            }
        }

        return $this->render('answer', ['model' => $model]);
    }

    /**
     * Ajax form submit
     * @return string
     */
    public function actionAnswersubmit($feedback_id)
    {
        if(Yii::$app->request->isAjax) $this->layout = '//clean';

        $model = new FeedbackAnswers();
        $model->feedback_id = $feedback_id;

        if (Yii::$app->request->isPost AND $model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                $feedback = Feedback::findOne($feedback_id);
                $feedback->status = $model->status;
                $feedback->save();
                $success = [
                    'status' => true,
                    'id' => $model->feedback_id
                ];
                return json_encode($success);
            }
        }
        return $this->render('answer', ['model' => $model]);
    }
}
