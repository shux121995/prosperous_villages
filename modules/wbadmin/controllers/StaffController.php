<?php


namespace app\modules\wbadmin\controllers;

use app\models\PIUstaff;
use app\modules\wbadmin\models\SearchStaff;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class StaffController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new SearchStaff();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new PIUstaff();
        //choose file
        if ($model->load(Yii::$app->request->post())) {

            $model->photo = UploadedFile::getInstance($model, 'photo');

            if ($model->save()) {
                if($model->photo != null)
                {
                    $model->photo->saveAs($_SERVER['DOCUMENT_ROOT'] . "/wb/images/PIU_STAFF_IMAGES/" . $model->id . '_' . $model->photo->baseName . "." . $model->photo->extension);
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        //choose file
        if ($model->load(Yii::$app->request->post())) {

            $model->photo = UploadedFile::getInstance($model, 'photo');

            if ($model->save()) {
                if($model->photo != null)
                {
                    $model->photo->saveAs($_SERVER['DOCUMENT_ROOT'] . "/wb/images/PIU_STAFF_IMAGES/" . $model->id . '_' . $model->photo->baseName . "." . $model->photo->extension);
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }




            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = PIUstaff::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}