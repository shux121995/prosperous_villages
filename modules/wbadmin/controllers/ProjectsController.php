<?php

namespace app\modules\wbadmin\controllers;

use Yii;
use app\models\WbProjects;
use app\modules\wbadmin\models\SearchProjects;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\wbadmin\models\UploadForm;
use yii\web\UploadedFile;

/**
 * ProjectsController implements the CRUD actions for WbProjects model.
 */
class ProjectsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all WbProjects models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchProjects();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single WbProjects model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new WbProjects model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new WbProjects();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing WbProjects model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing WbProjects model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the WbProjects model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return WbProjects the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = WbProjects::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /*public function actionUpload($project_id)
    {
        if(Yii::$app->request->isAjax) $this->layout = '//clean';
        $model = new UploadForm();
        $model->project_id = $project_id;

        if (Yii::$app->request->isPost AND $model->load(Yii::$app->request->post())) {
            $model->iFiles = UploadedFile::getInstances($model, 'iFiles');
            if ($model->upload()) {
                // file is uploaded successfully
                return;
            }else{
                return $this->render('upload', ['model' => $model]);
            }
        }

        return $this->render('upload', ['model' => $model]);
    }*/

    /**
     * Ajax form
     * @return string
     */
    public function actionFileadd($project_id)
    {
        if(Yii::$app->request->isAjax) $this->layout = '//clean';
        $model = new UploadForm();
        $model->project_id = $project_id;

        if (Yii::$app->request->isPost AND $model->load(Yii::$app->request->post())) {
            $model->iFiles = UploadedFile::getInstances($model, 'iFiles');
            if ($model->upload()) {
                // file is uploaded successfully
                return;
            }else{
                return $this->render('upload', ['model' => $model]);
            }
        }

        return $this->render('upload', ['model' => $model]);
    }

    /**
     * Ajax form submit
     * @return string
     */
    public function actionFileaddsubmit($project_id)
    {
        if(Yii::$app->request->isAjax) $this->layout = '//clean';

        $model = new UploadForm();
        $model->project_id = $project_id;

        if (Yii::$app->request->isPost AND $model->load(Yii::$app->request->post())) {
            $model->iFiles = UploadedFile::getInstances($model, 'iFiles');
            if ($model->upload()) {
                $success = [
                    'status' => true,
                    'id' => $model->project_id
                ];
                return json_encode($success);
            }
        }
        return $this->render('upload', ['model' => $model]);
    }
}
