<?php


namespace app\modules\wbadmin\controllers;


use app\models\forms\Form5_14;
use app\models\WbTypes;
use app\modules\wbadmin\models\SearchForm_5_14;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class Form_5_14Controller extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new SearchForm_5_14();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new Form5_14();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    protected function findModel($id)
    {
        if (($model = Form5_14::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionSubprojects($id){
        $rows = \app\models\WbInvestment::find()->where(['locality_id' => $id])->all();

        echo "<option value=''>Select Project Investment</option>";

        if(count($rows)>0){
            foreach($rows as $row){
                $project_name = WbTypes::find()->where(['id'=>$row->project_id])->one()->title;
                echo "<option value='$row->id'>$project_name</option>";
            }
        }
    }

    public function actionInvestment($vil_id, $id){
        return \app\models\WbInvestment::find()->where(['locality_id' => $vil_id, 'project_id'=>$id])->one()->project_investment;
    }
}