<?php


namespace app\modules\wbregion\controllers;


use app\models\forms\ess\ProjectDesignStep;
use app\modules\wbregion\models\SearchProjectDesignStepRegion;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class EssProjectDesignRegionController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new SearchProjectDesignStepRegion();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new ProjectDesignStep();

        if ($model->load(Yii::$app->request->post())) {

            $model->form1_1_file = UploadedFile::getInstance($model, 'form1_1_file');
            $model->form1_2_file = UploadedFile::getInstance($model, 'form1_2_file');
            $model->form1_3_file = UploadedFile::getInstance($model, 'form1_3_file');
            $model->step_2_file = UploadedFile::getInstance($model, 'step_2_file');
            $model->step_3_file = UploadedFile::getInstance($model, 'step_3_file');
            $model->step_5_file = UploadedFile::getInstance($model, 'step_5_file');

            if(isset($model->form1_1_file)) {
                $model->form1_1_file->saveAs($_SERVER['DOCUMENT_ROOT'] . "/wb/documents/ess/" . time() . '_' . $model->form1_1_file->baseName . "." . $model->form1_1_file->extension);
                $model->form1_1_file = "/wb/documents/ess/" .time() . '_' . $model->form1_1_file->baseName . "." . $model->form1_1_file->extension;
            }
            if(isset($model->form1_2_file)) {
                $model->form1_2_file->saveAs($_SERVER['DOCUMENT_ROOT'] . "/wb/documents/ess/" . time() . '_' . $model->form1_2_file->baseName . "." . $model->form1_2_file->extension);
                $model->form1_2_file = "/wb/documents/ess/" .time() . '_' . $model->form1_2_file->baseName . "." . $model->form1_2_file->extension;
            }
            if(isset($model->form1_3_file)) {
                $model->form1_3_file->saveAs($_SERVER['DOCUMENT_ROOT'] . "/wb/documents/ess/" . time() . '_' . $model->form1_3_file->baseName . "." . $model->form1_3_file->extension);
                $model->form1_3_file = "/wb/documents/ess/" .time() . '_' . $model->form1_3_file->baseName . "." . $model->form1_3_file->extension;
            }
            if(isset($model->step_2_file)) {
                $model->step_2_file->saveAs($_SERVER['DOCUMENT_ROOT'] . "/wb/documents/ess/" . time() . '_' . $model->step_2_file->baseName . "." . $model->step_2_file->extension);
                $model->step_2_file = "/wb/documents/ess/" .time() . '_' . $model->step_2_file->baseName . "." . $model->step_2_file->extension;
            }
            if(isset($model->step_3_file)) {
                $model->step_3_file->saveAs($_SERVER['DOCUMENT_ROOT'] . "/wb/documents/ess/" . time() . '_' . $model->step_3_file->baseName . "." . $model->step_3_file->extension);
                $model->step_3_file = "/wb/documents/ess/" .time() . '_' . $model->step_3_file->baseName . "." . $model->step_3_file->extension;
            }
            if(isset($model->step_5_file)) {
                $model->step_5_file->saveAs($_SERVER['DOCUMENT_ROOT'] . "/wb/documents/ess/" . time() . '_' . $model->step_5_file->baseName . "." . $model->step_5_file->extension);
                $model->step_5_file = "/wb/documents/ess/" .time() . '_' . $model->step_5_file->baseName . "." . $model->step_5_file->extension;
            }

            $model->save();

            return $this->redirect(['view', 'id' => $model->id]);

        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $form1_1_file = $model->form1_1_file;
        $form1_2_file = $model->form1_2_file;
        $form1_3_file = $model->form1_3_file;
        $step_2_file = $model->step_2_file;
        $step_3_file = $model->step_3_file;
        $step_5_file = $model->step_5_file;

        if ($model->load(Yii::$app->request->post())) {

            $model->form1_1_file = UploadedFile::getInstance($model, 'form1_1_file');
            $model->form1_2_file = UploadedFile::getInstance($model, 'form1_2_file');
            $model->form1_3_file = UploadedFile::getInstance($model, 'form1_3_file');
            $model->step_2_file = UploadedFile::getInstance($model, 'step_2_file');
            $model->step_3_file = UploadedFile::getInstance($model, 'step_3_file');
            $model->step_5_file = UploadedFile::getInstance($model, 'step_5_file');

            if(isset($model->form1_1_file)) {
                $model->form1_1_file->saveAs($_SERVER['DOCUMENT_ROOT'] . "/wb/documents/ess/" . time() . '_' . $model->form1_1_file->baseName . "." . $model->form1_1_file->extension);
                $model->form1_1_file = "/wb/documents/ess/" .time() . '_' . $model->form1_1_file->baseName . "." . $model->form1_1_file->extension;
            }
            else{
                $model->form1_1_file = $form1_1_file;
            }
            if(isset($model->form1_2_file)) {
                $model->form1_2_file->saveAs($_SERVER['DOCUMENT_ROOT'] . "/wb/documents/ess/" . time() . '_' . $model->form1_2_file->baseName . "." . $model->form1_2_file->extension);
                $model->form1_2_file = "/wb/documents/ess/" .time() . '_' . $model->form1_2_file->baseName . "." . $model->form1_2_file->extension;
            }
            else{
                $model->form1_2_file = $form1_2_file;
            }
            if(isset($model->form1_3_file)) {
                $model->form1_3_file->saveAs($_SERVER['DOCUMENT_ROOT'] . "/wb/documents/ess/" . time() . '_' . $model->form1_3_file->baseName . "." . $model->form1_3_file->extension);
                $model->form1_3_file = "/wb/documents/ess/" .time() . '_' . $model->form1_3_file->baseName . "." . $model->form1_3_file->extension;
            }
            else{
                $model->form1_3_file = $form1_3_file;
            }
            if(isset($model->step_2_file)) {
                $model->step_2_file->saveAs($_SERVER['DOCUMENT_ROOT'] . "/wb/documents/ess/" . time() . '_' . $model->step_2_file->baseName . "." . $model->step_2_file->extension);
                $model->step_2_file = "/wb/documents/ess/" .time() . '_' . $model->step_2_file->baseName . "." . $model->step_2_file->extension;
            }
            else{
                $model->step_2_file = $step_2_file;
            }
            if(isset($model->step_3_file)) {
                $model->step_3_file->saveAs($_SERVER['DOCUMENT_ROOT'] . "/wb/documents/ess/" . time() . '_' . $model->step_3_file->baseName . "." . $model->step_3_file->extension);
                $model->step_3_file = "/wb/documents/ess/" .time() . '_' . $model->step_3_file->baseName . "." . $model->step_3_file->extension;
            }
            else{
                $model->step_3_file = $step_3_file;
            }
            if(isset($model->step_5_file)) {
                $model->step_5_file->saveAs($_SERVER['DOCUMENT_ROOT'] . "/wb/documents/ess/" . time() . '_' . $model->step_5_file->baseName . "." . $model->step_5_file->extension);
                $model->step_5_file = "/wb/documents/ess/" .time() . '_' . $model->step_5_file->baseName . "." . $model->step_5_file->extension;
            }
            else{
                $model->step_5_file = $step_5_file;
            }

            $model->save();

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    protected function findModel($id)
    {
        if (($model = ProjectDesignStep::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}