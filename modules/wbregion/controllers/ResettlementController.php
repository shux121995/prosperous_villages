<?php


namespace app\modules\wbregion\controllers;


use app\models\forms\ess\Resettlement;
use app\modules\wbregion\models\SearchResettlement;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class ResettlementController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new SearchResettlement();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new Resettlement();

        if ($model->load(Yii::$app->request->post())) {

            $model->form2_1_file = UploadedFile::getInstance($model, 'form2_1_file');

            if(isset($model->form2_1_file)) {
                $model->form2_1_file->saveAs($_SERVER['DOCUMENT_ROOT'] . "/wb/documents/ess/resettlement/" . time() . '_' . $model->form2_1_file->baseName . "." . $model->form2_1_file->extension);
                $model->form2_1_file = "/wb/documents/ess/resettlement/" .time() . '_' . $model->form2_1_file->baseName . "." . $model->form2_1_file->extension;
            }

            $model->save();

            return $this->redirect(['view', 'id' => $model->id]);

        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $form1_1_file = $model->form2_1_file;

        if ($model->load(Yii::$app->request->post())) {

            $model->form2_1_file = UploadedFile::getInstance($model, 'form2_1_file');

            if(isset($model->form2_1_file)) {
                $model->form2_1_file->saveAs($_SERVER['DOCUMENT_ROOT'] . "/wb/documents/ess/resettlement/" . time() . '_' . $model->form2_1_file->baseName . "." . $model->form2_1_file->extension);
                $model->form2_1_file = "/wb/documents/ess/resettlement/" .time() . '_' . $model->form2_1_file->baseName . "." . $model->form2_1_file->extension;
            }
            else{
                $model->form1_1_file = $form1_1_file;
            }

            $model->save();

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    protected function findModel($id)
    {
        if (($model = Resettlement::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}