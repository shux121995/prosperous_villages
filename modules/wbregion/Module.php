<?php

namespace app\modules\wbregion;

/**
 * wbadmin module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\wbregion\controllers';
    public $layout = '//wbregion';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        \Yii::$app->setHomeUrl('/wbregion');
        parent::init();

        // custom initialization code goes here
    }
}