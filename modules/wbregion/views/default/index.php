<?php $this->title = 'Prosperous Villages Project'; ?>
<div class="admin-default-index">
    <div class="jumbotron">
        <h1><?= $this->title ?></h1>
        <p class="lead">World Bank Regional Moderator</p>
        <p>
            <img src="/off-logo.png" alt="off-logo.jpg">
        </p>
    </div>
</div>