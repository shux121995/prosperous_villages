<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\forms\ess\ProjectDesignStep */

$this->title = Yii::t('app', 'Add Project Design Step');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Project Design Step'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>