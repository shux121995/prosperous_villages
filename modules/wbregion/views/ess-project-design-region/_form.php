<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\forms\ess\ProjectDesignStep */
/* @var $form yii\widgets\ActiveForm */
?>

<div>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'region_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Region::find()->where(['id'=> (new \app\models\UserAccess)->getRegionID()])->all(), 'id', 'title'),['prompt' => 'Select Region']) ?>

    <?= $form->field($model, 'district_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\District::find()->where(['region_id'=> (new \app\models\UserAccess)->getRegionID()])->all(), 'id', 'title'), ['prompt' => 'Select Village']) ?>

    <?= $form->field($model, 'locality_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Locality::find()->all(), 'id', 'title'), ['prompt' => 'Select Village']) ?>

    <?= $form->field($model, 'form1_1')->widget(\kartik\date\DatePicker::className(),['pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'autoclose' => true,
        'todayHighlight' => true,
    ]])?>

    <?= $form->field($model, 'form1_1_file')->fileInput() ?>

    <?= $form->field($model, 'form1_2')->widget(\kartik\date\DatePicker::className(),['pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'autoclose' => true,
        'todayHighlight' => true,
    ]])?>

    <?= $form->field($model, 'form1_2_file')->fileInput() ?>

    <?= $form->field($model, 'form1_3')->widget(\kartik\date\DatePicker::className(),['pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'autoclose' => true,
        'todayHighlight' => true,
    ]])?>

    <?= $form->field($model, 'form1_3_file')->fileInput() ?>

    <?= $form->field($model, 'form1_4')->widget(\kartik\date\DatePicker::className(),['pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'autoclose' => true,
        'todayHighlight' => true,
    ],'options' => ['disabled' => 'disabled',]])?>

    <?= $form->field($model, 'form1_5')->widget(\kartik\date\DatePicker::className(),['pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'autoclose' => true,
        'todayHighlight' => true,
    ], 'options' => ['disabled' => 'disabled',]])?>

    <?= $form->field($model, 'step_2')->widget(\kartik\date\DatePicker::className(),['pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'autoclose' => true,
        'todayHighlight' => true,
    ]])?>

    <?= $form->field($model, 'step_2_file')->fileInput() ?>

    <?= $form->field($model, 'step_3')->widget(\kartik\date\DatePicker::className(),['pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'autoclose' => true,
        'todayHighlight' => true,
    ]])?>

    <?= $form->field($model, 'step_3_file')->fileInput() ?>

    <?= $form->field($model, 'step_4')->widget(\kartik\date\DatePicker::className(),['pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'autoclose' => true,
        'todayHighlight' => true,
    ],'options' => ['disabled' => 'disabled',]])?>

    <?= $form->field($model, 'step_5')->widget(\kartik\date\DatePicker::className(),['pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'autoclose' => true,
        'todayHighlight' => true,
    ]])?>

    <?= $form->field($model, 'step_5_file')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>