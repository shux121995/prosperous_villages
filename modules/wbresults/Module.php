<?php

namespace app\modules\wbresults;

use Yii;
/**
 * wbresults module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\wbresults\controllers';
    public $defaultRoute = 'republic';
    public $layout = '//wb';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        Yii::$app->language = Yii::$app->lang->check_lang().'-'.strtoupper(Yii::$app->lang->check_lang());
        parent::init();

        // custom initialization code goes here
    }
}
