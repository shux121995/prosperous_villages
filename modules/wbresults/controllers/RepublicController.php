<?php

namespace app\modules\wbresults\controllers;

use app\models\Region;
use app\models\WbTypes;
use yii\web\Controller;
use Yii;
use yii\helpers\Url;

/**
 * Republic controller for the `wbresults` module
 */
class RepublicController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex($type=1)
    {
        $this->view->params['tn_url'] = '/'.\Yii::$app->controller->module->id.'/republic';
        //Вернем к главную
        //if($type==null) return $this->redirect(Url::to('/wbresults'));
        $locals = Region::find()->all();

        return $this->render('index',
            [
                'type'     => WbTypes::findOne($type),
                'types'     => WbTypes::find()->all(),
                'locals'    => $locals
            ]
        );
    }

    /**
     * Export for Excel
     * @param null $table
     * @param null $district
     * @return \yii\web\Response
     */
    public function actionExcelOut($type = 1)
    {
    $lang = (new \app\components\LangHelper())->check_lang();
    $this->view->params['tn_url'] = '/'.\Yii::$app->controller->module->id.'/republic';
    //Вернем к главную
    //if($type==null) return $this->redirect(Url::to('/wbresults'));
    $locals = Region::find()->all();
    $typed = WbTypes::findOne($type);
    $types = WbTypes::find()->all();
    $filename = 'result_type_' . $typed->id . '_republic.xls';

    header('Content-type: application/excel');
    header('Content-Disposition: attachment; filename=' . $filename);

    $data = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">
            <head>
                <!--[if gte mso 9]>
                <xml>
                    <x:ExcelWorkbook>
                        <x:ExcelWorksheets>
                            <x:ExcelWorksheet>
                                <x:Name>Sheet 1</x:Name>
                                <x:WorksheetOptions>
                                    <x:Print>
                                        <x:ValidPrinterInfo/>
                                    </x:Print>
                                </x:WorksheetOptions>
                            </x:ExcelWorksheet>
                        </x:ExcelWorksheets>
                    </x:ExcelWorkbook>
                </xml>
                <![endif]-->
            </head>
            
            <body>';
    $html = '
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th rowspan="2">#</th>
                        <th rowspan="2">' . Yii::t('app', 'Districts') . '</th>';
    foreach ($types as $type) {
        $html .= '<th colspan="2">' . $type->getTitleLang($lang) . '</th>';
    }
    $html .= '</tr>
                    <tr>';
    foreach ($types as $type) {
        $html .= '<th>' . Yii::t('app', 'Reja') . '</th>';
        $html .= '<th>' . Yii::t('app', 'Fact') . '</th>';
    }
    $html .= '</tr>
                </thead>
                <tbody>';

    /** @var $local \app\models\Region */
    $i = 1;
    foreach ($locals

    as $local){
    $html .= '<tr>
                            <td>' . $i . '</td>
                            <td>' . $local->getTitleLang($lang) ?></td>';
    <?php
    foreach ($types as $excel) {
        $html .= '<td class="sum">' . $excel->getPlanResp($local->id, $excel->id) . '</td>';
        $html .= '<td class="sum">' . $excel->getFactResp($local->id, $excel->id) . '</td>';
    }
    $html .= '</tr>';
    $i++;
    }

    $html .= '<td></td>';
    $html .= '<td class="all"><b>' . Yii::t('app', 'Jami:') . '</b></td>';
    foreach ($types as $excel) {
        $html .= '<td class="total"></td>';
        $html .= '<td class="total"></td>';
    }
    $html .= '
                </tbody>
            </table>';
    $data .= $html;
    $data .= '</body></html>';
    echo $data;
    exit();
    }
}
