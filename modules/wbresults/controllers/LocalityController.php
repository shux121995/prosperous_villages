<?php


namespace app\modules\wbresults\controllers;


use app\models\ComProsCategory;
use app\models\forms\ess\ProjectDesignStep;
use app\models\forms\ess\Resettlement;
use app\models\forms\Form5_14;
use app\models\forms\infrastructure_and_finance\Infra_and_finan;
use app\models\forms\mobilization\Aholi;
use app\models\forms\mobilization\Cdwo;
use app\models\forms\mobilization\CpmLeader;
use app\models\forms\mobilization\HouseholdsWelfare;
use app\models\forms\mobilization\MduImiScore;
use app\models\forms\mobilization\MduMembers;
use app\models\forms\mobilization\VillagesScore;
use app\models\forms\mobilization\VillagesVoters;
use app\models\Locality;
use app\models\SocialMobilization;
use app\models\forms\Form1_1;
use yii\base\Model;
use yii\helpers\Url;
use yii\web\Controller;

class LocalityController extends Controller
{
    public function actionIndex($type=1,$locality)
    {

        $parent = Locality::findOne($locality);
        /*Mobilization*/
        $population = Aholi::find()->where(['locality_id'=>$locality])->one();
        $villageScore = VillagesScore::find()->where(['locality_id'=>$locality])->one();
        $householdsWelfare = HouseholdsWelfare::find()->where(['locality_id'=>$locality])->one();
        $villageVoters = VillagesVoters::find()->where(['locality_id'=>$locality])->one();
        $mduMembers = MduMembers::find()->where(['locality_id'=>$locality])->one();
        $mduImiScore = MduImiScore::find()->where(['locality_id'=>$locality])->one();
        $cpmLeader = CpmLeader::find()->where(['locality_id'=>$locality])->one();
        $cdwo = Cdwo::find()->where(['locality_id'=>$locality])->one();

        /*ESS*/
        $projectDesignSteps = ProjectDesignStep::find()->where(['locality_id'=>$locality])->one();
        $resettlementForm = Resettlement::find()->where(['locality_id'=>$locality, 'visibility'=> 1])->all();

        /*Infrastructure*/
        $infrastructure = Infra_and_finan::find()->where(['locality_id'=>$locality])->all();

        /*Finance Table*/
        $form_5_14 = Form5_14::find()->where(['village_id'=>$locality])->all();

        if($locality==null) return $this->redirect(Url::to('/wbresults'));
        $this->view->params['tn_url'] = '/'.\Yii::$app->controller->module->id.'/locality?type=1&locality='.$locality;

        return $this->render('index',
            [
                'parent'                             => $parent,
                'form_5_14'=>$form_5_14,

                'population'=>$population,
                'villageScore'=>$villageScore,
                'householdsWelfare'=>$householdsWelfare,
                'villageVoters'=>$villageVoters,
                'mduMembers'=>$mduMembers,
                'mduImiScore'=>$mduImiScore,
                'cpmLeader'=>$cpmLeader,
                'cdwo'=>$cdwo,
                'projectDesignSteps'=>$projectDesignSteps,
                'resettlementForm'=>$resettlementForm,
                'infrastructure' =>$infrastructure

            ]
            );
    }
}