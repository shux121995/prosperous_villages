<?php

namespace app\modules\wbresults\controllers;

use app\models\District;
use app\models\Locality;
use app\models\Village_selection_process;
use app\models\WbTypes;
use yii\web\Controller;
use Yii;
use yii\helpers\Url;

/**
 * Republic controller for the `wbresults` module
 */
class DistrictController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex($type=1,$district)
    {
        //Вернем к главную
        if($district==null) return $this->redirect(Url::to('/wbresults'));
        $this->view->params['tn_url'] = '/'.\Yii::$app->controller->module->id.'/district?type=1&district='.$district;
        $locals = Locality::find()->where(['district_id'=>$district])->andWhere(['type'=>Locality::TYPE_WB])->all();
        $parent = District::findOne($district);

        $village_selection_process = Village_selection_process::find()->where(['district_id'=>$district])->all();


        return $this->render('index',
            [
                'type'     => WbTypes::findOne($type),
                'types'     => WbTypes::find()->all(),
                'locals'    => $locals,
                'parent'    => $parent,
                'village_selection_process'      => $village_selection_process,

            ]
        );
    }

    /**
     * Export for Excel
     * @param null $table
     * @param null $district
     * @return \yii\web\Response
     */
    public function actionExcelOut($type=null,$district=null)
    {
        $lang = (new \app\components\LangHelper())->check_lang();
        //Вернем к главную
        if($district==null AND $type==null) return $this->redirect(Url::to('/wbresults'));

        $this->view->params['tn_url'] = '/'.\Yii::$app->controller->module->id.'/district?type=1&district='.$district;
        $locals = Locality::find()->where(['district_id'=>$district])->andWhere(['type'=>Locality::TYPE_WB])->all();
        $parent = District::findOne($district);
        $typed   = WbTypes::findOne($type);
        $types  = WbTypes::find()->all();
        $filename = 'result_type_'.$typed->id.'_district_'.$district.'.xls';

        header('Content-type: application/excel');
        header('Content-Disposition: attachment; filename='.$filename);

        $data = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">
<head>
    <!--[if gte mso 9]>
    <xml>
        <x:ExcelWorkbook>
            <x:ExcelWorksheets>
                <x:ExcelWorksheet>
                    <x:Name>Sheet 1</x:Name>
                    <x:WorksheetOptions>
                        <x:Print>
                            <x:ValidPrinterInfo/>
                        </x:Print>
                    </x:WorksheetOptions>
                </x:ExcelWorksheet>
            </x:ExcelWorksheets>
        </x:ExcelWorkbook>
    </xml>
    <![endif]-->
</head>

<body>';
        $html = '
<table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th rowspan="2">#</th>
            <th rowspan="2">'.Yii::t('app','Localities').'</th>';
            foreach ($types as $type){
                $html .= '<th colspan="2">'.$type->getTitleLang($lang).'</th>';
            }
        $html .= '</tr>
        <tr>';
            foreach ($types as $type){
                $html .= '<th>'.Yii::t('app','Reja').'</th>';
                $html .= '<th>'.Yii::t('app','Fact').'</th>';
            }
        $html .= '</tr>
    </thead>
    <tbody>';

        /** @var $local \app\models\Region*/
        $i = 1;
        foreach ($locals as $local){
            $html .= '<tr>
                <td>'.$i.'</td>
                <td>'.$local->getTitleLang($lang)?></td>';
                <?php
                foreach ($types as $excel) {
                    $html .= '<td class="sum">'.$excel->getPlanDis($local->id, $excel->id).'</td>';
                    $html .= '<td class="sum">'.$excel->getFactDis($local->id, $excel->id).'</td>';
                }
                $html .= '</tr>';
            $i++;
        }

                $html .= '<td></td>';
        $html .= '<td class="all"><b>'.Yii::t('app','Jami:').'</b></td>';
        foreach ($types as $excel) {
            $html .= '<td class="total"></td>';
            $html .= '<td class="total"></td>';
        }
                $html .= '
    </tbody>
</table>';
        $data .= $html;
        $data .= '</body></html>';
        echo $data;
        exit();
    }
}
