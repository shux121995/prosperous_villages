<?php
use app\modules\results\models\GenerateTable;
/* @var $this yii\web\View */
/** @var $type \app\models\WbTypes */
/** @var $parent \app\models\District */
/*** @var $village_selection_process \app\models\Village_selection_process */
/**** @var $community_progress_categories \app\models\ComProsCategory */
/***** @var $social_mobilization \app\models\SocialMobilization */

//* * * Mobilizations models * * *
/****** @var $villageScore \app\models\forms\mobilization\VillagesScore */
/****** @var $householdsWelfare \app\models\forms\mobilization\HouseholdsWelfare */
/****** @var $villageVoters \app\models\forms\mobilization\VillagesVoters */
/****** @var $mduMembers \app\models\forms\mobilization\MduMembers */
/****** @var $mduImiScore \app\models\forms\mobilization\MduImiScore */
/****** @var $cpmLeader \app\models\forms\mobilization\CpmLeader */
/****** @var $cdwo \app\models\forms\mobilization\Cdwo */

/* * * EES models * * */
/****** @var $projectDesignSteps \app\models\forms\ess\ProjectDesignStep */
/****** @var $resettlementForm \app\models\forms\ess\Resettlement */
//* * * Start of Form 5.14 * * *
/****** @var $form_5_14 \app\models\forms\Form5_14 */

$lang = (new \app\components\LangHelper())->check_lang();
$this->title = Yii::t('app',$parent->getTitleLang($lang));
?>


<style>
    .heading{
        background-color: rgba(244, 245, 250, 0.85);
        display: block;
        line-height: 70px;
    }
    .collapse-button{
        vertical-align: middle;
        background-color: transparent;
        background-repeat:no-repeat;
        border: none;
        cursor:pointer;
        overflow: hidden;
        outline:none;
        font-size: 18px;
        font-weight: bold;
    }
    .collapse-image{
        width: 70px;
        height: 70px;
    }
    .progressbar{
        counter-reset: step;
    }
    .progressbar li{
        list-style-type: none;
        float: left;
        font-weight: bold;
        width: 11.11%;
        position: relative;
        text-align: center;
    }
    .progressbar li:before{
        content: counter(step);
        counter-increment: step;
        width: 30px;
        height: 30px;
        line-height: 30px;
        border: 2px solid #ddd;
        display: block;
        text-align: center;
        margin: 0 auto 10px auto;
        border-radius: 50%;
        background-color: white;
        position: relative;
        z-index: 1;
    }
    .progressbar li:after{
        content: '';
        position: absolute;
        width: 100%;
        height: 2px;
        background-color: #ddd;
        top: 15px;
        left: -50%;
        z-index: 0;
    }
    .progressbar li:first-child:after{
        content: none;
    }
    .progressbar li.active{
        color: green;
    }
    .progressbar li.active:before{
        border-color: green;
        color: white;
        background-color: green;
    }
    .progressbar li.active + li:after{
        background-color: green;
    }
</style>

<div style="width: content-box; margin-top: 15px">
    <div class="accordion" id="accordionExample">
        <!--Social Mobilization-->
        <div class="card">
            <div class="heading" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                <img src="../../wb/images/village_page_icons/social_mobilization_icon.png" class="float-left collapse-image" alt="project icon">
                <button class="collapse-button"> Social Mobilization </button>
                <img src="../../wb/images/main_page_icons/arrow_down.png" alt="arrow down icon" class="float-right collapse-image">
            </div>
            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="card-body">
                    <!-- here you can place your code, later I will split them into separate layouts-->
                    <div class="card-columns">
                        <div class="card shadow p-3 mb-5 bg-white rounded ">
                                <div class="card-body">
                                    <h5 class="card-title font-weight-bold">"<?=$parent->getTitleLang($lang)?>" Population</h5>
                                    <p class="card-text">&#8226; Total number of households: <b><?= $population->xonadon_soni?></b></p>
                                    <p class="card-text">&#8226; Male: <b><?= $population->erkak_soni?></b></p>
                                    <p class="card-text">&#8226; Female: <b><?= $population->ayol_soni?></b></p>
                                    <p class="card-text">&#8226; Total: <b><?= $population->aholi_soni?></b> </p>
                                </div>
                        </div>
                            <div class="card shadow p-3 mb-5 bg-white rounded">
                                <div class="card-body">
                                    <h5 class="card-title font-weight-bold">"<?=$parent->getTitleLang($lang)?>" Score: <?= $villageScore->remoteness + $villageScore->water?></h5>
                                    <p class="card-text">&#8226; Remoteness score: <b><?= $villageScore->remoteness?></b></p>
                                    <p class="card-text">&#8226; Water score: <b><?= $villageScore->water?></b></p>
                                    <table class="table table-hover table-bordered">
                                        <thead>
                                        <tr>
                                            <th scope="col">Score</th>
                                            <th scope="col">Remoteness (km)</th>
                                            <th scope="col">Access to water (%)</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <th scope="row">1</th>
                                            <td>less than 5</td>
                                            <td>80-100</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">2</th>
                                            <td>5-10</td>
                                            <td>60-80</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">3</th>
                                            <td>10-15</td>
                                            <td>40-60</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">4</th>
                                            <td>15-20</td>
                                            <td>20-40</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">5</th>
                                            <td>more than 20</td>
                                            <td>0-20</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                        </div>
                            <div class="card shadow p-3 mb-5 bg-white rounded">
                                    <div class="card-body">
                                        <h5 class="card-title font-weight-bold">Households Welfare</h5>
                                        <p class="card-text">&#8226; Households with high income: <b><?= $householdsWelfare->high?></b></p>
                                        <p class="card-text">&#8226; Households with middle income: <b><?= $householdsWelfare->middle?></b></p>
                                        <p class="card-text">&#8226; Households with low income: <b><?= $householdsWelfare->low?></b></p>
                                        <p class="card-text">&#8226; Households that are in need: <b><?= $householdsWelfare->in_need?></b></p>
                                        <p class="card-text">&#8226; Households without access to drinking water: <b><?= $householdsWelfare->without_water?></b></p>
                                    </div>
                        </div>
                            <div class="card shadow p-3 mb-5 bg-white rounded">
                                <div class="card-body">
                                    <h5 class="card-title font-weight-bold">"<?=$parent->getTitleLang($lang)?>" Voters</h5>
                                    <p class="card-text">&#8226; Male voters: <b><?= $villageVoters->male?></b></p>
                                    <p class="card-text">&#8226; Female voters: <b><?= $villageVoters->female?></b></p>
                                    <p class="card-text">&#8226; Male participants: <b><?= round($villageVoters->p_male/$villageVoters->male*100, 2)?>%</b></p>
                                    <p class="card-text">&#8226; Female participants: <b><?= round($villageVoters->p_female/$villageVoters->female*100, 2)?>%</b></p>
                                    <p class="card-text">&#8226; Total participans: <b><?= round($villageVoters->p_total/$villageVoters->total*100, 2)?>%</b></p>
                                </div>
                        </div>
                            <div class="card shadow p-3 mb-5 bg-white rounded">
                                <div class="card-body">
                                    <h5 class="card-title font-weight-bold">MDU Members & Score</h5>
                                    <p class="card-text">&#8226; Chair person: <b><?= $mduMembers->chair?></b></p>
                                    <p class="card-text">&#8226; Vice Chair person: <b><?= $mduMembers->vice_chair?></b></p>
                                    <p class="card-text">&#8226; Secretary: <b><?= $mduMembers->secretary?></b></p>
                                    <p class="card-text">&#8226; Phone number: <b><?= $mduMembers->phone_number?></b></p>
                                    <p class="card-text">&#8226; MDU IMI Score: <b><?= $mduImiScore->score?>%</b></p>
                                </div>
                        </div>
                            <div class="card shadow p-3 mb-5 bg-white rounded">
                                <div class="card-body">
                                    <h5 class="card-title font-weight-bold">CPM Leader</h5>
                                    <p class="card-text">&#8226; CPM Leader: <b><?= $cpmLeader->name?></b></p>
                                    <p class="card-text">&#8226; Phone number: <b><?= $cpmLeader->phone?></b></p>
                                </div>
                            </div>
                            <div class="card shadow p-3 mb-5 bg-white rounded">
                                <div class="card-body">
                                    <h5 class="card-title font-weight-bold">CDWO Staff members</h5>
                                    <p class="card-text">&#8226; CDWO: <b><?= $cdwo->cdwo_name?></b></p>
                                    <p class="card-text">&#8226; Director: <b><?= $cdwo->director?></b></p>
                                    <p class="card-text">&#8226; Vice Director: <b><?= $cdwo->vice_director?></b></p>
                                    <p class="card-text">&#8226; Secretary: <b><?= $cdwo->secretary?></b></p>
                                    <p class="card-text">&#8226; Phone number: <b><?= $cdwo->phone?></b></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--Infrastructure-->
        <div class="card">
            <div class="heading" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                <img src="../../wb/images/village_page_icons/infrastructure icon.png" class="float-left collapse-image" alt="project icon">
                <button class="collapse-button"> Infrastructure </button>
                <img src="../../wb/images/main_page_icons/arrow_down.png" alt="arrow down icon" class="float-right collapse-image">
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                <div class="card-body"><!--Here you have to check whether model is empty or not-->
                    <?php foreach ($infrastructure as $inf):?>
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">

                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100" src="<?=$inf->sub_before?>" alt="Before image">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5><?php $subproject_name = \app\models\WbTypes::findOne($inf->sub_id); if($subproject_name!=null) echo $subproject_name->getTitleLang($lang);?></h5>
                                    <p><?=$inf->constraction_start?></p>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="<?=$inf->sub_after?>" alt="After image">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5><?php $subproject_name = \app\models\WbTypes::findOne($inf->sub_id); if($subproject_name!=null) echo $subproject_name->getTitleLang($lang);?></h5>
                                    <p><?=$inf->constraction_end?></p>
                                </div>
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                    <?php endforeach;?>
                    <?php if ($infrastructure != null):?>
                    <h5 style="text-align:center; margin-top: 30px; margin-bottom:30px; font-weight: bold">Information about subprojects in <?=Yii::t('app',$parent->getTitleLang($lang));?></h5>
                    <table class="table table-hover text-center table-bordered">
                        <thead class="table-dark">
                        <tr>
                            <th scope="col" style="width: 4%">#</th>
                            <th scope="col" style="width: 16%">Subproject name</th>
                            <th scope="col" style="width: 16%">Type of work</th>
                            <th scope="col" style="width: 16%">Direct/Indirect beneficiaries</th>
                            <th scope="col" style="width: 16%">General plan</th>
                            <th scope="col" style="width: 16%">Construction start/end date</th>
                            <th scope="col" style="width: 16%">Subproject progress</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $i = 0; foreach ($infrastructure as $inf) $i = $i +1;?>
                        <tr>
                            <th scope="row"><?=$i?></th>
                            <td><?php $subproject_name = \app\models\WbTypes::findOne($inf->sub_id); if($subproject_name!=null) echo $subproject_name->getTitleLang($lang);?></td>
                            <td><?php $subwork_name = \app\models\WbSubTypes::findOne($inf->sub_work_id); if($subwork_name!=null) echo $subwork_name->getTitleLang($lang);?></td>
                            <td><?php echo $inf->direct_b;?>/<?php echo $inf->indirect_b;?></td>
                            <td>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: <?php echo $inf->gen_plan_progress;?>%;" aria-valuemin="0" aria-valuemax="100"><?php echo $inf->gen_plan_progress;?>%</div>
                                </div>
                            </td>
                            <td><?php echo $inf->constraction_start;?> ~ <?php echo $inf->constraction_end;?></td>
                            <td>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: <?php echo $inf->sub_progress;?>%;" aria-valuemin="0" aria-valuemax="100"><?php echo $inf->sub_progress;?>%</div>
                                </div>
                            </td>
                        </tr>
                        <?php endif;?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

        <!--Environmental and Social Screening-->
        <div class="card">
            <div class="heading" id="headingFive" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                <img src="../../wb/images/village_page_icons/environmental_and_social_icon.png" class="float-left collapse-image" alt="project icon">
                <button class="collapse-button"> Environmental and Social Screening </button>
                <img src="../../wb/images/main_page_icons/arrow_down.png" alt="arrow down icon" class="float-right collapse-image">
            </div>
            <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                <div class="card-body">
                    <?php if ($projectDesignSteps != null):?>
                        <h5 style="text-align: center; margin-bottom: 30px; font-weight: bold">Environmental Specialist Project Design Steps Completion</h5>
                        <ul class="progressbar">
                            <li class="<?php if ($projectDesignSteps->form1_1): echo "active"; endif;?>">Form 1.1<p style="font-weight: bold"><?= $projectDesignSteps->form1_1?></p></li>
                            <li class="<?php if ($projectDesignSteps->form1_2): echo "active"; endif;?>">Form 1.2<p style="font-weight: bold"><?= $projectDesignSteps->form1_2?></p></li>
                            <li class="<?php if ($projectDesignSteps->form1_3): echo "active"; endif;?>">Form 1.3<p style="font-weight: bold"><?= $projectDesignSteps->form1_3?></p></li>
                            <li class="<?php if ($projectDesignSteps->form1_4): echo "active"; endif;?>">Form 1.4<p style="font-weight: bold"><?= $projectDesignSteps->form1_4?></p></li>
                            <li class="<?php if ($projectDesignSteps->form1_5): echo "active"; endif;?>">Form 1.5<p style="font-weight: bold"><?= $projectDesignSteps->form1_5?></p></li>
                            <li class="<?php if ($projectDesignSteps->step_2): echo "active"; endif;?>">Subprojects Environmental and Social Impact Assessment<p style="font-weight: bold"><?= $projectDesignSteps->step_2?></p></li>
                            <li class="<?php if ($projectDesignSteps->step_3): echo "active"; endif;?>">Public Consultation<p style="font-weight: bold"><?= $projectDesignSteps->step_3?></p></li>
                            <li class="<?php if ($projectDesignSteps->step_4): echo "active"; endif;?>">World Bank Acceptance<p style="font-weight: bold"><?= $projectDesignSteps->step_4?></p></li>
                            <li class="<?php if ($projectDesignSteps->step_5): echo "active"; endif;?>">ESA Information Disclosure<p style="font-weight: bold"><?= $projectDesignSteps->step_5?></p></li>
                        </ul>
                    <div style="text-align: center;">
                        <?php if ($resettlementForm != null):?>
                        <h5 style="margin-top: 30px; margin-bottom:30px; display: inline-block; font-weight: bold">Resettlement form was completed for the projects down below</h5>
                        <table class="table table-hover text-center table-bordered">
                            <thead class="table-dark">
                            <tr>
                                <th scope="col" style="width: 10%">#</th>
                                <th scope="col" style="width: 30%">Subproject name</th>
                                <th scope="col" style="width: 30%">Date of completion</th>
                                <th scope="col" style="width: 30%">PDF file</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i = 0; foreach ($resettlementForm as $res) $i = $i +1;?>
                            <tr>
                                <th scope="row"><?=$i?></th>
                                <td><?php $subproject_name = \app\models\WbTypes::findOne($res->project_id); if($subproject_name!=null) echo $subproject_name->getTitleLang($lang);?></td>
                                <td><?=$res->form2_1?></td>
                                <td><a href="<?=$res->form2_1_file?>" target="_blank"><i class="fa fa-eye" style="font-size:24px"></i></a></td>
                            </tr>
                            <?php endif;?>
                            </tbody>
                        </table>
                    </div>
                    <?php else:
                        echo "No records have been received yet!"?>
                    <?php endif;?>
                </div>
            </div>
        </div>

        <!--Procurement
        <div class="card">
            <div class="heading" id="headingThree" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                <img src="../../wb/images/village_page_icons/procurment_icon.png" class="float-left collapse-image" alt="project icon">
                <button class="collapse-button"> Procurement </button>
                <img src="../../wb/images/main_page_icons/arrow_down.png" alt="arrow down icon" class="float-right collapse-image">
            </div>
            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                <div class="card-body">
                    No records have been received yet!
                </div>
            </div>
        </div>
        -->
        <!--Finance-->
        <div class="card">
            <div class="heading" id="headingFour" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                <img src="../../wb/images/village_page_icons/investment_icon.png" class="float-left collapse-image" alt="project icon">
                <button class="collapse-button"> Finance </button>
                <img src="../../wb/images/main_page_icons/arrow_down.png" alt="arrow down icon" class="float-right collapse-image">
            </div>
            <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                <div class="card-body">
                    <h4 style="text-align: center; margin-bottom: 15px">Information about subprojects financiacion</h4>
                    <table class="table table-hover text-center table-bordered">
                        <thead class="table-dark">
                        <tr>
                            <th>#</th>
                            <th>Subproject name</th>
                            <th>Contractor name</th>
                            <th>Contract number</th>
                            <th>Contract start/end date</th>
                            <th>Contract Value, USD</th>
                            <th>Undisbursed amount, USD</th>
                            <th>Status of subproject</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $index = 1; foreach ($infrastructure as $inf){?>
                            <tr>
                                <td><?php echo $index?></td>
                                <td><?php $subproject_name = \app\models\WbTypes::findOne($inf->sub_id); if($subproject_name!=null) echo $subproject_name->getTitleLang($lang);?></td>
                                <td><?php $contractor_name = \app\models\WbDevelopers::findOne($inf->res_contractor); if($contractor_name!=null) echo $contractor_name->getTitleLang($lang)?></td>
                                <td><?php echo $inf->contract_number?></td>
                                <td><?php echo $inf->contract_start?> ~ <?php echo $inf->contract_end?></td>
                                <td><?php echo $inf->total_contract_value?></td>
                                <td><?php echo $inf->total_undis_amount?></td>
                                <td><?php $status = \app\models\WbStatus::findOne($inf->status_of_subproject); if($status!=null) echo $status->getTitleLang($lang)?></td>
                            </tr>
                            <?php
                            $index++;}?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!--Trainings-->
        <div class="card">
            <div class="heading" id="headingSeven" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                <img src="../../wb/images/village_page_icons/trainings_icon.png" class="float-left collapse-image" alt="project icon">
                <button class="collapse-button"> Trainings </button>
                <img src="../../wb/images/main_page_icons/arrow_down.png" alt="arrow down icon" class="float-right collapse-image">
            </div>
            <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordionExample">
                <div class="card-body">
                    No records have been received yet!
                </div>
            </div>
        </div>

        <!--Monitoring
        <div class="card">
            <div class="heading" id="headingSix" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                <img src="../../wb/images/village_page_icons/monitoring_icon.png" class="float-left collapse-image" alt="project icon">
                <button class="collapse-button"> Monitoring </button>
                <img src="../../wb/images/main_page_icons/arrow_down.png" alt="arrow down icon" class="float-right collapse-image">
            </div>
            <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample">
                <div class="card-body">

                </div>
            </div>
        </div>
    </div>-->

</div>
