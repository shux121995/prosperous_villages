<?php
use app\modules\results\models\GenerateTable;
/* @var $this yii\web\View */
/** @var $type \app\models\WbTypes */
$lang = (new \app\components\LangHelper())->check_lang();

$this->title = Yii::t('app','Results on republic');
?>
<!--<h2><?=\yii\helpers\Html::a(Yii::t('app','Export XLS'),['excel-out','type'=>$type->id],['class'=>'btn btn-danger float-right'])?> <?= $this->title?></h2>-->
<table class="table table-borderless table-hover">
    <thead>
        <tr>
            <!--
            <th rowspan="2">#</th>
            <th rowspan="2"><?=Yii::t('app','Regions')?></th>
            <?php
            foreach ($types as $type){
                echo '<th colspan="2">'.$type->getTitleLang($lang).'</th>';
            }
            ?>
            -->
        </tr>
        <!--<tr>
            <?php
            foreach ($types as $type){
                echo '<th>'.Yii::t('app','Plan').'</th>';
                echo '<th>'.Yii::t('app','Fact').'</th>';
            }
            ?>
        </tr>-->
    </thead>

    <tbody>
        <?php
        /** @var $local \app\models\Region*/
        $i = 1;
        foreach ($locals as $local){ ?>
            <tr>
                <?php if($local->id == 2 || $local->id == 7 ||$local->id == 12 ||$local->id == 10 ||$local->id == 4){?>


                <td><?=

                    \yii\helpers\Html::a($i.") ".$local->getTitleLang($lang),'/'.$lang.\yii\helpers\Url::toRoute(['/wbresults/region','type'=>1,'region'=>$local->id,]))

                    ?></td>

                <?php
                    $i++; } ?>
            </tr>
        <?php
        }
        ?>
    </tbody>
</table>
<?php
$js = <<<JS
$(function()
{
    function tally (selector) 
    {
      $(selector).each(function () 
      {
          var total = 0,
          column = $(this).siblings(selector).addBack().index(this);
          $(this).parents().prevUntil(':has(' + selector + ')').each(function () 
          {
            total += parseFloat($('td.sum:eq(' + column + ')', this).html()) || 0;
          })
          $(this).html(total);
     });
    }
  //tally('td.subtotal');
  tally('td.total');
});
JS;
$this->registerJs($js, \yii\web\View::POS_END);
