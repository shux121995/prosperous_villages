<?php
use app\modules\results\models\GenerateTable;
/* @var $this yii\web\View */
/** @var $type \app\models\WbTypes */
$lang = (new \app\components\LangHelper())->check_lang();

//$this->title = Yii::t('app','Results on region');
?>
<!--<h2><?=\yii\helpers\Html::a(Yii::t('app','Export XLS'),['excel-out','type'=>$type->id,'region'=>$parent->id],['class'=>'btn btn-danger float-right'])?>
    <?= $this->title?>
    <?= \yii\helpers\Html::a($parent->getTitleLang($lang),
        '/'.$lang.\yii\helpers\Url::toRoute(['/wbresults/republic','type'=>1]),
        ['class'=>'btn btn-default'])?></h2>-->
<table class="table table-borderless table-hover">
    <thead>
        <tr>

        </tr>
    </thead>
    <tbody>
        <?php
        /** @var $local \app\models\Region*/
        $i = 1;
        foreach ($locals as $local){ ?>
            <tr>
            <?php if($local->id == 24 || $local->id == 19 || $local->id == 23|| $local->id == 28|| $local->id == 25
                || $local->id == 82|| $local->id == 83|| $local->id == 74|| $local->id == 84|| $local->id == 78
                || $local->id == 148|| $local->id == 149|| $local->id == 142|| $local->id == 147
                || $local->id == 112|| $local->id == 116|| $local->id == 119
                || $local->id == 46|| $local->id == 51|| $local->id == 52|| $local->id == 42){?>
                <td><?= \yii\helpers\Html::a($i.") ".$local->getTitleLang($lang),'/'.$lang.\yii\helpers\Url::toRoute(['/wbresults/district','type'=>1,'district'=>$local->id,]))?></td>

                <?php
                $i++; } ?>
            </tr>
        <?php

        }


        ?>
    </tbody>
</table>
<?php
$js = <<<JS
$(function()
{
    function tally (selector) 
    {
      $(selector).each(function () 
      {
          var total = 0,
          column = $(this).siblings(selector).addBack().index(this);
          $(this).parents().prevUntil(':has(' + selector + ')').each(function () 
          {
            total += parseFloat($('td.sum:eq(' + column + ')', this).html()) || 0;
          })
          $(this).html(total);
     });
    }
  //tally('td.subtotal');
  tally('td.total');
});
JS;
$this->registerJs($js, \yii\web\View::POS_END);
