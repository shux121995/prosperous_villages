<?php
use app\models\Qurulish;
use app\models\BozorInfratuzilmasi;
$lang = (new \app\components\LangHelper())->check_lang();
?>
<div class="results-default-index">
    <h1>Килинган ишлар буйича натижалар:</h1>
    <h2>Жой номини танланг:</h2>
    <table style="margin: auto;" class="table table-bordered table-responsive table-hover">
        <thead>
        <th>#</th>
        <th>Республика кесимида</th>
        <th>Вилоят номи</th>
        <th>Туман номи</th>
        </thead>
        <?php
        $locals = \app\models\District::find()->joinWith('localities')->where(['locality.type'=>\app\models\Locality::TYPE_WB])->orderBy('region_id')->all();
        $lcs = [];
        /** @var $local \app\models\District*/
        foreach ($locals as $key=>$local){
            $lcs[$key]['region']['id'] = $local->region_id;
            $lcs[$key]['region']['title'] = $local->region->getTitleLang($lang);
            $lcs[$key]['locality']['id'] = $local->id;
            $lcs[$key]['locality']['title'] = $local->getTitleLang($lang);
        }

        $i = 1;
        $region = 0;
        foreach ($lcs as $lcl){
            echo '<tr>';
            echo '<td>'.$i.'</td>';
            if($i==1) echo '<td rowspan="'.count($locals).'" class="pointer" data-toggle="modal" data-target="#resp" style="vertical-align:top; text-align:center;" ><a href="#">Республика кечимида</a></td>';
            if ($region != $lcl['region']['id']){
                $lcl['region']['rowspan'] = \app\models\District::find()->where(['region_id'=>$lcl['region']['id']])->count();
                echo '<td class="pointer" data-toggle="modal" data-target="#reg_'.$lcl['region']['id'].'" style="vertical-align:middle; text-align:center;" rowspan="'.($lcl['region']['rowspan']).'"><strong><a href="#" onclick="return false;">'.$lcl['region']['title'].'</a></strong></td>';
            }
            $region = $lcl['region']['id'];
            echo '<td class="pointer" data-toggle="modal" data-target="#dis_'.$lcl['locality']['id'].'"><a href="#" onclick="return false;">'.$lcl['locality']['title'].'</a></td>';
            echo '</tr>';
            $i++;
        }
        ?>
    </table>
</div>
<?php $regions = \app\models\Region::find()->all(); ?>
<?php foreach ($regions as $region){?>
    <!-- Modal -->
    <div class="modal fade" id="reg_<?=$region->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><?=$region->getTitleLang($lang)?>:<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button></h5>
                </div>
                <div class="modal-body">
                    <ul>
                        <?php
                        $types = \app\models\WbTypes::find()->All();
                        foreach ($types as $type) {
                            echo '<li>'.\yii\helpers\Html::a($type->getTitleLang($lang),
                                    \yii\helpers\Url::toRoute(['/wbresults/region','type'=>$type->id,'region'=>$region->id]),
                                    ['title' => $type->getTitleLang($lang)]
                                ).'</li>';
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<?php $districts = \app\models\District::find()->all(); ?>
<?php foreach ($districts as $district){?>
    <!-- Modal -->
    <div class="modal fade" id="dis_<?=$district->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><?=$district->getTitleLang($lang)?>:<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button></h5>
                </div>
                <div class="modal-body">
                    <ul>
                        <?php
                        $types = \app\models\WbTypes::find()->All();
                        foreach ($types as $type) {
                            echo '<li>'.\yii\helpers\Html::a($type->getTitleLang($lang),
                                    \yii\helpers\Url::toRoute(['/wbresults/district','type'=>$type->id,'district'=>$district->id]),
                                    ['title' => $type->getTitleLang($lang)]
                                ).'</li>';
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<!-- Modal -->
<div class="modal fade" id="resp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Республика кесимида:<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button></h5>
            </div>
            <div class="modal-body">
                <ul>
                    <?php
                    $types = \app\models\WbTypes::find()->All();
                    foreach ($types as $type) {
                        echo '<li>'.\yii\helpers\Html::a($type->getTitleLang($lang),
                                \yii\helpers\Url::toRoute(['/wbresults/republic','type'=>$type->id]),
                                ['title' => $type->getTitleLang($lang)]
                            ).'</li>';
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>
</div>
