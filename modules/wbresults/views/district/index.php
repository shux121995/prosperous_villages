<?php
use app\modules\results\models\GenerateTable;
/* @var $this yii\web\View */
/** @var $type \app\models\WbTypes */
/** @var $parent \app\models\District */
/*** @var $village_selection_process \app\models\Village_selection_process */

$lang = (new \app\components\LangHelper())->check_lang();

//$this->title = Yii::t('app','Results on district');
?>
<!--<h2><?=\yii\helpers\Html::a(Yii::t('app','Export XLS'),['excel-out','type'=>$type->id,'district'=>$parent->id],['class'=>'btn btn-danger float-right'])?>
    <?= $this->title?>
    <?= \yii\helpers\Html::a($parent->getTitleLang($lang),
        '/'.$lang.\yii\helpers\Url::toRoute(['/wbresults/region','type'=>1,'region'=>$parent->region_id,]),
        ['class'=>'btn btn-default'])?></h2>-->

<table class="table table-borderless table-hover">
    <thead>
        <tr>
        </tr>

    </thead>
    <tbody>
        <?php
        /** @var $local \app\models\Region*/
        $i = 1;
        foreach ($locals as $local){ ?>
            <tr>
                <!--<td>
                    <a href="#" data-toggle="modal" data-target=".bd-example-modal-lg"><?= $local->getTitleLang($lang)?></a>
                </td>-->
                <td><?= \yii\helpers\Html::a($i.") ".$local->getTitleLang($lang),'/'.$lang.\yii\helpers\Url::toRoute(['/wbresults/locality','type'=>1,'locality'=>$local->id,]))?></td>

            </tr>
        <?php
            $i++;
        }

        ?>
    </tbody>
</table>


<div style="width: content-box;">
    <div class="card">
        <div class="card-header" id="headingContent">
            <h5 class="mb-0">
                <button class="btn btn-default btn-lg" type="button" data-toggle="collapse" data-target="#collapseContent" aria-expanded="true" aria-controls="collapseContent">
                    + Villages Selection Process
                </button>
            </h5>
        </div>

        <div id="collapseContent" class="collapse hide" aria-labelledby="headingContent" data-parent="#accordion">
            <div class="card-body">

                <dl class="row">
                    <dt class="col-sm-6">Name of Facilitating Partner:</dt>
                    <dd class="col-sm-12">
                        "<?php
                        foreach ($village_selection_process as $vil_sel_pro){
                            echo $vil_sel_pro->fp_name;
                        }
                        ?>"
                    </dd>

                    <dt class="col-sm-6">Date of District Project Committee establishment:</dt>
                    <dd class="col-sm-12">
                        "<?php
                        foreach ($village_selection_process as $vil_sel_pro){
                            echo $vil_sel_pro->date_of_dpc;
                        }
                        ?>"
                    </dd>

                    <!--PDF-->
                    <dt class="col-sm-6">District Project Committee members list</dt>
                    <dd class="col-sm-12">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Click to open pdf form!
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseOne" class="collapse hide" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    <embed src="<?=$vil_sel_pro->dpc_members_list?>" width="100%" height="700"
                                           type="application/pdf">
                                </div>
                            </div>
                        </div>
                    </dd>

                    <dt class="col-sm-6">Date of village ranking and selection meeting:</dt>
                    <dd class="col-sm-12">
                        "<?php
                        foreach ($village_selection_process as $vil_sel_pro){
                            echo $vil_sel_pro->date_of_vrasm;
                        }
                        ?>"
                    </dd>

                    <!--PDF-->
                    <dt class="col-sm-6">Meeting protocol:</dt>
                    <dd class="col-sm-12">
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                        Click to open pdf form!
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse hide" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="card-body">
                                    <embed src="<?=$vil_sel_pro->meeting_protocol?>" width="100%" height="700"
                                           type="application/pdf">
                                </div>
                            </div>
                        </div>
                    </dd>

                    <!--PDF-->
                    <dt class="col-sm-6">Final list of villages by years:</dt>
                    <dd class="col-sm-12">
                        <div class="card">
                            <div class="card-header" id="headingThree">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                        Click to open pdf form!
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseThree" class="collapse hide" aria-labelledby="headingThree" data-parent="#accordion">
                                <div class="card-body">
                                    <embed src="<?=$vil_sel_pro->final_list_of_villages_by_years?>" width="100%" height="700"
                                           type="application/pdf">
                                </div>
                            </div>
                        </div>
                    </dd>

                    <dt class="col-sm-6">Meetings photos:</dt>
                    <dd class="col-sm-12">
                        <div align="center">
                            <a href="/<?=$vil_sel_pro->photo_1?>" target="_blank">
                                <img style="max-width: 100%; margin: 5px;" height="250px" src="/<?=$vil_sel_pro->photo_1?>" alt="...">
                            </a>
                            <a href="/<?=$vil_sel_pro->photo_2?>" target="_blank">
                                <img style="max-width: 100%; margin: 5px;" height="250px" src="/<?=$vil_sel_pro->photo_2?>" alt="...">
                            </a>
                            <a href="/<?=$vil_sel_pro->photo_3?>" target="_blank">
                                <img style="max-width: 100%; margin: 5px;" height="250px" src="/<?=$vil_sel_pro->photo_3?>" alt="...">
                            </a>
                            <a href="/<?=$vil_sel_pro->photo_4?>" target="_blank">
                                <img style="max-width: 100%; margin: 5px;" height="250px" src="/<?=$vil_sel_pro->photo_4?>" alt="...">
                            </a>
                            <a href="/<?=$vil_sel_pro->photo_5?>" target="_blank">
                                <img style="max-width: 100%; margin: 5px;" height="250px" src="/<?=$vil_sel_pro->photo_5?>" alt="...">
                            </a>
                        </div>
                    </dd>
                </dl>

            </div>
        </div>
    </div>

</div>

<?php
$js = <<<JS
$(function()
{
    function tally (selector) 
    {
      $(selector).each(function () 
      {
          var total = 0,
          column = $(this).siblings(selector).addBack().index(this);
          $(this).parents().prevUntil(':has(' + selector + ')').each(function () 
          {
            total += parseFloat($('td.sum:eq(' + column + ')', this).html()) || 0;
          })
          $(this).html(total);
     });
    }
  //tally('td.subtotal');
  tally('td.total');
});

JS;
$this->registerJs($js, \yii\web\View::POS_END);
