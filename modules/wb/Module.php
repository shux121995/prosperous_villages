<?php

namespace app\modules\wb;

/**
 * wb module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\wb\controllers';

    public $layout = '//wb';
    /**
     * {@inheritdoc}
     */
    public function init()
    {
        if(!isset($_SESSION['year'])) $_SESSION['year'] = date('Y');
        parent::init();

        // custom initialization code goes here
    }
}
