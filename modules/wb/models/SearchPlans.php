<?php

namespace app\modules\wb\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\WbPlans;

/**
 * SearchPlans represents the model behind the search form of `app\models\WbPlans`.
 */
class SearchPlans extends WbPlans
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'locality_id', 'district_id', 'region_id', 'dev_id'], 'integer'],
            [['year'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = WbPlans::find()->select('locality_id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        if(!empty($this->locality_id)){
            $query->andFilterWhere([ 'locality_id' => $this->locality_id ]);
        }elseif ($this->district_id){
            $query->andFilterWhere([ 'district_id' => $this->district_id, ]);
        }elseif ($this->region_id){
            $query->andFilterWhere([ 'region_id' => $this->region_id, ]);
        }

        //$query->andFilterWhere(['like', 'year', $this->year]);

        return $dataProvider;
    }
}
