<?php
use yii\web\View;
use yii\helpers\Url;
use app\models\Tables;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = Yii::t('app','Bosh sahifa');
$lang = (new \app\components\LangHelper())->check_lang();

?>
<style>
    main.container-fluid{/*margin: 0px!important;padding: 0px!important;*/}
    .map-filter {
        position: absolute;
        top: 20px;
        z-index: 22;
        width: 97%;
        padding: 0 2%;
        margin: auto;
    }
    .myspinner{
        margin-bottom: 10px;
        display: none;
        justify-content: center;
        align-items: center;
    }
</style>

    <script src="//code-ya.jivosite.com/widget/e4B9WgEdhA" async></script>

<!--MAP VIEW-->
<div class="row">
    <div class="col-lg-3" style="padding-right: 0px!important;">
        <div class="myspinner" id="my_spinner">
            <div class="spinner-grow" id="my_spinner"></div>
        </div>
        <div>
            <div id="project_list" style="height: 100vh; width:100%; overflow-y:auto;">
            <?php echo $this->render('test', [
                'model'=> $model,
                'reg_id'=>$model->region_id,
                'dis_id'=>$model->district_id,
                'vil_id'=>$model->locality_id,
            ]);?>
            </div>
        </div>
    </div>
    <div class="col-lg-9">
        <div class="map-filter">
            <?php $form = ActiveForm::begin(['method'=>'get']); ?>
            <div class="row">
                <div class="col-md-3">
                    <?= $form->field($model, 'region_id')->dropDownList($regions,
                        [
                            'class' => 'form-control',
                            'id'=>'selected_region',
                            'prompt' => Yii::t('app', 'Select region'),
                            'onchange' => '
                            var x = document.getElementById("my_spinner");
                            x.style.display = "flex";
                            $.get( "' . \yii\helpers\Url::toRoute('districts') . '", { id: $(this).val() } )
                                .done(function( data ) {
                                    $( "#selected_district" ).html( data );
                                    }
                            );
                            $.get( "' . \yii\helpers\Url::toRoute('clear') . '", { id: $(this).val() } )
                                .done(function( data ) {
                                    $( "#selected_village" ).html( data );
                                    }
                            );
                            $.get( "' . \yii\helpers\Url::toRoute('projects') . '", { reg_id: $(this).val(), dis_id: 0, vil_id: 0 } )
                                .done(function( data ) {
                                    $( "#total_projects" ).html( data );        
                                }
                            );
                            $.get( "' . \yii\helpers\Url::toRoute('populations') . '", { reg_id: $(this).val(), dis_id: 0, vil_id: 0 } )
                                .done(function( data ) {
                                    $( "#total_beneficiaries" ).html( data );                    
                                }
                            );
                            $.get( "' . \yii\helpers\Url::toRoute('investments') . '", { reg_id: $(this).val(), dis_id: 0, vil_id: 0 } )
                                .done(function( data ) {
                                    $( "#total_investment" ).html( data );                   
                                }
                            );
                            $.get( "' . \yii\helpers\Url::toRoute('specific') . '", { reg_id: $(this).val(), dis_id: 0, vil_id: 0 } )
                                .done(function( data ) {
                                    $( "#project_list" ).html( data );
                                    $("#my_spinner").hide();                        
                                }
                            );
                                                 
                            '
                        ])->label(false) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'district_id')->dropDownList($districts,
                        [
                            'prompt' => Yii::t('app', 'Select district'),
                            'id'=>'selected_district',
                            'onchange' => '
                            var x = document.getElementById("my_spinner");
                            x.style.display = "flex";
                            $.get( "' . \yii\helpers\Url::toRoute('localities') . '", { id: $(this).val() } )
                                .done(function( data ) {
                                    $( "#selected_village" ).html( data );                        
                                }
                            );
                            $.get( "' . \yii\helpers\Url::toRoute('projects') . '", { reg_id: $(selected_region).val(), dis_id: $(this).val(), vil_id: 0 } )
                                .done(function( data ) {
                                    $( "#total_projects" ).html( data );                        
                                }
                            );
                            $.get( "' . \yii\helpers\Url::toRoute('populations') . '", { reg_id: $(selected_region).val(), dis_id: $(this).val(), vil_id: 0 } )
                                .done(function( data ) {
                                    $( "#total_beneficiaries" ).html( data );                        
                                }
                            );
                            $.get( "' . \yii\helpers\Url::toRoute('investments') . '", { reg_id: $(selected_region).val(), dis_id: $(this).val(), vil_id: 0 } )
                                .done(function( data ) {
                                    $( "#total_investment" ).html( data );                        
                                }
                            );                            
                            $.get( "' . \yii\helpers\Url::toRoute('specific') . '", { reg_id: $(selected_region).val(), dis_id: $(this).val(), vil_id: 0 } )
                                .done(function( data ) {
                                    $( "#project_list" ).html( data );
                                    $("#my_spinner").hide();                        
                                }
                            );'
                        ])->label(false)?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'locality_id')->dropDownList($localities,
                        ['prompt' => Yii::t('app', 'Select village'),
                            'id'=>'selected_village',
                            'onchange' => '
                            var x = document.getElementById("my_spinner");
                            x.style.display = "flex";
                            $.get( "' . \yii\helpers\Url::toRoute('projects') . '", { reg_id: $(selected_region).val(), dis_id: $(this).val(), vil_id: $(this).val() } )
                                .done(function( data ) {
                                    $( "#total_projects" ).html( data );                        
                                }
                            );
                            $.get( "' . \yii\helpers\Url::toRoute('populations') . '", { reg_id: $(selected_region).val(), dis_id: $(selected_district).val(), vil_id: $(this).val() } )
                                .done(function( data ) {
                                    $( "#total_beneficiaries" ).html( data );                        
                                }
                            );
                            $.get( "' . \yii\helpers\Url::toRoute('investments') . '", { reg_id: $(selected_region).val(), dis_id: $(selected_district).val(), vil_id: $(this).val() } )
                                .done(function( data ) {
                                    $( "#total_investment" ).html( data );                        
                                }
                            );
                            $.get( "' . \yii\helpers\Url::toRoute('specific') . '", { reg_id: $(selected_region).val(), dis_id: $(this).val(), vil_id: $(this).val() } )
                                .done(function( data ) {
                                    $( "#project_list" ).html( data );
                                    $("#my_spinner").hide();                          
                                }
                            );
                            '])
                        ->label(false)?>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', 'Filter'), ['class' => 'btn btn-success w-100']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
        <div id="map" style="height: 100vh; width:100%; overflow:hidden; position: relative!important; /*left: 0; right: 0; bottom: 0; top: 0*/"></div>
    </div>
</div>

<?php
$searchUrl = \yii\helpers\Url::toRoute(['search','language'=>Yii::$app->lang->check_lang()]);
$contenter = '';
$html = 'var location = [';
$i = 0;
/**
 * @var  $key
 * @var  $local
 */
foreach($locals as $key => $local) :
    /** @var $local \app\models\Locality */
    if($local->map_latitude!=null && $local->map_longitude!=null) :

        $localTitle = str_replace(["\r","\n","\r\n"], "", $local->getTitleLang($lang).', '.$local->district->getTitleLang($lang));
        $contenter = '<div id="content"><div id="siteNotice"></div><h5 id="firstHeading" class="firstHeading">'.
            \yii\helpers\Html::a($localTitle,
                '/'.Yii::$app->lang->check_lang().\yii\helpers\Url::toRoute(['locality', 'id' => $local->id]),
                ['class' => 'map_title']
            )
            .'</h5><div id="bodyContent"><ul>';
        /*foreach ($local->wbPlans as $plan){
            $pid = $plan->id;
        }*/
        /*$projects = \app\models\WbProjects::find()->where(['plan_id'=>$pid])->all();

        foreach ($projects as $project){
            $contenter .= '<li class="menu__item">' . \yii\helpers\Html::a(str_replace("\r\n", "", $project->getTitleLang($lang)),
                    '/'.Yii::$app->lang->check_lang().\yii\helpers\Url::toRoute(['view', 'id' => $project->id, 'local' => $local->id]),
                    ['class' => 'menu__item-link']
                ) . '</li>';
        }*/

        $contenter .= '</ul></div></div>';

        $contenter = str_replace("'","",$contenter);

        $html .='["'.$i.'",'.$local->map_latitude.','.$local->map_longitude.',\''.$contenter.'\',\''.str_replace(["\r","\n","\r\n"], "", $local->getTitleLang($lang).', '.$local->district->getTitleLang($lang)).'\'],';
        $i++;
    endif;

endforeach;
$html .= '];';
$lat = $maps['lat'];
$lng = $maps['lng'];
$zoom = $maps['zoom'];

$script = <<< JS
    function initMap() {
        numberMarkerImg = {
            url: '/wb/map_big.png',/*
            size: new google.maps.Size(32, 38),
            scaledSize: new google.maps.Size(32, 38),
            labelOrigin: new google.maps.Point(9, 9)*/
        };
        var coordinates = {lat: $lat, lng: $lng},
        map = new google.maps.Map(document.getElementById('map'), {
            center: coordinates,
            zoom: $zoom,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true,
            scrollwheel: false, 
            mapTypeControl: true,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: google.maps.ControlPosition.BOTTOM_LEFT
            },
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_CENTER
            },
            scaleControl: true,
            streetViewControl: true,
            streetViewControlOptions: {
                position: google.maps.ControlPosition.RIGHT_CENTER
            },
        });         
        $html 
        
        var infowindow = new google.maps.InfoWindow();
    
        for (var i = 0; i < location.length; i++) {
    
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(location[i][1], location[i][2]),
                map: map,
                icon:numberMarkerImg,
                title: location[i][4]
            });
    
            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    infowindow.setContent(location[i][3]);
                    infowindow.open(map, marker);
                }
            })(marker, i)); 
        }
    }
    
    function init(){
        var myMap = new ymaps.Map('map', {
            center: [42.00, 64.00],
            zoom: 6,
            type: 'yandex#map',
            controls: ['fullscreenControl']
        }); 
        myMap.behaviors.disable('scrollZoom'); 
    }
    $(document).ready(function () {
        //$('header').addClass('fixed-top');
        initMap();
        //ymaps.ready(init);
    });
JS;

// DON'T FORGET TO CHANGE IT BACK WHEN YOU UPLOAD IT TO THE SERVER
$map_api = strrpos(Yii::$app->request->hostName,'obodqishloq.uz')!==false?Yii::$app->params['map_api_real']:Yii::$app->params['map_api'];
$this->registerJs($script, View::POS_READY);
//$this->registerJsFile('https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey='.Yii::$app->params['ymap_api_key'], ['depends'=>'yii\web\JqueryAsset'], \yii\web\View::POS_READY);
$this->registerJsFile('https://maps.googleapis.com/maps/api/js?key='.$map_api.'&language='.$lang, ['depends'=>'yii\web\JqueryAsset'], View::POS_READY);