<?php

use yii\bootstrap\Progress;
use yii\web\View;
use app\models\TablesExcel;
use app\modules\results\models\GenerateTable;
use app\models\Tables;

/** @var $table \app\models\Tables*/

$lang = \app\components\LangHelper::lang();

$this->title = $locality->getTitleLang($lang);
/** @var $plan \app\models\WbPlans */
$plan = \app\models\WbPlans::find()->where(['year'=>2019,'locality_id'=>$locality->id])->one();
?>


<style type="text/css">


</style>
    <div class="container_fluid" style="margin: 0 15px;">
        <div class="v_wella"></div>
        <div class="v_title"><?=$locality->district->region->getTitleLang($lang)?>,
            <?=$locality->district->getTitleLang($lang)?>, <?=$locality->getTitleLang($lang)?>
        </div>
    </div>
    <div class="container_fluid" style="margin: 0 15px;">
        <div class="row">
            <div class="col-md-7">
                <br>
                <div class="v_title"><?=Yii::t('app','Qilingan ishlar bo\'yicha natijalar')?></div>

                <div class="progress" style="height: 20px;">
                    <div class="progress-bar progress-bar-striped bg-success progress-bar-animated" role="progressbar" style="width: 100%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">Environmental screening 100%</div>
                </div>
                <br>
                <div class="progress" style="height: 20px;">
                    <div class="progress-bar progress-bar-striped bg-success progress-bar-animated" role="progressbar" style="width: 100%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">Social screening 100%</div>
                </div>
                <!--<ul class="stepper" style="height: fit-content">
                        <li class="step active">
                            <div class="step-title waves-effect waves-dark">Environmental Screening</div>
                            <div class="step-new-content">

                            </div>
                        </li>
                        <li class="step">
                            <div class="step-title waves-effect waves-dark">Social Screening</div>
                            <div class="step-new-content">
                                Finish!
                            </div>
                        </li>
                        <li class="step">
                            <div class="step-title waves-effect waves-dark">Mobilization</div>
                            <div class="step-new-content">
                                Finish!
                            </div>
                        </li>
                </ul>-->


                <br>
                <a href="<?=\yii\helpers\Url::to('/wbresults/locality?type=1&locality='.$locality->id)?>"><?=Yii::t('app','Learn more')?></a>
                <br><br>
                <div><a href="<?=\yii\helpers\Url::to('/wb/feedback')?>" class="yellow_button float-right"><?=Yii::t('app','Izoh qoldirisha')?></a><span class="v_title"><?=Yii::t('app','Аҳоли тамонидан қолдирилган изоҳлар')?></span></div>
                <div style="margin-top: 20px;
">
                    <?php
                    $feeds = \app\models\Feedback::find()->all();
                    if($feeds!=null){
                        foreach ($feeds as $item){
                            if($item->anonymous == 0 && $item->status!=2 && $item->status!=3) {
                                ?>
                                <blockquote class="blockquote text-left">
                                    <p class="mb-0"><?= $item->text ?></p>
                                    <div class="blockquote-footer"><b><?= $item->name ?></b> | <cite
                                                title="<?= date('d.m.Y й', $item->created_at) ?>"><?= date('d.m.Y й', $item->created_at) ?></cite>
                                    </div>

                                </blockquote>
                                <?php
                            }
                        }
                    }else{
                        ?>
                        <blockquote class="blockquote text-left">
                            <p class="mb-0"><?=Yii::t('app','Изоҳ қолдиринг ва биринчи бўлинг!')?></p>
                        </blockquote>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <div class="col-md-5" style="margin-top: 15px; position: relative; padding: 0!important;">
                <div class="sticky" style="height: 768px; right: auto;">
                    <div id="map" style="overflow:hidden; position: absolute!important; left: 0; right: 0; bottom: 0; top: 0; max-height: 512px;"></div>
                </div>
            </div>
        </div>

    </div>

<?php
$data = '';

$html = '';
$zoom = 6;
$lat = 39.00;
$lng = 64.00;
//,animation:google.maps.Animation.BOUNCE
if($locality->map_latitude!=null && $locality->map_longitude!=null) :
    $zoom = 12;
    $lat = $locality->map_latitude;
    $lng = $locality->map_longitude;
    $contenter = '';

    /*foreach (Tables::find()->all() as $tabled) {
        $contenter .= '<li class="menu__item">'.\yii\helpers\Html::a(str_replace(["\r","\n","\r\n"],"",$tabled->getTitleLang($lang)),
                '/'.Yii::$app->lang->check_lang().\yii\helpers\Url::toRoute(['view','id'=>$tabled->id,'local'=>$locality->id]),
                ['class'=>'menu__item-link']
            ).'</li>';
    }*/
    $html .= '
    var contentString = \'<div id="content">\'+
            \'<div id="siteNotice"></div>\'+
            \'<h5 id="firstHeading" class="firstHeading">'.str_replace(["\r","\n","\r\n"], "", $locality->getTitleLang($lang).', '.$locality->district->getTitleLang($lang)).'</h5>\'+
            \'<div id="bodyContent"><ul>\'+\''.$contenter.'\'+\'</ul></div>\'+
            \'</div>\';
            
    var infowindow = new google.maps.InfoWindow({
          content: contentString
        });
    
    var marker = new google.maps.Marker(
     {position:{lat:'.$locality->map_latitude.',lng:'.$locality->map_longitude.'},map:map,icon:numberMarkerImg}
     );         
     marker; 
     
     marker.addListener(\'click\', function() {
          infowindow.open(map, marker);
        });
     ';
endif;
$titleMab = ''; //Yii::t('app','Йўналтириладиган маблағлар');
$summa = Yii::t('app','Сумма');

$script = <<< JS
    numberMarkerImg = {
        url: '/wb/map_big.png'/*,
        size: new google.maps.Size(32, 38),
        scaledSize: new google.maps.Size(32, 38),
        labelOrigin: new google.maps.Point(9, 9)*/
    };
    
    function initMap() {
        var coordinates = {lat: $lat, lng: $lng},
        map = new google.maps.Map(document.getElementById('map'), {
            center: coordinates,
            zoom: $zoom,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true,
            scrollwheel: false,
            mapTypeControl: true,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: google.maps.ControlPosition.BOTTOM_LEFT
            },
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_CENTER
            },
            scaleControl: true,
            streetViewControl: true,
            streetViewControlOptions: {
                position: google.maps.ControlPosition.RIGHT_CENTER
            },
        });
        $html 
    }
    
    $(document).ready(function () {
        $('.stepper').mdbStepper();
        
        $(".sticky").stick_in_parent({recalc_every: 1});
        initMap();
        var panelH = $('.searching_resluts').innerHeight();

        $('.searching_wella').blur(function(){
            /*$(".searching_resluts").stop(1).animate({height: 0},500, function(){
                $(this).hide();
            });*/
            $('.searching_resluts').slideUp();
        });

        $('.searching_wella').focus(function() {
            //$(".searching_resluts").stop(1).show().height(0).animate({height: panelH},500);
            $('.searching_resluts').slideDown();
        });

    });    
JS;
$this->registerJs($script, View::POS_READY);
$map_api = strrpos(Yii::$app->request->hostName,'obodqishloq.geekart.club')!==false?Yii::$app->params['map_api_real']:Yii::$app->params['map_api'];
$this->registerJsFile('https://maps.googleapis.com/maps/api/js?key='.$map_api, ['depends'=>'yii\web\JqueryAsset'], View::POS_READY);