<?php
use yii\web\View;
use app\models\TablesExcel;
use app\modules\results\models\GenerateTable;
use app\models\Tables;

/** @var $table \app\models\Tables*/

$lang = \app\components\LangHelper::lang();

$this->title = $locality->getTitleLang($lang).', '.$table->getTitleLang($lang)
?>
    <div class="container_fluid" style="margin: 0 15px;">
        <div class="row">
            <div class="col-md-7">
                <div class="v_wella"><?=$locality->district->region->getTitleLang($lang)?>, <?=$locality->district->getTitleLang($lang)?></div>
                <div class="v_title"><?=$locality->getTitleLang($lang)?>, <?=$table->getTitleLang($lang)?></div>

                <div class="v_title"><?=Yii::t('app','Qilingan ishlar bo\'yicha natijalar')?></div>
                <div id="container"></div>

                <?php
                $excels = GenerateTable::getExcelInputs($table->id, null, TablesExcel::TYPE_ONLY_SECTION);
                /** @var $excel TablesExcel */
                foreach ($excels as $excel) {
                    $input = \app\models\Sections::findOne($excel->section_id);
                    $title = $input->parent!=null?$input->parent->parent!=null?$input->parent->parent->getTitleLang($lang).' '.$input->parent->getTitleLang($lang).' '.$input->getTitleLang($lang):$input->parent->getTitleLang($lang).' '.$input->getTitleLang($lang):$input->getTitleLang($lang);

                    $fact = round((int)Tables::factLocality($excel->section_id, $locality->id));///8700
                    $plan = round((int)Tables::planLocality($excel->section_id, $locality->id));///8700
                    $percent = round(($plan==0)?0:($fact*100)/$plan);
                    ?>
                    <div class="v_item">
                        <div class="v_item_heading"><?=$title?></div>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="v_item_title"><?=Yii::t('app','Fact')?></div>
                                <div class="v_item_stat"><?=$fact?></div>
                            </div>
                            <div class="col-md-2 spliter">
                                <div class="v_item_title"><?=Yii::t('app','Режа')?></div>
                                <div class="v_item_stat"><?=$plan?></div>
                            </div>
                            <div class="col-md-8 spliter">
                                <div class="v_item_title"><?=Yii::t('app','Молялаштириш')?></div>
                                <div class="v_item_stat"><?=Yii::t('app','Маҳаллий бюджет маблағлари')?></div>
                            </div>
                        </div>
                        <div class="progress"><div class="progress-bar progress-bar-striped" role="progressbar"
                                                   style="width: <?=$percent?>%" aria-valuenow="<?=$percent?>" aria-valuemin="0" aria-valuemax="100"><?=$percent?>%</div>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <div><a href="<?=\yii\helpers\Url::to('/wb/feedback')?>" class="yellow_button float-right"><?=Yii::t('app','Izoh qoldirish')?></a><span class="v_title"><?=Yii::t('app','Аҳоли тамонидан қолдирилган изоҳлар')?></span></div>
                <div style="margin-top: 20px;
">
                    <?php
                    $feeds = \app\models\Feedback::find()->all();
                    if($feeds!=null){
                        foreach ($feeds as $item){ ?>
                            <blockquote class="blockquote text-left">
                                <p class="mb-0"><?=$item->text?></p>
                                <div class="blockquote-footer"><b><?=$item->name?></b>       |       <cite title="<?=date('d.m.Y й',$item->created_at)?>"><?=date('d.m.Y й',$item->created_at)?></cite></div>
                            </blockquote>
                        <?php
                        }
                    }else{
                        ?>
                        <blockquote class="blockquote text-left">
                            <p class="mb-0"><?=Yii::t('app','Изоҳ қолдиринг ва биринчи бўлинг!')?></p>
                        </blockquote>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <div class="col-md-5" style="position: relative; padding: 0!important;">
                <div class="sticky" style="height: 768px; right: auto;">
                    <div id="map" style="overflow:hidden; position: absolute!important; left: 0; right: 0; bottom: 0; top: 0; max-height: 768px;"></div>
                </div>
            </div>
        </div>

    </div>
<?php
$data = '';
foreach ($excels as $excel) {
    $input = \app\models\Sections::findOne($excel->section_id);
    $title = $input->parent!=null?$input->parent->getTitleLang($lang).' '.$input->getTitleLang($lang):$input->getTitleLang($lang);
    $fact = round((int)Tables::planLocality($excel->section_id, $locality->id),2);
    $data .= "['$title', $fact],\r\n";
}

$html = '';
$zoom = 6;
$lat = 39.00;
$lng = 64.00;
//,animation:google.maps.Animation.BOUNCE
if($locality->map_latitude!=null && $locality->map_longitude!=null) :
    $zoom = 12;
    $lat = $locality->map_latitude;
    $lng = $locality->map_longitude;
    $contenter = '';

    foreach (Tables::find()->all() as $tabled) {
        $contenter .= '<li class="menu__item">'.\yii\helpers\Html::a(str_replace(["\r","\n","\r\n"],"",$tabled->getTitleLang($lang)),
                '/'.Yii::$app->lang->check_lang().\yii\helpers\Url::toRoute(['view','id'=>$tabled->id,'local'=>$locality->id]),
                ['class'=>'menu__item-link']
            ).'</li>';
    }
    $html .= '
    var contentString = \'<div id="content">\'+
            \'<div id="siteNotice"></div>\'+
            \'<h5 id="firstHeading" class="firstHeading">'.str_replace(["\r","\n","\r\n"], "", $locality->getTitleLang($lang).', '.$locality->district->getTitleLang($lang)).'</h5>\'+
            \'<div id="bodyContent"><ul>\'+\''.$contenter.'\'+\'</ul></div>\'+
            \'</div>\';
            
    var infowindow = new google.maps.InfoWindow({
          content: contentString
        });
    
    var marker = new google.maps.Marker(
     {position:{lat:'.$locality->map_latitude.',lng:'.$locality->map_longitude.'},map:map,icon:numberMarkerImg}
     );         
     marker; 
     
     marker.addListener(\'click\', function() {
          infowindow.open(map, marker);
        });
     ';
endif;
$titleMab = ''; //Yii::t('app','Йўналтириладиган маблағлар');
$summa = Yii::t('app','Сумма');

$script = <<< JS
    Highcharts.chart('container', {
        credits:{
            enabled:false
        },
        chart: {
            backgroundColor: 'none',
            type: 'pie',
            options3d: {
                enabled: false,
                alpha: 45
            },
        },
        title: {
            text: ''
        },
        plotOptions: {
            pie: {
                innerSize: 100,
                depth: 70
            },
            series: {
                dataLabels: {
                    enabled: true,
                    formatter: function() {
                        return Math.round(this.percentage*100)/100 + ' %';
                    },
                    distance: -40,
                    color:'#fff',
                    style: {
                        textOutline: false
                    }
                }
            }
        },
        legend: {
            title: {
                text: '$titleMab',
                style: {
                    fontStyle: 'normal'
                }
            },
            layout: 'vertical',
            floating: false,
            align: 'right',
            verticalAlign: 'middle',
            itemMarginBottom: 5,
            width: 350,
            x: 0
        },
        series: [{
            showInLegend: true,
            name: '$summa',
            data: [
                $data
            ]
        }]
    });

    numberMarkerImg = {
        url: '/wb/map_big.png'/*,
        size: new google.maps.Size(32, 38),
        scaledSize: new google.maps.Size(32, 38),
        labelOrigin: new google.maps.Point(9, 9)*/
    };
    
    function initMap() {
        var coordinates = {lat: $lat, lng: $lng},
        map = new google.maps.Map(document.getElementById('map'), {
            center: coordinates,
            zoom: $zoom,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true,
            scrollwheel: false,
            mapTypeControl: true,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: google.maps.ControlPosition.BOTTOM_LEFT
            },
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_CENTER
            },
            scaleControl: true,
            streetViewControl: true,
            streetViewControlOptions: {
                position: google.maps.ControlPosition.RIGHT_CENTER
            },
        });
        $html 
    }
    
    $(document).ready(function () {
        $(".sticky").stick_in_parent({recalc_every: 1});
        initMap();
        var panelH = $('.searching_resluts').innerHeight();

        $('.searching_wella').blur(function(){
            /*$(".searching_resluts").stop(1).animate({height: 0},500, function(){
                $(this).hide();
            });*/
            $('.searching_resluts').slideUp();
        });

        $('.searching_wella').focus(function() {
            //$(".searching_resluts").stop(1).show().height(0).animate({height: panelH},500);
            $('.searching_resluts').slideDown();
        });
    });
JS;
$this->registerJs($script, View::POS_READY);
$map_api = strrpos(Yii::$app->request->hostName,'obodqishloq.geekart.club')!==false?Yii::$app->params['map_api_real']:Yii::$app->params['map_api'];
$this->registerJsFile('https://maps.googleapis.com/maps/api/js?key='.$map_api, ['depends'=>'yii\web\JqueryAsset'], View::POS_READY);