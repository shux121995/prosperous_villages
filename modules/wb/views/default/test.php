<?php
$lang = \app\components\LangHelper::lang();
$investment = (new app\models\WbInvestment);
$tables = null;
$population = (new \app\models\forms\mobilization\Aholi)->getPopulation($reg_id, $dis_id, $vil_id);
$total_projects = (new \app\models\WbInvestment())->getTotalProjects($reg_id, $dis_id, $vil_id);
$total_investment = (new \app\models\WbInvestment())->getTotalInvestments($reg_id, $dis_id, $vil_id);
?>
    <style>
        .heading{
            background-color: rgba(244, 245, 250, 0.85);
            display: block;
            line-height: 50px;
        }
        .collapse-button{
            vertical-align: middle;
            background-color: transparent;
            background-repeat:no-repeat;
            border: none;
            cursor:pointer;
            overflow: hidden;
            outline:none;
        }
        .collapse-image{
            width: 50px;
            height=50px;
        }
    </style>

    <div class="card">
        <div class="heading">
            <img src="../wb/images/main_page_icons/projects_icon.png" class="float-left collapse-image" alt="project icon">
            <button class="collapse-button">
                <?= Yii::t('app','Total projects')?>:<span class="nowrap" id="total_projects"><?=$total_projects?></span>
            </button>
        </div>
    </div>
    <div class="card">
        <div class="heading">
            <img src="../wb/images/main_page_icons/beneficiaries_icon.png" class="float-left collapse-image" alt="project icon">
            <button class="collapse-button">
                <?= Yii::t('app','Beneficiaries')?>: <span class="nowrap" id="total_beneficiaries"><?=$population?></span>
            </button>
        </div>
    </div>
    <div class="card">
        <div class="heading">
            <img src="../wb/images/main_page_icons/investment_icon.png" class="float-left collapse-image" alt="project icon">
            <button class="collapse-button">
                <?= Yii::t('app','Total investment')?>: $<span class="nowrap" id="total_investment"><?=$total_investment?></span>
            </button>
        </div>
    </div>
<?php foreach (\app\models\WbTypes::find()->all() as $project_type):?>
    <div id="accordion<?=$project_type->id?>">
        <div class="card">
            <div id="heading<?=$project_type->id?>" data-toggle="collapse" data-target="#collapse<?=$project_type->id?>" aria-expanded="true" aria-controls="collapse<?=$project_type->id?>" class="heading" onclick="myFunction()">
                <img src="../wb/images/main_page_icons/<?= \app\models\WbTypes::find()->where(['id'=>$project_type->id])->one()->icon?>" class="float-left collapse-image" alt="project icon">
                <button  onclick="this.blur();" class="collapse-button">
                    <?=$project_type->getTitleLang($lang)?>:
                    <span class="nowrap" id="<?=$project_type->id?>">
                        <?php

                        if (!empty($vil_id)){
                            $tables = $investment->getSpecificProjectInfo(0,0, $vil_id, $project_type->id);
                            echo count($investment->getSpecificProjectInfo(
                                0,
                                0,
                                $vil_id,
                                $project_type->id));
                        } else if (!empty($dis_id)){
                            $tables = $investment->getSpecificProjectInfo(0,$dis_id, 0, $project_type->id);
                            echo count ($investment->getSpecificProjectInfo(
                                0,
                                $dis_id,
                                0,
                                $project_type->id));
                        } else if(!empty($reg_id)){
                            $tables = $investment->getSpecificProjectInfo($reg_id,0, 0, $project_type->id);
                            echo count ($investment->getSpecificProjectInfo(
                                $reg_id,
                                0,
                                0,
                                $project_type->id));
                        } else{
                            $tables = $investment->getSpecificProjectInfo($model->region_id,$model->district_id, $model->locality_id, $project_type->id);
                            echo count ($investment->getSpecificProjectInfo(
                                $model->region_id,
                                $model->district_id,
                                $model->locality_id,
                                $project_type->id));
                        }
                        ?>
                    </span>
                </button>
                <img src="../wb/images/main_page_icons/arrow_down.png" alt="arrow down icon" class="float-right collapse-image">
            </div>
            <div id="collapse<?=$project_type->id?>" class="collapse" aria-labelledby="heading<?=$project_type->id?>" data-parent="#accordion<?=$project_type->id?>">
                <div class="card-body">
                    <table class="table table-bordered" style="text-align: center">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col"><?= Yii::t('app','Project ID')?></th>
                            <th scope="col"><?= Yii::t('app','Year')?></th>
                            <th scope="col"><?= Yii::t('app','Investment')?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($tables as $table):?>
                        <tr>
                            <th scope="row"><?php echo $table->id?></th>
                            <td><?php echo $table->project_year?></td>
                            <td>$<?php echo $table->project_investment?></td>
                        </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>
