<?php
use yii\web\View;
use app\models\TablesExcel;
use app\modules\results\models\GenerateTable;
use app\models\Tables;
use yii\helpers\Html;

use yii\helpers\Url;

use yii\widgets\ActiveForm;


/** @var $table \app\models\Tables*/

$lang = \app\components\LangHelper::lang();

$this->title = $locality->getTitleLang($lang);
/** @var $project \app\models\WbProjects */
/** @var $plan \app\models\WbPlans */
$plan = \app\models\WbPlans::find()->where(['id'=>$project->plan_id])->one();
?>
    <div class="container_fluid" style="margin: 0 15px;">
        <div class="v_wella"></div>
        <div class="v_title"><div style="float: right;"><?=$plan!=null?Yii::t('app','Bosh pudratchi: ').
                    $plan->developer->getTitleLang($lang):''?></div>
            <a href="<?='/'.$lang.\yii\helpers\Url::to(['locality','id'=>$locality->id])?>"><?=$locality->district->region->getTitleLang($lang)?>,
                <?=$locality->district->getTitleLang($lang)?>, <?=$locality->getTitleLang($lang)?></a>
        </div>
    </div>
    <div class="container_fluid" style="margin: 0 15px;">
        <div class="row">
            <div class="col-md-7">
                <br>
                <div class="v_title"><?=Yii::t('app','Qilingan ishlar bo\'yicha natijalar')?>
                    : <?=$project->getTitleLang($lang)?>
                </div>
                <?php
                $plan = $project->getCalcedPlan();
                $fact = $project->getCalcedFact();
                $percent = round(($fact/$plan)*100);
                ?>
                <div class="section__progress-list">
                    <div class="section__progress-item">
                        <div class="item__data">
                            <div class="row">
                                <div class="col-3">
                                    <div class="item__data-col">
                                        <div class="item__data-title"><?=Yii::t('app','Fact')?></div>
                                        <div class="item__data-value"><?=$fact?> <sup class="sym">$</sup></div>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="item__data-col">
                                        <div class="item__data-title"><?=Yii::t('app','Reja')?></div>
                                        <div class="item__data-value"><?= $plan;?> <sup class="sym">$</sup></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="progress item__progress">
                            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                                 style="width: <?=$percent?>%;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"
                            ><?=$percent?>%</div>
                        </div>
                    </div>
                </div>

                <div id="container"></div>
                <br>
                <div class="projects">
                    <div class="v_title"><?=Yii::t('app','Masalalar:')?>
                    </div>
                    <div class="section__progress-list">
                        <?php
                        if($project->wbTasks!=null){
                            /** @var $task \app\models\WbTasks */
                            foreach ($project->wbTasks as $task){
                                $percent = 0;
                                if($task->status==1) $percent = 100;
                                ?>
                                <div class="section__progress-item">
                                    <div class="item__title"><?= $task->subType->getTitleLang($lang);?></div>
                                    <div class="progress item__progress">
                                        <div class="progress-bar bg-success progress-bar-striped progress-bar-animated" role="progressbar"
                                             style="width: <?=$percent?>%;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"
                                        ><?=$percent?>%</div>
                                    </div>
                                </div>
                            <?php
                            }
                        }
                        ?>
                    </div>
                </div>
                <br>
                <div class="pdfviewer">
                    <!--
                    <div class="v_title"><?=Yii::t('app','Tarmoq jadvali asosida amalga oshirilayotgan tadbirlar jadval ko\'rinishda')?></div>
                    <embed src="<?=$project->table_file?>" width="100%" height="400"
                           type="application/pdf">
                    -->
                    <div id="accordion">

                        <div class="card" >
                            <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <?php echo Yii::t('app','Tarmoq jadvali asosida amalga oshirilayotgan tadbirlar jadval ko\'rinishda')?>
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseOne" class="collapse hide" aria-labelledby="headingOne" data-parent="#accordion" >
                                <div class="card-body">
                                    <embed src="<?=$project->table_file?>" width="100%" height="400"
                                           type="application/pdf">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-5" style="margin-top: 15px; position: relative; padding: 0!important;">
                <div> <!--class="sticky" style="right: auto;"-->
                    <div><h3 class="text-md-center"><?=Yii::t('app','Images for project')?></h3></div>
                    <div class="images row">
                        <?php $files = \app\models\WbFiles::find()->where(['project_id'=>$project->id])->all();
                        if($files!=null){ ?>
                        <div class="col-md-6">
                            <div><h3 class="text-md-center"><?=Yii::t('app','Before')?></h3></div>
                            <?php
                            /** @var $file \app\models\WbFiles */
                            foreach ($files as $file){ if($file->type==\app\models\WbFiles::TYPE_AFTER) continue; ?>
                                <div class="">
                                    <?php if($file->extension=='mp4' OR $file->extension=='mpg' OR $file->extension=='mpeg'){?>
                                        <embed src="<?=$project->table_file?>" width="100%" height="200"
                                               type="application/<?=$file->extension?>">
                                    <?php }else{ ?>
                                        <a href="/<?=$file->path?>" target="_blank">
                                            <img style="max-width: 100%;" src="/<?=$file->path?>">
                                        </a>
                                    <?php } ?>
                                </div>
                                <?php
                            }?>
                        </div>
                        <div class="col-md-6">
                            <div><h3 class="text-md-center"><?=Yii::t('app','After')?></h3></div>
                            <?php
                            /** @var $file \app\models\WbFiles */
                            foreach ($files as $file){ if($file->type==\app\models\WbFiles::TYPE_BEFORE) continue; ?>
                                <div class="">
                                    <?php if($file->extension=='mp4' OR $file->extension=='mpg' OR $file->extension=='mpeg'){?>
                                        <embed src="<?=$project->table_file?>" width="100%" height="200"
                                               type="application/<?=$file->extension?>">
                                    <?php }else{ ?>
                                        <a href="/<?=$file->path?>" target="_blank">
                                            <img style="max-width: 100%;" src="/<?=$file->path?>">
                                        </a>
                                    <?php } ?>
                                </div>
                                <?php
                            }?>
                        </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
$script = <<< JS
    $(document).ready(function () {
        $(".sticky").stick_in_parent({recalc_every: 1});
        var panelH = $('.searching_resluts').innerHeight();

        $('.searching_wella').blur(function(){
            /*$(".searching_resluts").stop(1).animate({height: 0},500, function(){
                $(this).hide();
            });*/
            $('.searching_resluts').slideUp();
        });

        $('.searching_wella').focus(function() {
            //$(".searching_resluts").stop(1).show().height(0).animate({height: panelH},500);
            $('.searching_resluts').slideDown();
        });
    });
JS;
$this->registerJs($script, View::POS_READY);

