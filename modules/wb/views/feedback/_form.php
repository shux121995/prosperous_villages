<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Feedback */
/* @var $form yii\widgets\ActiveForm */

$tables = [];
$regions = [];
$districts = [];
$localities = [];

foreach (\app\models\WbTypes::find()->all() as $table){
    $tables[$table->id] = $table->getTitleLang((new \app\components\LangHelper())->check_lang());
}

foreach (\app\models\Region::find()->all() as $region) {
    if ($region->id == 2 || $region->id == 7 || $region->id == 12 || $region->id == 10 || $region->id == 4)/*I MAKE AN IF STATEMENT FOR WORLD BANK PAGE ONLY*/ {
        $regions[$region->id] = $region->getTitleLang((new \app\components\LangHelper())->check_lang());
    }
}
?>

<div class="feedback-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'style'=>'font-size:14px;']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'style'=>'font-size:14px;']) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true, 'style'=>'font-size:14px;']) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true, 'style'=>'font-size:14px;']) ?>

    <?= $form->field($model, 'gender')->dropDownList(['0' => Yii::t('app','Male'), '1' => Yii::t('app','Female'),],
        [
                'prompt' => Yii::t('app', 'Gender'),
                'style'=>'font-size:14px;'
        ])
        ->label(false)?>

    <?= $form->field($model, 'region')->dropDownList($regions,
        [
            'class' => 'form-control',
            'style'=>'font-size:14px',
            'prompt' => Yii::t('app', 'Select region'),
            'onchange' => '
                $.get( "' . Url::toRoute('districts') . '", { id: $(this).val() } )
                    .done(function( data ) {
                        $( "#' . Html::getInputId($model, 'district') . '" ).html( data );                        
                    }
                );'
        ])->label(false) ?>

    <?= $form->field($model, 'district')->dropDownList($districts,
        [
            'class' => 'form-control',
            'style'=>'font-size:14px;',
            'prompt' => Yii::t('app', 'Select district'),
            'onchange' => '
                $.get( "' . Url::toRoute('localities') . '", { id: $(this).val() } )
                    .done(function( data ) {
                        $( "#' . Html::getInputId($model, 'locality') . '" ).html( data );                        
                    }
                );'
        ])->label(false)?>

    <?= $form->field($model, 'locality')->dropDownList($localities,
        ['prompt' => Yii::t('app', 'Select village'), 'style'=>'font-size:14px;'])
        ->label(false)?>

    <!--<?= $form->field($model, 'locality_id')->hiddenInput()->label(false) ?>-->

    <?= $form->field($model, 'table_id')->dropDownList($tables, ['style'=>'font-size:14px;']) ?>

    <?php //= $form->field($model, 'section_id')->textInput() ?>

    <?= $form->field($model, 'type')->dropDownList(\app\models\Feedback::typeLables(),['style'=>'font-size:14px;']) ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6, 'style'=>'font-size:14px;']) ?>

    <?= $form->field($model, 'image')->fileInput(['style'=>'font-size:14px;']) ?>

    <div style="display:flex; flex-direction: row; justify-content: center; align-items: center;">

        <div style="float: left">
        <?= $form->field($model, "anonymous")->checkbox(['style'=>'font-size:14px;']);?>
        </div>
        <div class="reCaptcha" style="margin: auto">
            <?= $form->field($model, 'reCaptcha', ['template' => '{input}'])->widget(
                \himiklab\yii2\recaptcha\ReCaptcha::className()
            //['widgetOptions'=>['class'=>'text-right']]
            ) ?>
        </div>
        <div class="form-group" style="float: right">
            <?= Html::submitButton(Yii::t('app', 'Izoh qoldirish'), ['class' => 'btn btn-success btn-lg', 'style'=>'font-size:20px;']) ?>
        </div>
    </div>
    <?php //= $form->field($model, 'created_at')->textInput() ?>

    <?php //= $form->field($model, 'updated_at')->textInput() ?>

    <?php //= $form->field($model, 'status')->textInput() ?>

    <?php ActiveForm::end(); ?>

</div>
