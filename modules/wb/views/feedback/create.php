<?php

/* @var $this yii\web\View */
/* @var $model app\models\Feedback */

$this->title = Yii::t('app', 'Izoh qoldirish');
$this->params['breadcrumbs'][] = $this->title;

use app\models\TotalFeedback;
use dosamigos\chartjs\ChartJs; ?>

<div class="row">
    <!--Fill out form-->
    <div class="feedback-form col-md-6">
        <div class="feedback-form" style="background: #f4f5fa; border-radius: 10px; padding: 20px">
            <div style="text-align: center">
                <span>
                    <h3><?= Yii::t('app','Feedback Form')?></h3>
                </span>
            </div>
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>

    <!--Statistical Data-->
    <div class="col-md-6">
        <div class="row-md-6">
            <?php
                    try {
                    echo ChartJs::widget([
                        'type' => 'bar',
                        'options' => [
                            'height' => 300,
                            'width' => 400
                        ],
                        'data' => [
                            'labels' => ["2020", "2021", "2022", "2023", "2024"],
                            'datasets' => [
                                [
                                    'label' => "Andijan",//id 2
                                    'backgroundColor' => "#EF9A9A",
                                    'borderColor' => "rgba(179,181,198,1)",
                                    'pointBackgroundColor' => "rgba(179,181,198,1)",
                                    'pointBorderColor' => "#fff",
                                    'pointHoverBackgroundColor' => "#fff",
                                    'pointHoverBorderColor' => "rgba(179,181,198,1)",
                                    'data' => [TotalFeedback::find()->where(['region_id'=>2, 'feedback_year'=>2020])->one()->feedback_quantity, TotalFeedback::find()->where(['region_id'=>2, 'feedback_year'=>2021])->one()->feedback_quantity, TotalFeedback::find()->where(['region_id'=>2, 'feedback_year'=>2022])->one()->feedback_quantity, TotalFeedback::find()->where(['region_id'=>2, 'feedback_year'=>2023])->one()->feedback_quantity, TotalFeedback::find()->where(['region_id'=>2, 'feedback_year'=>2024])->one()->feedback_quantity]
                                ],
                                [
                                    'label' => "Jizzakh",//id 4
                                    'backgroundColor' => "#CE93D8",
                                    'borderColor' => "rgba(255,99,132,1)",
                                    'pointBackgroundColor' => "rgba(255,99,132,1)",
                                    'pointBorderColor' => "#fff",
                                    'pointHoverBackgroundColor' => "#fff",
                                    'pointHoverBorderColor' => "rgba(255,99,132,1)",
                                    'data' => [TotalFeedback::find()->where(['region_id'=>4, 'feedback_year'=>2020])->one()->feedback_quantity, TotalFeedback::find()->where(['region_id'=>4, 'feedback_year'=>2021])->one()->feedback_quantity, TotalFeedback::find()->where(['region_id'=>4, 'feedback_year'=>2022])->one()->feedback_quantity, TotalFeedback::find()->where(['region_id'=>4, 'feedback_year'=>2023])->one()->feedback_quantity, TotalFeedback::find()->where(['region_id'=>4, 'feedback_year'=>2024])->one()->feedback_quantity]
                                ],
                                [
                                    'label' => "Namangan",//id 7
                                    'backgroundColor' => "#673AB7",
                                    'borderColor' => "rgba(179,181,198,1)",
                                    'pointBackgroundColor' => "rgba(179,181,198,1)",
                                    'pointBorderColor' => "#fff",
                                    'pointHoverBackgroundColor' => "#fff",
                                    'pointHoverBorderColor' => "rgba(179,181,198,1)",
                                    'data' => [TotalFeedback::find()->where(['region_id'=>7, 'feedback_year'=>2020])->one()->feedback_quantity, TotalFeedback::find()->where(['region_id'=>7, 'feedback_year'=>2021])->one()->feedback_quantity, TotalFeedback::find()->where(['region_id'=>7, 'feedback_year'=>2022])->one()->feedback_quantity, TotalFeedback::find()->where(['region_id'=>7, 'feedback_year'=>2023])->one()->feedback_quantity, TotalFeedback::find()->where(['region_id'=>7, 'feedback_year'=>2024])->one()->feedback_quantity]
                                ],
                                [
                                    'label' => "Syrdarya",//id 10
                                    'backgroundColor' => "#2196F3",
                                    'borderColor' => "rgba(179,181,198,1)",
                                    'pointBackgroundColor' => "rgba(179,181,198,1)",
                                    'pointBorderColor' => "#fff",
                                    'pointHoverBackgroundColor' => "#fff",
                                    'pointHoverBorderColor' => "rgba(179,181,198,1)",
                                    'data' => [TotalFeedback::find()->where(['region_id'=>10, 'feedback_year'=>2020])->one()->feedback_quantity, TotalFeedback::find()->where(['region_id'=>10, 'feedback_year'=>2021])->one()->feedback_quantity, TotalFeedback::find()->where(['region_id'=>10, 'feedback_year'=>2022])->one()->feedback_quantity, TotalFeedback::find()->where(['region_id'=>10, 'feedback_year'=>2023])->one()->feedback_quantity, TotalFeedback::find()->where(['region_id'=>10, 'feedback_year'=>2024])->one()->feedback_quantity]
                                ],
                                [
                                    'label' => "Fergana",//id 12
                                    'backgroundColor' => "#81C784",
                                    'borderColor' => "rgba(179,181,198,1)",
                                    'pointBackgroundColor' => "rgba(179,181,198,1)",
                                    'pointBorderColor' => "#fff",
                                    'pointHoverBackgroundColor' => "#fff",
                                    'pointHoverBorderColor' => "rgba(179,181,198,1)",
                                    'data' => [TotalFeedback::find()->where(['region_id'=>12, 'feedback_year'=>2020])->one()->feedback_quantity, TotalFeedback::find()->where(['region_id'=>12, 'feedback_year'=>2021])->one()->feedback_quantity, TotalFeedback::find()->where(['region_id'=>12, 'feedback_year'=>2022])->one()->feedback_quantity, TotalFeedback::find()->where(['region_id'=>12, 'feedback_year'=>2023])->one()->feedback_quantity, TotalFeedback::find()->where(['region_id'=>12, 'feedback_year'=>2024])->one()->feedback_quantity]
                                ],
                            ]
                        ]
                    ]);
                } catch (Exception $e) {}
            ?>
        </div>
    </div>
</div>




