<?php

namespace app\modules\wb\controllers;

use app\components\LangHelper;
use app\models\forms\mobilization\Aholi;
use app\models\District;
use app\models\Locality;
use app\models\Region;
use app\models\Tables;
use app\models\WbPlans;
use app\models\WbProjects;
use app\models\WbTypes;
use app\modules\wb\models\SearchFeedback;
use app\modules\wb\models\SearchPlans;
use yii\helpers\Html;
use yii\helpers\Url;
use app\controllers\HomeController;
use Yii;

/**
 * Default controller for the `wb` module
 */
class DefaultController extends HomeController
{
    /**
     * Renders the index view for the module
     * @return string
     */

    public function actionIndex($type='wb')
    {
        $model = new SearchPlans();
        $dataProvider = $model->search(Yii::$app->request->queryParams);

        //print_r($model->region_id);

        $locals = Locality::find()->with(['wbPlans','wbPlans.wbProjects']);
        $locals->where(['type'=>$type])
            ->andWhere(['in', 'id', $dataProvider]);

        $lang = (new \app\components\LangHelper())->check_lang();

        //Regions
        $regioned = \app\models\Region::find()->all();
        $regions = [];
        /** @var $region \app\models\Region*/
        foreach ($regioned as $region){
            if($region->id == 2 || $region->id == 7 ||$region->id == 12 ||$region->id == 10 ||$region->id == 4)/*I MAKE AN IF STATEMENT FOR WORLD BANK PAGE ONLY*/
            {
                $regions[$region->id] = $region->getTitleLang($lang);
            }
        }

        //Districts
        $districted = \app\models\District::find()->where(['region_id'=>$model->region_id])->all();
        $districts = [];
        /** @var $district \app\models\District*/
        foreach ($districted as $district){
            if($district->id == 24 || $district->id == 19 || $district->id == 23|| $district->id == 28|| $district->id == 25
            || $district->id == 82|| $district->id == 83|| $district->id == 74|| $district->id == 84|| $district->id == 78
                || $district->id == 148|| $district->id == 149|| $district->id == 142|| $district->id == 147
                || $district->id == 112|| $district->id == 116|| $district->id == 119
                || $district->id == 46|| $district->id == 51|| $district->id == 52|| $district->id == 42) {

                $districts[$district->id] = $district->getTitleLang($lang);
            }
        }

        //Villages
        $localited = \app\models\Locality::find()->where(['type'=>\app\models\Locality::TYPE_WB, 'district_id'=>$model->district_id])->all();
        $localities = [];
        /** @var $localitie \app\models\District*/
        foreach ($localited as $localitie){
            $localities[$localitie->id] = $localitie->getTitleLang($lang);
        }

        // map coords and zoom
        $mapped = $model;
        $mapped->load(Yii::$app->request->queryParams);

        if(!empty($mapped->locality_id)){
            $map = Locality::findOne($mapped->locality_id);
        }elseif (!empty($mapped->district_id)){
            $map = District::findOne($mapped->district_id);
        }elseif (!empty($mapped->region_id)){
            $map = Region::findOne($mapped->region_id);
        }else{
            $map = null;
        }
        $maps = [
            'lat' => 41.311151,
            'lng' => 69.279737,
            'zoom' => 13
        ];

        if($map!=null && $map->map_latitude!=null && $map->map_longitude!=null){
            $maps['lat'] = $map->map_latitude;
            $maps['lng'] = $map->map_longitude;
            $maps['zoom'] = empty($map->map_zoom)?13:$map->map_zoom;
        }

        return $this->render('index',
            [
                'maps'           => $maps,
                'model'         =>$model,
                'regions'       =>$regions,
                'districts'     =>$districts,
                'localities'    =>$localities,
                'locals'        => $locals->all(),
                'types'         => WbTypes::find()->all(),
            ]);
    }

    /**
     * @param $id table id
     * @param $local locality id
     * @return string
     */
    public function actionView($id,$local)
    {
        $project = WbProjects::findOne((int)$id);
        $locality = Locality::findOne((int)$local);
        $this->view->params['tn_url'] = '/view?id='.$id.'&local='.$local;
        return $this->render('project',[
            'project' => $project,
            'locality' => $locality,
        ]);
    }

    /**
     * @param $q
     */
    public function actionSearch($q)
    {
        if(!Yii::$app->request->isAjax) return 'Error Request!';
        $rows = Locality::find()->where(['like','title', $q])->orWhere(['like','title_en', $q])
            ->andWhere(['type'=>Locality::TYPE_WB])->all();

        if (count($rows) > 0) {
            /** @var $row Locality*/
            foreach ($rows as $row) {
                $title = '<div class="sr_title">'.$row->getTitleLang(Yii::$app->lang->check_lang()).'</div>
                            <div class="sr_label">'.$row->district->region->getTitleLang(Yii::$app->lang->check_lang()).', '.$row->district->getTitleLang(Yii::$app->lang->check_lang()).'</div>';
                $a = Html::a($title, '/'.Yii::$app->lang->check_lang().Url::toRoute(['locality', 'id'=>$row->id]));//['wella', 'id'=>$row->id]
                echo '<li>'.$a.'</li>';
            }
        }else{
            echo '<li>'.Html::a('<div class="sr_title">No resluts</div>', '#').'</li>';
        }
        exit;
    }

    public function actionTable($id,$region=null,$district=null)
    {
        $table = Tables::findOne((int)$id);
        $regioned = Region::findOne((int)$region);
        $districts = $region==null?null:District::find()->where(['region_id'=>$regioned->id])->all();
        $districted = District::findOne((int)$district);
        $locals = $district==null?null:Locality::find()->where(['district_id'=>$districted->id])->all();
        $url_params = '?id='.$id;
        if($region!=null){
            $url_params .= '&region='.$region;
        }
        if($district!=null){
            $url_params .= '&district='.$district;
        }
        $this->view->params['tn_url'] = '/'.Yii::$app->controller->module->id.'/default/table'.$url_params;
        return $this->render('table', [
            'table'=>$table,
            'region'=>$regioned,
            'district'=>$districted,
            'districts'=>$districts,
            'locals'=>$locals,
            ]
        );
    }
    /*public function actionRegion($table,$region)
    {
        $table = Tables::findOne((int)$table);
        return $this->render('table',['table'=>$table]);
    }
    public function actionDistrict($id,$region,$district)
    {
        $table = Tables::findOne((int)$id);
        return $this->render('table',['table'=>$table]);
    }*/

    /**
     * @param $id Locality id
     * @return string
     */
    public function actionLocality($id)
    {
        /*$locality = Locality::find()
            ->where(['id'=>(int)$id, 'type'=>'wb'])
            //->andWhere(['wbPlans.year'=>'2019'])
            ->one();

        $this->view->params['tn_url'] = Url::to(['/wb/default/locality','id'=>$id]);
        return $this->render('locality',[
            'locality' => $locality,
        ]);*/

        return $this->redirect(['/wbresults/locality?type=1&locality='.$id]);
    }

    /**
     * @param $id
     */
    public function actionDistricts($id){
        $rows = \app\models\District::find()->where(['region_id' => $id])->all();

        echo "<option value=''>".Yii::t('app', 'Select district')."</option>";

        if(count($rows)>0){
            /** @var $row District*/
            foreach($rows as $row){
                if($row->id == 24 || $row->id == 19 ||$row->id == 23 ||$row->id == 28 ||$row->id == 25 ||$row->id == 82 ||
                    $row->id == 83 ||$row->id == 74 ||$row->id == 84 ||$row->id == 78 ||$row->id == 148 ||$row->id == 149 ||
                    $row->id == 142 ||$row->id == 147 ||$row->id == 112 ||$row->id == 116 ||$row->id == 119 ||$row->id == 46 ||
                    $row->id == 51 ||$row->id == 52 ||$row->id == 42)
                echo "<option value='$row->id'>".$row->getTitleLang(Yii::$app->lang->check_lang())."</option>";

            }
        }
        die();
    }

    /**
     * @param $id
     */
    public function actionLocalities($id){
        $rows = \app\models\Locality::find()->where(['type'=>Locality::TYPE_WB, 'district_id' => $id])->all();

        echo "<option value=''>".Yii::t('app', 'Select village')."</option>";

        if(count($rows)>0){
            /** @var $row District*/
            foreach($rows as $row){
                echo "<option value='$row->id'>".$row->getTitleLang(Yii::$app->lang->check_lang())."</option>";
            }
        }
        die();
    }

    public function actionPopulations($reg_id, $dis_id, $vil_id){
        return (new \app\models\forms\mobilization\Aholi)->getPopulation($reg_id, $dis_id, $vil_id);
    }

    public function actionProjects($reg_id, $dis_id, $vil_id){
        return (new \app\models\WbInvestment())->getTotalProjects($reg_id, $dis_id, $vil_id);
    }

    public function actionInvestments($reg_id, $dis_id, $vil_id){
        return (new \app\models\WbInvestment())->getTotalInvestments($reg_id, $dis_id, $vil_id);
    }

    public function actionSpecific($reg_id, $dis_id, $vil_id){
        $model = new SearchPlans();
        return $this->renderPartial('test', [
            'model'=> $model,
            'reg_id'=>$reg_id,
            'dis_id'=>$dis_id,
            'vil_id'=>$vil_id,
        ]);

    }

    public function actionClear($id){

        echo "<option value=''>".Yii::t('app', 'Select village')."</option>";

        die();
    }
}
