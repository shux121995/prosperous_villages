<?php

namespace app\modules\wb\controllers;

use app\models\District;
use app\models\Locality;
use app\models\TotalFeedback;
use Yii;
use app\models\Feedback;
use app\models\FeedbackFiles;
use app\modules\wb\models\SearchFeedback;
use app\controllers\HomeController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * FeedbackController implements the CRUD actions for Feedback model.
 */
class FeedbackController extends HomeController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Feedback models.
     * @return mixed
     */

    public function actionIndex()
    {
        /** @var $model Feedback **/
        $model = new Feedback();
        $model->project_type = Locality::TYPE_WB;

        if ($model->load(Yii::$app->request->post())) {
            $model->image = UploadedFile::getInstance($model, 'image');



                if ($model->save()) {
                    if($model->image != null)
                    {
                        $model->image->saveAs($_SERVER['DOCUMENT_ROOT'] . "/uploads/" . $model->id . '_' . $model->image->baseName . "." . $model->image->extension);

                        $file = new FeedbackFiles();
                        $file->feed_id = $model->id;
                        $file->filename = $model->id . '_' . $model->image->baseName . '.' . $model->image->extension;
                        $file->save(false);

                        //$model->upload();
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                    else{
                        $file = new FeedbackFiles();
                        $file->feed_id = $model->id;
                        $file->filename = $model->id;
                        $file->save(false);
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
            }

        }

        $this->view->params['tn_url'] = '/'.Yii::$app->controller->module->id.'/feedback';
        return $this->render('create', [
            'model' => $model,
        ]);
        /*$searchModel = new SearchFeedback();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);*/
    }





    /**
     * Displays a single Feedback model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Feedback model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    /*public function actionCreate()
    {
        /** @var $model Feedback*
        $model = new Feedback();

        if ($model->load(Yii::$app->request->post())) { print_r($model);
            $model->image = UploadedFile::getInstance($model, 'image');
            print_r($model->image);
            if($model->save()){
                $model->image->saveAs($_SERVER['DOCUMENT_ROOT'] . "/uploads/".$model->image->baseName.".".$model->image->extension);

                $file = new FeedbackFiles();
                $file->feed_id = $this->id;
                $file->filename = $model->image->baseName.'.'.$model->image->extension;
                $file->save(false);

                //$model->upload();
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }*/

    /**
     * Updates an existing Feedback model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    /*public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }*/

    /**
     * Deletes an existing Feedback model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    /*public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }*/

    /**
     * Finds the Feedback model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Feedback the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Feedback::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionDistricts($id){
        $rows = \app\models\District::find()->where(['region_id' => $id])->all();

        echo "<option value=''>".Yii::t('app', 'Select district')."</option>";

        if(count($rows)>0){
            /** @var $row District*/
            foreach($rows as $row){
                if($row->id == 24 || $row->id == 19 ||$row->id == 23 ||$row->id == 28 ||$row->id == 25 ||$row->id == 82 ||
                    $row->id == 83 ||$row->id == 74 ||$row->id == 84 ||$row->id == 78 ||$row->id == 148 ||$row->id == 149 ||
                    $row->id == 142 ||$row->id == 147 ||$row->id == 112 ||$row->id == 116 ||$row->id == 119 ||$row->id == 46 ||
                    $row->id == 51 ||$row->id == 52 ||$row->id == 42)
                    echo "<option value='$row->id'>".$row->getTitleLang(Yii::$app->lang->check_lang())."</option>";
            }
        }
        die();
    }

    public function actionLocalities($id){
        $rows = \app\models\Locality::find()->where(['type'=>Locality::TYPE_WB, 'district_id' => $id])->all();

        echo "<option value=''>".Yii::t('app', 'Select village')."</option>";

        if(count($rows)>0){
            /** @var $row District*/
            foreach($rows as $row){
                echo "<option value='$row->id'>".$row->getTitleLang(Yii::$app->lang->check_lang())."</option>";
            }
        }
        die();
    }

}
