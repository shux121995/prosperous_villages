<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TablePlanEnd */

$this->title = Yii::t('app', 'Create Table Plan End');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Table Plan Ends'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="table-plan-end-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
