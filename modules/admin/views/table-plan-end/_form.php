<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TablePlanEnd */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="table-plan-end-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'plan_id')->textInput() ?>

    <?= $form->field($model, 'table_id')->dropDownList(
            \yii\helpers\ArrayHelper::map(\app\models\Tables::find()->all(),'id','title')
    ) ?>

    <?= $form->field($model, 'status')->dropDownList(
            \app\models\TablePlanEnd::statusLabels()
    ) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
