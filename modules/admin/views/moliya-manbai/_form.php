<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\MoliyaManbai */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="moliya-manbai-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'parent_id')
        ->widget(Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\app\models\MoliyaManbai::find()->All(), 'id', 'title'),
            'options' => [
                'prompt' => Yii::t('app', 'Молия манбаини танланг')
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
