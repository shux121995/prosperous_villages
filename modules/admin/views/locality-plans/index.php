<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\LocalityPlansSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Locality Plans');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="locality-plans-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Locality Plans'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'year',
            [
                'attribute'=>'locality_id',
                'value'=>'locality.title',
                'filter' => \kartik\select2\Select2::widget([
                    'name' => 'LocalityPlansSearch[locality_id]',
                    'data' => ArrayHelper::map(\app\models\Locality::find()->asArray()->all(), 'id', 'title'),
                    'value' => $searchModel->locality_id,
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'Жой номини ёзинг'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'selectOnClose' => true,
                    ]
                ])
            ],
            [
                'attribute'=>'district_id',
                'value'=>'locality.district.title',
                'filter' => \kartik\select2\Select2::widget([
                    'name' => 'LocalityPlansSearch[district_id]',
                    'data' => ArrayHelper::map(\app\models\District::find()->asArray()->all(), 'id', 'title'),
                    'value' => $searchModel->district_id,
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'Туман номини ёзинг'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'selectOnClose' => true,
                    ]
                ])
            ],
            [
                'attribute'=>'region_id',
                'value'=>'locality.district.region.title',
                'filter' => \kartik\select2\Select2::widget([
                    'name' => 'LocalityPlansSearch[region_id]',
                    'data' => ArrayHelper::map(\app\models\Region::find()->asArray()->all(), 'id', 'title'),
                    'value' => $searchModel->region_id,
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'Вилоят номини ёзинг'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'selectOnClose' => true,
                    ]
                ])
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
