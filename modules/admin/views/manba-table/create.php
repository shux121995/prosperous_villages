<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MoliyaManbaiTable */

$this->title = Yii::t('app', 'Create Moliya Manbai Table');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Moliya Manbai Tables'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="moliya-manbai-table-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
