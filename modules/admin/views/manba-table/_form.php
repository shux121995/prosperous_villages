<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\MoliyaManbaiTable */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="moliya-manbai-table-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model,'table_id')
        ->widget(Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\app\models\Tables::find()->All(), 'id', 'title'),
            'options' => [
                'class' => 'form-control',
                'prompt' => Yii::t('app', 'Свод жадвални танланг'),
                'onchange' => '
                            $.get( "' . \yii\helpers\Url::to('manbalar') . '", { id: $(this).val() } )
                                .done(function( data ) {
                                    $( "#' . Html::getInputId($model, 'manba_id') . '" ).html( data );                        
                                }
                            );',
                /*'onready' => '
                            $.get( "' . \yii\helpers\Url::to('manbalar') . '", { id: $(this).val() } )
                                .done(function( data ) {
                                    $( "#' . Html::getInputId($model, 'manba_id') . '" ).html( data );
                                }
                            );'*/
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label(false); ?>

    <?= $form->field($model, 'manba_id')
        ->widget(Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\app\models\MoliyaManbai::find()->All(), 'id', 'title'),
            'options' => [
                'prompt' => Yii::t('app', 'Молия манбаини танланг')
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>


    <?= $form->field($model, 'other')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
