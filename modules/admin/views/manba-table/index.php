<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\SearchMoliyaManbaiTable */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Moliya Manbai Tables');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="moliya-manbai-table-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Moliya Manbai Table'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'table_id',
                'value' => function($model){
                    $table = \app\models\Tables::findOne($model->table_id);
                    return $table!=null?$table->title:null;
                }
            ],
            [
                'attribute' => 'manba_id',
                'value' => function($model){
                    return \app\models\MoliyaManbai::findOne($model->manba_id)->title;
                }
            ],
            'other',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
