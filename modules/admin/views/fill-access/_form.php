<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use app\models\FillAccess;

/* @var $this yii\web\View */
/* @var $model app\models\FillAccess */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="fill-access-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row fill-access-form">
        <div class="col-lg-6">
            <?= $form->field($model, 'year')->dropDownList(FillAccess::years()) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'month')->dropDownList(FillAccess::selectMonths()) ?>
        </div>
        <div class="select-date">
            <div class="col-lg-6">
                <?=  $form->field($model, 'start')->widget(DatePicker::classname(),[
                'options' => ['placeholder' => 'Факт киртишни бошланиши'],
                'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true
                ]
                ]) ?>
            </div>
            <div class="col-lg-6">
                <?=  $form->field($model, 'end')->widget(DatePicker::classname(),[
                    'options' => ['placeholder' => 'Факт киртишни бошланиши'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
                ]) ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
