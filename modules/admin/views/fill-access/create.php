<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FillAccess */

$this->title = 'Create Fill Access';
$this->params['breadcrumbs'][] = ['label' => 'Fill Accesses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fill-access-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
