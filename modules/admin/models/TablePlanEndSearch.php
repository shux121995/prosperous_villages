<?php

namespace app\modules\admin\models;

use app\models\Tables;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TablePlanEnd;

/**
 * TablePlanEndSearch represents the model behind the search form of `app\models\TablePlanEnd`.
 */
class TablePlanEndSearch extends TablePlanEnd
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'plan_id'], 'integer'],
            [['table_id'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TablePlanEnd::find()->orderBy('id DESC');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'plan_id' => $this->plan_id,
        ]);
        $sub = [];
        if(!empty($this->table_id)){
            $sub = Tables::find()->select('id')
                ->where(['like','title',$this->table_id])
                ->orWhere(['like','excel_name',$this->table_id])
                ->orWhere(['like','excel_description',$this->table_id]);
        }

        $query->andFilterWhere([
            'in', 'table_id', $sub,
        ]);

        return $dataProvider;
    }
}
