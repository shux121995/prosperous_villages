<?php
use app\models\Qurulish;
use app\models\BozorInfratuzilmasi;

$lang = (new \app\components\LangHelper())->check_lang();
?>
<?php $regions = \app\models\Region::find()->all(); ?>

<div class="results-default-index tabled">
    <h1><?=$table->title?></h1>
    <h2><?=Yii::t('app','Жой номини танланг:')?></h2>
    <div class="row">
        <div class="col col-lg col-md-auto">
            <ul class="list-group li_region">
                <?php foreach ($regions as $reg){
                    echo '<li class="list-group-item list-group-item-action '.(isset($region->id)&&$region->id==$reg->id?"list-group-item-dark":null).'">'.\yii\helpers\Html::a($reg->getTitleLang(\app\components\LangHelper::lang()),
                            '/'.$lang.\yii\helpers\Url::toRoute(['table','id'=>$table->id,'region'=>$reg->id]),
                            ['title' => '(' . $table->excel_name . ') ' . $table->excel_description]
                        ).'</li>';
                } ?>
            </ul>
        </div>
        <div class="col col-lg col-md-auto">
            <ul class="list-group li_district">
                <?php
                if($districts!=null) {
                    foreach ($districts as $dist) {
                        echo '<li class="list-group-item list-group-item-action '.(isset($district->id)&&$district->id==$dist->id?"list-group-item-dark":null).'">' . \yii\helpers\Html::a($dist->getTitleLang(\app\components\LangHelper::lang()),
                                '/'.$lang.\yii\helpers\Url::toRoute(['table',
                                    'id' => $table->id,
                                    'region' => $region->id,
                                    'district' => $dist->id
                                ]),
                                ['title' => '(' . $table->excel_name . ') ' . $table->excel_description]
                            ) . '</li>';
                    }
                }
                ?>
            </ul>
        </div>
        <div class="col col-lg col-md-auto">
            <ul class="list-group li_locality">
                <?php
                if($locals!=null) {
                    foreach ($locals as $local) {
                        echo '<li class="list-group-item list-group-item-action">' . \yii\helpers\Html::a($local->getTitleLang(\app\components\LangHelper::lang()),
                                '/'.$lang.\yii\helpers\Url::toRoute(['view', 'id' => $table->id, 'local' => $local->id]),
                                ['title' => '(' . $table->excel_name . ') ' . $table->excel_description]) .
                            '</li>';
                    }
                }
                ?>
            </ul>
        </div>
    </div>

</div>
