<?php
use yii\web\View;
use yii\helpers\Url;
use app\models\Tables;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->title = Yii::t('app','Bosh sahifa');
$lang = (new \app\components\LangHelper())->check_lang();
?>
<!--INCLUDE THIS STYLE IF YOU WANT TO MAKE IT WITHOUT BORDER-->
<style>
    main.container-fluid{/*margin: 0px!important;padding: 0px!important;*/}
    .map-filter {
        position: absolute;
        top: 20px;
        z-index: 22;
        width: 97%;
        padding: 0 2%;
        margin: auto;
    }
</style>
<div class="row">
    <div class="col-md-7">
        <div class="map-filter">
            <?php $form = ActiveForm::begin(['method'=>'get']); ?>
            <div class="row">
                <div class="col-md-3">
                    <?= $form->field($model, 'region_id')->dropDownList($regions,
                        [
                            'class' => 'form-control',
                            'prompt' => Yii::t('app', 'Select region'),
                            'onchange' => '
                $.get( "' . \yii\helpers\Url::toRoute('districts') . '", { id: $(this).val() } )
                    .done(function( data ) {
                        $( "#' . Html::getInputId($model, 'district_id') . '" ).html( data );                        
                    }
                );'
                        ])->label(false) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'district_id')->dropDownList($districts,
                        [
                            'prompt' => Yii::t('app', 'Select district'),
                            'onchange' => '
                    $.get( "' . \yii\helpers\Url::toRoute('localities') . '", { id: $(this).val() } )
                        .done(function( data ) {
                            $( "#' . Html::getInputId($model, 'locality_id') . '" ).html( data );                        
                        }
                    );'
                        ])->label(false)?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'locality_id')->dropDownList($localities,
                        ['prompt' => Yii::t('app', 'Select village')])
                        ->label(false)?>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', 'Filter'), ['class' => 'btn btn-success w-100']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>



        <div id="map" style="height: 100vh; width:100%; overflow:hidden; position: relative!important; /*left: 0; right: 0; bottom: 0; top: 0*/"></div>
        <!--<div class="search_wrap">
            <div class="wella_container">
                <input type="text" class="searching_wella" placeholder="<?=Yii::t('app','Жой номини киритинг...')?>">
                <button type="submit" class="wella_btn"></button>
                <div class="searching_resluts" style="display: none;">
                    <ul class="sr_lists">
                        <li>
                            <a href="#">
                                <div class="sr_title"><?=Yii::t('app','Жой номини киритинг...')?></div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>-->
    </div>
    <div class="col-md-5">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4><?= Html::label(Yii::t('app', 'Statistics Dashboard'))?></h4>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered table-striped">
                        <tbody><tr>
                            <td></td>
                            <td style="text-align:center"><?= Html::label(Yii::t('app', 'Current Year'))?>(<?=$_SESSION['year']?>)</td>
                            <td style="text-align:center"><?= Html::label(Yii::t('app', 'Cumulative'))?></td>
                            <!--<td style="text-align:center">Current Year (<?=$_SESSION['year']?>)</td>-->
                            <!--<td style="text-align:center">Cumulative</td>-->
                        </tr>
                        <tr>
                            <td><?= Html::label(Yii::t('app', 'Townships'))?></td>
                            <td style="text-align:center">
                                <a href="#" id="om" style="font-weight: bold">0</a>
                            </td>
                            <td style="text-align:center">0</td>
                        </tr>
                        <tr>
                            <td><?= Html::label(Yii::t('app', 'Village Tracts'))?></td>
                            <td style="text-align:center">
                                <a href="#" id="kpi2" style="font-weight: bold">0</a>
                            </td>
                            <td style="text-align:center">0</td>
                        </tr>
                        <tr>
                            <td><?= Html::label(Yii::t('app', 'Villages'))?></td>
                            <td style="text-align:center">
                                <a href="#" id="kpi3" style="font-weight: bold">1</a></td>
                            <td style="text-align:center">1</td>
                        </tr>
                        <tr>
                            <td><?= Html::label(Yii::t('app', 'Population'))?></td>
                            <td style="text-align:center">0</td>
                            <td style="text-align:center">0</td>
                        </tr>
                        <tr>
                            <td><?= Html::label(Yii::t('app', 'Beneficiaries - # million'))?></td>
                            <td style="text-align:center">
                                <a href="#" id="kpi4" style="font-weight: bold">0</a>
                            </td>
                            <td style="text-align:center">0</td>
                        </tr>
                        <tr>
                            <td><?= Html::label(Yii::t('app', 'Beneficiaries - % female'))?></td>
                            <td style="text-align:center">
                                <a href="#" id="kpi5" style="font-weight: bold">0</a>
                            </td>
                            <td style="text-align:center">0</td>
                        </tr>
                        <tr>
                            <td><?= Html::label(Yii::t('app', 'Block Grants - MMK Million'))?></td>
                            <td style="text-align:center">
                                <a href="#" id="kpi6" style="font-weight: bold">0</a>
                            </td>
                            <td style="text-align:center">0</td>
                        </tr>
                        <tr>
                            <td><?= Html::label(Yii::t('app', 'Actual Cost CDD - MMK'))?></td>
                            <td style="text-align:center">
                                <a href="#" id="kpi6" style="font-weight: bold">0</a>
                            </td>
                            <td style="text-align:center">0</td>
                        </tr>
                        <tr>
                            <td><?= Html::label(Yii::t('app', 'Actual Cost Community - MMK'))?></td>
                            <td style="text-align:center">
                                <a href="#" id="kpi6" style="font-weight: bold">0</a>
                            </td>
                            <td style="text-align:center">0</td>
                        </tr>
                        <tr>
                            <td><?= Html::label(Yii::t('app', '# Sub-Projects - Approved TPIC'))?></td>
                            <td style="text-align:center">
                                <a href="#" id="kpi7" style="font-weight: bold">0</a>
                            </td>
                            <td style="text-align:center">0</td>
                        </tr>
                        <tr>
                            <td><?= Html::label(Yii::t('app', '# Sub-Projects - Completed'))?></td>
                            <td style="text-align:center">
                                <a href="#" id="kpi8" style="font-weight: bold">0</a>
                            </td>
                            <td style="text-align:center">0</td>
                        </tr>
                        <tr>
                            <td><?= Html::label(Yii::t('app', 'Community Participation - %'))?></td>
                            <td style="text-align:center">
                                <a href="#" id="kpi9" style="font-weight: bold">0</a>
                            </td>
                            <td style="text-align:center">0</td>
                        </tr>
                        <tr>
                            <td><?= Html::label(Yii::t('app', 'Committee Membership - #'))?></td>
                            <td style="text-align:center">
                                <a href="#" id="kpi10" style="font-weight: bold">0</a>
                            </td>
                            <td style="text-align:center">0</td>
                        </tr>
                        <tr>
                            <td><?= Html::label(Yii::t('app', 'Committee Membership - % female'))?></td>
                            <td style="text-align:center">
                                <a href="#" id="kpi11" style="font-weight: bold">0</a>
                            </td>
                            <td style="text-align:center">0</td>
                        </tr>
                        <tr>
                            <td><?= Html::label(Yii::t('app', 'Grievances - # submitted'))?></td>
                            <td style="text-align:center">
                                <a href="#" id="kpi12" style="font-weight: bold">0</a>
                            </td>

                            <td style="text-align:center">0</td>
                        </tr>
                        <tr>
                            <td><?= Html::label(Yii::t('app', 'Grievances - % resolved or addressed'))?></td>
                            <td style="text-align:center">
                                <a href="#" id="kpi13" style="font-weight: bold">0</a></td>
                            <td style="text-align:center">0</td>
                        </tr>
                        <tr>
                            <td><?= Html::label(Yii::t('app', 'Labour days Male'))?></td>
                            <td style="text-align:center">0</td>
                            <td style="text-align:center">0</td>
                        </tr>
                        <tr>
                            <td><?= Html::label(Yii::t('app', 'Labour days Female'))?></td>
                            <td style="text-align:center">0</td>
                            <td style="text-align:center">0</td>
                        </tr>

                        <tr>
                            <td><?= Html::label(Yii::t('app', 'Labour Male'))?></td>
                            <td style="text-align:center">0</td>
                            <td style="text-align:center">0</td>
                        </tr>
                        <tr>
                            <td><?= Html::label(Yii::t('app', 'Labour Female'))?></td>
                            <td style="text-align:center">0</td>
                            <td style="text-align:center">0</td>
                        </tr>
                        <tr>
                            <td><?= Html::label(Yii::t('app', 'Involvement in CFA - Wages - MMK billion'))?></td>
                            <td style="text-align:center">
                                <a href="#" id="kpi15" style="font-weight: bold">0</a></td>
                            <td style="text-align:center">0</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>

    </div>
    <div class="clear clearfix"></div>
</div>

<div class="footered" style="position: relative;">

    <div class="container_fluid">
        <div class="row" style="color: #003674;">
            <div class="smalled"><?=Yii::t('app','Қилинган ишлар буйича <span class="bigged">натижалар:</span>')?></div>

        </div>
        <div class="row">
            <div class="handleslide">
                <div class="frame" id="basic">
                    <ul class="clearfix">
                        <?php
                        /** @var $table \app\models\Tables */
                        foreach ($tables as $table){
                            $fact = round((int)$table->fact);///8700
                            $plan = round((int)$table->plan);///8700
                            $percent = round(($plan==0)?0:($fact*100)/$plan);
                            ?>
                            <style>
                                .donut-segment-<?=$table->id?> {
                                    stroke: #003674;
                                    animation: donut<?=$table->id?> 3s;
                                }
                                @keyframes donut<?=$table->id?> {
                                    0% {
                                        stroke-dasharray: 0, 100;
                                    }
                                    100% {
                                       stroke-dasharray: <?=$percent?>, <?=(100-$percent)?>;
                                    }
                               }
                            </style>
                            <li title="<?=str_replace('"',"''",$table->excel_description)?>">
                                <div class="hs_svg">
                                    <div class="svg-item">
                                        <svg width="100%" height="100%" viewBox="0 0 40 40" class="donut">
                                            <circle class="donut-hole" cx="20" cy="20" r="15.91549430918954" fill="#fff"></circle>
                                            <circle class="donut-ring" cx="20" cy="20" r="15.91549430918954" fill="transparent" stroke-width="3.5"></circle>
                                            <circle class="donut-segment donut-segment-<?=$table->id?>" cx="20" cy="20" r="15.91549430918954" fill="transparent" stroke-width="3.5" stroke-dasharray="<?=$percent?> <?=(100-$percent)?>" stroke-dashoffset="25"></circle>
                                            <g class="donut-text donut-text-1">
                                                <text y="52%" transform="translate(0, 2)">
                                                    <tspan x="52%" text-anchor="middle" class="donut-percent"><?=$percent?>%</tspan>
                                                </text>
                                            </g>
                                        </svg>
                                    </div>
                                </div>
                                <div class="hs_texts">
                                    <span class="hst_bigger"><?=number_format($fact, 0,'.',',')?><?=Yii::t('app','млн.сум')?><a href="/<?=$lang?><?=Url::toRoute(['table','id'=>$table->id])?>" title="<?=str_replace('"',"''",$table->getDescriptionLang(\app\components\LangHelper::lang()))?>" class="hs_info"></a></span>
                                    <span class="hst_smaller">/ <?=number_format($plan, 0,'.',',')?><?=Yii::t('app','млн.сум')?></span>
                                    <span class="hst_texter"><?=$table->getTitleLang(\app\components\LangHelper::lang())?></span>
                                </div>
                            </li>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
                <div class="scrollbar">
                    <div class="handle">
                        <div class="mousearea"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$searchUrl = \yii\helpers\Url::toRoute(['search','language'=>Yii::$app->lang->check_lang()]);
$contenter = '';
$html = 'var location = [';
$i = 0;
  foreach($locals as $key => $local) :
      if($local->map_latitude!=null && $local->map_longitude!=null) :

          $contenter = '<div id="content"><div id="siteNotice"></div><h5 id="firstHeading" class="firstHeading">'.str_replace(["\r","\n","\r\n"], "", $local->getTitleLang($lang).', '.$local->district->getTitleLang($lang)).'</h5><div id="bodyContent"><ul>';
          foreach (Tables::find()->all() as $tabled) {
              $contenter .= '<li class="menu__item">' . \yii\helpers\Html::a(str_replace("\r\n", "", $tabled->getTitleLang($lang)),
                      '/'.Yii::$app->lang->check_lang().\yii\helpers\Url::toRoute(['view', 'id' => $tabled->id, 'local' => $local->id]),
                      ['class' => 'menu__item-link']
                  ) . '</li>';
          }

         $contenter .= '</ul></div></div>';

         $contenter = str_replace("'","",$contenter);

         $html .='["'.$i.'",'.$local->map_latitude.','.$local->map_longitude.',\''.$contenter.'\',\''.str_replace(["\r","\n","\r\n"], "", $local->getTitleLang($lang).', '.$local->district->getTitleLang($lang)).'\'],';
         $i++;
      endif;

  endforeach;
$html .= '];';
$lat = $maps['lat'];
$lng = $maps['lng'];
$zoom = $maps['zoom'];

$script = <<< JS
    function initMap() {
        numberMarkerImg = {
            url: '/wb/map_big.png'/*,
            size: new google.maps.Size(32, 38),
            scaledSize: new google.maps.Size(32, 38),
            labelOrigin: new google.maps.Point(9, 9)*/
        };
        var coordinates = {lat: $lat, lng: $lng},
        map = new google.maps.Map(document.getElementById('map'), {
            center: coordinates,
            zoom: $zoom,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true,
            scrollwheel: false,
            mapTypeControl: true,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: google.maps.ControlPosition.BOTTOM_LEFT
            },
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_CENTER
            },
            scaleControl: true,
            streetViewControl: true,
            streetViewControlOptions: {
                position: google.maps.ControlPosition.RIGHT_CENTER
            },
        });         
        $html 
        
        var infowindow = new google.maps.InfoWindow();
    
        for (var i = 0; i < location.length; i++) {
    
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(location[i][1], location[i][2]),
                map: map,
                icon:numberMarkerImg,
                title: location[i][4]
            });
    
            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    infowindow.setContent(location[i][3]);
                    infowindow.open(map, marker);
                }
            })(marker, i)); 
        }
    }
    
    function init(){
        var myMap = new ymaps.Map('map', {
            center: [42.00, 64.00],
            zoom: 6,
            type: 'yandex#map',
            controls: ['fullscreenControl']
        }); 
        myMap.behaviors.disable('scrollZoom'); 
    }
    $(document).ready(function () {
        //$('header').addClass('fixed-top');
        initMap();  
        //ymaps.ready(init);
    });
JS;
$map_api = strrpos(Yii::$app->request->hostName,'obodqishloq.uz')!==false?Yii::$app->params['map_api_real']:Yii::$app->params['map_api'];
$this->registerJs($script, View::POS_READY);
//$this->registerJsFile('https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey='.Yii::$app->params['ymap_api_key'], ['depends'=>'yii\web\JqueryAsset'], \yii\web\View::POS_READY);
$this->registerJsFile('https://maps.googleapis.com/maps/api/js?key='.$map_api.'&language='.$lang, ['depends'=>'yii\web\JqueryAsset'], View::POS_READY);