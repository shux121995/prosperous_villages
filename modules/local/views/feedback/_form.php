<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Feedback */
/* @var $form yii\widgets\ActiveForm */

$tables = [];
foreach (\app\models\Tables::find()->all() as $table){
    $tables[$table->id] = $table->getTitleLang((new \app\components\LangHelper())->check_lang());
}
?>

<div class="feedback-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'project_type')->dropDownList(\app\models\Feedback::projectLabels()) ?>

    <?= $form->field($model, 'locality_id')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'table_id')->dropDownList($tables) ?>

    <?php //= $form->field($model, 'section_id')->textInput() ?>

    <?= $form->field($model, 'type')->dropDownList(\app\models\Feedback::typeLables()) ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'image')->fileInput() ?>

    <?php //= $form->field($model, 'created_at')->textInput() ?>

    <?php //= $form->field($model, 'updated_at')->textInput() ?>

    <?php //= $form->field($model, 'status')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Izoh qoldirish'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
