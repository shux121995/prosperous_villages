<?php

namespace app\modules\local;

/**
 * wb module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\local\controllers';

    public $layout = '//local';
    /**
     * {@inheritdoc}
     */
    public function init()
    {
        if(!isset($_SESSION['year'])) $_SESSION['year'] = date('Y');
        parent::init();

        // custom initialization code goes here
    }
}
