<?php

namespace app\modules\local\controllers;

use app\components\LangHelper;
use app\models\District;
use app\models\Locality;
use app\models\Region;
use app\models\Tables;
use app\modules\wb\models\SearchPlans;
use yii\helpers\Html;
use yii\helpers\Url;
use app\controllers\HomeController;
use Yii;

/**
 * Default controller for the `wb` module
 */
class DefaultController extends HomeController
{
    /**
     * Renders the index view for the module
     * @return string
     */

    public function actionIndex($type='all')
    {
        $model = new SearchPlans();
        $tables = Tables::find()->all();
        $locals = Locality::find();

        $regioned = \app\models\Region::find()->all();
        $regions = [];
        $districted = \app\models\District::find()->all();
        $districts = [];
        $localited = \app\models\Locality::find()->all();
        $localities = [];
        $lang = (new \app\components\LangHelper())->check_lang();

        /** @var $region \app\models\Region*/
        foreach ($regioned as $region){
            $regions[$region->id] = $region->getTitleLang($lang);
        }
        /** @var $district \app\models\District*/
        foreach ($districted as $district){
                $districts[$district->id] = $district->getTitleLang($lang);
        }
        /** @var $localitie \app\models\District*/
        foreach ($localited as $localitie){
            $localities[$localitie->id] = $localitie->getTitleLang($lang);
        }

        // map coords and zoom
        $mapped = $model;
        $mapped->load(Yii::$app->request->queryParams);

        if(!empty($mapped->locality_id)){
            $map = Locality::findOne($mapped->locality_id);
        }elseif (!empty($mapped->district_id)){
            $map = District::findOne($mapped->district_id);
        }elseif (!empty($mapped->region_id)){
            $map = Region::findOne($mapped->region_id);
        }else{
            $map = null;
        }
        $maps = [
            'lat' => 41.311151,
            'lng' => 69.279737,
            'zoom' => 13
        ];

        if($map!=null && $map->map_latitude!=null && $map->map_longitude!=null){
            $maps['lat'] = $map->map_latitude;
            $maps['lng'] = $map->map_longitude;
            $maps['zoom'] = empty($map->map_zoom)?13:$map->map_zoom;
        }


        if($type=='qishloq' OR $type=='mahalla'){
            $locals->where(['type'=>$type]);
        }
        return $this->render('index', [
            'maps'          => $maps,
            'model'         =>$model,
            'regions'       =>$regions,
            'districts'     =>$districts,
            'localities'    =>$localities,
            'tables'        => $tables,
            'locals'        => $locals->all()
        ]);
    }

    /**
     * @param $id table id
     * @param $local locality id
     * @return string
     */
    public function actionView($id,$local)
    {
        $table = Tables::findOne((int)$id);
        $locality = Locality::findOne((int)$local);
        $this->view->params['tn_url'] = '/view?id='.$id.'&local='.$local;
        return $this->render('view',[
            'table' => $table,
            'locality' => $locality,
        ]);
    }

    /**
     * @param $q
     */
    public function actionSearch($q)
    {
        if(!Yii::$app->request->isAjax) return 'Error Request!';
        $rows = Locality::find()->where(['like','title', $q])->orWhere(['like','title_en', $q])->all();

        if (count($rows) > 0) {
            /** @var $row Locality*/
            foreach ($rows as $row) {
                $title = '<div class="sr_title">'.$row->getTitleLang(Yii::$app->lang->check_lang()).'</div>
                            <div class="sr_label">'.$row->district->region->getTitleLang(Yii::$app->lang->check_lang()).', '.$row->district->getTitleLang(Yii::$app->lang->check_lang()).'</div>';
                $a = Html::a($title, '/'.Yii::$app->lang->check_lang().Url::toRoute(['view', 'id'=>Tables::find()->one()->id,'local'=>$row->id]));//['wella', 'id'=>$row->id]
                echo '<li>'.$a.'</li>';
            }
        }else{
            echo '<li>'.Html::a('<div class="sr_title">No resluts</div>', '#').'</li>';
        }
        exit;
    }

    public function actionTable($id,$region=null,$district=null)
    {
        $table = Tables::findOne((int)$id);
        $regioned = Region::findOne((int)$region);
        $districts = $region==null?null:District::find()->where(['region_id'=>$regioned->id])->all();
        $districted = District::findOne((int)$district);
        $locals = $district==null?null:Locality::find()->where(['district_id'=>$districted->id])->all();
        $url_params = '?id='.$id;
        if($region!=null){
            $url_params .= '&region='.$region;
        }
        if($district!=null){
            $url_params .= '&district='.$district;
        }
        $this->view->params['tn_url'] = '/'.Yii::$app->controller->module->id.'/default/table'.$url_params;
        return $this->render('table', [
            'table'=>$table,
            'region'=>$regioned,
            'district'=>$districted,
            'districts'=>$districts,
            'locals'=>$locals,
            ]
        );
    }
    /*public function actionRegion($table,$region)
    {
        $table = Tables::findOne((int)$table);
        return $this->render('table',['table'=>$table]);
    }
    public function actionDistrict($id,$region,$district)
    {
        $table = Tables::findOne((int)$id);
        return $this->render('table',['table'=>$table]);
    }*/

    public function actionDistricts($id){
        $rows = \app\models\District::find()->where(['region_id' => $id])->all();

        echo "<option value=''>".Yii::t('app', 'Select district')."</option>";

        if(count($rows)>0){
            /** @var $row District*/
            foreach($rows as $row){
                echo "<option value='$row->id'>".$row->getTitleLang(Yii::$app->lang->check_lang())."</option>";
            }
        }
        die();

    }

    /**
     * @param $id
     */
    public function actionLocalities($id){
        $rows = \app\models\Locality::find()->all();

        echo "<option value=''>".Yii::t('app', 'Select village')."</option>";

        if(count($rows)>0){
            /** @var $row District*/
            foreach($rows as $row){
                echo "<option value='$row->id'>".$row->getTitleLang(Yii::$app->lang->check_lang())."</option>";
            }
        }
        die();
    }

}
