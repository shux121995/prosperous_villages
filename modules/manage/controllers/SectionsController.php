<?php

namespace app\modules\manage\controllers;

use kartik\grid\EditableColumnAction;
use Yii;
use app\models\Sections;
use app\models\Tables;
use app\models\TablesExcel;
use app\models\Inputs;
use app\modules\manage\models\SectionsSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SectionsController implements the CRUD actions for Sections model.
 */
class SectionsController extends Controller
{
    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            'edits' => [                                       // identifier for your editable column action
                'class' => EditableColumnAction::className(),     // action class name
                'modelClass' => Sections::className(),                // the model for the record being edited
                'outputValue' => function ($model, $attribute, $key, $index) {
                    return (int) $model->$attribute;      // return any custom output value if desired
                },
                'outputMessage' => function($model, $attribute, $key, $index) {
                    return '';                                  // any custom error to return after model save
                },
                'showModelErrors' => true,                        // show model validation errors after save
                'errorOptions' => ['header' => '']                // error summary HTML options
                // 'postOnly' => true,
                // 'ajaxOnly' => true,
                // 'findModel' => function($id, $action) {},
                // 'checkAccess' => function($action, $model) {}
            ]
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Sections models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SectionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Sections model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Sections model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Sections();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Sections model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Sections model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Sections model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Sections the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Sections::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * @param $id
     */
    public function actionParents($id){
        $this->layout = '//clean';
        return $this->render('parents', [
            'rows' => Sections::find()->where(['tables_id' => $id])->all(),
        ]);
    }

    /**
     * @return string
     */
    public function actionGenLevel()
    {
        $tables = Tables::find()->all();
        //Очистка таблица tables_excel
        Yii::$app->db->createCommand()->truncateTable(TablesExcel::tableName())->execute();

        /** @var $table Tables*/
        foreach ($tables as $table){
            $sections = Sections::find()->where(['tables_id'=>$table->id,'parent_id'=>null])->all();
            $levels = $this->sectionLevel($sections);
            rsort($levels);
            $table->levels = isset($levels[0])?$levels[0]:1;
            $table->update(false);

            //Generation table excels
            TablesExcel::GenExcelTable($table->id);
        }
        return 'ok';
    }

    /**
     * @param $sections
     * @return null
     */
    public function sectionLevel($sections, $levels = [])
    {
        if($sections==null) return null;
        /** @var $section Sections*/
        foreach ($sections as $section){
            $section->level = $this->calcSectionLevel($section,1);
            $section->cols = $this->calcSectionCols($section);
            $section->cols_input = $this->calcSectionColsInput($section);
            $section->update(false);
            $levels[$section->level] = $section->level;
            if($section->sections!=null) $levels = $this->sectionLevel($section->sections, $levels);
        }
        return $levels;
    }

    /**
     * @param $section
     * @param int $level
     * @return int
     */
    public function calcSectionLevel($section,$level=1,$start = true)
    {
        /** @var $section Sections */
        $parent = $section->parent;
        /*if($section->tables_id==16){
            echo $section->title.' level: '.$level.'</br>';
        }*/
        if($parent!=null) {
            $rows = $parent->rows>1?$level+$parent->rows:$level+1;
            $level = $this->calcSectionLevel($parent,$rows,false);
        }
        return $level;
    }

    /**
     * Кулькулияция количество колонки для colspan
     * @param $section
     * @param int $cols
     * @return int
     */
    public function calcSectionCols($section,$cols=0)
    {
        $sections = Sections::find()->where(['parent_id'=>$section->id])->all();
        if($sections!=null) {
            $cols2 = 0;
            /** @var $sec Sections*/
            foreach ($sections as $sec){
                $cols2 = $cols2 + $this->calcSectioncols($sec,$cols);
            }
            $cols = $cols2;
        }else{
            $cols = $cols + 1;
        }
        return $cols;
    }

    /**
     * Кулькулияция количество колонки для colspan with inputs
     * @param $section
     * @param int $cols
     * @return int
     */
    public function calcSectionColsInput($section,$cols=0)
    {
        $sections = Sections::find()->where(['parent_id'=>$section->id])->all();
        if($sections!=null) {
            $cols2 = 0;
            /** @var $sec Sections*/
            foreach ($sections as $sec){
                $cols2 = $cols2 + $this->calcSectionColsInput($sec,$cols);
            }
            $cols = $cols2;
        }else{
            /** @var $section Sections*/
            $calcInputs = Inputs::find()->where(['forms_id'=>$section->forms_id])->count();
            $cols = $cols + $calcInputs;
        }
        return $cols;
    }
}
