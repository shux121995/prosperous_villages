<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TablesExcel */

$this->title = Yii::t('app', 'Create Tables Excel');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tables Excels'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tables-excel-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
