<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\manage\models\TabesExcelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Tables Excels');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tables-excel-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Tables Excel'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'table_id',
            'section_id',
            'input_id',
            'parent_id',
            //'type',
            //'level',
            //'colspan',
            //'rowspan',
            //'order',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
