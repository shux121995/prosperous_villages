<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ValuesFact */

$this->title = Yii::t('app', 'Create Values Fact');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Values Facts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="values-fact-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
