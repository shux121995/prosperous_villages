<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ValuesPlan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="values-plan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'plan_id')->textInput() ?>

    <?= $form->field($model, 'section_id')->textInput() ?>

    <?= $form->field($model, 'inputs_id')->textInput() ?>

    <?= $form->field($model, 'reja')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'moliya_manbai_id')->textInput() ?>

    <?= $form->field($model, 'molled')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
