<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ValuesPlan */

$this->title = Yii::t('app', 'Create Values Plan');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Values Plans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="values-plan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
