<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\ValuesPlanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="values-plan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'plan_id') ?>

    <?= $form->field($model, 'section_id') ?>

    <?= $form->field($model, 'inputs_id') ?>

    <?= $form->field($model, 'reja') ?>

    <?php // echo $form->field($model, 'moliya_manbai_id') ?>

    <?php // echo $form->field($model, 'molled') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
