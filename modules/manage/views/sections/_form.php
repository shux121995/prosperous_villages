<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Sections */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sections-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title_en')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tables_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Tables::find()->all(), 'id', 'title'),
        [
                'class' => 'form-control',
                'prompt' => Yii::t('app', 'Select table'),
                'onchange' => '
                $.get( "' . \yii\helpers\Url::to('parents') . '", { id: $(this).val() } )
                    .done(function( data ) {
                        $( "#' . Html::getInputId($model, 'parent_id') . '" ).html( data );                        
                    }
                );'
            ]) ?>

    <?= $form->field($model, 'parent_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Sections::find()->all(), 'id', 'title'), ['prompt' => Yii::t('app','Select parent section')]) ?>

    <?= $form->field($model, 'forms_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Forms::find()->all(), 'id', 'title'), ['prompt' => Yii::t('app','Select form')]) ?>

    <?= $form->field($model, 'is_group')->checkbox() ?>

    <?= $form->field($model, 'level')->textInput() ?>

    <?= $form->field($model, 'cols')->textInput() ?>

    <?= $form->field($model, 'rows')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
