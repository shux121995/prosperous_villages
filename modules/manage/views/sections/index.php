<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\SectionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Sections');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sections-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Sections'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Generate levels'), ['gen-level'], ['class' => 'btn btn-primary','target'=>'_blank']) ?>
    </p>

    <?php
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        'title',
        [
            'class' => 'kartik\grid\EditableColumn',
            'attribute' => 'title_en',
            'editableOptions'=> [
                'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                'formOptions' => ['action' => ['edits']]
            ],
            //'pageSummary' => 'Page Total',
            /*'vAlign'=>'middle',
            'headerOptions'=>['class'=>'kv-sticky-column'],
            'contentOptions'=>['class'=>'kv-sticky-column'],
            'editableOptions'=>['header'=>'Order', 'size'=>'md']*/
        ],
        ['attribute'=>'tables_id','value'=>'tables.excel_name'],
        ['attribute'=>'parent_id','value'=>'parent.title'],
        ['attribute'=>'forms_id','value'=>'forms.title'],
        'is_group',

        ['class' => 'yii\grid\ActionColumn'],
    ];
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
        'pjax' => true,
        'bordered' => true,
        'striped' => false,
        'condensed' => false,
        'responsive' => false,
        'hover' => true,
    ]); ?>
</div>
