<?php
/**
 * Created by PhpStorm.
 * User: Muxtorov Ulugbek
 * Date: 25.04.2019
 * Time: 21:17
 */
use yii\helpers\Html;
?>
<option value=''>Select parent section</option>
<?php
if(count($rows)>0){
    foreach($rows as $row){ ?>
        <option value="<?=$row->id?>"><?=Html::encode($row->title)?></option>
    <?php
    }
}
