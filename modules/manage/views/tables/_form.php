<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Tables */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tables-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title_en')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'project_type')->dropDownList(\app\models\Locality::typeLables(), ['prompt' => Yii::t('app','Select project')]) ?>

    <?= $form->field($model, 'year')->dropDownList(Yii::$app->params['years'], ['prompt' => Yii::t('app','Select year')]) ?>

    <?= $form->field($model, 'excel_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'excel_description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'excel_description_en')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->dropDownList(\app\models\Tables::statusLables(), ['prompt' => Yii::t('app','Select status')]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
