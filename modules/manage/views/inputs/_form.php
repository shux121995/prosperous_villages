<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Inputs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="inputs-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'forms_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Forms::find()->all(), 'id', 'title'), ['prompt' => 'Select form']) ?>

    <?= $form->field($model, 'title')->textInput() ?>

    <?= $form->field($model, 'title_en')->textInput() ?>

    <?= $form->field($model, 'name')->textInput() ?>
    
    <?= $form->field($model, 'type')->dropDownList($model->inputTypes()) ?>

    <?= $form->field($model, 'required')->checkbox() ?>

    <?= $form->field($model, 'is_admin')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
