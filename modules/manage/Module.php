<?php

namespace app\modules\manage;

/**
 * admin module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\manage\controllers';
    public $layout = '//manage';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        \Yii::$app->setHomeUrl('/manage');
        if(!isset($_SESSION['year'])) $_SESSION['year'] = date('Y');
        parent::init();

        // custom initialization code goes here
    }
}
