<?php

namespace app\modules\manage\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ValuesPlan;

/**
 * ValuesPlanSearch represents the model behind the search form of `app\models\ValuesPlan`.
 */
class ValuesPlanSearch extends ValuesPlan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'plan_id', 'section_id', 'inputs_id', 'moliya_manbai_id'], 'integer'],
            [['reja'], 'safe'],
            [['molled'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ValuesPlan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'plan_id' => $this->plan_id,
            'section_id' => $this->section_id,
            'inputs_id' => $this->inputs_id,
            'moliya_manbai_id' => $this->moliya_manbai_id,
            'molled' => $this->molled,
        ]);

        $query->andFilterWhere(['like', 'reja', $this->reja]);

        return $dataProvider;
    }
}
