<?php

namespace app\modules\manage\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TablesExcel;

/**
 * TabesExcelSearch represents the model behind the search form of `app\models\TablesExcel`.
 */
class TabesExcelSearch extends TablesExcel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'table_id', 'section_id', 'input_id', 'parent_id', 'type', 'level', 'colspan', 'rowspan', 'order'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TablesExcel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'table_id' => $this->table_id,
            'section_id' => $this->section_id,
            'input_id' => $this->input_id,
            'parent_id' => $this->parent_id,
            'type' => $this->type,
            'level' => $this->level,
            'colspan' => $this->colspan,
            'rowspan' => $this->rowspan,
            'order' => $this->order,
        ]);

        return $dataProvider;
    }
}
