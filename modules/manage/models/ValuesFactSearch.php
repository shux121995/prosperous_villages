<?php

namespace app\modules\manage\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ValuesFact;

/**
 * ValuesFactSearch represents the model behind the search form of `app\models\ValuesFact`.
 */
class ValuesFactSearch extends ValuesFact
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'plan_value_id', 'inputs_id', 'user_id', 'created_at', 'updated_at'], 'integer'],
            [['fact', 'post_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ValuesFact::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'plan_value_id' => $this->plan_value_id,
            'inputs_id' => $this->inputs_id,
            'user_id' => $this->user_id,
            'post_date' => $this->post_date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'fact', $this->fact]);

        return $dataProvider;
    }
}
