<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\forms\mobilization\HouseholdsWelfare */

$this->title = Yii::t('app', 'Add household welfare');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Households welfare'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="householdwelfare-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>