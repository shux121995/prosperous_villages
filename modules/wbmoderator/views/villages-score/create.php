<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\forms\mobilization\VillagesScore */

$this->title = Yii::t('app', 'Add village score');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Villages Score'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>