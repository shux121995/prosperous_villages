<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\forms\mobilization\VillagesScore */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="householdwelfare-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'region_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Region::find()->all(), 'id', 'title'), ['prompt' => 'Select Village']) ?>

    <?= $form->field($model, 'district_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\District::find()->all(), 'id', 'title'), ['prompt' => 'Select Village']) ?>

    <?= $form->field($model, 'locality_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Locality::find()->all(), 'id', 'title'), ['prompt' => 'Select Village']) ?>

    <?= $form->field($model, 'remoteness')->textInput() ?>

    <?= $form->field($model, 'water')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>