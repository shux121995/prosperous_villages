<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\forms\mobilization\Aholi */

$this->title = Yii::t('app', 'Add population');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Population'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aholi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
