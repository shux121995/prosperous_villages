<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\forms\mobilization\Aholi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="aholi-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'region_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Region::find()->all(), 'id', 'title'), ['prompt' => 'Select Village']) ?>

    <?= $form->field($model, 'district_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\District::find()->all(), 'id', 'title'), ['prompt' => 'Select Village']) ?>

    <?= $form->field($model, 'locality_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Locality::find()->all(), 'id', 'title'), ['prompt' => 'Select Village']) ?>

    <?= $form->field($model, 'aholi_soni')->textInput() ?>

    <?= $form->field($model, 'xonadon_soni')->textInput() ?>

    <?= $form->field($model, 'oila_soni')->textInput() ?>

    <?= $form->field($model, 'erkak_soni')->textInput() ?>

    <?= $form->field($model, 'ayol_soni')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
