<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\wbmoderator\models\SearchMduImiScore */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'MDU IMI Scores');
$this->params['breadcrumbs'][] = $this->title;
?>
<div>

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Add MDU IMI Score'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute'=>'locality_id',
                'value'=>'locality.title',
                'filter' => \kartik\select2\Select2::widget([
                    'name' => 'SearchMduImiScore[locality_id]',
                    'data' => ArrayHelper::map(\app\models\Locality::find()->asArray()->all(), 'id', 'title'),
                    'value' => $searchModel->locality_id,
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'Enter village'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'selectOnClose' => true,
                    ]
                ])
            ],
            'score',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>