<?php

use yii\grid\ActionColumn;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\wbregion\models\SearchResettlement*/
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Resettlement Monitoring Form');
$this->params['breadcrumbs'][] = $this->title;
?>
<div>

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Add Resettlement Form'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],

        [
            'attribute'=>'region_id',
            'value'=>'locality.district.region.title',
            'filter' => \kartik\select2\Select2::widget([
                'name' => 'SearchResettlement[region_id]',
                'data' => ArrayHelper::map(\app\models\Region::find()->asArray()->all(), 'id', 'title'),
                'value' => $searchModel->region_id,
                'options' => [
                    'class' => 'form-control',
                    'placeholder' => 'Enter region'
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'selectOnClose' => true,
                ]
            ])
        ],
        [
            'attribute'=>'district_id',
            'value'=>'locality.district.title',
            'filter' => \kartik\select2\Select2::widget([
                'name' => 'SearchResettlement[district_id]',
                'data' => ArrayHelper::map(\app\models\District::find()->asArray()->all(), 'id', 'title'),
                'value' => $searchModel->district_id,
                'options' => [
                    'class' => 'form-control',
                    'placeholder' => 'Enter district'
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'selectOnClose' => true,
                ]
            ])
        ],
        [
            'attribute'=>'locality_id',
            'value'=>'locality.title',
            'filter' => \kartik\select2\Select2::widget([
                'name' => 'SearchResettlement[locality_id]',
                'data' => ArrayHelper::map(\app\models\Locality::find()->asArray()->all(), 'id', 'title'),
                'value' => $searchModel->locality_id,
                'options' => [
                    'class' => 'form-control',
                    'placeholder' => 'Enter village'
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'selectOnClose' => true,
                ]
            ])
        ],
        [
            'attribute' => 'form2_1',
            'format' => 'raw',
            'value' => function ($data) {
                return Html::a(Html::encode($data->form2_1),$data->form2_1_file, ['target'=> '_blank']);
            },
        ],
        [
            'class' => 'kartik\grid\EditableColumn',
            'attribute' => 'visibility',
            'editableOptions'=> [
                'inputType' => \kartik\editable\Editable::INPUT_CHECKBOX,
                'formOptions' => ['action' => ['edits']],
                'displayValueConfig' => [0 => Yii::t('app','No'), 1 => Yii::t('app','Yes')],
            ],
        ],

        ['class' => ActionColumn::className(),'template'=>'{view} {delete}'],
    ];
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
        'pjax' => true,
        'bordered' => true,
        'striped' => false,
        'condensed' => false,
        'responsive' => false,
        'hover' => true,
    ]); ?>
</div>