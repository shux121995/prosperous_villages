<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\forms\infrastructure_and_finance\Infra_and_finan */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Finance Monitoring Form'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div>

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'region_id',
            'district_id',
            'locality_id',
            'sub_id',
            'sub_work_id',
            'contract_number',
            'contract_start',
            'contract_end',
            'total_contract_value',
            'total_undis_amount',
            'status_of_subproject',
        ],
    ]) ?>

</div>