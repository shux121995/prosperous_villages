<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\wbmoderator\models\SearchInfraAndFinan */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Finance Monitoring Form');
$this->params['breadcrumbs'][] = $this->title;
?>
<div>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => ['style' => 'width:100%'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute'=>'region_id',
                'value'=>'locality.district.region.title',
                'filter' => \kartik\select2\Select2::widget([
                    'name' => 'SearchInfraAndFinan[region_id]',
                    'data' => ArrayHelper::map(\app\models\Region::find()->asArray()->all(), 'id', 'title'),
                    'value' => $searchModel->region_id,
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'Enter region'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'selectOnClose' => true,
                    ]
                ])
            ],
            [
                'attribute'=>'district_id',
                'value'=>'locality.district.title',
                'filter' => \kartik\select2\Select2::widget([
                    'name' => 'SearchInfraAndFinan[district_id]',
                    'data' => ArrayHelper::map(\app\models\District::find()->asArray()->all(), 'id', 'title'),
                    'value' => $searchModel->district_id,
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'Enter district'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'selectOnClose' => true,
                    ]
                ])
            ],
            [
                'attribute'=>'locality_id',
                'value'=>'locality.title',
                'filter' => \kartik\select2\Select2::widget([
                    'name' => 'SearchInfraAndFinan[locality_id]',
                    'data' => ArrayHelper::map(\app\models\Locality::find()->asArray()->all(), 'id', 'title'),
                    'value' => $searchModel->locality_id,
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'Enter village'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'selectOnClose' => true,
                    ]
                ])
            ],
            [
                'attribute'=>'sub_id',
                'value'=>'subproject.title_en',
            ],
            [
                'attribute'=>'sub_work_id',
                'value'=>'work.title_en',
            ],
            'contract_number',
            'contract_start',
            'contract_end',
            'total_contract_value',
            'total_undis_amount',
            [
                'attribute'=>'status_of_subproject',
                'value'=>'status.title_en',
            ],

            ['class' => 'yii\grid\ActionColumn','template'=>'{view} {update}', 'header'=>"Actions"],
        ],
    ]); ?>
</div>