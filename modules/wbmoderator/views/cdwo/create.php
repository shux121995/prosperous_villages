<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\forms\mobilization\Cdwo */

$this->title = Yii::t('app', 'Add CDWO');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'CDWO'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>