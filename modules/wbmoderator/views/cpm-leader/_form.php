<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\forms\mobilization\CpmLeader */
/* @var $form yii\widgets\ActiveForm */
?>

<div>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'locality_id')->widget(\kartik\select2\Select2::className(), [
        'data'=> ArrayHelper::map(\app\models\Locality::find()->asArray()->all(), 'id', 'title'),
        'options'=>['placeholder'=>'Select a village...'],
        'pluginOptions'=>[
            'allowClear'=>true
        ],
    ]); ?>

    <?= $form->field($model, 'name')->textInput() ?>

    <?= $form->field($model, 'phone')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>