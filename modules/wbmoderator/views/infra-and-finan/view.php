<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\forms\infrastructure_and_finance\Infra_and_finan */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Infrastructure and Finance'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div>

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'region_id',
            'district_id',
            'locality_id',
            'sub_id',
            'sub_unique',
            'year',
            'sub_work_id',
            'direct_b',
            'indirect_b',
            'land_allocation',
            'topograpic',
            'architectual',
            'gen_plan_des',
            'gen_plan_progress',
            'proj_des_des',
            'proj_des_progress',
            'sta_env_exper',
            'sta_min_exper',
            'res_contractor',
            'constraction_start',
            'constraction_end',
            'contract_number',
            //'contract_start',
            //'contract_end',
            'total_contract_value',
            //'total_undis_amount',
            //'status_of_subproject',
            'sub_progress',
            'sub_acceptance',
            'sub_before',
            'sub_during',
            'sub_after',
        ],
    ]) ?>

</div>