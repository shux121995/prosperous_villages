<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\forms\infrastructure_and_finance\Infra_and_finan */

$this->title = Yii::t('app', 'Add Infrastructure and Finance');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Infrastructure and Finance'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>