<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\forms\infrastructure_and_finance\Infra_and_finan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="householdwelfare-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'region_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Region::find()->all(), 'id', 'title'), ['prompt' => 'Select Village',]) ?>

    <?= $form->field($model, 'district_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\District::find()->all(), 'id', 'title'), ['prompt' => 'Select Village',]) ?>

    <?= $form->field($model, 'locality_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Locality::find()->all(), 'id', 'title'), ['prompt' => 'Select Village',]) ?>

    <?= $form->field($model, 'sub_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\WbTypes::find()->all(), 'id', 'title'), ['prompt' => 'Select Subproject',]) ?>

    <?= $form->field($model, 'year')->dropDownList(Yii::$app->params['years'], ['prompt' => 'Select Year']) ?>

    <?= $form->field($model, 'sub_work_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\WbSubTypes::find()->all(), 'id', 'title'), ['prompt' => 'Select Subproject work',]) ?>

    <?= $form->field($model, 'direct_b')->textInput() ?>
    <?= $form->field($model, 'indirect_b')->textInput() ?>

    <?= $form->field($model, 'land_allocation')->textInput() ?>
    <?= $form->field($model, 'topograpic')->textInput() ?>
    <?= $form->field($model, 'architectual')->textInput() ?>

    <?= $form->field($model, 'gen_plan_des')->textInput() ?>
    <?= $form->field($model, 'gen_plan_progress')->textInput() ?>

    <?= $form->field($model, 'proj_des_des')->textInput() ?>
    <?= $form->field($model, 'proj_des_progress')->textInput() ?>
    <?= $form->field($model, 'sta_env_exper')->textInput() ?>
    <?= $form->field($model, 'sta_min_exper')->textInput() ?>



    <?= $form->field($model, 'constraction_start')->widget(\kartik\date\DatePicker::className(),['pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'autoclose' => true,
        'todayHighlight' => true,
    ],])?>

    <?= $form->field($model, 'constraction_end')->widget(\kartik\date\DatePicker::className(),['pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'autoclose' => true,
        'todayHighlight' => true,
    ],])?>

    <?= $form->field($model, 'sub_progress')->textInput() ?>
    <?= $form->field($model, 'sub_acceptance')->textInput() ?>

    <?= $form->field($model, 'sub_before')->fileInput() ?>
    <?= $form->field($model, 'sub_during')->fileInput() ?>
    <?= $form->field($model, 'sub_after')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>