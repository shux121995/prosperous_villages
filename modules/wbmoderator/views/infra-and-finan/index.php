<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\wbmoderator\models\SearchInfraAndFinan */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Infrastructure and Finance');
$this->params['breadcrumbs'][] = $this->title;
?>
<div>

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Add Infrastructure and Finance'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => ['style' => 'width:100%'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute'=>'region_id',
                'value'=>'locality.district.region.title',
                'filter' => \kartik\select2\Select2::widget([
                    'name' => 'SearchInfraAndFinan[region_id]',
                    'data' => ArrayHelper::map(\app\models\Region::find()->asArray()->all(), 'id', 'title'),
                    'value' => $searchModel->region_id,
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'Enter region'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'selectOnClose' => true,
                    ]
                ])
            ],
            [
                'attribute'=>'district_id',
                'value'=>'locality.district.title',
                'filter' => \kartik\select2\Select2::widget([
                    'name' => 'SearchInfraAndFinan[district_id]',
                    'data' => ArrayHelper::map(\app\models\District::find()->asArray()->all(), 'id', 'title'),
                    'value' => $searchModel->district_id,
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'Enter district'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'selectOnClose' => true,
                    ]
                ])
            ],
            [
                'attribute'=>'locality_id',
                'value'=>'locality.title',
                'filter' => \kartik\select2\Select2::widget([
                    'name' => 'SearchInfraAndFinan[locality_id]',
                    'data' => ArrayHelper::map(\app\models\Locality::find()->asArray()->all(), 'id', 'title'),
                    'value' => $searchModel->locality_id,
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'Enter village'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'selectOnClose' => true,
                    ]
                ])
            ],
            [
                'attribute'=>'sub_id',
                'value'=>'subproject.title_en',
            ],
            'sub_unique',
            'year',
            [
                'attribute'=>'sub_work_id',
                'value'=>'work.title_en',
            ],
            'direct_b',
            'indirect_b',
            'land_allocation',
            'topograpic',
            'architectual',
            'gen_plan_des',
            'gen_plan_progress',
            'proj_des_des',
            'proj_des_progress',
            'sta_env_exper',
            'sta_min_exper',
            [
                'attribute'=>'res_contractor',
                'value'=>'contractor.title_en',
            ],
            'constraction_start',
            'constraction_end',
            'contract_number',
            //'contract_start',
            //'contract_end',
            'total_contract_value',
            //'total_undis_amount',
            //'status_of_subproject',
            'sub_progress',
            'sub_acceptance',
            [
                'attribute' => 'sub_before',
                'format' => 'html',
                'value' => function ($data) {
                    return Html::img(Yii::$app->request->BaseUrl.$data['sub_before'],
                        ['width' => '60px'] );
                },
            ],
            [
                'attribute' => 'sub_during',
                'format' => 'html',
                'value' => function ($data) {
                    return Html::img(Yii::$app->request->BaseUrl.$data['sub_during'],
                        ['width' => '60px']);
                },
            ],
            [
                'attribute' => 'sub_after',
                'format' => 'html',
                'value' => function ($data) {
                    return Html::img(Yii::$app->request->BaseUrl.$data['sub_after'],
                        ['width' => '60px']);
                },

            ],



            ['class' => 'yii\grid\ActionColumn',
                'header'=>"Actions"],
        ],
    ]); ?>
</div>