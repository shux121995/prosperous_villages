<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\forms\ess\ProjectDesignStep */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Project Design Step'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div>

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'region_id',
            'district_id',
            'locality_id',
            'form1_1',
            'form1_2',
            'form1_3',
            'form1_4',
            'form1_5',
            'step_2',
            'step_3',
            'step_4',
            'step_5',
            'form1_1_file',
            'form1_2_file',
            'form1_3_file',
            'form1_4_file',
            'form1_5_file',
            'step_2_file',
            'step_3_file',
            'step_4_file',
            'step_5_file',
        ],
    ]) ?>

</div>