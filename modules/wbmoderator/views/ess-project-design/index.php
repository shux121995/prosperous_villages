<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\wbmoderator\models\SearchProjectDesignStepRegion */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Project Design Step');
$this->params['breadcrumbs'][] = $this->title;
?>
<div>

    <h1><?= Html::encode($this->title) ?></h1>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => ['style' => 'width:100%'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute'=>'region_id',
                'value'=>'locality.district.region.title',
                'filter' => \kartik\select2\Select2::widget([
                    'name' => 'SearchProjectDesignStepRegion[region_id]',
                    'data' => ArrayHelper::map(\app\models\Region::find()->asArray()->all(), 'id', 'title'),
                    'value' => $searchModel->region_id,
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'Enter region'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'selectOnClose' => true,
                    ]
                ])
            ],
            [
                'attribute'=>'district_id',
                'value'=>'locality.district.title',
                'filter' => \kartik\select2\Select2::widget([
                    'name' => 'SearchProjectDesignStepRegion[district_id]',
                    'data' => ArrayHelper::map(\app\models\District::find()->asArray()->all(), 'id', 'title'),
                    'value' => $searchModel->district_id,
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'Enter district'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'selectOnClose' => true,
                    ]
                ])
            ],
            [
                'attribute'=>'locality_id',
                'value'=>'locality.title',
                'filter' => \kartik\select2\Select2::widget([
                    'name' => 'SearchProjectDesignStepRegion[locality_id]',
                    'data' => ArrayHelper::map(\app\models\Locality::find()->asArray()->all(), 'id', 'title'),
                    'value' => $searchModel->locality_id,
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'Enter village'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'selectOnClose' => true,
                    ]
                ])
            ],
            [
                'attribute' => 'form1_1',
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a(Html::encode($data->form1_1),$data->form1_1_file, ['target'=> '_blank']);
                },
            ],
            [
                'attribute' => 'form1_2',
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a(Html::encode($data->form1_2),$data->form1_2_file, ['target'=> '_blank']);
                },
            ],
            [
                'attribute' => 'form1_3',
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a(Html::encode($data->form1_3),$data->form1_3_file, ['target'=> '_blank']);
                },
            ],
            [
                'attribute' => 'form1_4',
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a(Html::encode($data->form1_4),$data->form1_4_file, ['target'=> '_blank']);
                },
            ],
            [
                'attribute' => 'form1_5',
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a(Html::encode($data->form1_4),$data->form1_5_file, ['target'=> '_blank']);
                },
            ],
            [
                'attribute' => 'step_2',
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a(Html::encode($data->step_2),$data->step_2_file, ['target'=> '_blank']);
                },
            ],
            [
                'attribute' => 'step_3',
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a(Html::encode($data->step_3),$data->step_3_file, ['target'=> '_blank']);
                },
            ],
            [
                'attribute' => 'step_4',
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a(Html::encode($data->step_4),$data->step_4_file, ['target'=> '_blank']);
                },
            ],
            [
                'attribute' => 'step_5',
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a(Html::encode($data->step_5),$data->step_5_file, ['target'=> '_blank']);
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>