<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\forms\infrastructure_and_finance\Infra_and_finan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="householdwelfare-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'region_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Region::find()->all(), 'id', 'title'), ['prompt' => 'Select Village','disabled'=>'disabled']) ?>

    <?= $form->field($model, 'district_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\District::find()->all(), 'id', 'title'), ['prompt' => 'Select Village','disabled'=>'disabled']) ?>

    <?= $form->field($model, 'locality_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Locality::find()->all(), 'id', 'title'), ['prompt' => 'Select Village','disabled'=>'disabled']) ?>

    <?= $form->field($model, 'sub_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\WbTypes::find()->all(), 'id', 'title'), ['prompt' => 'Select Subproject','disabled'=>'disabled']) ?>

    <?= $form->field($model, 'sub_work_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\WbSubTypes::find()->all(), 'id', 'title'), ['prompt' => 'Select Subproject work','disabled'=>'disabled']) ?>

    <?= $form->field($model, 'res_contractor')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\WbDevelopers::find()->all(), 'id', 'title_en'), ['prompt' => 'Select Contractor',]) ?>

    <?= $form->field($model, 'contract_number')->textInput() ?>

    <?= $form->field($model, 'total_contract_value')->textInput() ?>

    <?= $form->field($model, 'contract_start')->widget(\kartik\date\DatePicker::className(),['pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'autoclose' => true,
        'todayHighlight' => true,
    ],])?>

    <?= $form->field($model, 'contract_end')->widget(\kartik\date\DatePicker::className(),['pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'autoclose' => true,
        'todayHighlight' => true,
    ],])?>

       <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>