<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\forms\mobilization\MduMembers */

$this->title = Yii::t('app', 'Add Mdu members');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mdu Members'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>