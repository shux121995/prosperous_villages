<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\forms\mobilization\VillagesVoters */
/* @var $form yii\widgets\ActiveForm */
?>

<div>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'region_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Region::find()->all(), 'id', 'title'), ['prompt' => 'Select Village']) ?>

    <?= $form->field($model, 'district_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\District::find()->all(), 'id', 'title'), ['prompt' => 'Select Village']) ?>

    <?= $form->field($model, 'locality_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Locality::find()->all(), 'id', 'title'), ['prompt' => 'Select Village']) ?>

    <?= $form->field($model, 'male')->textInput() ?>

    <?= $form->field($model, 'female')->textInput() ?>

    <?= $form->field($model, 'p_male')->textInput() ?>

    <?= $form->field($model, 'p_female')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>