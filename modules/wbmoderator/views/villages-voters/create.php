<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\forms\mobilization\VillagesVoters */

$this->title = Yii::t('app', 'Add village voters');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Villages Voters'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>