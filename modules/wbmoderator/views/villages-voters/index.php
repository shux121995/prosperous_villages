<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\wbmoderator\models\SearchVillagesVoters */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Villages Voters');
$this->params['breadcrumbs'][] = $this->title;
?>
<div>

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Add Village Voters'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute'=>'region_id',
                'value'=>'locality.district.region.title',
                'filter' => \kartik\select2\Select2::widget([
                    'name' => 'SearchVillagesVoters[region_id]',
                    'data' => ArrayHelper::map(\app\models\Region::find()->asArray()->all(), 'id', 'title'),
                    'value' => $searchModel->region_id,
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'Enter region'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'selectOnClose' => true,
                    ]
                ])
            ],
            [
                'attribute'=>'district_id',
                'value'=>'locality.district.title',
                'filter' => \kartik\select2\Select2::widget([
                    'name' => 'SearchVillagesVoters[district_id]',
                    'data' => ArrayHelper::map(\app\models\District::find()->asArray()->all(), 'id', 'title'),
                    'value' => $searchModel->district_id,
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'Enter district'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'selectOnClose' => true,
                    ]
                ])
            ],
            [
                'attribute'=>'locality_id',
                'value'=>'locality.title',
                'filter' => \kartik\select2\Select2::widget([
                    'name' => 'SearchVillagesVoters[locality_id]',
                    'data' => ArrayHelper::map(\app\models\Locality::find()->asArray()->all(), 'id', 'title'),
                    'value' => $searchModel->locality_id,
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'Enter village'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'selectOnClose' => true,
                    ]
                ])
            ],
            'male',
            'female',
            'total',
            'p_male',
            'p_female',
            'p_total',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>