<?php

namespace app\modules\wbmoderator;

/**
 * wbadmin module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\wbmoderator\controllers';
    public $layout = '//wbmoderator';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        \Yii::$app->setHomeUrl('/wbmoderator');
        parent::init();

        // custom initialization code goes here
    }
}