<?php


namespace app\modules\wbmoderator\models;


use app\models\forms\mobilization\MduImiScore;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class SearchMduImiScore extends MduImiScore
{
    public function rules()
    {
        return [
            [['id','locality_id'], 'integer'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    public function search($params)
    {
        $query = MduImiScore::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'locality_id' => $this->locality_id,
        ]);

        return $dataProvider;
    }
}