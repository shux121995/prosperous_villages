<?php

namespace app\modules\wbmoderator\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\forms\mobilization\Aholi;

/**
 * AholiSearch represents the model behind the search form of `app\models\Aholi`.
 */
class AholiSearch extends Aholi
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'region_id', 'district_id', 'locality_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Aholi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'region_id' => $this->region_id,
            'district_id' => $this->district_id,
            'locality_id' => $this->locality_id,
            //'aholi_soni' => $this->aholi_soni,
            //'xonadon_soni' => $this->xonadon_soni,
            //'oila_soni' => $this->oila_soni,
            //'erkak_soni' => $this->erkak_soni,
            //'ayol_soni' => $this->ayol_soni,
        ]);

        return $dataProvider;
    }
}
