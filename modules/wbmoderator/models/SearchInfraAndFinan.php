<?php


namespace app\modules\wbmoderator\models;


use app\models\forms\infrastructure_and_finance\Infra_and_finan;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class SearchInfraAndFinan extends Infra_and_finan
{
    public function rules()
    {
        return [
            [['id','region_id','district_id','locality_id'], 'integer'],
            [['sub_unique'], 'string'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    public function search($params)
    {
        $query = Infra_and_finan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'region_id' => $this->region_id,
            'district_id' => $this->district_id,
            'locality_id' => $this->locality_id,
            'sub_unique' => $this->sub_unique,
        ]);

        return $dataProvider;
    }
}