<?php


namespace app\modules\wbmoderator\controllers;


use app\models\forms\infrastructure_and_finance\Infra_and_finan;
use app\modules\wbmoderator\models\SearchInfraAndFinan;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class FinanceController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new SearchInfraAndFinan();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $sub_before = $model->sub_before;
        $sub_during = $model->sub_during;
        $sub_after = $model->sub_after;

        if ($model->load(Yii::$app->request->post())) {

            $model->sub_before = UploadedFile::getInstance($model, 'sub_before');
            $model->sub_during = UploadedFile::getInstance($model, 'sub_during');
            $model->sub_after = UploadedFile::getInstance($model, 'sub_after');

            if(isset($model->sub_before)) {
                $model->sub_before->saveAs($_SERVER['DOCUMENT_ROOT'] . "/wb/documents/infrastructure/before/" . time() . '_' . $model->sub_before->baseName . "." . $model->sub_before->extension);
                $model->sub_before = "/wb/documents/infrastructure/before/" .time() . '_' . $model->sub_before->baseName . "." . $model->sub_before->extension;
            }else{
                $model->sub_before = $sub_before;
            }
            if(isset($model->sub_during)) {
                $model->sub_during->saveAs($_SERVER['DOCUMENT_ROOT'] . "/wb/documents/infrastructure/during/" . time() . '_' . $model->sub_during->baseName . "." . $model->sub_during->extension);
                $model->sub_during = "/wb/documents/infrastructure/during/" .time() . '_' . $model->sub_during->baseName . "." . $model->sub_during->extension;
            }else{
                $model->sub_during = $sub_during;
            }
            if(isset($model->sub_after)) {
                $model->sub_after->saveAs($_SERVER['DOCUMENT_ROOT'] . "/wb/documents/infrastructure/after/" . time() . '_' . $model->sub_after->baseName . "." . $model->sub_after->extension);
                $model->sub_after = "/wb/documents/infrastructure/after/" .time() . '_' . $model->sub_after->baseName . "." . $model->sub_after->extension;
            }else{
                $model->sub_after = $sub_after;
            }

            $model->save();

            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    protected function findModel($id)
    {
        if (($model = Infra_and_finan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}