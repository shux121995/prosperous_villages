<?php


namespace app\modules\wbmoderator\controllers;


use app\models\forms\ess\ProjectDesignStep;
use app\modules\wbmoderator\models\SearchProjectDesignStep;
use app\widgets\Alert;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class EssProjectDesignController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new SearchProjectDesignStep();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $form1_4_file = $model->form1_4_file;
        $form1_5_file = $model->form1_5_file;
        $step_4_file = $model->step_4_file;

        if ($model->load(Yii::$app->request->post())) {

            $model->form1_4_file = UploadedFile::getInstance($model, 'form1_4_file');
            $model->form1_5_file = UploadedFile::getInstance($model, 'form1_5_file');
            $model->step_4_file = UploadedFile::getInstance($model, 'step_4_file');

            if(isset($model->form1_4_file)) {
                $model->form1_4_file->saveAs($_SERVER['DOCUMENT_ROOT'] . "/wb/documents/ess/" . time() . '_' . $model->form1_4_file->baseName . "." . $model->form1_4_file->extension);
                $model->form1_4_file = "/wb/documents/ess/" .time() . '_' . $model->form1_4_file->baseName . "." . $model->form1_4_file->extension;
            }else{
                $model->form1_4_file = $form1_4_file;
            }
            if(isset($model->form1_5_file)) {
                $model->form1_5_file->saveAs($_SERVER['DOCUMENT_ROOT'] . "/wb/documents/ess/" . time() . '_' . $model->form1_5_file->baseName . "." . $model->form1_5_file->extension);
                $model->form1_5_file = "/wb/documents/ess/" .time() . '_' . $model->form1_5_file->baseName . "." . $model->form1_5_file->extension;
            }else{
                $model->form1_5_file = $form1_5_file;
            }
            if(isset($model->step_4_file)) {
                $model->step_4_file->saveAs($_SERVER['DOCUMENT_ROOT'] . "/wb/documents/ess/" . time() . '_' . $model->step_4_file->baseName . "." . $model->step_4_file->extension);
                $model->step_4_file = "/wb/documents/ess/" .time() . '_' . $model->step_4_file->baseName . "." . $model->step_4_file->extension;
            }else{
                $model->step_4_file = $step_4_file;
            }

            $model->save();

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    protected function findModel($id)
    {
        if (($model = ProjectDesignStep::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}