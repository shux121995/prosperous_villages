<?php
/**
 * Created by PhpStorm.
 * User: Muxtorov Ulugbek
 * Date: 13.03.2019
 * Time: 12:28
 */
return [
    //Umumiy
    'Жой номини киритинг...' => 'Type locality...',
    'Izoh qoldirish' => 'Feedback',
    '<span class="color-secondary">Qishloq</span><br/><span class="color-primary">infratuzilmasini rivojlantirish</span>' => '<span class="color-secondary">Rural</span><br><span class="color-primary">infrastructure development</span>',
    'Қидирув...' => 'Searching...',
    'Qilingan ishlar boyicha <span class="bigged">natijalar:</span>' => 'Results of work <span class="bigged">done:</span>',
    'млн.сум' => 'bl.sum',
    '© O‘ZBEKISTON RESPUBLIKASI IQTISODIY TARAQQIYOT VA KAMBAG‘ALLIKNI QISQARTIRISH VAZIRLIGI' => '© MINISTRY OF ECONOMIC DEVELOPMENT AND POVERTY REDUCTION OF THE REPUBLIC OF UZBEKISTAN',
    'Bosh sahifa' => 'Home page',
    'Дастур ҳақида' => 'About program',
    'Ҳамкорларимиз' => 'Partners',
    'Янгиликлар' => 'News',
    'Контактлар' => 'Contact',
    'Kirish' => 'Login',
    'Agar parolni unutgan bo\'lsangiz bu yerni bosing:' => 'If you forgot your password, click here:',
    'tiklash!' => 'recovery!',
    'Login yo\'ki e-mail' => 'Login or e-mail',
    'Parol' => 'Password',
    'Eslab qol!' => 'Remember',
    'Жой номини танланг:' => 'Type a village name:',
    'Qilingan ishlar bo\'yicha natijalar' => 'Village Progress Report',
    'Изоҳ қолдиринг ва биринчи бўлинг!' => 'Write a review!',
    'Аҳоли тамонидан қолдирилган изоҳлар' => 'Peoples reviews',
    'Йўналтириладиган маблағлар' => 'Йўналтириладиган маблағлар',
    'Маҳаллий бюджет маблағлари' => 'Local budget funds',
    'Jahon banki' => 'PVP',
    'Obod mahalla' => 'Obod mahalla',
    'Obod qishloq' => 'Obod qishloq',
    'Fact' => 'Fact',
    'Reja' => 'Plan',
    'Молялаштириш' => 'Financing',
    'Сумма' => 'Summary',
    'Taklif' => 'Suggestion',
    'Murojat' => 'Appeal',
    'Boshqa' => 'Other',
    'Yangiliklar' => 'News',
    'Loyiha haqida' => 'About project',
    'Meyoriy hujjatlar' => 'Documents',
    'Ochiq malumotlar' => 'Open data',
    'Bog\'lanish' => 'Contact us',
    'Bosh pudratchi: ' => 'General contractor: ',
    'Tarmoq jadvali asosida amalga oshirilayotgan tadbirlar jadval ko\'rinishda' => 'Works plans are tabulated in the table',
    'Masalalar:' => 'Project works planned:',
    'Jami:' => 'Summary:',
    'Mobil ilova'=>'Mobile application'
];