<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\BozorInfratuzilmasiAmaldaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bozor-infratuzilmasi-amalda-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'bozor_id') ?>

    <?= $form->field($model, 'loyiha_qiy_amalda') ?>

    <?= $form->field($model, 'mol_mab_amalda') ?>

    <?= $form->field($model, 'moliya_kredit_amalda') ?>

    <?php // echo $form->field($model, 'moliya_invest_amalda') ?>

    <?php // echo $form->field($model, 'ish_urin_amalda') ?>

    <?php // echo $form->field($model, 'ishga_tush_mud_amalda') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
