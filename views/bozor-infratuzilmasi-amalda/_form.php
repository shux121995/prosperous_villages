<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BozorInfratuzilmasiAmalda */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bozor-infratuzilmasi-amalda-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'bozor_id')->dropDownList((new \app\models\BozorInfratuzilmasi)->getBozorInfratuzilmasiIdAndTitleByPlanId(),['prompt'=>'Бозор инфратузилмаси объектини танланг']) ?>

    <?= $form->field($model, 'loyiha_qiy_amalda')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mol_mab_amalda')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'moliya_kredit_amalda')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'moliya_invest_amalda')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ish_urin_amalda')->textInput() ?>

    <?= $form->field($model, 'ishga_tush_mud_amalda')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сақлаш', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
