<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BozorInfratuzilmasiAmalda */

$this->title = 'Фактни ўзгартириш: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Бозор инфратузилмаси объектлари', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Ўзгартириш';
?>
<div class="bozor-infratuzilmasi-amalda-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
