<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LocalityPlans */

$this->title = Yii::t('app', 'Йил плани учун махалла ёки кишлок кушиш');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Йил плани'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="locality-plans-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
