<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\LocalityPlansSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Ахоли сони');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="locality-plans-index">

    <h1><?= Html::a(Yii::t('app', 'Киритилмаганлари'), ['index'], ['class' => 'pull-right btn btn-danger']) ?><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'year',
            [
                'label'=>'Махалла ёки кишлок',
                'value'=>'locality.title'
            ],
            [
                'label'=>'Туман',
                'value'=>'locality.district.title'
            ],
            [
                'label'=>'Ахоли сони',
                 'value'=>'aholis.aholi_soni'
            ],
            [
                'label'=>'Хонадон сони',
                'value'=>'aholis.xonadon_soni'
            ],
            [
                'label'=>'Оила сони',
                'value'=>'aholis.oila_soni'
            ]

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
<div class="modal remote fade" id="modalAholi">
    <div class="modal-dialog">
        <div class="modal-content loader-lg"></div>
    </div>
</div>