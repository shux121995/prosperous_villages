<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\LandscapingWorks */
/* @var $form yii\widgets\ActiveForm */

//list of regions
$region = \app\models\Region::find()->select(['id', 'title'])->all();

//list of districts
$district = \app\models\District::find()->select(['id', 'title']);
if (!empty($model->region_id)) $district->where(['region_id' => $model->region_id]);
$district = $district->all();

//list of villages
$locality = \app\models\Locality::find()->select(['id', 'title'])->all();

?>

<div class="landscaping-works-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'year')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'region_id')->dropDownList(ArrayHelper::map($region, 'id', 'title'),
        [
            'prompt' => 'Регион танланг',
            'onchange' => '
                $.get( "' . Url::to('districts') . '", { year: $(\'#' . Html::getInputId($model, 'year') . '\').val(), id: $(this).val() } )
                    .done(function( data ) {
                        $( "#' . Html::getInputId($model, 'district_id') . '" ).html( data );
                    }
                );
                $.get( "' . Url::to('locals') . '", { year: $(\'#' . Html::getInputId($model, 'year') . '\').val(), id: null, region_id: $(this).val() } )
                    .done(function( data ) {
                        $( "#' . Html::getInputId($model, 'locality_id') . '" ).html( data );
                    }
                );
            '
        ]
    )?>

    <?= $form->field($model, 'district_id')->dropDownList(ArrayHelper::map($district, 'id', 'title'),
        [
            'prompt' => 'Туман ёки шахарни танланг',
            'onchange' => '
                $.get( "' . Url::to('locals') . '", { year: $(\'#' . Html::getInputId($model, 'year') . '\').val(), id: $(this).val() } )
                    .done(function( data ) {
                        $( "#' . Html::getInputId($model, 'locality_id') . '" ).html( data );
                    }
                );
            '
        ]
    ) ?>

    <?= $form->field($model, 'locality_id')->dropDownList(ArrayHelper::map($locality, 'id', 'title'), ['prompt' => 'Кишлок ёки махаллани танланг']) ?>

    <?= $form->field($model, 'aholi_soni')->textInput(['maxlength' => true])->label("Аҳоли сони (нафар)") ?>
    <?= $form->field($model, 'honadonlar_soni')->textInput(['maxlength' => true])->label("Хонадонлар сони") ?>
    <?= $form->field($model, 'tashkilot_nomi')->textInput(['maxlength' => true])->label("Бириктирилган лойиҳа ташкилоти номи") ?>
    <?= $form->field($model, 'sektor_raqami')->textInput(['maxlength' => true])->label("Сектор рақами") ?>
    <?= $form->field($model, 'viloyatdan_masul')->textInput(['maxlength' => true])->label("Вилоятдан масъул (сектор раҳбари)") ?>
    <?= $form->field($model, 'tumandan_masul')->textInput(['maxlength' => true])->label("Тумандан масъул (сектор раҳбари)") ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
