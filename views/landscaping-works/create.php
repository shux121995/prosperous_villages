<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LandscapingWorks */

$this->title = Yii::t('app', '"Обод қишлоқ" дастури доирасида қурилиш ва ободонлаштириш ишлари амалга ошириладиган қишлоқларнинг 
МАНЗИЛЛИ РЎЙХАТИ кушиш');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Йил плани'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="landscaping-works-create">

    <h3 style="text-align: center"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>