<?php
use yii\helpers\Html;
$lang = (new \app\components\LangHelper())->check_lang();
/* @var $this yii\web\View */
/* @var $model bupy7\pages\models\Page */

if (empty($model->title_browser)) {
    $this->title = $model->title;
} else {
    $this->title = $model->title_browser;
}
if (!empty($model->meta_description)) {
    $this->registerMetaTag(['content' => Html::encode($model->meta_description), 'name' => 'description']);
}
if (!empty($model->meta_keywords)) {
    $this->registerMetaTag(['content' => Html::encode($model->meta_keywords), 'name' => 'keywords']);
}
?>
<style type="text/css">
    td {
        text-align: center;
        vertical-align: middle;
    }
</style>

<div class="clearfix">
    <table class="table table-bordered table-hover">
        <thead class="thead-light">
        <tr style="text-align: center">
            <th scope="col" style="width: 30%"><?=Yii::t('app','Title')?></th>
            <th scope="col" style="width: 20%"><?=Yii::t('app','Open')?></th>
            <th scope="col" style="width: 20%"><?=Yii::t('app','Download')?></th>
            <th scope="col" style="width: 20%"><?=Yii::t('app','Url')?></th>
        </tr>
        </thead>
        <tbody>

        <?php foreach (\app\models\Documents::find()->orderBy(['id' => SORT_DESC])->all() as $document):?>

            <tr>
                <th scope="row"><?= $document->getTitle($lang)?></th>
                <td><a href="/../wb/documents/<?=$document->getFile($lang)?>" target="_blank"><i class="fa fa-eye"></i></a></td>
                <td><a href="/../wb/documents/<?=$document->getFile($lang)?>" download="<?=$document->getFile($lang)?>"><i class="fa fa-download"></i></a></td>
                <td><a href="<?=$document->url?>" target="_blank"><?=Yii::t('app','Read more')?></a></td></td>
            </tr>

        <?php endforeach; ?>
        </tbody>
    </table>
</div>
