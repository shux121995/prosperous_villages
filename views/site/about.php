<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model bupy7\pages\models\Page */
/* @var $pui_staffs \app\models\PIUstaff */
$lang = (new \app\components\LangHelper())->check_lang();

if (empty($model->title_browser)) {
    $this->title = $model->title;
} else {
    $this->title = $model->title_browser;
}
if (!empty($model->meta_description)) {
    $this->registerMetaTag(['content' => Html::encode($model->meta_description), 'name' => 'description']);
}
if (!empty($model->meta_keywords)) {
    $this->registerMetaTag(['content' => Html::encode($model->meta_keywords), 'name' => 'keywords']);
}
?>

<?php if ($model->display_title) : ?>

    <?php
    foreach ($pui_staffs as $piu_staff) {?>
    <div class="modal fade" id="<?php echo $piu_staff->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle"><?php echo $piu_staff->full_name?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?php echo $piu_staff->getCV($lang)?>
                </div>
                <div class="modal-footer">
                    <a href="mailto: <?php echo $piu_staff->email?>" class="btn btn-secondary">Email</a>
                </div>
            </div>
        </div>
    </div>
    <?php }?>

    <section id="banner">
        <!--
        <div class="inner">
            <h1>Prosperous Villages</h1>
            <p>The Obod Qishloq state program aims to improve rural residents’ quality of life by constructing new infrastructure, rehabilitating existing infrastructure, and investing in employment-generating opportunities.</p>
        </div>
        -->
        <video autoplay loop muted playsinline src="/../wb/images/banner.mp4"></video>
    </section>

    <!-- Testimonials -->
    <section class="wrapper">
        <div class="inner" align="center">
            <header class="special">
                <br>
                <h4><?=Yii::t('app','Ministry of Economic Development and Poverty Reduction, World Bank and Asian Infrastructure Investment Bank')?></h4>
                <br>
                <p><?=Yii::t('app','The Rural Infrastructure Development (RID) is an initiative of the government of Uzbekistan to assist the Obod Qishloq state program. The RID will trial design adjustments aimed at increasing village participation in project decision-making and oversight, transparency and accountability in project implementation, and the quality and sustainability of subproject investments that can be scaled up through the Obod Qishloq state program. The RID is implemented by the Ministry of Economic Development and Poverty Reduction with support from the World Bank and the Asian Infrastructure Investment Bank. The development objective of the RID is to (i) improve the quality of basic infrastructure, and (ii) strengthen participatory local governance processes in selected qishloqs. The RID will be implemented from 2020 to 2024 in five regions: Jizzakh, Syrdarya, Ferghana, Namangan, and Andijan. Villages covered by other ongoing state infrastructure development programs, including Obod Qishloq (“Prosperous Villages”), Obod Mahalla (“Prosperous Cities”), or Obod Markaz (“Prosperous Towns”) are not eligible for participation in the RID. Eligible villages are prioritized for participation in the RID based on criteria including remoteness from the district center and percentage of the population lacking regular access to clean drinking water.')?>
                </p>
                <br>
            </header>
            <header class="special">
                <br>
                <h4><?=Yii::t('app','The RID will be implemented in accordance with the following principles:')?></h4>
                <br>
                <p style="text-align: left">
                    <?=Yii::t('app','• Community-driven decision-making. The RID will finance subprojects identified and prioritized by communities through participatory and inclusive social analysis that identifies communities’ development needs and presents them in the form of a Qishloq Development Plan accessible to all stakeholders.')?>
                    <br><br>
                    <?=Yii::t('app','• Capacity building for good local governance practices. The RID will support the establishment of inclusive Mahalla Development Units (MDU) that represent and give voice to all residents, including men and women, youth, the poor, and the vulnerable. The RID will also build the capacity of village-level institutions in a variety of areas, including holding participatory and inclusive meetings and decision-making processes and managing the day-to-day development affairs of communities. The RID will also help to link MDUs to other government and nongovernmental agencies/organizations to improve access to services and resources.')?>
                    <br><br>
                    <?=Yii::t('app','• Gender equitable development. The RID will support focus on closing the gender gaps in (i) voice and participation in village-level decision-making, by ensuring that 50% of MDU members are women, and through community outreach to ensure that women (including young women) are able to participate in project planning and decision-making meetings; and (ii) access to services, through subprojects that address the needs of women.')?>
                    <br><br>
                    <?=Yii::t('app','• Transparency and accountability. The RID will support various measures to ensure transparency and accountability during implementation, including outreach and orientation meetings at the national, regional, district, and village levels; information on project implementation accessible through the RID website; community participatory monitoring of subproject procurement and implementation; the use of social audits to measure community perceptions of the quality of implementation; and a grievance redress mechanism that is accessible to all stakeholders.')?>
                    <br><br>
                    <?=Yii::t('app','• Social and environmental sustainability. In line with the GoU’s objective of raising living standards in rural areas, the RID will finance investments and activities that yield positive socioeconomic benefits for rural residents. Activities that involve permanent physical displacement, require forcible evictions, negatively impact residents’ incomes or livelihoods, contribute to child or forced labor, or result in environmental impacts that are large scale and irreversible will not be supported by the RID. The RID will support awareness-raising efforts for local officials and communities, on national laws and regulations pertaining to social and environmental safeguards, and World Bank standards for environmental and social-safeguards-related policies and due process.')?>
                    <br><br>
                    <?=Yii::t('app','Please refer to section “Documents” to get access to detailed RID operational manual, environmental and social safeguards documents.')?>

                </p>
                <br>
            </header>

            <h4><?=Yii::t('app','Project Implementation Unit Staff')?></h4>
            <div class="testimonials">
                <?php
                foreach ($pui_staffs as $piu_staff) {?>
                <section>
                    <div class="content" align="center"  data-toggle="modal" data-target="#<?php echo $piu_staff->id?>">
                        <div class="author">
                            <div class="image">
                                <img src="<?php echo "/../wb/images/PIU_STAFF_IMAGES/" .$piu_staff->id.'_'.$piu_staff->photo?>" alt="PIU staff member photo"/>
                            </div>
                        </div>
                        <blockquote>
                            <br>
                            <p class="credit">
                                <strong><?php echo $piu_staff->full_name?></strong>
                                <br>
                                <span><?php echo $piu_staff->getPosition($lang)?></span>
                            </p>
                        </blockquote>
                    </div>
                </section>
                <?php }?>
            </div>
        </div>
    </section>


    <!-- Scripts
    <script src="/../wb/js/about/jquery.min.js"></script>
    <script src="/../wb/js/about/browser.min.js"></script>
    <script src="/../wb/js/about/breakpoints.min.js"></script>
    <script src="/../wb/js/about/util.js"></script>
    <script src="/../wb/js/about/main.js"></script>
    -->

<?php endif; ?>

<div class="clearfix"></div>


