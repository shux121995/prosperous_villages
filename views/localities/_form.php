<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Locality */
/* @var $form yii\widgets\ActiveForm */
?>
    <style>
        .ui-autocomplete {
            background-color: white;
            width: 300px;
            border: 1px solid #cfcfcf;
            list-style-type: none;
            padding-left: 0px;
        }
    </style>
    <div class="locality-form">

        <?php $form = ActiveForm::begin(); ?>

        <!--<div class="form-group field-locality-district_id has-success">
        <label class="control-label" for="locality-district_id">Region ID</label>
        <?/*= Html::dropDownList('a',null,\yii\helpers\ArrayHelper::map(\app\models\Region::find()->all(),'id','title'),
            [
                'class' => 'form-control',
                'prompt' => Yii::t('app', 'Select region'),
                'onchange' => '
                $.get( "' . \yii\helpers\Url::to('districts') . '", { id: $(this).val() } )
                    .done(function( data ) {
                        $( "#' . Html::getInputId($model, 'district_id') . '" ).html( data );
                    }
                );'
            ]) */?>
        <div class="help-block"></div>
    </div>-->

        <?php //= $form->field($model, 'district_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\District::find()->all(),'id','title')) ?>

        <?= $form->field($model, 'title')->textInput(['maxlength' => true,'readonly'=>true]) ?>

        <?= $form->field($model, 'title_en')->textInput(['maxlength' => true]) ?>

        <?php //= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

        <?php //= $form->field($model, 'type')->dropDownList([ 'qishloq' => 'Qishloq', 'mahalla' => 'Mahalla' ], ['prompt' => '']) ?>

        <?= $form->field($model, 'sector_reg')->dropDownList([ '1' => '1', '2' => '2', '3' => '3', '4' => '4' ], ['prompt' => '']) ?>

        <?= $form->field($model, 'sector_dis')->dropDownList([ '1' => '1', '2' => '2', '3' => '3', '4' => '4' ], ['prompt' => '']) ?>

        <div class="form-group field-locality-district_id has-success">
            <label>Махалла ёки кишлок номини ёзинг(туман номи) ёки картадан жой номига босинг: </label>
            <input class="form-control" id="address" type="text"/>
        </div>
        <div id="map_canvas" style="width:100%; height:350px"></div><br/>

        <?= $form->field($model, 'map_latitude')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'map_longitude')->textInput(['maxlength' => true]) ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
<?php
if($model->map_latitude!=null AND $model->map_longitude!=null){
    $lat = $model->map_latitude;
    $lng = $model->map_longitude;
}else{
    $lat = 41.2994958;
    $lng = 69.24007340000003;
}
$script = <<< JS
ymaps.ready(init);
function init() {
	//var suggest = new ymaps.SuggestView('address');
	var sv = new ymaps.SuggestView('address');
	sv.events.add('select', function (e) {    
		ymaps.geocode(e.get('item').value).then(function (res) {  
			firstGeoObject = res.geoObjects.get(0);
			//myMap.geoObjects.add(res.geoObjects.get(0));
			// Координаты геообъекта.
			coords = firstGeoObject.geometry.getCoordinates(),
			// Область видимости геообъекта.
			bounds = firstGeoObject.properties.get('boundedBy');
				
			// Если метка уже создана – просто передвигаем ее.
			if (myPlacemark) {
				myPlacemark.geometry.setCoordinates(coords);
			}
			// Если нет – создаем.
			else {
				myPlacemark = createPlacemark(coords);
				myMap.geoObjects.add(myPlacemark);
				// Слушаем событие окончания перетаскивания на метке.
				myPlacemark.events.add('dragend', function () {
					//getAddress(coords);
				});
			}		
			myMap.setBounds(bounds);  
			getAddress(coords);
		});
	});
	
    var myPlacemark,
        myMap = new ymaps.Map('map_canvas', {
            center: [$lat, $lng],
            zoom: 9
        }, {
            searchControlProvider: 'yandex#search',
        });
		
    // Слушаем клик на карте.
    myMap.events.add('click', function (e) {
        var coords = e.get('coords');

        // Если метка уже создана – просто передвигаем ее.
        if (myPlacemark) {
            myPlacemark.geometry.setCoordinates(coords);
        }
        // Если нет – создаем.
        else {
            myPlacemark = createPlacemark(coords);
            myMap.geoObjects.add(myPlacemark);
            // Слушаем событие окончания перетаскивания на метке.
            myPlacemark.events.add('dragend', function () {
                getAddress(myPlacemark.geometry.getCoordinates());
            });
        }
        getAddress(coords);
    });

    // Создание метки.
    function createPlacemark(coords) {
        return new ymaps.Placemark(coords, {
            iconCaption: 'Жой номини кидириш...'
        }, {
            preset: 'islands#violetDotIconWithCaption',
            draggable: true
        });
    }

    // Определяем адрес по координатам (обратное геокодирование).
    function getAddress(coords) {
        myPlacemark.properties.set('iconCaption', 'Жой номини кидириш...');
        ymaps.geocode(coords).then(function (res) {
            var firstGeoObject = res.geoObjects.get(0);

            myPlacemark.properties
                .set({
                    // Формируем строку с данными об объекте.
                    iconCaption: [
                        // Название населенного пункта или вышестоящее административно-территориальное образование.
                        firstGeoObject.getLocalities().length ? firstGeoObject.getLocalities() : firstGeoObject.getAdministrativeAreas(),
                        // Получаем путь до топонима, если метод вернул null, запрашиваем наименование здания.
                        firstGeoObject.getThoroughfare() || firstGeoObject.getPremise()
                    ].filter(Boolean).join(', '),
                    // В качестве контента балуна задаем строку с адресом объекта.
                    balloonContent: firstGeoObject.getAddressLine()
                });
			$('#address').val(firstGeoObject.getAddressLine());
			$('#locality-map_latitude').val(coords[0]);
			$('#locality-map_longitude').val(coords[1]);
        });
    }
}
	
JS;

$this->registerJs($script, \yii\web\View::POS_READY);
$this->registerJsFile('https://api-maps.yandex.ru/2.1/?lang=ru_RU&apikey='.Yii::$app->params['ymap_api_key'], ['depends'=>'yii\web\JqueryAsset'], \yii\web\View::POS_READY);
?>