<?php
/**
 * Created by PhpStorm.
 * User: Muxtorov Ulugbek
 * Date: 13.09.2019
 * Time: 13:57
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
//use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $model bupy7\pages\models\Page */

if (empty($model->title_browser)) {
    $this->title = $model->title;
} else {
    $this->title = $model->title_browser;
}
if (!empty($model->meta_description)) {
    $this->registerMetaTag(['content' => Html::encode($model->meta_description), 'name' => 'description']);
}
if (!empty($model->meta_keywords)) {
    $this->registerMetaTag(['content' => Html::encode($model->meta_keywords), 'name' => 'keywords']);
}
?>


<link rel="stylesheet" type="text/css" href="/../wb/css/contacts/main_contacts.css?ver=2.3.6">

<?php $form = ActiveForm::begin([
    'id' => 'contact-form'
]); ?>

<div class="container-contact100">



    <div class="wrap-contact100">

        <?php if (Yii::$app->session->hasFlash('success')): ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <!--<span type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></span>-->
                <strong><?php echo Yii::t('app','Thank you for your message! We will contact you shortly')?></strong>
            </div>
        <?php endif; ?>

        <?php if (Yii::$app->session->hasFlash('error')): ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <!--<span type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></span>-->
                <strong>Warning!</strong>
            </div>
        <?php endif; ?>

        <div class="contact100-form-title" style="background-image: url(/../wb/images/bg-01.jpg);">
				<span class="contact100-form-title-1">
					<?php echo Yii::t('app','CONTACT US')?>
				</span>

            <span class="contact100-form-title-2">
					<?php echo Yii::t('app', 'Please contact us if you have any questions. You may call, e-mail or fill out the contact form and let us know your needs.')?>
				</span>
        </div>

        <div class="panel-body">
            <div class = "panel-body-padding">
            <?php $form = ActiveForm::begin([
                'id' => 'contact-form'
            ]); ?>
            <?= $form->field($model2, 'name')->textInput(['placeholder'=>Yii::t('app','Enter full name')])->label(Yii::t('app','Full Name'))  ?>
            <?= $form->field($model2, 'email')->textInput(['placeholder'=>Yii::t('app','Enter email address')])->label(Yii::t('app','Email')) ?>
            <?= $form->field($model2, 'subject')->textInput(['placeholder'=>Yii::t('app','Enter phone number')])->label(Yii::t('app','Phone')) ?>
            <?= $form->field($model2, 'body')->textArea(['rows' => 6, 'placeholder'=>Yii::t('app','Your Comment...')])->label(Yii::t('app','Message')) ?>



            <div class="reCaptcha">
                <?= $form->field($model2, 'reCaptcha', ['template' => '{input}'])->widget(
                    \himiklab\yii2\recaptcha\ReCaptcha::className()
                //['widgetOptions'=>['class'=>'text-right']]
                ) ?>
            </div>


            <div class="submit_button">
                <?= Html::submitButton(Yii::t('app','Submit'), ['class' => 'btn btn-success btn-lg', 'name' => 'contact-button']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
        </div>

    </div>

</div>
<div id="dropDownSelect1"></div>





