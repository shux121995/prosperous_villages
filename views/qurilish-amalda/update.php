<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\QurulishAmalda */

$this->title = 'Фактни ўзгартириш: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Qurulish Amaldas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Ўзгартириш';
?>
<div class="qurulish-amalda-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
