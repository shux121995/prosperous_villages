<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\GenerateForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\LocalityPlansSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $table \app\models\Tables */

?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4><?=$qurulish->title?></h4>
    <div class="warning text-danger" style="display: none;">Диккат! Ушбу тулдириш формаси факат бир марта саклашга имкон беради! Шунинг учун тугри маълумот киритишингиз суралади!</div>
</div>
<div class="modal-body">
    <div class="modal-body">
        <table class="table-bordered table">
            <tr>
                <?php $column = 'oh_yer' ?>
                <td><?=$model->getAttributeLabel($column)?></td>
                <td><?=$model->$column?></td>
            </tr>
            <tr>
                <?php $column = 'oh_poydevor' ?>
                <td><?=$model->getAttributeLabel($column)?></td>
                <td><?=$model->$column?></td>
            </tr>
            <tr>
                <?php $column = 'oh_gisht_terish' ?>
                <td><?=$model->getAttributeLabel($column)?></td>
                <td><?=$model->$column?></td>
            </tr>
            <tr>
                <?php $column = 'oh_tom_yopish' ?>
                <td><?=$model->getAttributeLabel($column)?></td>
                <td><?=$model->$column?></td>
            </tr>
            <tr>
                <?php $column = 'oh_demontaj' ?>
                <td><?=$model->getAttributeLabel($column)?></td>
                <td><?=$model->$column?></td>
            </tr>
            <tr>
                <?php $column = 'oh_oraliq_devor' ?>
                <td><?=$model->getAttributeLabel($column)?></td>
                <td><?=$model->$column?></td>
            </tr>
            <tr>
                <?php $column = 'oh_eshik_deraza' ?>
                <td><?=$model->getAttributeLabel($column)?></td>
                <td><?=$model->$column?></td>
            </tr>
            <tr>
                <?php $column = 'oh_ichki_pardozlash' ?>
                <td><?=$model->getAttributeLabel($column)?></td>
                <td><?=$model->$column?></td>
            </tr>
            <tr>
                <?php $column = 'oh_tashqi_pardozlash' ?>
                <td><?=$model->getAttributeLabel($column)?></td>
                <td><?=$model->$column?></td>
            </tr>
            <?php $column = 'uzlashtirildi'; ?>
            <td><?=$model->getAttributeLabel($column)?></td>
            <?php
            $values_plan = \app\models\ValuesPlan::find()->where(['plan_id'=>$plan->id, 'qurulish_id'=>$model->qurulish_id])->one();
            if($values_plan != null) {
                $values_fact = \app\models\ValuesFact::find()->where(['plan_value_id'=>$values_plan->id])->one();
            }
            ?>
            <td>
                <?php if($values_plan != null and isset($values_fact)): ?>
                    <?php
                    $values_manba_fact = \app\models\ValuesManbaFact::find()->where(['values_fact_id'=>$values_fact->id])->all();
                    ?>
                    <?=$values_fact->fact!=null ? 'Жами факт:' : ''?> <b><?=$values_fact->fact?></b><br>
                    <?php foreach($values_manba_fact as $item): ?>
                        <?=(new \app\models\MoliyaManbai())->getOneRecoord($item->moliya_manba_id)['title']?>: <?=$item->fact?><br>
                    <?php endforeach; ?>
                <?php endif; ?>
            </td>
        </table>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo Yii::t('app', 'Close')?></button>
    </div>

    <?php
    $js = <<<JS
    // Initiate form validation
    $.validate({
    lang: 'uz'
    });
JS;
    $this->registerJs($js);
    ?>
