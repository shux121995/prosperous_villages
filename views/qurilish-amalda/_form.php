<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\QurulishAmalda */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="qurulish-amalda-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'qurulish_id')->dropDownList((new \app\models\Qurulish)->getQurilishIdAndTitleByPlanId(), ['prompt' => 'Қурилиш объектини танланг']) ?>

    <?= $form->field($model, 'qiy_used')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'oh_yer')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'oh_poydevor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'oh_gisht_terish')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'oh_tom_yopish')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'oh_demontaj')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'oh_oraliq_devor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'oh_eshik_deraza')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'oh_ichki_pardozlash')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'oh_tashqi_pardozlash')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сақлаш', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
