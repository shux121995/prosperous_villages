<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\QurulishAmaldaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="qurulish-amalda-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'qurulish_id') ?>

    <?= $form->field($model, 'qiy_used') ?>

    <?= $form->field($model, 'oh_yer') ?>

    <?= $form->field($model, 'oh_poydevor') ?>

    <?php // echo $form->field($model, 'oh_gisht_terish') ?>

    <?php // echo $form->field($model, 'oh_tom_yopish') ?>

    <?php // echo $form->field($model, 'oh_demontaj') ?>

    <?php // echo $form->field($model, 'oh_oraliq_devor') ?>

    <?php // echo $form->field($model, 'oh_eshik_deraza') ?>

    <?php // echo $form->field($model, 'oh_ichki_pardozlash') ?>

    <?php // echo $form->field($model, 'oh_tashqi_pardozlash') ?>

    <?php // echo $form->field($model, 'date') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
