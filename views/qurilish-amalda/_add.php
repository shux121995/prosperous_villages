<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\PlanForm */
/* @var $form yii\widgets\ActiveForm */
$Qurulish = new \app\models\Qurulish();
?>

<div class="plan-form">

    <?php $form = ActiveForm::begin([
        'id' => 'QurulishAmadaForm',
        //'enableClientValidation' => false,
    ]); ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?=$plan->locality->title.' '.$model->getAttributeLabel($input)?>га факт кушиш</h4>
        <div class="warning text-danger" style="display: none;">Диккат! Ушбу тулдириш формаси факат бир марта саклашга имкон беради! Шунинг учун тугри маълумот киритишингиз суралади!</div>
    </div>
    <div class="modal-body">
            <?= $form->field($model, $input)->textInput(['maxlength' => true]) ?>
    </div>
    <div class="modal-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo Yii::t('app', 'Close')?></button>
    </div>


    <?php ActiveForm::end(); ?>

</div>
