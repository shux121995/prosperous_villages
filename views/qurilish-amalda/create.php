<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\QurulishAmalda */

$this->title = 'Create Qurulish Amalda';
$this->params['breadcrumbs'][] = ['label' => 'Qurulish Amaldas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="qurulish-amalda-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
