<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\GenerateForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\LocalityPlansSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $table \app\models\Tables */

?>
<?php $form = ActiveForm::begin([
    'id' => 'QurulishAmadaForm',
    //'enableClientValidation' => false,
]); ?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4><?=$qurulish->title.'га факт киритиш'?></h4>
    <div class="warning text-danger" style="display: none;">Диккат! Ушбу тулдириш формаси факат бир марта саклашга имкон беради! Шунинг учун тугри маълумот киритишингиз суралади!</div>
</div>
<div class="modal-body">
    <div class="modal-body">

        <?= $form->field($model, 'oh_yer')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'oh_poydevor')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'oh_gisht_terish')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'oh_tom_yopish')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'oh_demontaj')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'oh_oraliq_devor')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'oh_eshik_deraza')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'oh_ichki_pardozlash')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'oh_tashqi_pardozlash')->textInput(['maxlength' => true]) ?>

        <?php if(!empty($moliya_manbai)): ?>
            <div class="moliya_manbai">
                <p><b><?=$model->getAttributeLabel('uzlashtirildi')?></b></p>
                <?php if($model->qurulish_id != null):
                    $values_plan = \app\models\ValuesPlan::find()->where(['plan_id'=>$plan->id, 'qurulish_id'=>$model->qurulish_id])->one();
                    $values_fact = \app\models\ValuesFact::find()->where(['plan_value_id'=>$values_plan->id])->one();
                    ?>
                    <?=isset($values_fact->fact) ? 'Жами факт:' : ''?> <b><?=isset($values_fact->fact) ? $values_fact->fact : '' ?></b><p></p>
                <?php endif; ?>
                <?php foreach($moliya_manbai as $key => $value): ?>
                    <?php
                    if(isset($values_fact)) {
                        $values_manba_facts = \app\models\ValuesManbaFact::find()->where(['values_fact_id'=>$values_fact->id, 'moliya_manba_id'=>$value['id']])->all();
                    }
                    ?>
                    <?php if(isset($values_manba_facts)): ?>
                        <?php foreach($values_manba_facts as $item): ?>
                            <?= $form->field($model, 'uzlashtirildi['.$value['id'].']')->textInput(['placeholder'=>"Режа", 'required'=>'required', 'value'=>$item->fact])->label($value['title']) ?>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <?= $form->field($model, 'uzlashtirildi['.$value['id'].']')->textInput(['placeholder'=>"Режа", 'required'=>'required',])->label($value['title']) ?>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
    <div class="modal-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo Yii::t('app', 'Close')?></button>
    </div>


    <?php ActiveForm::end(); ?>
<?php
$js = <<<JS
    // Initiate form validation
    $.validate({
    lang: 'uz'
    });
// permit only float type for moliya_manbai
$('.moliya_manbai input').on('input', function() {
  this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
});

//Clear modal window after close
$(document).ready(function()
{
    $('.modal').on('hidden.bs.modal', function(e)
    { 
        $(this).removeData();
    }) ;
});
JS;
$this->registerJs($js);
?>
