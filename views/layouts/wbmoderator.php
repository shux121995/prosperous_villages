<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\User;

if(!isset($_SESSION['year'])) $_SESSION['year'] = date('Y');

Yii::$app->language = Yii::$app->lang->check_lang();
$user = User::findOne(Yii::$app->user->id);

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody() ?>

    <div class="wrap">
        <?php
        NavBar::begin([
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'navbar-inverse navbar-fixed-top',
            ],
        ]);

        if (Yii::$app->user->isGuest || $user->role == User::ROLE_REGION) {
            $menuItems[] = ['label' => 'Кириш', 'url' => ['/site/login']];
        } else {

            $menuItems[] = ['label' => 'Mobilization', 'url' => ['#'],
                'items' => [
                    ['label' => 'Villages Population', 'url' => ['/wbmoderator/aholi']],
                    ['label' => 'Villages score', 'url' => ['/wbmoderator/villages-score']],
                    ['label' => 'Households Welfare', 'url' => ['/wbmoderator/households-welfare']],
                    ['label' => 'Villages Voters', 'url' => ['/wbmoderator/villages-voters']],
                    ['label' => 'MDU Members', 'url' => ['/wbmoderator/mdu-members']],
                    ['label' => 'MDU IMI Scores', 'url' => ['/wbmoderator/mdu-imi-score']],
                    ['label' => 'CPM Leader', 'url' => ['/wbmoderator/cpm-leader']],
                    ['label' => 'CDWO staff', 'url' => ['/wbmoderator/cdwo']],
                ],
            ];

            $menuItems[] = ['label' => 'ESS', 'url' => ['#'],
                'items' => [
                    ['label' => 'Project Design Steps', 'url' => ['/wbmoderator/ess-project-design']],
                    ['label' => 'Resettlement Monitoring Form', 'url' => ['/wbmoderator/resettlement']],
                ],
            ];

            $menuItems[] = ['label' => 'Infrastructure', 'url' => ['#'],
                'items' => [
                    ['label' => 'Infrastructure Monitoring Form', 'url' => ['/wbmoderator/infra-and-finan']],
                ],
            ];

            $menuItems[] = ['label' => 'Procurement', 'url' => ['#'],
                'items' => [
                    ['label' => 'Procurement Monitoring Form', 'url' => ['/wbmoderator/procurement']],
                ],
            ];

            $menuItems[] = ['label' => 'Finance', 'url' => ['#'],
                'items' => [
                    ['label' => 'Finance Monitoring Form', 'url' => ['/wbmoderator/finance']],
                ],
            ];

            $menuItems[] = '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Log Out (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>';
        }
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => $menuItems,
        ]);
        NavBar::end();
        ?>

        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <p class="pull-left">&copy; World Bank <?= date('Y') ?></p>

            <p class="pull-right"><?php //= Yii::powered() ?></p>
        </div>
    </footer>

    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>