<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\User;

if(!isset($_SESSION['year'])) $_SESSION['year'] = date('Y');

Yii::$app->language = Yii::$app->lang->check_lang();

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        //'brandLabel' => \yii\helpers\StringHelper::byteSubstr(Yii::$app->access->getRegionTitle(''),0,39),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => 'Бош саҳифа', 'url' => ['/site/index']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Кириш', 'url' => ['/site/login']];
    } else {
        $user = User::findOne(Yii::$app->user->id);

        if($user->role == User::ROLE_ADMIN) {
            $menuItems[] = ['label' => 'Админ панел', 'url' => ['/admin']];
            $menuItems[] = ['label' => 'Натижалар', 'url' => ['/results']];
        }
        elseif($user->role == User::ROLE_REGION) {

            $menuItems[] = ['label' => 'Режа',// 'url' => ['#'],
                'items' => \app\components\TablesMenu::userMenu('plans/index'),
                ['label' => 'Аҳоли сони', 'url' => ['/aholi/index']],
            ];

            $menuItems[] = ['label' => 'Факт/Амалда', 'url' => ['#'],
                'items' => \app\components\TablesMenu::userMenu('facts/index'),
            ];

            $menuItems[] = ['label' => 'Қўшимча', 'url' => ['#'],
                'items' => [
                    ['label' => 'Ахоли сони', 'url' => ['/aholi/index']],
                    ['label' => 'Йил плани', 'url' => ['/locality-plans']],
                    ['label' => 'CDWO staff', 'url' => ['/wbmoderator/cdwo']],
                ],
            ];


        }elseif($user->role == User::ROLE_MODER){
            $menuItems[] = ['label' => 'Mobilization', 'url' => ['#'],
                'items' => [
                    ['label' => 'Villages Population', 'url' => ['/wbmoderator/aholi']],
                    ['label' => 'Villages score', 'url' => ['/wbmoderator/villages-score']],
                    ['label' => 'Households Welfare', 'url' => ['/wbmoderator/households-welfare']],
                    ['label' => 'Villages Voters', 'url' => ['/wbmoderator/villages-voters']],
                    ['label' => 'MDU Members', 'url' => ['/wbmoderator/mdu-members']],
                    ['label' => 'MDU IMI Scores', 'url' => ['/wbmoderator/mdu-imi-score']],
                    ['label' => 'CPM Leader', 'url' => ['/wbmoderator/cpm-leader']],
                    ['label' => 'CDWO staff', 'url' => ['/wbmoderator/cdwo']],
                ],
            ];

            $menuItems[] = ['label' => 'ESS', 'url' => ['#'],
                'items' => [
                    ['label' => 'Project Design Steps', 'url' => ['/wbmoderator/ess-project-design']],
                ],
            ];

            $menuItems[] = ['label' => 'Infrastructure', 'url' => ['#'],
                'items' => [

                ],
            ];

            $menuItems[] = ['label' => 'Procurement', 'url' => ['#'],
                'items' => [

                ],
            ];

            $menuItems[] = ['label' => 'Finance', 'url' => ['#'],
                'items' => [
                    ['label' => 'Finance, Form: 5.14', 'url' => ['/wbadmin/form_5_14']],
                ],
            ];

            $menuItems[] = ['label' => 'Additions', 'url' => ['#'],
                'items'=>[

                ]
            ];
        }

        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Чиқиш (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= \app\widgets\Alert::widget(); ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Ўзбекистон республикаси Иқтисодиёт ва саноат вазирлиги <?= date('Y') ?></p>

        <p class="pull-right"></p>
    </div>
</footer>
<div class="modal remote" data-easein="bounceIn" id="modal">
    <div class="modal-dialog  modal-dialog-centered">
        <div id="modalContent" class="modal-content loader-lg"></div>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
