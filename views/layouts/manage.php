<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

if(!isset($_SESSION['year'])) $_SESSION['year'] = date('Y');

Yii::$app->language = Yii::$app->lang->check_lang();

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Обод кишлок ва махалла админ панель',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => 'Бош сахифа', 'url' => ['/manage']],
    ];
    if (Yii::$app->user->isGuest) {
        //$menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
        $menuItems[] = ['label' => 'Кириш', 'url' => ['/site/login']];
    } else {
        $menuItems[] = '<li>'
            . Html::beginForm(['/year/index'], 'get')
            . Html::dropDownList('year', isset($_SESSION['year'])?$_SESSION['year']:null,
                Yii::$app->params['years'],
                ['class' => '','onchange'=>'this.form.submit();','style'=>'margin-top: 15px;']
            )
            . Html::submitButton(
                '',
                ['style' => 'display: none;']
            )
            . Html::endForm()
            . '</li>';
        $menuItems[] = ['label' => 'Tables', 'url' => ['/manage/tables']];
        $menuItems[] = ['label' => 'Sections', 'url' => ['/manage/sections']];
        $menuItems[] = ['label' => 'Forms', 'url' => ['/manage/forms']];
        $menuItems[] = ['label' => 'Inputs', 'url' => ['/manage/inputs']];
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Чикиш (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Ўзбекистон республикаси Иқтисодиёт ва саноат вазирлиги <?= date('Y') ?></p>

        <p class="pull-right"><?php //= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
