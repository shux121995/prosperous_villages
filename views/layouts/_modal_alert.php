<?php
Yii::$app->language = Yii::$app->lang->check_lang();
?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Упс!!! нимададур хатолик кузатилди!</h4>

    </div>
    <div class="modal-body">
        <div class="warning text-danger"><?=$message?></div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo Yii::t('app', 'Close')?></button>
    </div>