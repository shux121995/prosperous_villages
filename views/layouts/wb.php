<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\AppAssetBootstrp4;
use yii\helpers\Url;
use app\models\Tables;

AppAssetBootstrp4::register($this);
if(!isset($_SESSION['year'])) $_SESSION['year'] = date('Y');

$lang = (new \app\components\LangHelper())->check_lang();
Yii::$app->language = Yii::$app->lang->check_lang().'-'.strtoupper(Yii::$app->lang->check_lang());

$tables = \app\models\Tables::find()->All();
?>

<style>

    .dropdown{
        position: relative!important;
    }

    .dropdown-menu{

    }

    .dropdown-submenu>.dropdown-menu {
        top: 0;
        left: 100%;
        margin-top: -6px;
        margin-left: -1px;
        -webkit-border-radius: 6px 6px 6px 6px;
        -moz-border-radius: 6px 6px 6px 6px;
        border-radius: 6px 6px 6px 6px;
        position: absolute;
    }

    .dropdown-submenu:hover>.dropdown-menu {
        display: block;
    }

    .dropdown-submenu>a:after {
        display: block;
        content: " ";
        float: right;
        width: 0;
        height: 0;
        border-color: transparent;
        border-style: solid;
        border-width: 5px 0 5px 5px;
        border-left-color: #ccc;
        margin-top: 5px;
        margin-right: -10px;
    }

    .dropdown-submenu:hover>a:after {
        border-left-color: #fff;
    }

    .dropdown-submenu.pull-left>.dropdown-menu {
        left: -100%;
        margin-left: 10px;
        -webkit-border-radius: 6px 0 6px 6px;
        -moz-border-radius: 6px 0 6px 6px;
        border-radius: 6px 0 6px 6px;
    }
    .dropdown>a {
        text-decoration: none !important;
    }
</style>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<header>
    <div class="container-fluid">
        <div class="header-top">
            <div class="row align-items-center">
                <div class="col-auto order-md-0 order-0">
                    <div class="logo">
                        <a href="/<?=$lang?>" class="d-flex align-items-center">
                            <img src="/../../wb/images/logo.png" alt="" class="img-fluid logo__img">
                            <div class="logo__text text-uppercase">
                                <?=Yii::t('app','<span class="color-secondary">Qishloq</span><br/><span class="color-primary">infratuzilmasini rivojlantirish</span>')?>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col">
                    <ul class="footer__menu nav justify-content-end align-items-center">
                            <!--<li>
                                <?php
                                echo Html::beginForm(['/year/index'], 'get');
                                echo Html::dropDownList('year', isset($_SESSION['year'])?$_SESSION['year']:null,
                                Yii::$app->params['years'],
                                ['class' => '','onchange'=>'this.form.submit();','style'=>'margin-top: 10px;']
                                );
                                echo Html::submitButton(
                                '',
                                ['style' => 'display: none;']
                                );
                                echo Html::endForm();
                                ?>
                            </li>-->
                            <li>
                                <div class="lang-switcher">
                                    <ul class="lang">
                                        <li class="lang__item <?=$lang=='uz'?'current':''?>"><a href="/uz<?=$this->params['tn_url']?>" class="lang__item-link">O'z</a></li>
                                        <!--<li class="lang__item"><a href="#" class="lang__item-link">Ру</a></li>-->
                                        <li class="lang__item <?=$lang=='en'?'current':''?>"><a href="/en<?=$this->params['tn_url']?>" class="lang__item-link">En</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <div class="profile" style="display: flex; float: left; margin-right: 15px; margin-left: 20px;">
                                    <a href="<?=Url::to('/'.$lang.'/wb/feedback')?>" class="review__btn btn btn-secondary"><i class="flaticon flaticon-comment flaticon-right"></i> <?=Yii::t('app','Izoh qoldirish')?></a>
                                </div>
                            </li>
                            <li>
                                <div class="profile">
                                    <a href="<?=Url::to('/'.$lang.'/site/login')?>" class="profile__btn btn btn-link"><i class="flaticon flaticon-login"></i></a>
                                </div>
                            </li>
                        </ul>
                </div>
            </div>
        </div>
        <div class="row justify-content-end align-items-center">

            <div class="header-top2 col-auto order-md-6 order-last" style="border: none; font-size: 20px!important;">
                <ul class="footer__menu nav justify-content-end">
                    <li style="display: none;" class="footer__menu-item nav-item"><a href="<?=Url::toRoute('/'.$lang)?>" class="footer__menu-link nav-link"><?=Yii::t('app','Bosh sahifa')?></a></li>
                    <li class="footer__menu-item nav-item"><a href="/<?=$lang?><?=Url::toRoute(['/pages/about'])?>" class="footer__menu-link nav-link"><?=Yii::t('app','Loyiha haqida')?></a></li>
                    <li class="footer__menu-item nav-item"><a href="/<?=$lang?><?=Url::toRoute(['/news'])?>" class="footer__menu-link nav-link"><?=Yii::t('app','Yangiliklar')?></a></li>
                    <li class="footer__menu-item nav-item"><a href="/<?=$lang?><?=Url::toRoute(['/pages/docs'])?>" class="footer__menu-link nav-link"><?=Yii::t('app','Meyoriy hujjatlar')?></a></li>
                    <li style="display: none" class="footer__menu-item nav-item"><a href="/<?=$lang?><?=Url::toRoute(['/wbresults'])?>" class="footer__menu-link nav-link"><?=Yii::t('app','Ochiq malumotlar')?></a></li>
                    <li class="footer__menu-item nav-item"><a href="https://play.google.com/store/apps/details?id=com.shukhrat.wbpvp" target="_blank" class="footer__menu-link nav-link"><?=Yii::t('app','Mobil ilova')?></a></li>
                    <li class="footer__menu-item nav-item"><a href="/<?=$lang?><?=Url::toRoute(['/pages/contacts'])?>" class="footer__menu-link nav-link"><?=Yii::t('app','Bog\'lanish')?></a></li>
                </ul>
            </div>

            <!--Search Box we will include later
            <div style="display: none" class="col-auto order-md-6 order-last">
                <div class="search">
                    <a href="#" onclick="$('.search__form').toggle(); $(this).hide();" class="search__btn btn btn-link"><i class="flaticon flaticon-search"></i></a>
                    <from class="searching_container search__form" style="display: none;">
                        <input type="text" class="search__form-input searching" placeholder="<?=Yii::t('app','Қидирув...')?>">
                        <button type="submit" onclick="$('.search__form').toggle(); $('.search__btn').show();"  class="search_form-btn searching_btn"></button>
                    </from>
                </div>
            </div>-->
        </div>
    </div>
</header>

<main class="container-fluid">
    <?= $content ?>
</main>


<footer>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">


    <!-- Footer Links -->
    <div class="container text-center text-md-left mt-5">

        <!-- Grid row -->
        <div class="row mt-3">

            <!-- Grid column -->
            <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">

                <!-- Content -->
                <h6 class="text-uppercase font-weight-bold"><?php echo Yii::t('app','Rural Infrastructure Development')?></h6>
                <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                <p><?php echo Yii::t('app','While using the site information, a link to the source is required')?></p>

            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">

                <!-- Links -->
                <h6 class="text-uppercase font-weight-bold"><?php echo Yii::t('app','Social network')?></h6>
                <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                <p>
                    <a href="https://www.facebook.com/%D0%9F%D1%80%D0%BE%D0%B5%D0%BA%D1%82-%D0%91%D0%BB%D0%B0%D0%B3%D0%BE%D1%83%D1%81%D1%82%D1%80%D0%BE%D0%B5%D0%BD%D0%BD%D1%8B%D0%B5-%D1%81%D0%B5%D0%BB%D0%B0-%D0%A3%D0%B7%D0%B1%D0%B5%D0%BA%D0%B8%D1%81%D1%82%D0%B0%D0%BD%D0%B0-%D0%BF%D1%80%D0%B8-%D0%9C%D0%AD%D0%9F-%D0%A0%D0%A3%D0%B7-727457484339084/" target="_blank">Facebook</a>
                </p>
                <p>
                    <a href="https://t.me/obodqishloqchannel" target="_blank">Telegram</a>
                </p>
                <p>
                    <!--<a href="#!">BrandFlow</a>-->
                </p>
                <p>
                    <!--<a href="#!">Bootstrap Angular</a>-->
                </p>

            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">

                <!-- Links -->
                <h6 class="text-uppercase font-weight-bold"><?=Yii::t('app','Useful links')?></h6>
                <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                <p>
                    <a href="/<?=$lang?><?=Url::toRoute(['/pages/about'])?>"><?=Yii::t('app','Loyiha haqida')?></a>
                </p>
                <p>
                    <a href="/<?=$lang?><?=Url::toRoute(['/news'])?>"><?=Yii::t('app','Yangiliklar')?></a>
                </p>
                <p>
                    <a href="/<?=$lang?><?=Url::toRoute(['/pages/docs'])?>"><?=Yii::t('app','Meyoriy hujjatlar')?></a>
                </p>
                <p>
                    <a href="https://play.google.com/store/apps/details?id=com.shukhrat.wbpvp" target="_blank"><?=Yii::t('app','Mobil ilova')?></a>
                </p>

            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">

                <!-- Links -->
                <h6 class="text-uppercase font-weight-bold"><?php echo Yii::t('app','CONTACT US')?></h6>
                <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                <p>
                    <i class="fas fa-home mr-3"></i> <?php echo Yii::t('app','45A Ave Islam Karimov, Tashkent, Uzbekistan, 100003')?></p>
                <p>
                    <i class="fas fa-envelope mr-3"></i> info@obodqishloq.uz</p>
                <p>
                    <i class="fas fa-phone mr-3"></i> +99871 232-63-31</p>
                <p>
                    <i class="fas fa-print mr-3"></i> +99871 232-63-32</p>

            </div>
            <!-- Grid column -->

        </div>
        <!-- Grid row -->

    </div>
    <!-- Footer Links -->

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">
        <span><?=Yii::t('app','© O‘ZBEKISTON RESPUBLIKASI IQTISODIY TARAQQIYOT VA KAMBAG‘ALLIKNI QISQARTIRISH VAZIRLIGI')?> 2020</span>
    </div>
    <!-- Copyright -->

</footer>
<!-- Footer -->


<?php
$locals = \app\models\Locality::find()->all();
$searchUrl = \yii\helpers\Url::toRoute(['search','language'=>Yii::$app->lang->check_lang()]);
$contenter = '';
$html = 'var location = [';
$i = 0;
  foreach($locals as $key => $local) :
      if($local->map_latitude!=null && $local->map_longitude!=null) :

          $contenter = '<div id="content"><div id="siteNotice"></div><h5 id="firstHeading" class="firstHeading">'.str_replace(["\r","\n","\r\n"], "", $local->getTitleLang($lang).', '.$local->district->getTitleLang($lang)).'</h5><div id="bodyContent"><ul>';
          foreach (Tables::find()->all() as $tabled) {
              $contenter .= '<li class="menu__item">' . \yii\helpers\Html::a(str_replace("\r\n", "", $tabled->getTitleLang($lang)),
                      \yii\helpers\Url::toRoute(['view', 'id' => $tabled->id, 'local' => $local->id]),
                      ['class' => 'menu__item-link']
                  ) . '</li>';
          }

         $contenter .= '</ul></div></div>';

         $contenter = str_replace("'","",$contenter);

         $html .='["'.$i.'",'.$local->map_latitude.','.$local->map_longitude.',\''.$contenter.'\',\''.str_replace(["\r","\n","\r\n"], "", $local->getTitleLang($lang).', '.$local->district->getTitleLang($lang)).'\'],';
         $i++;
      endif;

  endforeach;
$html .= '];';
$script = <<< JS
    $(document).ready(function () {
        var panelH = $('.searching_resluts').innerHeight();

        $('.searching_wella,.searching_wella_mob').blur(function(){
            $('.searching_resluts').fadeOut();
        });

        $('.searching_wella,.searching_wella_mob').focus(function() {
            $('.searching_resluts').fadeIn();
        });
        
        $.ajax({
            url: "$searchUrl",
            cache: false,
            data: { q: $(this).val() }
        }).done(function( html ) {
            $( ".sr_lists" ).html(html);
            //$('.searching_resluts').fadeIn();
        });
        
        $('.searching_wella,.searching_wella_mob').keyup(function() {
            $.ajax({
                url: "$searchUrl",
                cache: false,
                data: { q: $(this).val() }
            })
            .done(function( html ) {
                $( ".sr_lists" ).html(html);
                //$('.searching_resluts').fadeIn();
            });
        })
        
    });
JS;
$this->registerJs($script, \yii\web\View::POS_READY);
?>
<?php $this->endBody() ?>
<script>
    $(document).ready(function () {
        $(".menu__btn").on('click',  function(event) {
            $("header").toggleClass('menu-opened');
        });
        if($('main').innerHeight()<500){
            $('footer').addClass('fixed');
        }
    })
</script>
</body>
</html>
<?php $this->endPage() ?>
