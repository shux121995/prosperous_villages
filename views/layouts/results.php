<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\User;

if(!isset($_SESSION['year'])) $_SESSION['year'] = date('Y');

Yii::$app->language = Yii::$app->lang->check_lang();

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Обод кишлок ва махалла '. \yii\helpers\StringHelper::byteSubstr(Yii::$app->access->getRegionTitle('/ '),0,39),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse  navbar-default',
            'style' => 'border-radius: 0!important;',
        ],
    ]);
    $menuItems = [
        ['label' => 'Бош сахифа', 'url' => ['/site/index']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Кириш', 'url' => ['/site/login']];
    } else {
        $user = User::findOne(Yii::$app->user->id);
        $menuItems[] = '<li>'
            . Html::beginForm(['/year/index'], 'get')
            . Html::dropDownList('year', isset($_SESSION['year'])?$_SESSION['year']:null,
                Yii::$app->params['years'],
                ['class' => '','onchange'=>'this.form.submit();','style'=>'margin-top: 15px;']
            )
            . Html::submitButton(
                '',
                ['style' => 'display: none;']
            )
            . Html::endForm()
            . '</li>';
        //$menuItems[] = ['label' => 'Feedback', 'url' => ['/results/feedback']];
        if($user->role == User::ROLE_ADMIN) {
            $menuItems[] = ['label' => 'Админ панел', 'url' => ['/admin']];
            $menuItems[] = ['label' => 'Килинган ишлар буйича натижалар',// 'url' => ['#'],
                'items' => \app\components\TablesMenu::userMenu('respublika/index'),
                ['label' => 'Ахоли сони', 'url' => ['/aholi/index']],
            ];
        }
        elseif($user->role == User::ROLE_REGION) {
            $menuItems[] = ['label' => 'Килинган ишлар буйича натижалар',// 'url' => ['#'],
                'items' => \app\components\TablesMenu::userMenu('region/index'),
                ['label' => 'Ахоли сони', 'url' => ['/aholi/index']],
            ];
        }elseif($user->role == User::ROLE_VIEW) {
            $menuItems[] = ['label' => 'Килинган ишлар буйича натижалар',// 'url' => ['#'],
                'items' => \app\components\TablesMenu::userMenu('respublika/index'),
                ['label' => 'Ахоли сони', 'url' => ['/aholi/index']],
            ];
        }

        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Чикиш (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="containers">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= \app\widgets\Alert::widget(); ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Ўзбекистон республикаси Иқтисодиёт ва саноат вазирлиги <?= date('Y') ?></p>

        <p class="pull-right"></p>
    </div>
</footer>
<div class="modal remote" data-easein="bounceIn" id="modal">
    <div class="modal-dialog  modal-dialog-centered">
        <div id="modalContent" class="modal-content loader-lg"></div>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
