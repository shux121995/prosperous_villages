<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\User;


if(!isset($_SESSION['year'])) $_SESSION['year'] = date('Y');

Yii::$app->language = Yii::$app->lang->check_lang();
$user = User::findOne(Yii::$app->user->id);

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        //'brandLabel' => 'Обод қишлоқ Жаҳон банки админка',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    /*$menuItems = [
        ['label' => 'Main page', 'url' => ['/site/index']],
    ];*/
    if ($user->role != User::ROLE_ADMIN) {
        //$menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
        $menuItems[] = ['label' => 'Кириш', 'url' => ['/site/login']];
    } else {
        /*$menuItems[] = ['label' => 'Проектлар', 'url' => ['#'],
            'items' => [
                ['label' => 'Ахоли сони', 'url' => ['/admin/aholi']],
            ],
        ];*/
        $menuItems[] = '<li>'
        . Html::beginForm(['/year/index'], 'get')
        /*. Html::dropDownList('year', isset($_SESSION['year'])?$_SESSION['year']:null,
            Yii::$app->params['years'],
            ['class' => '','onchange'=>'this.form.submit();','style'=>'margin-top: 15px;']
        )*/
        . Html::submitButton(
            '',
            ['style' => 'display: none;']
        )
        . Html::endForm()
        . '</li>';

        $menuItems[] = ['label' => 'Users & Access', 'url' => ['#'],
            'items' => [
                ['label' => 'Users', 'url' => ['/wbadmin/users']],
                ['label' => 'Users Access', 'url' => ['/wbadmin/access']],
            ],
        ];
        //$menuItems[] = ['label' => 'Users', 'url' => ['/wbadmin/users']];
        $menuItems[] = ['label' => 'WB plans', 'url' => ['/wbadmin/plans']];
        $menuItems[] = ['label' => 'WB Investment', 'url' => ['/wbadmin/investment']];
        //$menuItems[] = ['label' => 'Натижалар', 'url' => ['/'.Yii::$app->lang->check_lang().'/wbresults']];

        /*$menuItems[] = ['label' => 'Mobilization', 'url' => ['#'],
            'items' => [
                ['label' => 'Villages Population', 'url' => ['/wbadmin/aholi']],
                ['label' => 'Villages score', 'url' => ['/wbadmin/villages-score']],
                ['label' => 'Households Welfare', 'url' => ['/wbadmin/households-welfare']],
                ['label' => 'Villages Voters', 'url' => ['/wbadmin/villages-voters']],
                ['label' => 'MDU Members', 'url' => ['/wbadmin/mdu-members']],
                ['label' => 'MDU IMI Scores', 'url' => ['/wbadmin/mdu-imi-score']],
                ['label' => 'CPM Leader', 'url' => ['/wbadmin/cpm-leader']],
                ['label' => 'CDWO staff', 'url' => ['/wbadmin/cdwo']],
            ],
        ];*/

        /*$menuItems[] = ['label' => 'ESS', 'url' => ['#'],
            'items' => [

            ],
        ];*/

        /*$menuItems[] = ['label' => 'Infrastructure', 'url' => ['#'],
            'items' => [

            ],
        ];*/

        /*$menuItems[] = ['label' => 'Procurement', 'url' => ['#'],
            'items' => [

            ],
        ];*/

        /*$menuItems[] = ['label' => 'Finance', 'url' => ['#'],
            'items' => [
                    ['label' => 'Finance, Form: 5.14', 'url' => ['/wbadmin/form_5_14']],
            ],
        ];*/

        $menuItems[] = ['label' => 'Additions', 'url' => ['#'],
            'items'=>[
                //['label' => 'Масалалар', 'url' => ['/wbadmin/tasks']],
                ['label' => 'Regions', 'url' => ['/wbadmin/regions']],
                ['label' => 'Districts', 'url' => ['/wbadmin/districts']],
                ['label' => 'Villages', 'url' => ['/wbadmin/localities']],
                ['label' => 'Projects', 'url' => ['/wbadmin/types']],
                //['label' => 'Суб турлар', 'url' => ['/wbadmin/sub-types']],
                ['label' => 'Contractors', 'url' => ['/wbadmin/developers']],
                ['label' => 'Feedback', 'url' => ['/wbadmin/feedback']],
                ['label' => 'Total Feedback', 'url' => ['/wbadmin/total-feedback']],
                //['label' => 'Файллар', 'url' => ['/wbadmin/files']],
                //['label' => 'Pages', 'url' => ['/pages/manager']], INCLUDE IT LATER, SHUKHRAT
                ['label' => 'News', 'url' => ['/wbadmin/news']],
                ['label' => 'Documents', 'url' => ['/wbadmin/documents']],
                ['label' => 'PIU Staff', 'url' => ['/wbadmin/staff']],
            ]
        ];

        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Log Out (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; World Bank <?= date('Y') ?></p>

        <p class="pull-right"><?php //= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
