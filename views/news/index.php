<?php

use yii\bootstrap4\LinkPager;
use yii\helpers\Html;


/* @var $models app\models\News */
$this->title = Yii::t('news', 'News');
?>

<style type="text/css">
    .center {
        display: block;
        margin-left: auto;
        margin-right: auto;
        width: 50%;
    }
    .pagination_center{
        margin: auto;
        width: fit-content;
    }
</style>

<div class="b-content-wrapper">
    <div class="row">
            <?php if ($models) : ?>
                <?php foreach ($models as $model) : ?>
                    <div class="col-md-4">
                        <div class="card shadow p-3 mb-4 bg-white">
                            <img class="card-img-top center" src="../../wb/images/news_images/<?= $model->news_image ?>" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title"><?=$model->news_title?></h5>
                                <p class="card-text"><?=$model->news_description?></p>
                                <?=
                                Html::a(Yii::t('news', 'READ MORE'), ['/'.(new \app\components\LangHelper())->check_lang().'/news/view', 'alias' => $model->news_alias], ['class' => 'b-read-more']); ?>
                            </div>
                            <div class="card-footer bg-transparent">
                                <small class="text-muted"><?=$model->news_date?></small>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php else : ?>
                <?= Yii::t('news', 'We have not any news yet!'); ?>
            <?php endif; ?>
        <div class="b-clear"></div>
    </div>
    <div class="pagination_center">
        <div>
        <?= LinkPager::widget([
            'pagination' => $pages,
        ]);?>
        </div>
    </div>
</div>