<?php
$this->title = Yii::t('news', $model->news_title);
?>
<style>
img{max-width: 100%;}
</style>

<div class="container-fluid shadow-sm p-3 mb-5 bg-white rounded" style="width: 50%;">
    <div class="b-content">
        <div class="b-pages">
            <img src="../../wb/images/news_images/<?= $model->news_image?>" class="img-fluid" alt="Responsive image" style="display: block; margin-left: auto; margin-right: auto; width: 50%">
            <div class="row" style="margin-top: 5px">
                <div class="col-sm" style="text-align: left;">
                    <h3 style="display: none"><?=$model->news_title?></h3>
                </div>
                <div class="col-sm" style="text-align: right">
                    <p><?=$model->news_date?></p>
                </div>
            </div>
            <h4><?=$model->news_description?></h4>
            <p><?=$model->news_text?></p>
            <div style="text-align: center">
                <a class="btn-floating btn-lg btn-fb" type="button" role="button"><i class="fab fa-facebook-f"></i></a>
                <a class="btn-floating btn-lg btn-tw" type="button" role="button"><i class="fab fa-twitter"></i></a>
                <a class="btn-floating btn-lg btn-gplus" type="button" role="button"><i class="fab fa-google-plus-g"></i></a>
                <a class="btn-floating btn-lg btn-li" type="button" role="button"><i class="fab fa-linkedin-in"></i></a>
            </div>
        </div>

    </div>
</div>



