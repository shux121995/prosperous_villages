<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\LocalityPlansSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $table \app\models\Tables */

$this->title = $table->title.' / '.$table->excel_name;
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index','table'=>$table->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Факт кушиш');
?>
<div class="plans-index">
    <h1>Факт киритиш: <?= $plan->year. ' / '. $plan->locality->district->title.' / '.$plan->locality->title ?></h1>
    <h3 style="display: none;"><?= Html::encode($this->title) ?></h3>
    <h3><?=$table->excel_description?></h3>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php $sections = $table->sectionsParents;

    if($sections != null){
        //ActiveForm::begin();
        echo \app\models\GenerateForm::genSectionsTabled($sections, $plan->id, $plan_status, 'fact');
        echo '<br>';
        /*echo \app\models\GenerateForm::genSections($sections,$plan->id, $check_table_plan_end==null?0:$check_table_plan_end->status, 'fact', false);
        if($check_table_plan_end!=null && $check_table_plan_end->status == 1) {
            echo Html::submitButton('Фактларни киритиш!',['class'=>"btn btn-danger"]);
        }
        ActiveForm::end();*/

    }
    ?>
</div>
<?php
$js = <<<JS
    // Initiate form validation
    $.validate({
    lang: 'uz'
    });
JS;
$this->registerJs($js);
?>
