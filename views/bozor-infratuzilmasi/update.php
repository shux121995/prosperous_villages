<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BozorInfratuzilmasi */

$this->title = 'Режани ўзгартириш: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Бозор инфратузилмаси объектлари', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Ўзгартириш';
?>
<div class="bozor-infratuzilmasi-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
