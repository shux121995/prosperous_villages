<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BozorInfratuzilmasi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bozor-infratuzilmasi-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'plan_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\LocalityPlans::find()->where(['region_id'=>(new \app\models\UserAccess())->getRegionId()])->all(), 'id', 'yearplan'), ['prompt' => 'Кишлок ёки махаллани танланг']) ?>

    <?= $form->field($model, 'loyiha_tashabbuskori')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'loyiha_yunalishi')->dropDownList(\app\models\BozorInfratuzilmasi::types(), ['prompt'=>'Лойиҳа йўналишини танланг']) ?>

    <?= $form->field($model, 'quvvat_ulchov_birlik')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'quvvat_natural_qiy')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'quvvat_ish_chiq_hajm')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'yil_eksport_hajm')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'yil_byudjetga_tushum_hajm')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'loyiha_qiy_reja')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'moliya_mab_reja')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'moliya_kredit_reja')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'moliya_invest_reja')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'xorij_hamkor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'horij_davlat')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'xiz_kursatuvchi_bank')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ish_urin_reja')->textInput() ?>

    <?= $form->field($model, 'ishga_tush_mud_reja')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ishga_tush_mud_asos')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'loyiha_muhan_yer_holati')->textInput() ?>

    <?= $form->field($model, 'loyiha_muhan_elektr_holati')->textInput() ?>

    <?= $form->field($model, 'loyiha_muhan_gaz_holati')->textInput() ?>

    <?= $form->field($model, 'loyiha_kredit_jadval_topshirish')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'loyiha_kredit_haq_topshirish')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'loyiha_kredit_jadval_ajratish')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'loyiha_kredit_haq_ajratish')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сақлаш', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
