<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\GenerateForm;
use kartik\date\DatePicker;
use yii\helpers\Url;
$Bozor = new \app\models\BozorInfratuzilmasi();

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\LocalityPlansSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $table \app\models\Tables */
?>
<?php $form = ActiveForm::begin([
    'id' => 'bozorForm',
]); ?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4><?=$model->title != null ? $model->title.' маълумотларини ўзгартириш' : 'Янги режа кушиш'?></h4>
    <div class="warning text-danger" style="display: none;">Диккат! Ушбу тулдириш формаси факат бир марта саклашга имкон беради! Шунинг учун тугри маълумот киритишингиз суралади!</div>
</div>
<div class="modal-body">

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'loyiha_tashabbuskori')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'loyiha_yunalishi')->dropDownList($Bozor->types(),['prompt'=>'Лойиха йўналишини танланг']) ?>

    <?= $form->field($model, 'quvvat_ulchov_birlik')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'quvvat_natural_qiy')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'quvvat_ish_chiq_hajm')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'yil_eksport_hajm')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'yil_byudjetga_tushum_hajm')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'loyiha_qiy_reja')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'xorij_hamkor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'horij_davlat')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'xiz_kursatuvchi_bank')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ish_urin_reja')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ishga_tush_mud_reja')->widget(DatePicker::classname(),[
        'options' => ['placeholder' => ''],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ]) ?>

    <?= $form->field($model, 'ishga_tush_mud_asos')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'loyiha_muhan_yer_holati')->dropDownList($Bozor->project_status1(),['prompt'=>'Танлаш']) ?>

    <?= $form->field($model, 'loyiha_muhan_elektr_holati')->dropDownList($Bozor->project_status2(),['prompt'=>'Танлаш']) ?>

    <?= $form->field($model, 'loyiha_muhan_gaz_holati')->dropDownList($Bozor->project_status2(),['prompt'=>'Танлаш']) ?>

    <?= $form->field($model, 'loyiha_kredit_jadval_topshirish')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'loyiha_kredit_haq_topshirish')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'loyiha_kredit_jadval_ajratish')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'loyiha_kredit_haq_ajratish')->textInput(['maxlength' => true]) ?>


    <?php if(!empty($moliya_manbai)): ?>
        <div class="moliya_manbai">
            <p><b><?=$model->getAttributeLabel('moliya_manbai')?></b></p>
            <?php if(!empty($id)):
                $values_plan = \app\models\ValuesPlan::find()->where(['plan_id'=>$plan->id, 'bozor_id'=>$id])->one();
                ?>
                <?=$values_plan->reja!=null ? 'Жами режа:' : ''?> <b><?=$values_plan->reja?></b><p></p>
            <?php endif; ?>
            <?php foreach($moliya_manbai as $key => $value): ?>
                <?php
                if(isset($values_plan)) {
                    $values_manba_plans = \app\models\ValuesManbaPlan::find()->where(['values_plan_id'=>$values_plan->id, 'moliya_manbai_id'=>$value['id']])->all();
                }
                ?>
                <?php if(isset($values_manba_plans)): ?>
                    <?php foreach($values_manba_plans as $item): ?>
                        <?= $form->field($model, 'moliya_manbai['.$value['id'].']')->textInput(['placeholder'=>"Режа", 'required'=>'required', 'value'=>$item->reja])->label($value['title']) ?>
                    <?php endforeach; ?>
                <?php else: ?>
                    <?= $form->field($model, 'moliya_manbai['.$value['id'].']')->textInput(['placeholder'=>"Режа", 'required'=>'required',])->label($value['title']) ?>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</div>
<div class="modal-footer">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo Yii::t('app', 'Close')?></button>
</div>
<?php ActiveForm::end(); ?>
<div class="clearfix clear"></div>
<hr/>
<div class="status-buttons-div">
    <?php
    if($model->status==0 OR $model->status==null) {
        $form = ActiveForm::begin();
        ?>
        <input type="hidden" name="status" value="1">
        <?php
        echo Html::submitButton('Активлаштириш!',['class'=>"btn btn-default plan-activate-btn marg-top-15"]);
        ActiveForm::end();
        ?>
        <?php
        $form = ActiveForm::begin();
        ?>
        <input type="hidden" name="status" value="2">
        <?php
        echo Html::submitButton('Режа йўқ!',['class'=>"btn btn-default plan-noPlan-btn marg-top-15"]);
        ActiveForm::end();
    }
    ?>
</div>

<?php
$js = <<<JS
// permit only float type for moliya_manbai
$('.moliya_manbai input').on('input', function() {
  this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
});

//Clear modal window after close
$(document).ready(function()
{
    $('.modal').on('hidden.bs.modal', function(e)
    { 
        $(this).removeData();
    }) ;
});
JS;
$this->registerJs($js);
?>
