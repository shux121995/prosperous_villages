<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BozorInfratuzilmasiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bozor-infratuzilmasi-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'plan_id') ?>

    <?= $form->field($model, 'loyiha_tashabbuskori') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'loyiha_yunalishi') ?>

    <?php // echo $form->field($model, 'quvvat_ulchov_birlik') ?>

    <?php // echo $form->field($model, 'quvvat_natural_qiy') ?>

    <?php // echo $form->field($model, 'quvvat_ish_chiq_hajm') ?>

    <?php // echo $form->field($model, 'yil_eksport_hajm') ?>

    <?php // echo $form->field($model, 'yil_byudjetga_tushum_hajm') ?>

    <?php // echo $form->field($model, 'loyiha_qiy_reja') ?>

    <?php // echo $form->field($model, 'moliya_mab_reja') ?>

    <?php // echo $form->field($model, 'moliya_kredit_reja') ?>

    <?php // echo $form->field($model, 'moliya_invest_reja') ?>

    <?php // echo $form->field($model, 'xorij_hamkor') ?>

    <?php // echo $form->field($model, 'horij_davlat') ?>

    <?php // echo $form->field($model, 'xiz_kursatuvchi_bank') ?>

    <?php // echo $form->field($model, 'ish_urin_reja') ?>

    <?php // echo $form->field($model, 'ishga_tush_mud_reja') ?>

    <?php // echo $form->field($model, 'ishga_tush_mud_asos') ?>

    <?php // echo $form->field($model, 'loyiha_muhan_yer_holati') ?>

    <?php // echo $form->field($model, 'loyiha_muhan_elektr_holati') ?>

    <?php // echo $form->field($model, 'loyiha_muhan_gaz_holati') ?>

    <?php // echo $form->field($model, 'loyiha_kredit_jadval_topshirish') ?>

    <?php // echo $form->field($model, 'loyiha_kredit_haq_topshirish') ?>

    <?php // echo $form->field($model, 'loyiha_kredit_jadval_ajratish') ?>

    <?php // echo $form->field($model, 'loyiha_kredit_haq_ajratish') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
