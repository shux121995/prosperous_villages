<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BozorInfratuzilmasi */

$this->title = 'Режа қўшиш';
$this->params['breadcrumbs'][] = ['label' => 'Бозор инфратузилмаси объектлари', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bozor-infratuzilmasi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
