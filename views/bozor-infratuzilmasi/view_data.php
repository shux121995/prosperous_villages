<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Qurulish */

$this->title = $model->title;
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h2><?= Html::encode($this->title) ?></h2>
</div>
<div class="modal-body">
    <table class="table-bordered table">
        <tr>
            <?php $column = 'title'; ?>
            <td><?=$model->getAttributeLabel($column)?></td>
            <td><?=$model->$column?></td>
        </tr>
        <tr>
            <?php $column = 'loyiha_tashabbuskori'; ?>
            <td><?=$model->getAttributeLabel($column)?></td>
            <td><?=$model->$column?></td>
        </tr>
        <tr>
            <?php $column = 'loyiha_yunalishi'; ?>
            <td><?=$model->getAttributeLabel($column)?></td>
            <td><?=\app\models\BozorInfratuzilmasi::types()[$model->$column]?></td>
        </tr>
        <tr>
            <?php $column = 'quvvat_ulchov_birlik'; ?>
            <td><?=$model->getAttributeLabel($column)?></td>
            <td><?=$model->$column?></td>
        </tr>
        <tr>
            <?php $column = 'quvvat_natural_qiy'; ?>
            <td><?=$model->getAttributeLabel($column)?></td>
            <td><?=$model->$column?></td>
        </tr>
        <tr>
            <?php $column = 'quvvat_ish_chiq_hajm'; ?>
            <td><?=$model->getAttributeLabel($column)?></td>
            <td><?=$model->$column?></td>
        </tr>
        <tr>
            <?php $column = 'yil_eksport_hajm'; ?>
            <td><?=$model->getAttributeLabel($column)?></td>
            <td><?=$model->$column?></td>
        </tr>
        <tr>
            <?php $column = 'yil_byudjetga_tushum_hajm'; ?>
            <td><?=$model->getAttributeLabel($column)?></td>
            <td><?=$model->$column?></td>
        </tr>
        <tr>
            <?php $column = 'loyiha_qiy_reja'; ?>
            <td><?=$model->getAttributeLabel($column)?></td>
            <td><?=$model->$column?></td>
        </tr>
        <tr>
            <?php $column = 'xorij_hamkor'; ?>
            <td><?=$model->getAttributeLabel($column)?></td>
            <td><?=$model->$column?></td>
        </tr>
        <tr>
            <?php $column = 'horij_davlat'; ?>
            <td><?=$model->getAttributeLabel($column)?></td>
            <td><?=$model->$column?></td>
        </tr>
        <tr>
            <?php $column = 'xiz_kursatuvchi_bank'; ?>
            <td><?=$model->getAttributeLabel($column)?></td>
            <td><?=$model->$column?></td>
        </tr>
        <tr>
            <?php $column = 'ish_urin_reja'; ?>
            <td><?=$model->getAttributeLabel($column)?></td>
            <td><?=$model->$column?></td>
        </tr>
        <tr>
            <?php $column = 'ishga_tush_mud_reja'; ?>
            <td><?=$model->getAttributeLabel($column)?></td>
            <td><?=$model->$column?></td>
        </tr>
        <tr>
            <?php $column = 'ishga_tush_mud_asos'; ?>
            <td><?=$model->getAttributeLabel($column)?></td>
            <td><?=$model->$column?></td>
        </tr>
        <tr>
            <?php $column = 'loyiha_muhan_yer_holati'; ?>
            <td><?=$model->getAttributeLabel($column)?></td>
            <td><?=$model->$column?></td>
        </tr>
        <tr>
            <?php $column = 'loyiha_muhan_elektr_holati'; ?>
            <td><?=$model->getAttributeLabel($column)?></td>
            <td><?=$model->$column?></td>
        </tr>
        <tr>
            <?php $column = 'loyiha_muhan_gaz_holati'; ?>
            <td><?=$model->getAttributeLabel($column)?></td>
            <td><?=$model->$column?></td>
        </tr>
        <tr>
            <?php $column = 'loyiha_kredit_jadval_topshirish'; ?>
            <td><?=$model->getAttributeLabel($column)?></td>
            <td><?=$model->$column?></td>
        </tr>
        <tr>
            <?php $column = 'loyiha_kredit_haq_topshirish'; ?>
            <td><?=$model->getAttributeLabel($column)?></td>
            <td><?=$model->$column?></td>
        </tr>
        <tr>
            <?php $column = 'loyiha_kredit_jadval_ajratish'; ?>
            <td><?=$model->getAttributeLabel($column)?></td>
            <td><?=$model->$column?></td>
        </tr>
        <tr>
            <?php $column = 'loyiha_kredit_haq_ajratish'; ?>
            <td><?=$model->getAttributeLabel($column)?></td>
            <td><?=$model->$column?></td>
        </tr>
        <?php $column = 'moliya_manbai'; ?>
        <td><?=$model->getAttributeLabel($column)?></td>
        <?php
        $values_plan = \app\models\ValuesPlan::find()->where(['plan_id'=>$plan->id, 'bozor_id'=>$model->id])->one();
        ?>
        <td>
            <?php if($values_plan != null): ?>
                <?php
                $values_manba_plan = \app\models\ValuesManbaPlan::find()->where(['values_plan_id'=>$values_plan->id])->all();
                ?>
                <?=$values_plan->reja!=null ? 'Жами режа:' : ''?> <b><?=$values_plan->reja?></b><br>
                <?php foreach($values_manba_plan as $item): ?>
                    <?=(new \app\models\MoliyaManbai())->getOneRecoord($item->moliya_manbai_id)['title']?>: <?=$item->reja?><br>
                <?php endforeach; ?>
            <?php endif; ?>
        </td>
    </table>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo Yii::t('app', 'Close')?></button>
</div>