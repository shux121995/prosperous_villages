<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BozorInfratuzilmasiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = \app\models\BozorInfratuzilmasi::TableName;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bozor-infratuzilmasi-index">

    <h1>РЕЖА</h1>
    <h2><?= Html::encode($this->title) ?></h2>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'year',
            [
                'label' => 'Махалла ёки кишлок',
                'value' => 'locality.title'
            ],
            [
                'label' => 'Туман',
                'value' => 'locality.district.title'
            ],
            [
                'label' => 'Киритинг',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a(Yii::t('app', 'Режа киритиш!'),
                        ['construction-objects', 'plan_id' => $model->id],
                        [
                            'title' => Yii::t('app', $this->title.'га режа киритиш'),
                            //'data-toggle' => 'modal',
                            //'data-target' => '#modal',
                            'class' => 'showModalButton btn btn-danger'
                        ]
                    );
                }
            ]

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
