<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\LocalityPlansSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $table \app\models\Tables */

$this->title = $table->title.' / '.$table->excel_name;
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index','table'=>$table->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Режа кушиш');
?>
<div class="plans-index">
    <h1>Режа киритиш: <?= $plan->year. ' / '. $plan->locality->district->title.' / '.$plan->locality->title ?></h1>
    <h3 style="display: none;"><?= Html::encode($this->title) ?></h3>
    <h3><?=$table->excel_description?></h3>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php $sections = $table->sectionsParents;

    if($sections != null){
        //ActiveForm::begin();
        echo \app\models\GenerateForm::genSectionsTabled($sections, $plan->id, $plan_status, 'plan');
        echo '<br>';
        //echo Html::submitButton('Режаларни киритиш!',['class'=>"btn btn-danger"]);
        //ActiveForm::end();
    }
    ?>
    <div class="clearfix clear"></div>
    <?php
        if($plan_status==0 OR $plan_status==null) {
        $form = ActiveForm::begin();
    ?>
        <input type="hidden" name="TablePlanEnd[plan_id]" value="<?=(int)$_GET['plan_id']?>">
        <input type="hidden" name="TablePlanEnd[table_id]" value="<?=(int)$_GET['table_id']?>">
        <input type="hidden" name="TablePlanEnd[status]" value="1">
    <?php
        echo Html::submitButton('Активлаштириш!',['class'=>"btn btn-success plan-activate-btn marg-top-15"]);
        ActiveForm::end();

    ?>
    <?php
    $form = ActiveForm::begin();
    ?>
    <input type="hidden" name="TablePlanEnd[plan_id]" value="<?=(int)$_GET['plan_id']?>">
    <input type="hidden" name="TablePlanEnd[table_id]" value="<?=(int)$_GET['table_id']?>">
    <input type="hidden" name="TablePlanEnd[status]" value="2">
    <?php
        echo Html::submitButton('Режа йўқ!',['class'=>"btn btn-warning plan-noPlan-btn marg-top-15"]);
        ActiveForm::end();
        }
    ?>
</div>
<?php
$js = <<<JS
    // Initiate form validation
    $.validate({
    lang: 'uz'
    });
JS;
$this->registerJs($js);
?>
