<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\results\models\GenerateTable;
use app\models\ValuesPlan;
use app\models\ValuesManbaPlan;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\LocalityPlansSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $table \app\models\Tables */

$this->title = $table->title.' / '.$table->excel_name;
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index','table'=>$table->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Режа кушиш');
error_reporting(0);
$pTitle = is_object($prevID)?$prevID->locality->title.' / '.$prevID->locality->district->title:null;
$nTitle = is_object($nextID)?$nextID->locality->title.' / '.$nextID->locality->district->title:null;
?>
<div class="plans-index">
    <h3 style="display: none;"><?= Html::encode($this->title) ?></h3>
    <h3><?=$table->excel_description?></h3>
    <h1>Режа киритиш: <?= $plan->year. ' / '. $plan->locality->district->title.' / '.$plan->locality->title ?>
        <?= Html::a('<b><<</b> '.$pTitle, ['fill', 'plan_id' => $prevID->id, 'table_id' => $table->id], ['class' => 'btn btn-danger r-align btn-sm '.$disablePrev]) ?>
        <?= Html::a($nTitle.' <b>>></b>', ['fill', 'plan_id' => $nextID->id, 'table_id' => $table->id], ['class' => 'btn btn-danger r-align btn-sm '.$disableNext]) ?>
    </h1>
    <style>.form-control {
            width: 100px;
            padding: 0;
            margin: 0;
        }
    </style>
    <?php $form = ActiveForm::begin([
        'id' => 'planform',
        'enableAjaxValidation' => true,
        //'enableClientValidation' => false,
        'action' => \yii\helpers\Url::to(['add-new','plan_id' => $plan->id,'table_id' => $table->id, ]),
    ]); ?>
    <table class="table table-bordered table-hover">
        <thead>
        <?=$header?>
        </thead>
        <tbody>
        <?php
        /** @var $local \app\models\MoliyaManbai*/
        $i = 1;
        foreach ($locals as $local){ ?>
            <tr>
                <td><?=$i?></td>
                <td><?=$local->title?></td>
                <?php
                $excels = GenerateTable::getExcelInputs($table->id,null,\app\models\TablesExcel::TYPE_ONLY_SECTION);
                /** @var $excel \app\models\TablesExcel*/
                foreach ($excels as $excel) {
                    $index = $excel->section_id.'_'.$local->id;
                    echo '<td>';
                if($plan_status==0 OR $plan_status==null) {
                    echo '<div style="display: none;">';
                    echo $form->field($models[$index], "[$index]id")->hiddenInput()->label(false);
                    echo $form->field($models[$index], "[$index]moliya_manbai_id")->hiddenInput()->label(false);
                    echo $form->field($models[$index], "[$index]section_id")->hiddenInput()->label(false);
                    echo '</div>';
                    $moliya = \app\models\MoliyaManbai::findOne($local->id);
                    echo '<div class="input_' . $index . '">';
                    echo $form->field($models[$index], "[$index]reja")->textInput(['placeholder' => $moliya->title, 'title' => $moliya->title])->label(false);
                }else{
                    /** @var $valuesPlan ValuesPlan*/
                    $valuesPlan = ValuesPlan::find()->where(['plan_id' => $plan->id, 'section_id' => $excel->section_id])->one();

                    if($valuesPlan!=null){
                        /** @var $manbaPlan ValuesManbaPlan*/
                        $manbaPlan = ValuesManbaPlan::find()->where(['values_plan_id'=>$valuesPlan->id,'moliya_manbai_id'=>$local->id])->one();
                        if($manbaPlan!=null){
                            echo $manbaPlan->reja;
                        }
                    }
                }
                    echo '</td>';
                }
                ?>
            </tr>
            <?php
            $i++;
        }
        ?>
        </tbody>
    </table>
    <?= ($plan_status==0 OR $plan_status==null)?Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']):'' ?>
    <?php ActiveForm::end(); ?>

    <div class="row fixbuttons">
        <div class="col-md-2"> <?php
            if($plan_status==0 OR $plan_status==null) {
            $form = ActiveForm::begin();
            ?>
            <input type="hidden" name="TablePlanEnd[plan_id]" value="<?=(int)$_GET['plan_id']?>">
            <input type="hidden" name="TablePlanEnd[table_id]" value="<?=(int)$_GET['table_id']?>">
            <input type="hidden" name="TablePlanEnd[status]" value="1">
            <?php
            echo Html::submitButton('Активлаштириш!',['onclick'=>"if(!confirm('Сиз хакикатдан активлаштирмокчимисиз? Хамма маълумот тугрими?')){return false;}",'class'=>"btn btn-danger marg-top-15"]);
            ActiveForm::end();

            ?></div>
        <div class="col-md-2"><?php
            $form = ActiveForm::begin();
            ?>
            <input type="hidden" name="TablePlanEnd[plan_id]" value="<?=(int)$_GET['plan_id']?>">
            <input type="hidden" name="TablePlanEnd[table_id]" value="<?=(int)$_GET['table_id']?>">
            <input type="hidden" name="TablePlanEnd[status]" value="2">
            <?php
            echo Html::submitButton('Режа йўқ!',['onclick'=>"if(!confirm('Хакикатдан ушбу режада режа йукми?')){return false;}",'class'=>"btn btn-warning marg-top-15"]);
            ActiveForm::end();
            }
            ?></div>
        <div class="clearfix clear"></div>
    </div>

</div>
<?php
$script = <<< JS
    // Initiate form validation
    $.validate({
    lang: 'uz'
    });

    $(document).ready(function () { 
        $("#planform").on('beforeSubmit', function (event) { 
            event.preventDefault();            
            var form_data = new FormData($('#planform')[0]);
            $.ajax({
                   url: $("#planform").attr('action'), 
                   dataType: 'JSON',  
                   cache: false,
                   contentType: false,
                   processData: false,
                   data: form_data, //$(this).serialize(),                      
                   type: 'post',                        
                   beforeSend: function() {
                   },
                   success: function(response){  
                       alert(response.message);
                        if(response.status!='error'){
                            location.reload();    
                        }                       
                   },
                   complete: function() {
                   },
                   error: function (data) {
                   }
                });                
            return false;
        });
    });       

JS;
$this->registerJs($script);
?>
