<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PlanForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="plan-form">

    <?php $form = ActiveForm::begin([
        'id' => 'planform',
        'enableAjaxValidation' => true,
        //'enableClientValidation' => false,
        'validationUrl' => \yii\helpers\Url::to(['fill-validate','plan_id' => $plan->id, 'section_id' => $section->id]),
    ]); ?>

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?php echo $plan->locality->title?> <?php echo $section->title?>га режа кушиш</h4>
        <div class="warning text-danger" style="display: none;">Диккат! Ушбу тулдириш формаси факат бир марта саклашга имкон беради! Шунинг учун тугри маълумот киритишингиз суралади!</div>
    </div>
    <div class="modal-body">
        <h4 class="modal-title"><b><?php echo $section->title?></b>ни молиялаштирш манбалари буйича режа:</h4>
        <?php
        foreach ($models as $index => $model) {
            echo $form->field($model, "[$index]id")->hiddenInput()->label(false);
            echo $form->field($model, "[$index]moliya_manbai_id")->hiddenInput()->label(false);

            $moliya = \app\models\MoliyaManbai::findOne($index);
            echo Html::label($moliya->title,$form->id.'-'.$index.'-reja',['onclick'=>'$("no__input_'.$index.'").slideToggle();']);
            echo '<div style="display:none;">'.$form->field($model, "[$index]is_fill")->checkbox()->label(false).'</div>';
            echo '<div class="input_'.$index.'">';
            echo $form->field($model, "[$index]reja")->textInput(['placeholder'=>"Режа"])->label(false);
            echo '</div>';
        }
        ?>
    </div>
    <div class="modal-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo Yii::t('app', 'Close')?></button>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$script = <<< JS
    $(document).ready(function () { 
        $("#planform").on('beforeSubmit', function (event) { 
            event.preventDefault();            
            var form_data = new FormData($('#planform')[0]);
            $.ajax({
                   url: $("#planform").attr('action'), 
                   dataType: 'JSON',  
                   cache: false,
                   contentType: false,
                   processData: false,
                   data: form_data, //$(this).serialize(),                      
                   type: 'post',                        
                   beforeSend: function() {
                   },
                   success: function(response){                       
                        if(response.status=='error'){
                            alert(response.message);    
                        }else{
                            $('#modal').modal('hide');
                            location.reload();    
                        }                       
                   },
                   complete: function() {
                   },
                   error: function (data) {
                   }
                });                
            return false;
        });
    });       

JS;
$this->registerJs($script);
?>
