<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\QurulishSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="qurulish-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'plan_id') ?>

    <?= $form->field($model, 'type') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'urni') ?>

    <?php // echo $form->field($model, 'baj_ish_turi') ?>

    <?php // echo $form->field($model, 'qiy_reja') ?>

    <?php // echo $form->field($model, 'qiy_moliya') ?>

    <?php // echo $form->field($model, 'qiy_molled') ?>

    <?php // echo $form->field($model, 'ishga_tushish_reja') ?>

    <?php // echo $form->field($model, 'asos_akt') ?>

    <?php // echo $form->field($model, 'asos_date') ?>

    <?php // echo $form->field($model, 'loyiha_smeta_hujjat') ?>

    <?php // echo $form->field($model, 'tarmoq_jadvali') ?>

    <?php // echo $form->field($model, 'loyihachi_tashkilot') ?>

    <?php // echo $form->field($model, 'pudratchi_tashkilot') ?>

    <?php // echo $form->field($model, 'maktab_raqam') ?>

    <?php // echo $form->field($model, 'mtm_mulkchilik_shakli') ?>

    <?php // echo $form->field($model, 'mtm_raqam') ?>

    <?php // echo $form->field($model, 'ssm') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
