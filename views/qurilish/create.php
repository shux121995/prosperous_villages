<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Qurulish */

$this->title = 'Режа қўшиш';
$this->params['breadcrumbs'][] = ['label' => 'Қуриш ва таъмирлаш ишлари', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="qurulish-create">

    <h1>Режа қўшиш</h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
