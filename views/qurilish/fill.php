<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\GenerateForm;
use kartik\date\DatePicker;
use yii\helpers\Url;
$Qurulish = new \app\models\Qurulish();

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\LocalityPlansSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $table \app\models\Tables */
if(!empty($display_type)){
    echo '<style>'
        .$display_type.' {
            display:block;
            }
         </style>';
}
if(!empty($display_type_second)){
    echo '<style>'
        .$display_type_second.' {
            display:block;
            }
         </style>';
}
?>
<?php $form = ActiveForm::begin([
    'id' => 'qurulishform',
]); ?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4><?=$model->title != null ? $model->title.' маълумотларини ўзгартириш' : 'Янги режа кушиш'?></h4>
    <div class="warning text-danger" style="display: none;">Диккат! Ушбу тулдириш формаси факат бир марта саклашга имкон беради! Шунинг учун тугри маълумот киритишингиз суралади!</div>
</div>
<div class="modal-body">

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->dropDownList($Qurulish->types(),['prompt'=>'Объект турини танланг']) ?>

    <?= $form->field($model, 'urni')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'baj_ish_turi')->dropDownList($Qurulish->workTypes(),['prompt'=>'Бажариладиган иш турини танланг']) ?>

    <?= $form->field($model, 'qiy_molled')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ishga_tushish_reja')->widget(DatePicker::classname(),[
        'options' => ['placeholder' => 'Факт киртишни бошланиши'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ]) ?>

    <?= $form->field($model, 'asos_akt')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'asos_date')->widget(DatePicker::classname(),[
        'options' => ['placeholder' => 'Факт киртишни бошланиши'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ]) ?>

    <?= $form->field($model, 'loyiha_smeta_hujjat')->dropDownList($Qurulish->exist_project_document(),['prompt'=>'Танлаш']) ?>

    <?= $form->field($model, 'tarmoq_jadvali')->dropDownList($Qurulish->exist_tarmoq_jadvali(),['prompt'=>'Танлаш']) ?>

    <?= $form->field($model, 'loyihachi_tashkilot')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pudratchi_tashkilot')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'maktab_raqam')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mtm_mulkchilik_shakli')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mtm_raqam')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ssm')->textInput(['maxlength' => true]) ?>
    <?php if(!empty($moliya_manbai)): ?>
        <div class="moliya_manbai">
            <p><b><?=$model->getAttributeLabel('moliya_manbai')?></b></p>
            <?php if(!empty($id)):
                $values_plan = \app\models\ValuesPlan::find()->where(['plan_id'=>$plan->id, 'qurulish_id'=>$id])->one();
            ?>
                <?=$values_plan->reja!=null ? 'Жами режа:' : ''?> <b><?=$values_plan->reja?></b><p></p>
            <?php endif; ?>
            <?php foreach($moliya_manbai as $key => $value): ?>
                <?php
                if(isset($values_plan)) {
                    $values_manba_plans = \app\models\ValuesManbaPlan::find()->where(['values_plan_id'=>$values_plan->id, 'moliya_manbai_id'=>$value['id']])->all();
                }
                ?>
                <?php if(isset($values_manba_plans)): ?>
                    <?php foreach($values_manba_plans as $item): ?>
                        <?= $form->field($model, 'moliya_manbai['.$value['id'].']')->textInput(['placeholder'=>"Режа", 'required'=>'required', 'value'=>$item->reja])->label($value['title']) ?>
                    <?php endforeach; ?>
                <?php else: ?>
                    <?= $form->field($model, 'moliya_manbai['.$value['id'].']')->textInput(['placeholder'=>"Режа", 'required'=>'required',])->label($value['title']) ?>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</div>
<div class="modal-footer">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo Yii::t('app', 'Close')?></button>
</div>
    <?php ActiveForm::end(); ?>
    <div class="clearfix clear"></div>
    <hr/>
    <div class="status-buttons-div">
    <?php
    if($model->status==0 OR $model->status==null) {
        $form = ActiveForm::begin();
        ?>
        <input type="hidden" name="status" value="1">
        <?php
        echo Html::submitButton('Активлаштириш!',['class'=>"btn btn-default plan-activate-btn marg-top-15"]);
        ActiveForm::end();
        ?>
        <?php
        $form = ActiveForm::begin();
        ?>
        <input type="hidden" name="status" value="2">
        <?php
        echo Html::submitButton('Режа йўқ!',['class'=>"btn btn-default plan-noPlan-btn marg-top-15"]);
        ActiveForm::end();
    }
    ?>
    </div>

<?php
$js = <<<JS
// Types event
$('select#qurulish-type').change(function() {
    if($(this).val() == 1) {
        $('.field-qurulish-maktab_raqam').css("display", "block");
        
        $('.field-qurulish-mtm_raqam').css("display", "none"); 
        $('.field-qurulish-mtm_mulkchilik_shakli').css("display", "none"); 
        $('.field-qurulish-ssm').css("display", "none"); 
    }
    else if($(this).val() == 2){
        $('.field-qurulish-mtm_raqam').css("display", "block"); 
        $('.field-qurulish-mtm_mulkchilik_shakli').css("display", "block"); 
        
        $('.field-qurulish-maktab_raqam').css("display", "none");
        $('.field-qurulish-ssm').css("display", "none");
    }
    else if($(this).val() == 3){
        $('.field-qurulish-ssm').css("display", "block"); 
        
        
        $('.field-qurulish-maktab_raqam').css("display", "none"); 
        $('.field-qurulish-mtm_raqam').css("display", "none"); 
        $('.field-qurulish-mtm_mulkchilik_shakli').css("display", "none"); 
    }
    else if($(this).val() == 4) {
        $('.field-qurulish-maktab_raqam').css("display", "none"); 
        $('.field-qurulish-mtm_raqam').css("display", "none"); 
        $('.field-qurulish-mtm_mulkchilik_shakli').css("display", "none"); 
        $('.field-qurulish-ssm').css("display", "none");
    }
});

// permit only float type for moliya_manbai
$('.moliya_manbai input').on('input', function() {
  this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
});

//Clear modal window after close
$(document).ready(function()
{
    $('.modal').on('hidden.bs.modal', function(e)
    { 
        $(this).removeData();
    }) ;
});
JS;
$this->registerJs($js);
?>
