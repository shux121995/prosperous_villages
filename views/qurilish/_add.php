<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\PlanForm */
/* @var $form yii\widgets\ActiveForm */
$Qurulish = new \app\models\Qurulish();
?>

<div class="plan-form">

    <?php $form = ActiveForm::begin([
        'id' => 'qurulishform',
    ]); ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?=$plan->locality->title.' '.$model->getAttributeLabel($input)?>га режа кушиш</h4>
        <div class="warning text-danger" style="display: none;">Диккат! Ушбу тулдириш формаси факат бир марта саклашга имкон беради! Шунинг учун тугри маълумот киритишингиз суралади!</div>
    </div>
    <div class="modal-body">
        <?php if($input == 'type'): ?>
            <?= $form->field($model, $input)->dropDownList($Qurulish->types(), ['prompt'=>'Объект турини танланг']) ?>
        <?php elseif($input == 'baj_ish_turi'): ?>
            <?= $form->field($model, $input)->dropDownList($Qurulish->workTypes(), ['prompt'=>'Бажариладиган иш турини танланг']) ?>
        <?php elseif($input == 'ishga_tushish_reja' or $input == 'asos_date'): ?>
            <?=  $form->field($model, $input)->widget(DatePicker::classname(),[
                'options' => ['placeholder' => 'Факт киртишни бошланиши'],
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true
                ]
            ]) ?>
        <?php elseif($input == 'moliya_manbai'): ?>
            <?php
                $values_plans = \app\models\ValuesPlan::find()->where(['plan_id'=>$plan->id, 'qurulish_id'=>$id])->one();
            ?>
            <?php foreach($moliya_manbai as $key => $value): ?>
                <?php
                    $values_manba_plans = \app\models\ValuesManbaPlan::find()->where(['values_plan_id'=>$values_plans->id, 'moliya_manbai_id'=>$value['id']])->all();
                ?>
                <?php if($values_manba_plans != null): ?>
                    <?php foreach($values_manba_plans as $item): ?>
                        <?= $form->field($model, 'moliya_manbai['.$value['id'].']')->textInput(['placeholder'=>"Режа", 'value'=>$item->reja])->label($value['title']) ?>
                    <?php endforeach; ?>
                <?php else: ?>
                    <?= $form->field($model, 'moliya_manbai['.$value['id'].']')->textInput(['placeholder'=>"Режа"])->label($value['title']) ?>
                <?php endif; ?>
             <?php endforeach; ?>
        <?php else: ?>
            <?= $form->field($model, $input)->textInput(['maxlength' => true]) ?>
        <?php endif; ?>
    </div>
    <div class="modal-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo Yii::t('app', 'Close')?></button>
    </div>


    <?php ActiveForm::end(); ?>

</div>