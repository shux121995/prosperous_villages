<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Qurulish */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="qurulish-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'plan_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\LocalityPlans::find()->where(['region_id'=>(new \app\models\UserAccess)->getRegionId()])->all(), 'id', 'yearplan'), ['prompt' => 'Кишлок ёки махаллани танланг']) ?>

    <?= $form->field($model, 'type')->dropDownList((new \app\models\Qurulish)->types(), ['prompt'=>'Объект турини танланг']) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'urni')->textInput() ?>

    <?= $form->field($model, 'baj_ish_turi')->textInput() ?>

    <?= $form->field($model, 'qiy_reja')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'qiy_moliya')->textInput() ?>

    <?= $form->field($model, 'qiy_molled')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ishga_tushish_reja')->textInput() ?>

    <?= $form->field($model, 'asos_akt')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'asos_date')->input('date') ?>

    <?= $form->field($model, 'loyiha_smeta_hujjat')->textInput() ?>

    <?= $form->field($model, 'tarmoq_jadvali')->textInput() ?>

    <?= $form->field($model, 'loyihachi_tashkilot')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pudratchi_tashkilot')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'maktab_raqam')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mtm_mulkchilik_shakli')->textInput() ?>

    <?= $form->field($model, 'mtm_raqam')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ssm')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сақлаш', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
