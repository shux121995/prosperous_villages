<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Qurulish;

/* @var $this yii\web\View */
/* @var $model app\models\Qurulish */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Қуриш ва таъмирлаш ишлари', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h2><?= Html::encode($this->title) ?></h2>
</div>
<div class="modal-body">
    <table class="table-bordered table">
        <tr>
        <?php $column = 'title'; ?>
        <td><?=$model->getAttributeLabel($column)?></td>
            <td><?=$model->$column?></td>
        </tr>
        <tr>
            <?php $column = 'type'; ?>
            <td><?=$model->getAttributeLabel($column)?></td>
            <td><?=Qurulish::types()[$model->$column]?></td>
        </tr>
        <tr>
            <?php $column = 'urni'; ?>
            <td><?=$model->getAttributeLabel($column)?></td>
            <td><?=$model->$column?></td>
        </tr>
        <tr>
            <?php $column = 'baj_ish_turi'; ?>
            <td><?=$model->getAttributeLabel($column)?></td>
            <td><?=Qurulish::workTypes()[$model->$column]?></td>
        </tr>
        <tr>
            <?php $column = 'qiy_molled'; ?>
            <td><?=$model->getAttributeLabel($column)?></td>
            <td><?=$model->$column?></td>
        </tr>
        <tr>
            <?php $column = 'ishga_tushish_reja'; ?>
            <td><?=$model->getAttributeLabel($column)?></td>
            <td><?=$model->$column?></td>
        </tr>
        <tr>
            <?php $column = 'asos_akt'; ?>
            <td><?=$model->getAttributeLabel($column)?></td>
            <td><?=$model->$column?></td>
        </tr>
        <tr>
            <?php $column = 'asos_date'; ?>
            <td><?=$model->getAttributeLabel($column)?></td>
            <td><?=$model->$column?></td>
        </tr>
        <tr>
            <?php $column = 'loyiha_smeta_hujjat'; ?>
            <td><?=$model->getAttributeLabel($column)?></td>
            <td><?=Qurulish::exist_project_document()[$model->$column]?></td>
        </tr>
        <tr>
            <?php $column = 'tarmoq_jadvali'; ?>
            <td><?=$model->getAttributeLabel($column)?></td>
            <td><?=Qurulish::exist_tarmoq_jadvali()[$model->$column]?></td>
        </tr>
        <tr>
            <?php $column = 'loyihachi_tashkilot'; ?>
            <td><?=$model->getAttributeLabel($column)?></td>
            <td><?=$model->$column?></td>
        </tr>
        <tr>
            <?php $column = 'pudratchi_tashkilot'; ?>
            <td><?=$model->getAttributeLabel($column)?></td>
            <td><?=$model->$column?></td>
        </tr>
        <?php if($model->type == 1): ?>
            <tr>
                <?php $column = 'maktab_raqam'; ?>
                <td><?=$model->getAttributeLabel($column)?></td>
                <td><?=$model->$column?></td>
            </tr>
        <?php elseif($model->type == 2): ?>
            <tr>
                <?php $column = 'mtm_raqam'; ?>
                <td><?=$model->getAttributeLabel($column)?></td>
                <td><?=$model->$column?></td>
            </tr>
            <tr>
                <?php $column = 'mtm_mulkchilik_shakli'; ?>
                <td><?=$model->getAttributeLabel($column)?></td>
                <td><?=$model->$column?></td>
            </tr>
        <?php elseif($model->type == 3): ?>
            <tr>
                <?php $column = 'ssm'; ?>
                <td><?=$model->getAttributeLabel($column)?></td>
                <td><?=$model->$column?></td>
            </tr>
        <?php endif; ?>
        <?php $column = 'moliya_manbai'; ?>
        <td><?=$model->getAttributeLabel($column)?></td>
        <?php
        $values_plan = \app\models\ValuesPlan::find()->where(['plan_id'=>$plan->id, 'qurulish_id'=>$model->id])->one();
        ?>
        <td>
            <?php if($values_plan != null): ?>
                <?php
                $values_manba_plan = \app\models\ValuesManbaPlan::find()->where(['values_plan_id'=>$values_plan->id])->all();
                ?>
                <?=$values_plan->reja!=null ? 'Жами режа:' : ''?> <b><?=$values_plan->reja?></b><br>
                <?php foreach($values_manba_plan as $item): ?>
                    <?=(new \app\models\MoliyaManbai())->getOneRecoord($item->moliya_manbai_id)['title']?>: <?=$item->reja?><br>
                <?php endforeach; ?>
            <?php endif; ?>
        </td>
    </table>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo Yii::t('app', 'Close')?></button>
</div>