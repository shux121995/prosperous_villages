<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\LocalityPlansSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app',  $plan->year. ' / '. $plan->locality->district->title.' / '.$plan->locality->title);
$this->params['breadcrumbs'][] = ['label' => \app\models\Qurulish::TableName, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="locality-plans-index">

    <h1><?='Режа киритиш: '.$this->title?></h1>
    <h3><?=\app\models\Qurulish::TableName?></h3>
    <p><?=Html::a('Яратиш',['fill', 'plan_id'=>$plan->id],['data-toggle' => 'modal', 'data-target' => '#modal','class'=>'btn btn-success'])?></p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'title',
            [
                'label' => 'Киритинг',
                'format' => 'raw',
                'value' => function ($dataProvider) use ($plan, $page, $per_page) {
                    return Html::a(Yii::t('app', 'Режа киритиш!'),
                        ['fill', 'plan_id' => $plan->id,'id'=>$dataProvider->id, 'page'=>$page, 'per_page'=>$per_page],
                        [
                            'title' => Yii::t('app', $this->title.'га режа киритиш'),
                            'data-toggle' => 'modal',
                            'data-target' => '#modal',
                            'class' => 'showModalButton btn btn-danger'
                        ]
                    );
                }
            ]

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
<div class="modal remote fade" id="modal">
    <div class="modal-dialog">
        <div id="modalContent" class="modal-content loader-lg"></div>
    </div>
</div>
