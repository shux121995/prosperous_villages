$(document).ready(function () {
    $(".modal").each(function(l){$(this).on("show.bs.modal",function(l){var o=$(this).attr("data-easein");"shake"==o?$(".modal-dialog").velocity("callout."+o):"pulse"==o?$(".modal-dialog").velocity("callout."+o):"tada"==o?$(".modal-dialog").velocity("callout."+o):"flash"==o?$(".modal-dialog").velocity("callout."+o):"bounce"==o?$(".modal-dialog").velocity("callout."+o):"swing"==o?$(".modal-dialog").velocity("callout."+o):$(".modal-dialog").velocity("transition."+o)})});
    (function($){
        $(document).on('click', '.showModalButton', function () {
            if ($('#modal').data('bs.modal').isShown) {
                $('#modal').find('#modalContent')
                    .load($(this).attr('href'));
            } else {
                //if modal isn't open; open it and load content
                $('#modal').modal('show')
                    .find('#modalContent')
                    .load($(this).attr('href'));
            }
        });
    })(jQuery);

//Clear modal window after close
    $('.modal').on('hidden.bs.modal', function(e)
    {
        $(this).removeData();
    });

});


