$(document).ready(function () {

    $('.devadd').click(function(event){ // нажатие на кнопку - выпадает модальное окно
        event.preventDefault();


        var url = '/wbadmin/developers/devadd';
        var clickedbtn = $(this);
        //var UserID = clickedbtn.data("userid");

        var modalContainer = $('#devadd');
        var modalBody = modalContainer.find('.modal-body');
        modalContainer.modal({show:true});
        $.ajax({
            url: url,
            type: "GET",
            data: {/*'userid':UserID*/},
            success: function (data) {
                $('.modal-body').html(data);
                modalContainer.modal({show:true});
            }
        });
    });
    $(document).on("submit", '.devadd-form', function (e) {
        e.preventDefault();
        var form = $(this);
        $.ajax({
            url: "/wbadmin/developers/devaddsubmit",
            type: "POST",
            data: form.serialize(),
            success: function (result) {
                console.log(result);
                var modalContainer = $('#devadd');
                var modalBody = modalContainer.find('.modal-body');

                var result = $.parseJSON(result);

                if (result.status) {
                    $.get( "devs")
                        .done(function( data ) {
                                $( "#wbplans-dev_id" ).html( data );
                                $("select#wbplans-dev_id").val(result.id);
                            }
                        );

                    setTimeout(function() { // скрываем modal через 4 секунды
                        $("#devadd").modal('hide');
                    }, 800);
                }
                else {
                    modalBody.html(result).hide().fadeIn();
                }
            }
        });
    });

    $('.iadd').click(function(event){ // нажатие на кнопку - выпадает модальное окно
        event.preventDefault();


        var url = $(this).attr('data-href');
        //'/wbadmin/developers/devadd';
        var clickedbtn = $(this);
        //var UserID = clickedbtn.data("userid");

        var modalContainer = $('#iadd');
        var modalBody = modalContainer.find('.modal-body');
        modalContainer.modal({show:true});
        $.ajax({
            url: url,
            type: "GET",
            data: {/*'userid':UserID*/},
            success: function (data) {
                $('.modal-body').html(data);
                modalContainer.modal({show:true});
            }
        });
    });

    $(document).on("submit", '.iadd-form', function (e) {
        e.preventDefault();
        var form = $(this);
        $.ajax({
            url: form.attr('action'),//"/wbadmin/developers/devaddsubmit",
            type: 'POST',
            data: new FormData( this ),
            processData: false,
            contentType: false,
            success: function (result) {
                console.log(result);
                var modalContainer = $('#iadd');
                var modalBody = modalContainer.find('.modal-body');

                try {
                    var result = $.parseJSON(result);
                    if (result.status) {
                        alert('Add successfully!');

                        setTimeout(function() { // скрываем modal через 4 секунды
                            $("#iadd").modal('hide');
                            modalBody.html('');
                        }, 800);
                    }
                } catch (e) {
                    modalBody.html(result).hide().fadeIn();
                }
            }
        });
    });

});