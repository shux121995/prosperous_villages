<?php
/**
 * Created by PhpStorm.
 * User: Muxtorov Ulugbek
 * Date: 12.09.2019
 * Time: 18:36
 */

namespace app\commands;

use Yii;
use yii\console\Controller;
use \app\rbac\UserGroupRule;

class RbacController extends Controller
{
    public function actionInit()
    {
        $authManager = \Yii::$app->authManager;

        // Create roles
        $guest  = $authManager->createRole('guest');
        $viewer  = $authManager->createRole('viewer');
        $author = $authManager->createRole('author');
        $region = $authManager->createRole('region');
        $admin  = $authManager->createRole('admin');

        // Create simple, based on action{$NAME} permissions
        $login  = $authManager->createPermission('login');
        $logout = $authManager->createPermission('logout');
        $error  = $authManager->createPermission('error');
        $signUp = $authManager->createPermission('sign-up');
        $index  = $authManager->createPermission('index');
        $view   = $authManager->createPermission('view');
        $create   = $authManager->createPermission('create');
        $update = $authManager->createPermission('update');
        $delete = $authManager->createPermission('delete');

        // Add permissions in Yii::$app->authManager
        $authManager->add($login);
        $authManager->add($logout);
        $authManager->add($error);
        $authManager->add($signUp);
        $authManager->add($index);
        $authManager->add($view);
        $authManager->add($create);
        $authManager->add($update);
        $authManager->add($delete);


        // Add rule, based on UserExt->group === $user->group
        $userGroupRule = new UserGroupRule();
        $authManager->add($userGroupRule);

        // Add rule "UserGroupRule" in roles
        $guest->ruleName  = $userGroupRule->name;
        $viewer->ruleName  = $userGroupRule->name;
        $author->ruleName = $userGroupRule->name;
        $region->ruleName = $userGroupRule->name;
        $admin->ruleName  = $userGroupRule->name;

        // Add roles in Yii::$app->authManager
        $authManager->add($guest);
        $authManager->add($viewer);
        $authManager->add($author);
        $authManager->add($region);
        $authManager->add($admin);

        // Add permission-per-role in Yii::$app->authManager
        // Guest
        $authManager->addChild($guest, $login);
        $authManager->addChild($guest, $logout);
        $authManager->addChild($guest, $error);
        $authManager->addChild($guest, $signUp);
        $authManager->addChild($guest, $index);

        // Viewer
        $authManager->addChild($viewer, $guest);

        // Author
        $authManager->addChild($region, $create);
        $authManager->addChild($region, $viewer);

        // Region
        $authManager->addChild($region, $update);
        $authManager->addChild($region, $author);

        // Admin
        $authManager->addChild($admin, $delete);
        $authManager->addChild($admin, $viewer);
        $authManager->addChild($admin, $author);
        $authManager->addChild($admin, $region);
    }
}