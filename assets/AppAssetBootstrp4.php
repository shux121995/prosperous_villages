<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;
use Yii;
use yii\web\View;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAssetBootstrp4 extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'wb/sly/horizontal.css?ver=2.0',
        'wb/css/svg.css?ver=2.0',
        'wb/css/main.css?ver=2.1.0.1',
        'wb/css/css.css?ver=2.0.1.0',
        'wb/css/about.css?ver=2.2.1.7',
        'wb/css/stepper/steppers.min.css?ver=1.0',
    ];
    public $js = [
        'wb/sly/plugins.js',
        'wb/sly/sly.min.js',
        'wb/sly/horizontal.js?v=1.0.0.6',
        'wb/js/jquery.sticky-kit.min.js',
        'wb/highcharts/highcharts.js',
        'wb/highcharts/highcharts-3d.js',
        'wb/highcharts/modules/exporting.js',
        'wb/highcharts/modules/export-data.js',
        'wb/js/popper.js?v=1.0.0.9',/*WHAT I INCLUDED*/
        'wb/js/bootstrap.js?v=1.0.0.9',/*WHAT I INCLUDED*/
        'wb/js/stepper/steppers.min.js?v=1.0',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];
    public function init()
    {
        parent::init(); // TODO: Change the autogenerated stub
        Yii::$app->view->on(View::EVENT_AFTER_RENDER, function (){
            unset(Yii::$app->view->assetBundles['yii\bootstrap\BootstrapPluginAsset']);
        });
        Yii::$app->view->on(View::EVENT_BEGIN_BODY, function (){
            unset(Yii::$app->view->assetBundles['yii\bootstrap\BootstrapPluginAsset']);
        });
    }
}
