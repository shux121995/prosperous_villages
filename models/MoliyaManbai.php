<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%moliya_manbai}}".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property int $parent_id
 *
 * @property ValuesPlan[] $valuesPlans
 */
class MoliyaManbai extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%moliya_manbai}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['parent_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'parent_id' => Yii::t('app', 'Parent ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValuesPlans()
    {
        return $this->hasMany(ValuesPlan::className(), ['moliya_manbai_id' => 'id']);
    }
    public function getOneRecoord($id)
    {
        return MoliyaManbai::findOne($id);
    }
}
