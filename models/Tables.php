<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tables}}".
 *
 * @property int $id
 * @property string $title
 * @property string $title_en
 * @property string $excel_name
 * @property string $excel_description
 * @property string $excel_description_en
 * @property string $levels
 * @property string $project_type
 * @property string $year
 * @property int $status
 *
 * @property Sections[] $sections
 * @property Sections[] $sectionsParents
 */
class Tables extends \yii\db\ActiveRecord
{
    const STATUS_ON = '1'; //Open for fill all
    const STATUS_PLAN = '2'; //Open for fill only plan
    const STATUS_FACT = '3'; //Open for fill only fact
    const STATUS_OFF = '5'; //Close for fill all

    /**
     * @return array
     */
    public static function statusLables()
    {
        return [
            self::STATUS_ON => 'Ҳаммаси очиқ',
            self::STATUS_PLAN => 'Фақат режа',
            self::STATUS_FACT => 'Фақат факт',
            self::STATUS_OFF => 'Ҳаммаси ёпиқ',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tables}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'project_type', 'year'], 'required'],
            [['excel_description', 'excel_description_en','project_type'], 'string'],
            [['title', 'title_en'], 'string', 'max' => 255],
            [['excel_name'], 'string', 'max' => 128],
            [['project_type'], 'string', 'max' => 50],
            [['year', 'status'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'title_en' => Yii::t('app', 'Title En'),
            'excel_name' => Yii::t('app', 'Excel Name'),
            'excel_description' => Yii::t('app', 'Excel Description'),
            'excel_description_en' => Yii::t('app', 'Excel Description En'),
            'levels' => Yii::t('app', 'Levels(rows)'),
            'project_type' => Yii::t('app', 'Project type'),
            'year' => Yii::t('app', 'Year'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @param $lang
     * @return mixed|string
     * Lang helper by lang title
     */
    public function getTitleLang($lang)
    {
        if($lang!='uz'){
            $lang = 'title_'.$lang;
            return !empty($this->$lang)?$this->$lang:$this->title;
        }else{
            return $this->title;
        }
    }

    /**
     * @param $lang
     * @return mixed|string
     * Lang helper by lang description
     */
    public function getDescriptionLang($lang)
    {
        if($lang!='uz'){
            $lang = 'excel_description_'.$lang;
            return !empty($this->$lang)?$this->$lang:$this->title;
        }else{
            return $this->title;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSections()
    {
        return $this->hasMany(Sections::className(), ['tables_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSectionsParents()
    {
        return $this->hasMany(Sections::className(), ['tables_id' => 'id'])->where(['parent_id'=>null]);
    }

    /**
     * @return mixed3
     */
    public function getPlan(){
        $subQuery = Sections::find()->select('id')->where(['tables_id'=>$this->id, 'forms_id'=>1]);
        return ValuesPlan::find()->where(['in', 'section_id', $subQuery])->sum('reja');
    }

    /**
     * @return mixed3
     */
    public function getFact(){
        $subQuery = Sections::find()->select('id')->where(['tables_id'=>$this->id, 'forms_id'=>1]);
        $subQuery2 = ValuesPlan::find()->select('id')->where(['in', 'section_id', $subQuery]);
        return ValuesFact::find()->where(['in', 'plan_value_id', $subQuery2])->sum('fact');
    }

    /**
     * @return mixed3
     */
    public static function planLocality($section_id, $local_id){
        $subQuery = LocalityPlans::find()->select('id')->where(['locality_id'=>$local_id, 'year'=>2019]);
        return ValuesPlan::find()
            ->where(['section_id' => $section_id])
            ->andWhere(['in', 'plan_id', $subQuery])
            ->sum('reja');
    }

    /**
     * @return mixed3
     */
    public static function factLocality($section_id, $local_id){
        $subQuery = LocalityPlans::find()->select('id')->where(['locality_id'=>$local_id, 'year'=>2019]);
        $subQuery2 = ValuesPlan::find()
            ->select('id')
            ->where(['section_id' => $section_id])
            ->andWhere(['in', 'plan_id', $subQuery]);
        return ValuesFact::find()->where(['in', 'plan_value_id', $subQuery2])->sum('fact');
    }
}
