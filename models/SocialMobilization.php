<?php


namespace app\models;


use yii\db\ActiveRecord;

class SocialMobilization extends ActiveRecord
{
    public static function tableName()
    {
        return 'social_mobilization';
    }
}