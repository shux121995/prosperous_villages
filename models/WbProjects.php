<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%wb_projects}}".
 *
 * @property int $id
 * @property int $plan_id
 * @property int $type_id
 * @property double $price
 * @property double $price_usd
 * @property string $table_file
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 *
 * @property WbFiles[] $wbFiles
 * @property WbPlans $plan
 * @property WbTypes $type
 * @property WbTasks[] $wbTasks
 */
class WbProjects extends \yii\db\ActiveRecord
{
    const STATUS_PLANNING = 0;
    const STATUS_WORKING = 1;
    const STATUS_ENDED = 2;

    public static function statusLabels()
    {
        return [
            self::STATUS_PLANNING => 'Режалаштирилаяпти',
            self::STATUS_WORKING => 'Бажарилаяпти',
            self::STATUS_ENDED => 'Якунланди',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%wb_projects}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['plan_id', 'type_id'], 'required'],
            [['plan_id', 'type_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['price', 'price_usd'], 'number'],
            [['table_file'], 'string', 'max' => 255],
            [['plan_id'], 'exist', 'skipOnError' => true, 'targetClass' => WbPlans::className(), 'targetAttribute' => ['plan_id' => 'id']],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => WbTypes::className(), 'targetAttribute' => ['type_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'plan_id' => Yii::t('app', 'Plan ID'),
            'type_id' => Yii::t('app', 'Type ID'),
            'price' => Yii::t('app', 'Price'),
            'price_usd' => Yii::t('app', 'Price Usd'),
            'table_file' => Yii::t('app', 'Table File'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWbFiles()
    {
        return $this->hasMany(WbFiles::className(), ['project_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlan()
    {
        return $this->hasOne(WbPlans::className(), ['id' => 'plan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(WbTypes::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWbTasks()
    {
        return $this->hasMany(WbTasks::className(), ['project_id' => 'id']);
    }

    public function getTitle()
    {
        return $this->type->title.' '.$this->plan->year.' '.$this->plan->locality->title;
    }

    public function getTitleOnly($lang='en')
    {
        return $this->plan->year.' '.$this->type->getTitleLang($lang);
    }

    /**
     * @param $lang
     * @return mixed|string
     * Lang helper by lang title
     */
    public function getTitleLang($lang)
    {
        return $this->getTitleOnly($lang);
    }

    public function getCalcedPlan()
    {
        $tasks = WbTasks::find()->where(['project_id'=>$this->id])->all();
        $calced = 0;
        foreach ($tasks as $task){
            $calced = $calced + $task->price_usd;
        }
        return $calced;
    }

    public function getCalcedFact()
    {
        $tasks = WbTasks::find()->where(['project_id'=>$this->id, 'status'=>1])->all();
        $calced = 0;
        foreach ($tasks as $task){
            $calced = $calced + $task->price_usd;
        }
        return $calced;
    }
}
