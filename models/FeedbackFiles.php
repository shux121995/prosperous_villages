<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%feedback_files}}".
 *
 * @property int $id
 * @property int $feed_id
 * @property string $filename
 *
 * @property Feedback $feed
 */
class FeedbackFiles extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%feedback_files}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['feed_id', 'filename'], 'required'],
            [['feed_id'], 'integer'],
            [['filename'], 'string', 'max' => 128],
            [['feed_id'], 'exist', 'skipOnError' => true, 'targetClass' => Feedback::className(), 'targetAttribute' => ['feed_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'feed_id' => Yii::t('app', 'Feed ID'),
            'filename' => Yii::t('app', 'Filename'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeed()
    {
        return $this->hasOne(Feedback::className(), ['id' => 'feed_id']);
    }
}
