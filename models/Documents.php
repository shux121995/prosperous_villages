<?php


namespace app\models;


use yii\db\ActiveRecord;

class Documents extends ActiveRecord
{
    public static function tableName()
    {
        return 'documents';
    }

    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title_en'], 'string'],
            [['url'], 'string'],
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'pdf'],
            [['file_en'], 'file', 'skipOnEmpty' => true, 'extensions' => 'pdf'],
        ];
    }

    public function getTitle($lang)
    {
        if ($lang == 'uz') {
            return $this->title;
        } else {
            return $this->title_en;
        }
    }
    public function getFile($lang)
    {
        if ($lang == 'uz') {
            return $this->file;
        } else {
            return $this->file_en;
        }
    }
}