<?php


namespace app\models;


use yii\db\ActiveRecord;

class ComProsCategory extends ActiveRecord
{
    public static function tableName()
    {
        return 'community_progress_categories';
    }
}