<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%moliya_manbai_table}}".
 *
 * @property int $id
 * @property int $manba_id
 * @property int $table_id
 * @property string $other
 */
class MoliyaManbaiTable extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%moliya_manbai_table}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['manba_id'], 'required'],
            [['manba_id', 'table_id'], 'integer'],
            [['other'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'manba_id' => Yii::t('app', 'Manba ID'),
            'table_id' => Yii::t('app', 'Table ID'),
            'other' => Yii::t('app', 'Other'),
        ];
    }
}
