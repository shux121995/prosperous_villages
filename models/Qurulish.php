<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "qurulish".
 *
 * @property int $id
 * @property int $plan_id
 * @property int $type maktab maktabgacha talim kasalxona malla obektlari boshqa ijtimoiy obektlar
 * @property string $title
 * @property int $urni
 * @property int $baj_ish_turi
 * @property string $qiy_reja
 * @property int $qiy_moliya
 * @property string $qiy_molled
 * @property string $ishga_tushish_reja
 * @property string $asos_akt
 * @property string $asos_date
 * @property int $loyiha_smeta_hujjat Лойиҳа смета ҳужжатининг мавжудлиги (ишлаб чиқилган, ишлаб чиқилмаган)
 * @property int $tarmoq_jadvali Тармоқ жадвалининг мавжудлиги (тасдиқланган, ишлаб чиқилмаган)
 * @property string $loyihachi_tashkilot
 * @property string $pudratchi_tashkilot
 * @property string $maktab_raqam Мактаб рақами  (Х-сонли мактаб)
 * @property int $mtm_mulkchilik_shakli Мулкчилик шакли (Давлат, ДХШ, Хусусий)
 * @property string $mtm_raqam МТМ рақами  (Х-сонли МТМ)
 * @property string $ssm Соғлиқни сақлаш муассасаси  (ҚВП, ҚОП, касалхона) номи
 *
 * @property LocalityPlans $plan
 * @property QurulishAmalda[] $qurulishAmaldas
 */
class Qurulish extends \yii\db\ActiveRecord
{
    public $moliya_manbai;

    const TYPE_MAKTAB = 1;
    const TYPE_MTM = 2;
    const TYPE_SSM = 3;
    const TYPE_MFY = 4;

    const TableName = '"Обод қишлоқ" дастури доирасида объектларни қуриш ва таъмирлаш борасида амалга оширилаётган ишлар';
    const TableNameShort = 'Қурилиш ва таъмирлаш ишлари';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'qurulish';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'asos_akt', 'loyihachi_tashkilot', 'pudratchi_tashkilot', 'plan_id', 'qiy_molled', 'type', 'urni', 'baj_ish_turi', 'loyiha_smeta_hujjat', 'tarmoq_jadvali', 'ishga_tushish_reja', 'asos_date'], 'required'],
            [['plan_id', 'type', 'urni', 'baj_ish_turi', 'qiy_moliya', 'loyiha_smeta_hujjat', 'tarmoq_jadvali', 'mtm_mulkchilik_shakli'], 'integer'],
            [['qiy_reja', 'qiy_molled'], 'number'],
            [['moliya_manbai'], 'safe'],
            [[ 'mtm_mulkchilik_shakli',  'mtm_raqam', 'ssm', 'maktab_raqam'], 'safe'],
            [['title', 'asos_akt', 'loyihachi_tashkilot', 'pudratchi_tashkilot', 'ssm'], 'string', 'max' => 255],
            [['maktab_raqam', 'mtm_raqam'], 'string', 'max' => 45],
            [['plan_id'], 'exist', 'skipOnError' => true, 'targetClass' => LocalityPlans::className(), 'targetAttribute' => ['plan_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'plan_id' => 'Ҳудуд ва йил',
            'type' => 'Қурилиш объекти тури',
            'title' => 'Лойиха номи',
            'urni' => 'Ўлчов бирлиги (ўрни, қатнов)',
            'baj_ish_turi' => 'Бажариладиган иш тури',
            'qiy_reja' => 'Қиймати (режа)',
            'qiy_moliya' => 'Қиймати (молиялаштириш манбаи)',
            'qiy_molled' => 'Қиймати (молиялаштирилди)',
            'ishga_tushish_reja' => 'Ишга тушиш муддати (режа)',
            'asos_akt' => 'Ишга тушиш муддати (асос АКТ рақами)',
            'asos_date' => 'Ишга тушиш муддати (асос муддати)',
            'loyiha_smeta_hujjat' => 'Лойиҳа смета ҳужжатининг мавжудлиги',
            'tarmoq_jadvali' => 'Тармоқ жадвалининг мавжудлиги',
            'loyihachi_tashkilot' => 'Лойиҳачи ташкилот номи',
            'pudratchi_tashkilot' => 'Пудрат ташкилот номи',
            'maktab_raqam' => 'Мактаб рақами',
            'mtm_mulkchilik_shakli' => 'МТМ мулкчилик шакли',
            'mtm_raqam' => 'МТМ рақами',
            'ssm' => 'Соғлиқни сақлаш муассасаси',
            'moliya_manbai' => 'Молиялаштириш манбалари',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlan()
    {
        return $this->hasOne(LocalityPlans::className(), ['id' => 'plan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQurulishAmaldas()
    {
        return $this->hasMany(QurulishAmalda::className(), ['qurulish_id' => 'id']);
    }

    public static function types()
    {
        return [
            self::TYPE_MAKTAB => 'Мактаб',
            self::TYPE_MTM => 'Мактабгача таълим муассасаси',
            self::TYPE_SSM => 'Соғлиқни сақлаш муассасаси',
            self::TYPE_MFY => 'Маҳалла (овул) фуқаролар йиғини',
        ];
    }

    public static function workTypes()
    {
        return [
          1=>'Янги қурилиш',
          2=>'Реконструкция',
          3=>'Мукаммал таъмирлаш',
          4=>'Жорий таъмирлаш',
        ];
    }

    public static function exist_project_document()
    {
        return [
            1 => 'Ишлаб чиқилган',
            0 => 'Ишлаб чиқилмаган',
        ];
    }

    public static function exist_tarmoq_jadvali()
    {
        return [
            1 => 'Тасдиқланган',
            0 => 'Ишлаб чиқилмаган',
        ];
    }

    public function getQurilishIdByPlanId() {
        $result =  Qurulish::find()
            ->where(['in','plan_id',(new LocalityPlans)->getPlanIdByUserRegion()])
            ->asArray()->all();
        $result = \yii\helpers\ArrayHelper::map($result, 'id', 'id');
        return $result;
    }

    public function getQurilishIdAndTitleByPlanId() {
        $result =  Qurulish::find()
            ->where(['in','plan_id',(new LocalityPlans)->getPlanIdByUserRegion()])
            ->asArray()->all();
        $result = \yii\helpers\ArrayHelper::map($result, 'id', 'title');
        return $result;
    }

}
