<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Url;

/**
 * This is the model class for table "{{%feedback}}".
 *
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property string $address
 * @property int $locality_id
 * @property int $table_id
 * @property int $section_id
 * @property int $type
 * @property string $project_type
 * @property string $text
 * @property int $created_at
 * @property int $updated_at
 * @property int $status 2 - новый
 * @property string $image
 *
 * @property FeedbackFiles[] $feedbackFiles
 */
class Feedback extends \yii\db\ActiveRecord
{
    const TYPE_TAKLIF = 1;
    const TYPE_MUROJAAT = 2;
    const TYPE_BOSHKA = 3;

    const STATUS_NEW = 2;
    const STATUS_ANSWERED = 1;
    const STATUS_CANCELLED = 3;

    public $image;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public static function typeLables()
    {
        return [
            self::TYPE_TAKLIF => Yii::t('app','Taklif'),
            self::TYPE_MUROJAAT => Yii::t('app','Murojat'),
            self::TYPE_BOSHKA => Yii::t('app','Boshqa'),
        ];
    }

    public function getActiveTypeLabel()
    {
        return isset(self::typeLables()[$this->type])?self::typeLables()[$this->type]:null;
    }

    public static function statusLabels()
    {
        return [
            self::STATUS_NEW => Yii::t('app','New'),
            self::STATUS_ANSWERED => Yii::t('app','Answered'),
            self::STATUS_CANCELLED => Yii::t('app','Cancelled'),
        ];
    }

    public function getActiveStatusLabel()
    {
        return isset(self::typeLables()[$this->type])?self::statusLabels()[$this->status]:null;
    }

    public static function projectLabels()
    {
        return [
            Locality::TYPE_QISHLOQ => Yii::t('app','Қишлоқ'),
            Locality::TYPE_MAHALLA => Yii::t('app','Маҳалла'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%feedback}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'address','gender','region','district','locality', 'text', 'anonymous'], 'required'],
            [['locality_id', 'table_id', 'section_id', 'type', 'created_at', 'updated_at', 'status'], 'integer'],
            [['text'], 'string'],
            [['name'], 'string', 'max' => 128],
            [['phone'], 'string', 'max' => 45],
            [['address'], 'string', 'max' => 255],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, gif, zip, doc, docx, pdf'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Full Name'),
            'phone' => Yii::t('app', 'Phone'),
            'address' => Yii::t('app', 'Address'),
            'gender' => Yii::t('app', 'Gender'),
            'region' => Yii::t('app', 'Select region'),
            'district' => Yii::t('app', 'Select district'),
            'locality' => Yii::t('app', 'Select locality'),
            'locality_id' => Yii::t('app', 'Locality'),
            'table_id' => Yii::t('app', 'Project'),
            'section_id' => Yii::t('app', 'Section'),
            'type' => Yii::t('app', 'Review type'),
            'project_type' => Yii::t('app', 'Project type'),
            'text' => Yii::t('app', 'Text'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'status' => Yii::t('app', 'Status'),
            'anonymous'=> Yii::t('app', 'Leave anonymous feedback'),
            'image' => Yii::t('app', 'Image'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeedbackFiles()
    {
        return $this->hasMany(FeedbackFiles::className(), ['feed_id' => 'id']);
    }

    public function upload(){
        $this->image->saveAs("uploads/{$this->image->baseName}.{$this->image->extension}");
        $file = new FeedbackFiles();
        $file->feed_id = $this->id;
        $file->filename = $this->image->baseName.'.'.$this->image->extension;
        $file->save(false);
    }
}
