<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bozor_infratuzilmasi".
 *
 * @property int $id
 * @property int $plan_id
 * @property string $loyiha_tashabbuskori
 * @property string $title
 * @property int $loyiha_yunalishi
 * @property string $quvvat_ulchov_birlik
 * @property string $quvvat_natural_qiy
 * @property string $quvvat_ish_chiq_hajm
 * @property string $yil_eksport_hajm
 * @property string $yil_byudjetga_tushum_hajm
 * @property string $loyiha_qiy_reja
 * @property string $moliya_mab_reja
 * @property string $moliya_kredit_reja
 * @property string $moliya_invest_reja
 * @property string $xorij_hamkor
 * @property string $horij_davlat
 * @property string $xiz_kursatuvchi_bank
 * @property int $ish_urin_reja
 * @property string $ishga_tush_mud_reja
 * @property string $ishga_tush_mud_asos
 * @property int $loyiha_muhan_yer_holati
 * @property int $loyiha_muhan_elektr_holati
 * @property int $loyiha_muhan_gaz_holati
 * @property string $loyiha_kredit_jadval_topshirish
 * @property string $loyiha_kredit_haq_topshirish
 * @property string $loyiha_kredit_jadval_ajratish
 * @property string $loyiha_kredit_haq_ajratish
 *
 * @property LocalityPlans $plan
 * @property BozorInfratuzilmasiAmalda[] $bozorInfratuzilmasiAmaldas
 */
class BozorInfratuzilmasi extends \yii\db\ActiveRecord
{

    public $moliya_manbai;

    const TYPE_SANOAT = 1;
    const TYPE_QISHLOQ_XUJALIGI = 2;
    const TYPE_XIZMAT_KURSATISH = 3;

    const TableName = '"Обод қишлоқ" дастури доирасида бозор инфратузилмаси объектларини барпо этиш ва таъмирлаш борасида амалга оширилаётган ишлар';
    const TableNameShort = 'Бозор инфратузилмаси объектларини барпо этиш ва таъмирлаш';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bozor_infratuzilmasi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['plan_id', 'loyiha_yunalishi', 'ish_urin_reja', 'loyiha_muhan_yer_holati', 'loyiha_muhan_elektr_holati', 'loyiha_muhan_gaz_holati', 'quvvat_ulchov_birlik', 'quvvat_natural_qiy', 'quvvat_ish_chiq_hajm', 'yil_eksport_hajm', 'yil_byudjetga_tushum_hajm', 'loyiha_qiy_reja', 'loyiha_tashabbuskori', 'title', 'xorij_hamkor', 'horij_davlat', 'xiz_kursatuvchi_bank', 'ishga_tush_mud_reja', 'ishga_tush_mud_asos', 'loyiha_kredit_jadval_topshirish', 'loyiha_kredit_haq_topshirish', 'loyiha_kredit_jadval_ajratish', 'loyiha_kredit_haq_ajratish'], 'required'],
            [['plan_id', 'loyiha_yunalishi', 'ish_urin_reja', 'loyiha_muhan_yer_holati', 'loyiha_muhan_elektr_holati', 'loyiha_muhan_gaz_holati'], 'integer'],
            [['quvvat_ulchov_birlik', 'quvvat_natural_qiy', 'quvvat_ish_chiq_hajm', 'yil_eksport_hajm', 'yil_byudjetga_tushum_hajm', 'loyiha_qiy_reja', 'moliya_mab_reja', 'moliya_kredit_reja', 'moliya_invest_reja'], 'number'],
            [['loyiha_tashabbuskori', 'title', 'xorij_hamkor', 'horij_davlat', 'xiz_kursatuvchi_bank', 'ishga_tush_mud_reja', 'ishga_tush_mud_asos', 'loyiha_kredit_jadval_topshirish', 'loyiha_kredit_haq_topshirish', 'loyiha_kredit_jadval_ajratish', 'loyiha_kredit_haq_ajratish'], 'string', 'max' => 128],
            [['moliya_manbai'], 'safe'],
            [['plan_id'], 'exist', 'skipOnError' => true, 'targetClass' => LocalityPlans::className(), 'targetAttribute' => ['plan_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'plan_id' => 'Ҳудуд ва йил',
            'loyiha_tashabbuskori' => 'Лойиҳа ташаббускори',
            'title' => 'Лойиҳа номи',
            'loyiha_yunalishi' => 'Лойиҳа йўналиши',
            'quvvat_ulchov_birlik' => 'Йиллик ишлаб чиқариш қуввати (ўлчов бирлиги)',
            'quvvat_natural_qiy' => 'Йиллик ишлаб чиқариш қуввати (натурал қийматда)',
            'quvvat_ish_chiq_hajm' => 'Йиллик ишлаб чиқариш қуввати (ишлаб чиқариш ҳажми, млн. сўм)',
            'yil_eksport_hajm' => 'Йиллик экспорт ҳажми (имконияти), минг доллар',
            'yil_byudjetga_tushum_hajm' => 'Йиллик бюджетга тушум ҳажми, млн сўм',
            'loyiha_qiy_reja' => 'Лойиҳа қиймати (Режа)',
            'moliya_mab_reja' => 'Молиялаштириш манбалари (ўз маблағи)',
            'moliya_kredit_reja' => 'Молиялаштириш манбалари (банк кредити (инвест-яси))',
            'moliya_invest_reja' => 'Молиялаштириш манбалари (хорижий инвестиция)',
            'xorij_hamkor' => 'Хорижий инвестор (Хорижий хамкор номи)',
            'horij_davlat' => 'Хорижий инвестор (Давлати)',
            'xiz_kursatuvchi_bank' => 'Хизмат кўрсатувчи тижорат банки',
            'ish_urin_reja' => 'Иш ўринлари (Режа)',
            'ishga_tush_mud_reja' => 'Ишга тушиш муддати (режа)',
            'ishga_tush_mud_asos' => 'Ишга тушиш муддати (асос)',
            'loyiha_muhan_yer_holati' => 'Лойиҳага ер ажратиш ҳамда муҳандислик коммуникация тармоқларига уланиш ҳолати (Ер ажратиш ҳолати)',
            'loyiha_muhan_elektr_holati' => 'Лойиҳага ер ажратиш ҳамда муҳандислик коммуникация тармоқларига уланиш ҳолати (Электр тармоқларига уланиш ҳолати)',
            'loyiha_muhan_gaz_holati' => 'Лойиҳага ер ажратиш ҳамда муҳандислик коммуникация тармоқларига уланиш ҳолати (Газ тармоқларига уланиш ҳолати)',
            'loyiha_kredit_jadval_topshirish' => 'Лойиҳага кредит ажратиш ҳолати (Тармоқ жадвалига мувофиқ тижорат банкига кредит олиш учун ҳужжат топшириш санаси)',
            'loyiha_kredit_haq_topshirish' => 'Лойиҳага кредит ажратиш ҳолати (Ҳақиқатда  тижорат банкига кредит олиш учун ҳужжат топширилган сана)',
            'loyiha_kredit_jadval_ajratish' => 'Лойиҳага кредит ажратиш ҳолати (Тармоқ жадвалига мувофиқ тижорат банки томонидан кредит ажратиш санаси)',
            'loyiha_kredit_haq_ajratish' => 'Лойиҳага кредит ажратиш ҳолати (Ҳақиқатда  тижорат банки томонидан кредит ажратилган сана)',
            'moliya_manbai' => 'Молиялаштириш манбалари',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlan()
    {
        return $this->hasOne(LocalityPlans::className(), ['id' => 'plan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBozorInfratuzilmasiAmaldas()
    {
        return $this->hasMany(BozorInfratuzilmasiAmalda::className(), ['bozor_id' => 'id']);
    }

    public function getBozorInfratuzilmasiIdByPlanId() {
        $result =  BozorInfratuzilmasi::find()
            ->where(['in','plan_id',(new LocalityPlans())->getPlanIdByUserRegion()])
            ->asArray()->all();
        $result = \yii\helpers\ArrayHelper::map($result, 'id', 'id');
        return $result;
    }

    public function getBozorInfratuzilmasiIdAndTitleByPlanId() {
        $result =  BozorInfratuzilmasi::find()
            ->where(['in','plan_id',(new LocalityPlans())->getPlanIdByUserRegion()])
            ->asArray()->all();
        $result = \yii\helpers\ArrayHelper::map($result, 'id', 'title');
        return $result;
    }

    public static function types()
    {
        return [
            self::TYPE_SANOAT => 'Саноат',
            self::TYPE_QISHLOQ_XUJALIGI => 'Қишлоқ хўжалиги',
            self::TYPE_XIZMAT_KURSATISH => 'Хизмат кўрсатиш',
        ];

    }

    public static function project_status1()
    {
        return [
            1 => 'Талаб этилмайди',
            2 => 'Ажратилган',
            3 => 'Ажратилмаган',
        ];
    }

    public static function project_status2()
    {
        return [
            1 => 'Талаб этилмайди',
            2 => 'Уланган',
            3 => 'Уланмаган',
        ];
    }
}
