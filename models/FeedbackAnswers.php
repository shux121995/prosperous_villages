<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%feedback_answers}}".
 *
 * @property int $id
 * @property int $feedback_id
 * @property string $text
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Feedback $feedback
 */
class FeedbackAnswers extends \yii\db\ActiveRecord
{
    public $status;

    public static function statusLabels()
    {
        return [
            Feedback::STATUS_ANSWERED => Yii::t('app','Answered'),
            Feedback::STATUS_CANCELLED => Yii::t('app','Cancelled'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%feedback_answers}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['feedback_id', 'text', 'status'], 'required'],
            [['feedback_id', 'created_at', 'updated_at'], 'integer'],
            [['text'], 'string'],
            [['feedback_id'], 'exist', 'skipOnError' => true, 'targetClass' => Feedback::className(), 'targetAttribute' => ['feedback_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'feedback_id' => Yii::t('app', 'Feedback ID'),
            'text' => Yii::t('app', 'Text'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeedback()
    {
        return $this->hasOne(Feedback::className(), ['id' => 'feedback_id']);
    }
}
