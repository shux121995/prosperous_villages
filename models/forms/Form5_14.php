<?php


namespace app\models\forms;


use yii\db\ActiveRecord;

class Form5_14 extends ActiveRecord
{
    public static function tableName()
    {
        return 'form_5_14';
    }

    public function rules()
    {
        return [
            [['village_id', 'sub_id', 'type_of_work', 'cont_name', 'cont_number','contract_date', 'total_value', 'total_undis','status_of_sub'], 'required'],
            [['contract_date'],'date', 'format' => 'yyyy-mm-dd']
        ];
    }

    public function attributeLabels()
    {
        return[
            /*I will include later*/
        ];
    }
}