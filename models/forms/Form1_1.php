<?php
namespace app\models\forms;
use yii\base\Model;

class Form1_1 extends Model
{
    //Full list of questions and answers
    public $project_name;
    public $building_rehabilitation;
    public $new_construction;
    public $individual_wastewater_treatment_system;
    public $historic_buildings_and_districts;
    public $acquisition_of_land;
    public $hazardous_and_toxic_materials;
    public $impact_on_forest_and_or_protected_areas;
    public $handling_management_of_medical_waste;
    public $traffic_and_pedestrian_safety;

    //you don't need to change the name itself
    public $Land_soil_degradat_olve_land_excavation;
    public $Generation_of_solid_cluding_toxic_wastes;
    public $Soil_and_underground_water_pollution;
    public $Will_the_project_pro_pollutant_emissions;
    public $Water_Quantity_will_ct_involve_water_use;
    public $Water_Quality_Poll_face_water_pollution;
    public $Will_the_project_ass_ng_near_project_area;
    public $Does_the_project_req_concerns_and_inputs;
    public $Social_impacts;
    public $SCS_full_name;
    public $MDU_mumber_full_name;
    public $FPs_QFs_QEs_full_name;

    public function Test($locality){
        $token="356e30536c69833a72ed993cb6a42259226b1eb1";
        $headers = array(
            'Authorization: Token '.$token
        );
        $url="https://kc.kobotoolbox.org/api/v1/data/431426?format=json";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPGET, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $return = curl_exec ($ch);
        curl_close ($ch);

        //echo $return['FPs_QFs_QEs_full_name'];
        $json_output = json_decode($return, true);
        if ($json_output && !empty($json_output)) {
            foreach ( $json_output as $output ) {
                if($output['_validation_status']['label'] == "Approved" && $output['Select_Village'] == $locality) {
                    $this->project_name = $output["_1_Project_Name"];
                    $this->building_rehabilitation = $output["Building_rehabilitation"];
                    $this->new_construction = $output["New_construction"];
                    $this->individual_wastewater_treatment_system = $output["Individual_wastewater_treatment_system"];
                    $this->historic_buildings_and_districts = $output["Historic_building_s_and_districts"];
                    $this->acquisition_of_land = $output["Acquisition_of_land"];
                    $this->hazardous_and_toxic_materials = $output["Hazardous_or_toxic_materials"];
                    $this->impact_on_forest_and_or_protected_areas = $output["Impacts_on_forests_a_d_or_protected_areas"];
                    $this->handling_management_of_medical_waste = $output["Handling_management_of_medical_waste"];
                    $this->traffic_and_pedestrian_safety = $output["Traffic_and_Pedestrian_Safety"];

                    $this->Land_soil_degradat_olve_land_excavation = $output["Land_soil_degradat_olve_land_excavation"];
                    $this->Generation_of_solid_cluding_toxic_wastes = $output["Generation_of_solid_cluding_toxic_wastes"];
                    $this->Soil_and_underground_water_pollution = $output["Soil_and_underground_water_pollution"];
                    $this->Will_the_project_pro_pollutant_emissions = $output["Will_the_project_pro_pollutant_emissions"];
                    $this->Water_Quantity_will_ct_involve_water_use = $output["Water_Quantity_will_ct_involve_water_use"];
                    $this->Water_Quality_Poll_face_water_pollution = $output["Water_Quality_Poll_face_water_pollution"];
                    $this->Will_the_project_ass_ng_near_project_area = $output["Will_the_project_ass_ng_near_project_area"];
                    $this->Does_the_project_req_concerns_and_inputs = $output["Does_the_project_req_concerns_and_inputs"];
                    $this->Social_impacts = $output["Social_impacts"];
                    $this->SCS_full_name = $output["SCS_full_name"];
                    $this->MDU_mumber_full_name = $output["MDU_mumber_full_name"];
                    $this->FPs_QFs_QEs_full_name = $output["FPs_QFs_QEs_full_name"];
                }
            }
        }
    }
}