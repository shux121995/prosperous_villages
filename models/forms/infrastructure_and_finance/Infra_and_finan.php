<?php

namespace app\models\forms\infrastructure_and_finance;

use app\models\Locality;
use app\models\WbDevelopers;
use app\models\WbStatus;
use app\models\WbSubTypes;
use app\models\WbTypes;
use Yii;

class Infra_and_finan extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%infra_and_finan}}';
    }
    public function rules()
    {
        return [
            [['region_id', 'district_id', 'locality_id', 'sub_id', 'sub_work_id'], 'required'],
            [['res_contractor','status_of_subproject'], 'integer', 'skipOnEmpty' => true,],
            [['direct_b','indirect_b', 'year'],'integer'],
            [['land_allocation','topograpic','architectual','gen_plan_des','gen_plan_progress','proj_des_des','proj_des_progress','sta_env_exper','sta_min_exper','contract_number','total_contract_value','total_undis_amount','sub_progress','sub_acceptance', 'sub_unique'],'string'],
            [['constraction_start','constraction_end','contract_start','contract_end'],'date', 'format' => 'yyyy-mm-dd'],
            [['sub_before','sub_during','sub_after'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, JPEG'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'region_id' => Yii::t('app', 'Region ID'),
            'district_id' => Yii::t('app', 'District ID'),
            'locality_id' => Yii::t('app', 'Village ID'),
            'sub_id' => Yii::t('app', 'Subproject ID'),
            'year' => Yii::t('app', 'Subproject implementaion year'),
            'sub_work_id' => Yii::t('app', 'Work ID'),
            'direct_b' => Yii::t('app', 'Direct beneficiaries'),
            'res_contractor' => Yii::t('app', 'Responsible contractor'),
            'status_of_subproject' => Yii::t('app', 'Status of subproject'),
            'land_allocation' => Yii::t('app', 'Decision of the khokim on allocation of the land plot for the construction'),
            'topograpic' => Yii::t('app', 'Topographic and geodetic survey of the land'),
            'architectual' => Yii::t('app', 'Architectural and planning task '),
            'gen_plan_des' => Yii::t('app', 'General Plan of the Detailed Planining of vilage\'s territory (Responsible designer)'),
            'gen_plan_progress' => Yii::t('app', 'General Plan of the Detailed Planining of vilage\'s territory (Progress)'),
            'proj_des_des' => Yii::t('app', 'Project desing (Responsible designer)'),
            'proj_des_progress' => Yii::t('app', 'Project design (Progress of developing)'),
            'sta_env_exper' => Yii::t('app', 'State Environmental Expertise'),
            'indirect_b' => Yii::t('app', 'Indirect beneficiaries'),
            'contract_end' => Yii::t('app', 'Contract end date'),
            'contract_start' => Yii::t('app', 'Contract start date'),
            'constraction_end' => Yii::t('app', 'Construction end date'),
            'constraction_start' => Yii::t('app', 'Construction start date'),
            'sub_before' => Yii::t('app', 'Subproject before photo'),
            'sub_during' => Yii::t('app', 'Subproject during photo'),
            'sta_min_exper' => Yii::t('app', 'State expertize of Ministry of constrcution'),
            'contract_number' => Yii::t('app', 'Contract number'),
            'total_contract_value' => Yii::t('app', 'Total_contract value'),
            'total_undis_amount' => Yii::t('app', 'Total undisbursed amount'),
            'sub_progress' => Yii::t('app', 'Subproject progress'),
            'sub_acceptance' => Yii::t('app', 'Subproject acceptance'),
            'sub_after' => Yii::t('app', 'Subproject after photo'),
        ];
    }
    public function getLocality()
    {
        return $this->hasOne(Locality::className(), ['id' => 'locality_id']);
    }
    public function getSubproject()
    {
        return $this->hasOne(WbTypes::className(), ['id' => 'sub_id']);
    }
    public function getWork()
    {
        return $this->hasOne(WbSubTypes::className(), ['id' => 'sub_work_id']);
    }
    public function getContractor()
    {
        return $this->hasOne(WbDevelopers::className(), ['id' => 'res_contractor']);
    }
    public function getStatus()
    {
        return $this->hasOne(WbStatus::className(), ['id' => 'status_of_subproject']);
    }
}