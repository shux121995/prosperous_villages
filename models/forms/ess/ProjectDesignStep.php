<?php

namespace app\models\forms\ess;
use app\models\Locality;
use Yii;

class ProjectDesignStep extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%project_design_step}}';
    }

    public function rules()
    {
        return [
            [['region_id', 'district_id', 'locality_id'], 'required'],
            [['form1_1','form1_2','form1_3','form1_4','form1_5','step_2','step_3','step_4','step_5', ],'date', 'format' => 'yyyy-mm-dd'],
            [['form1_1_file','form1_2_file','form1_3_file','form1_4_file','form1_5_file','step_2_file','step_3_file','step_4_file','step_5_file',], 'file', 'skipOnEmpty' => true, 'extensions' => 'pdf'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'locality_id' => Yii::t('app', 'Village ID'),
            'region_id' => Yii::t('app', 'Region ID'),
            'district_id' => Yii::t('app', 'District ID'),
            'form1_1' => Yii::t('app', 'Completion date of form 1.1'),
            'form1_2' => Yii::t('app', 'Completion date of form 1.2'),
            'form1_3' => Yii::t('app', 'Completion date of form 1.3'),
            'form1_4' => Yii::t('app', 'Completion date of form 1.4'),
            'form1_5' => Yii::t('app', 'Completion date of form 1.5'),
            'step_2' => Yii::t('app', 'Subprojects Environmental and Social Impact Assessment'),
            'step_3' => Yii::t('app', 'Public Consultation'),
            'step_4' => Yii::t('app', 'WB acceptance'),
            'step_5' => Yii::t('app', 'ESA Information Disclosure'),

            'form1_1_file' => Yii::t('app', 'Upload PDF form 1.1'),
            'form1_2_file' => Yii::t('app', 'Upload PDF form 1.2'),
            'form1_3_file' => Yii::t('app', 'Upload PDF form 1.3'),
            'form1_4_file' => Yii::t('app', 'Upload PDF form 1.4'),
            'form1_5_file' => Yii::t('app', 'Upload PDF form 1.5'),
            'step_2_file' => Yii::t('app', 'Upload PDF Subprojects Environmental and Social Impact Assessment'),
            'step_3_file' => Yii::t('app', 'Upload PDF Public Consultation'),
            'step_4_file' => Yii::t('app', 'Upload PDF WB acceptance'),
            'step_5_file' => Yii::t('app', 'Upload PDF ESA Information Disclosure'),
        ];
    }

    public function getLocality()
    {
        return $this->hasOne(Locality::className(), ['id' => 'locality_id']);
    }
}