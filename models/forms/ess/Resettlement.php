<?php


namespace app\models\forms\ess;


use app\models\Locality;
use Yii;
use yii\db\ActiveRecord;

class Resettlement extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%resettlement}}';
    }

    public function rules()
    {
        return [
            [['region_id', 'district_id', 'locality_id'], 'required'],
            [['form2_1'],'date', 'format' => 'yyyy-mm-dd'],
            [['visibility'],'boolean'],
            [['form2_1_file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'pdf'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'locality_id' => Yii::t('app', 'Village ID'),
            'region_id' => Yii::t('app', 'Region ID'),
            'district_id' => Yii::t('app', 'District ID'),

            'form2_1' => Yii::t('app', 'Completion date of form 2.1'),
            'form2_1_file' => Yii::t('app', 'Upload PDF form 2.1'),
            'visibility'=> Yii::t('app', 'Visibility'),

        ];
    }

    public function getLocality()
    {
        return $this->hasOne(Locality::className(), ['id' => 'locality_id']);
    }
}