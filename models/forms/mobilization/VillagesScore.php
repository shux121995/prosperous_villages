<?php


namespace app\models\forms\mobilization;


use app\models\Locality;
use Yii;
use yii\db\ActiveRecord;

class VillagesScore extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%villages_score}}';
    }

    public function rules()
    {
        return [
            [['region_id', 'district_id', 'locality_id', 'remoteness', 'water', 'total'], 'required'],
            [['remoteness', 'water', 'total'], 'required', 'on' => 'add'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'region_id' => Yii::t('app', 'Region ID'),
            'district_id' => Yii::t('app', 'District ID'),
            'locality_id' => Yii::t('app', 'Village ID'),
            'remoteness' => Yii::t('app', 'Remoteness score'),
            'water' => Yii::t('app', 'Water score'),
        ];
    }

    public function getLocality()
    {
        return $this->hasOne(Locality::className(), ['id' => 'locality_id']);
    }
}