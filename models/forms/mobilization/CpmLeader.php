<?php


namespace app\models\forms\mobilization;


use app\models\Locality;
use Yii;
use yii\db\ActiveRecord;

class CpmLeader extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%cpm_leader}}';
    }

    public function rules()
    {
        return [
            [['locality_id', 'name', 'phone'], 'required'],
            [['name', 'phone'], 'required', 'on' => 'add'],
        ];
    }
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'locality_id' => Yii::t('app', 'Village ID'),
            'name' => Yii::t('app', 'CPM Team Leader Name'),
            'phone' => Yii::t('app', 'CPM Phone Number')
        ];
    }

    public function getLocality()
    {
        return $this->hasOne(Locality::className(), ['id' => 'locality_id']);
    }
}