<?php


namespace app\models\forms\mobilization;


use app\models\Locality;
use Yii;
use yii\db\ActiveRecord;

class Cdwo extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%cdwo}}';
    }

    public function rules()
    {
        return [
            [['locality_id', 'cdwo_name', 'director', 'vice_director', 'secretary', 'phone'], 'required'],
            [['cdwo_name', 'director', 'vice_director', 'secretary', 'phone'], 'required', 'on' => 'add'],
        ];
    }
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'locality_id' => Yii::t('app', 'Village ID'),
            'cdwo_name' => Yii::t('app', 'CDWO Name'),
            'director' => Yii::t('app', 'Director'),
            'vice_director' => Yii::t('app', 'Vice Director'),
            'secretary' => Yii::t('app', 'Secretary'),
            'phone' => Yii::t('app', 'Phone Number')

        ];
    }

    public function getLocality()
    {
        return $this->hasOne(Locality::className(), ['id' => 'locality_id']);
    }
}