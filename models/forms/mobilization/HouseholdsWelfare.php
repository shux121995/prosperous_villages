<?php


namespace app\models\forms\mobilization;


use app\models\Locality;
use Yii;
use yii\db\ActiveRecord;

class HouseholdsWelfare extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%households_welfare}}';
    }

    public function rules()
    {
        return [
            [['region_id', 'district_id', 'locality_id', 'high', 'middle', 'low', 'in_need', 'without_water'], 'required'],
            [['high', 'middle', 'low', 'in_need', 'without_water'], 'required', 'on' => 'add'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'region_id' => Yii::t('app', 'Region ID'),
            'district_id' => Yii::t('app', 'District ID'),
            'locality_id' => Yii::t('app', 'Village ID'),
            'high' => Yii::t('app', '# high income'),
            'middle' => Yii::t('app', '# middle income'),
            'low' => Yii::t('app', '# low income'),
            'in_need' => Yii::t('app', '# in need'),
            'without_water' => Yii::t('app', '# without access to drinking water'),
        ];
    }

    public function getLocality()
    {
        return $this->hasOne(Locality::className(), ['id' => 'locality_id']);
    }
}