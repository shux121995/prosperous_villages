<?php


namespace app\models\forms\mobilization;


use app\models\Locality;
use Yii;
use yii\db\ActiveRecord;

class MduImiScore extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%mdu_imi_score}}';
    }

    public function rules()
    {
        return [
            [['locality_id', 'score'], 'required'],
            [['score'], 'required', 'on' => 'add'],
        ];
    }
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'locality_id' => Yii::t('app', 'Village ID'),
            'score' => Yii::t('app', 'MDU IMI Score in %')
        ];
    }

    public function getLocality()
    {
        return $this->hasOne(Locality::className(), ['id' => 'locality_id']);
    }
}