<?php

namespace app\models\forms\mobilization;

use app\models\Locality;
use app\models\LocalityPlans;
use Yii;

/**
 * This is the model class for table "{{%aholi}}".
 *
 * @property int $id
 * @property int $region_id
 * @property int $district_id
 * @property int $locality_id
 * @property int $aholi_soni
 * @property int $xonadon_soni
 * @property int $oila_soni
 * @property int $erkak_soni
 * @property int $ayol_soni
 *
 * @property Locality $locality
 * @property LocalityPlans $plan
 */
class Aholi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%aholi}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['region_id', 'district_id', 'locality_id', 'aholi_soni', 'xonadon_soni', 'oila_soni', 'erkak_soni', 'ayol_soni'], 'required'],
            [['aholi_soni', 'xonadon_soni', 'erkak_soni', 'ayol_soni'], 'required', 'on' => 'add'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'locality_id' => Yii::t('app', 'Village ID'),
            'region_id' => Yii::t('app', 'Region ID'),
            'district_id' => Yii::t('app', 'District ID'),
            'aholi_soni' => Yii::t('app', 'Total population'),
            'xonadon_soni' => Yii::t('app', 'Number of households'),
            'oila_soni' => Yii::t('app', 'Number of families'),
            'erkak_soni' => Yii::t('app', 'Number of males'),
            'ayol_soni' => Yii::t('app', 'Number of females'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocality()
    {
        return $this->hasOne(Locality::className(), ['id' => 'locality_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlan()
    {
        return $this->hasOne(LocalityPlans::className(), ['id' => 'plan_id']);
    }

    public function getPopulation($region_id, $district_id, $village_id){
        $sum = 0;
        if(!empty($village_id)) {
            return  Aholi::find()->where(['locality_id' => $village_id])->one()->aholi_soni;
        } else if(!empty($district_id)) {
            $population_districts =  Aholi::find()->where(['district_id' => $district_id])->all();
            foreach ($population_districts as $pop_dis):
                $sum = $sum + $pop_dis->aholi_soni;
            endforeach;
            return $sum;
        } else if(!empty($region_id)) {
            $population_regions =  Aholi::find()->where(['region_id' => $region_id])->all();
            foreach ($population_regions as $pop_reg):
                $sum = $sum + $pop_reg->aholi_soni;
            endforeach;
            return $sum;
        } else if(empty($region_id) && empty($district_id) && empty($village_id)){
            $population =  Aholi::find()->all();
            foreach ($population as $pop):
                $sum = $sum + $pop->aholi_soni;
            endforeach;
            return $sum;
        }
        return 0;
    }
}
