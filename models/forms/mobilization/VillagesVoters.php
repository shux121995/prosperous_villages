<?php


namespace app\models\forms\mobilization;


use app\models\Locality;
use Yii;
use yii\db\ActiveRecord;

class VillagesVoters extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%village_voters}}';
    }

    public function rules()
    {
        return [
            [['region_id', 'district_id', 'locality_id', 'male', 'female', 'total', 'p_male','p_female','p_total'], 'required'],
            [['male', 'female', 'p_male','p_female'], 'required', 'on' => 'add'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'region_id' => Yii::t('app', 'Region ID'),
            'district_id' => Yii::t('app', 'District ID'),
            'locality_id' => Yii::t('app', 'Village ID'),
            'male' => Yii::t('app', 'Male voters'),
            'female' => Yii::t('app', 'Female voters'),
            'p_male' => Yii::t('app', 'Male participants'),
            'p_female' => Yii::t('app', 'Female participants'),
            'p_total' => Yii::t('app', 'Total participants'),
        ];
    }

    public function getLocality()
    {
        return $this->hasOne(Locality::className(), ['id' => 'locality_id']);
    }
}