<?php


namespace app\models\forms\mobilization;


use app\models\Locality;
use Yii;
use yii\db\ActiveRecord;

class MduMembers extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%mdu_members}}';
    }

    public function rules()
    {
        return [
            [['locality_id', 'chair', 'vice_chair', 'secretary', 'phone_number'], 'required'],
            [['chair', 'vice_chair', 'secretary','phone_number'], 'required', 'on' => 'add'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'locality_id' => Yii::t('app', 'Village ID'),
            'chair' => Yii::t('app', 'Chair person'),
            'vice_chair' => Yii::t('app', 'Vice chair person'),
            'secretary' => Yii::t('app', 'Secretary'),
            'phone_number' => Yii::t('app', 'Phone number'),
        ];
    }

    public function getLocality()
    {
        return $this->hasOne(Locality::className(), ['id' => 'locality_id']);
    }

}