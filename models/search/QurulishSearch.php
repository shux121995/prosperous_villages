<?php

namespace app\models\search;

use app\models\LocalityPlans;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Qurulish;

/**
 * QurulishSearch represents the model behind the search form of `app\models\Qurulish`.
 */
class QurulishSearch extends Qurulish
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'plan_id', 'type', 'urni', 'baj_ish_turi', 'qiy_moliya', 'loyiha_smeta_hujjat', 'tarmoq_jadvali', 'mtm_mulkchilik_shakli'], 'integer'],
            [['title', 'ishga_tushish_reja', 'asos_akt', 'asos_date', 'loyihachi_tashkilot', 'pudratchi_tashkilot', 'maktab_raqam', 'mtm_raqam', 'ssm'], 'safe'],
            [['qiy_reja', 'qiy_molled'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $LocalityPlans = new LocalityPlans();
        if(!empty($LocalityPlans->getPlanIdByUserRegion())) {
            $query = Qurulish::find()->where("plan_id IN (" . implode(',', $LocalityPlans->getPlanIdByUserRegion()) . ")");
        }
        else {
            $query = Qurulish::find();
        }
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'plan_id' => $this->plan_id,
            'type' => $this->type,
            'urni' => $this->urni,
            'baj_ish_turi' => $this->baj_ish_turi,
            'qiy_reja' => $this->qiy_reja,
            'qiy_moliya' => $this->qiy_moliya,
            'qiy_molled' => $this->qiy_molled,
            'ishga_tushish_reja' => $this->ishga_tushish_reja,
            'asos_date' => $this->asos_date,
            'loyiha_smeta_hujjat' => $this->loyiha_smeta_hujjat,
            'tarmoq_jadvali' => $this->tarmoq_jadvali,
            'mtm_mulkchilik_shakli' => $this->mtm_mulkchilik_shakli,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'asos_akt', $this->asos_akt])
            ->andFilterWhere(['like', 'loyihachi_tashkilot', $this->loyihachi_tashkilot])
            ->andFilterWhere(['like', 'pudratchi_tashkilot', $this->pudratchi_tashkilot])
            ->andFilterWhere(['like', 'maktab_raqam', $this->maktab_raqam])
            ->andFilterWhere(['like', 'mtm_raqam', $this->mtm_raqam])
            ->andFilterWhere(['like', 'ssm', $this->ssm]);

        return $dataProvider;
    }

    public function searchByPlanId($params, $plan_id)
    {
        $query = Qurulish::find()->where(['plan_id'=>$plan_id])->orderBy(['id'=>SORT_DESC]);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'plan_id' => $this->plan_id,
            'type' => $this->type,
            'urni' => $this->urni,
            'baj_ish_turi' => $this->baj_ish_turi,
            'qiy_reja' => $this->qiy_reja,
            'qiy_moliya' => $this->qiy_moliya,
            'qiy_molled' => $this->qiy_molled,
            'ishga_tushish_reja' => $this->ishga_tushish_reja,
            'asos_date' => $this->asos_date,
            'loyiha_smeta_hujjat' => $this->loyiha_smeta_hujjat,
            'tarmoq_jadvali' => $this->tarmoq_jadvali,
            'mtm_mulkchilik_shakli' => $this->mtm_mulkchilik_shakli,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'asos_akt', $this->asos_akt])
            ->andFilterWhere(['like', 'loyihachi_tashkilot', $this->loyihachi_tashkilot])
            ->andFilterWhere(['like', 'pudratchi_tashkilot', $this->pudratchi_tashkilot])
            ->andFilterWhere(['like', 'maktab_raqam', $this->maktab_raqam])
            ->andFilterWhere(['like', 'mtm_raqam', $this->mtm_raqam])
            ->andFilterWhere(['like', 'ssm', $this->ssm]);

        return $dataProvider;
    }
}
