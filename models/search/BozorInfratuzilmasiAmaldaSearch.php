<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BozorInfratuzilmasiAmalda;

/**
 * BozorInfratuzilmasiAmaldaSearch represents the model behind the search form of `app\models\BozorInfratuzilmasiAmalda`.
 */
class BozorInfratuzilmasiAmaldaSearch extends BozorInfratuzilmasiAmalda
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'bozor_id', 'ish_urin_amalda'], 'integer'],
            [['loyiha_qiy_amalda', 'mol_mab_amalda', 'moliya_kredit_amalda', 'moliya_invest_amalda'], 'number'],
            [['ishga_tush_mud_amalda'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BozorInfratuzilmasiAmalda::find()->where(['in','bozor_id',(new \app\models\BozorInfratuzilmasi)->getBozorInfratuzilmasiIdByPlanId()]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'bozor_id' => $this->bozor_id,
            'loyiha_qiy_amalda' => $this->loyiha_qiy_amalda,
            'mol_mab_amalda' => $this->mol_mab_amalda,
            'moliya_kredit_amalda' => $this->moliya_kredit_amalda,
            'moliya_invest_amalda' => $this->moliya_invest_amalda,
            'ish_urin_amalda' => $this->ish_urin_amalda,
        ]);

        $query->andFilterWhere(['like', 'ishga_tush_mud_amalda', $this->ishga_tush_mud_amalda]);

        return $dataProvider;
    }
}
