<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\QurulishAmalda;

/**
 * QurulishAmaldaSearch represents the model behind the search form of `app\models\QurulishAmalda`.
 */
class QurulishAmaldaSearch extends QurulishAmalda
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'qurulish_id', 'created_at', 'updated_at'], 'integer'],
            [['qiy_used', 'oh_yer', 'oh_poydevor', 'oh_gisht_terish', 'oh_tom_yopish', 'oh_demontaj', 'oh_oraliq_devor', 'oh_eshik_deraza', 'oh_ichki_pardozlash', 'oh_tashqi_pardozlash'], 'number'],
            [['date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = QurulishAmalda::find()->where(['in','qurulish_id',(new \app\models\Qurulish)->getQurilishIdByPlanId()]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'qurulish_id' => $this->qurulish_id,
            'qiy_used' => $this->qiy_used,
            'oh_yer' => $this->oh_yer,
            'oh_poydevor' => $this->oh_poydevor,
            'oh_gisht_terish' => $this->oh_gisht_terish,
            'oh_tom_yopish' => $this->oh_tom_yopish,
            'oh_demontaj' => $this->oh_demontaj,
            'oh_oraliq_devor' => $this->oh_oraliq_devor,
            'oh_eshik_deraza' => $this->oh_eshik_deraza,
            'oh_ichki_pardozlash' => $this->oh_ichki_pardozlash,
            'oh_tashqi_pardozlash' => $this->oh_tashqi_pardozlash,
            'date' => $this->date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        return $dataProvider;
    }
}
