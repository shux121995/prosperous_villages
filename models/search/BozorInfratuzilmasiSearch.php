<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BozorInfratuzilmasi;
use app\models\LocalityPlans;

/**
 * BozorInfratuzilmasiSearch represents the model behind the search form of `app\models\BozorInfratuzilmasi`.
 */
class BozorInfratuzilmasiSearch extends BozorInfratuzilmasi
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'plan_id', 'loyiha_yunalishi', 'ish_urin_reja', 'loyiha_muhan_yer_holati', 'loyiha_muhan_elektr_holati', 'loyiha_muhan_gaz_holati'], 'integer'],
            [['loyiha_tashabbuskori', 'title', 'xorij_hamkor', 'horij_davlat', 'xiz_kursatuvchi_bank', 'ishga_tush_mud_reja', 'ishga_tush_mud_asos', 'loyiha_kredit_jadval_topshirish', 'loyiha_kredit_haq_topshirish', 'loyiha_kredit_jadval_ajratish', 'loyiha_kredit_haq_ajratish'], 'safe'],
            [['quvvat_ulchov_birlik', 'quvvat_natural_qiy', 'quvvat_ish_chiq_hajm', 'yil_eksport_hajm', 'yil_byudjetga_tushum_hajm', 'loyiha_qiy_reja', 'moliya_mab_reja', 'moliya_kredit_reja', 'moliya_invest_reja'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $LocalityPlans = new LocalityPlans();
        if(!empty($LocalityPlans->getPlanIdByUserRegion())) {
            $query = BozorInfratuzilmasi::find()->where("plan_id IN (".implode(',', $LocalityPlans->getPlanIdByUserRegion()).")");
        }
        else {
            $query = BozorInfratuzilmasi::find();
        }


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'plan_id' => $this->plan_id,
            'loyiha_yunalishi' => $this->loyiha_yunalishi,
            'quvvat_ulchov_birlik' => $this->quvvat_ulchov_birlik,
            'quvvat_natural_qiy' => $this->quvvat_natural_qiy,
            'quvvat_ish_chiq_hajm' => $this->quvvat_ish_chiq_hajm,
            'yil_eksport_hajm' => $this->yil_eksport_hajm,
            'yil_byudjetga_tushum_hajm' => $this->yil_byudjetga_tushum_hajm,
            'loyiha_qiy_reja' => $this->loyiha_qiy_reja,
            'moliya_mab_reja' => $this->moliya_mab_reja,
            'moliya_kredit_reja' => $this->moliya_kredit_reja,
            'moliya_invest_reja' => $this->moliya_invest_reja,
            'ish_urin_reja' => $this->ish_urin_reja,
            'loyiha_muhan_yer_holati' => $this->loyiha_muhan_yer_holati,
            'loyiha_muhan_elektr_holati' => $this->loyiha_muhan_elektr_holati,
            'loyiha_muhan_gaz_holati' => $this->loyiha_muhan_gaz_holati,
        ]);

        $query->andFilterWhere(['like', 'loyiha_tashabbuskori', $this->loyiha_tashabbuskori])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'xorij_hamkor', $this->xorij_hamkor])
            ->andFilterWhere(['like', 'horij_davlat', $this->horij_davlat])
            ->andFilterWhere(['like', 'xiz_kursatuvchi_bank', $this->xiz_kursatuvchi_bank])
            ->andFilterWhere(['like', 'ishga_tush_mud_reja', $this->ishga_tush_mud_reja])
            ->andFilterWhere(['like', 'ishga_tush_mud_asos', $this->ishga_tush_mud_asos])
            ->andFilterWhere(['like', 'loyiha_kredit_jadval_topshirish', $this->loyiha_kredit_jadval_topshirish])
            ->andFilterWhere(['like', 'loyiha_kredit_haq_topshirish', $this->loyiha_kredit_haq_topshirish])
            ->andFilterWhere(['like', 'loyiha_kredit_jadval_ajratish', $this->loyiha_kredit_jadval_ajratish])
            ->andFilterWhere(['like', 'loyiha_kredit_haq_ajratish', $this->loyiha_kredit_haq_ajratish]);

        return $dataProvider;
    }

    public function searchByPlanId($params, $plan_id)
    {
            $query = BozorInfratuzilmasi::find()->where(['plan_id'=>$plan_id])->orderBy(['id'=>SORT_DESC]);


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'plan_id' => $this->plan_id,
            'loyiha_yunalishi' => $this->loyiha_yunalishi,
            'quvvat_ulchov_birlik' => $this->quvvat_ulchov_birlik,
            'quvvat_natural_qiy' => $this->quvvat_natural_qiy,
            'quvvat_ish_chiq_hajm' => $this->quvvat_ish_chiq_hajm,
            'yil_eksport_hajm' => $this->yil_eksport_hajm,
            'yil_byudjetga_tushum_hajm' => $this->yil_byudjetga_tushum_hajm,
            'loyiha_qiy_reja' => $this->loyiha_qiy_reja,
            'moliya_mab_reja' => $this->moliya_mab_reja,
            'moliya_kredit_reja' => $this->moliya_kredit_reja,
            'moliya_invest_reja' => $this->moliya_invest_reja,
            'ish_urin_reja' => $this->ish_urin_reja,
            'loyiha_muhan_yer_holati' => $this->loyiha_muhan_yer_holati,
            'loyiha_muhan_elektr_holati' => $this->loyiha_muhan_elektr_holati,
            'loyiha_muhan_gaz_holati' => $this->loyiha_muhan_gaz_holati,
        ]);

        $query->andFilterWhere(['like', 'loyiha_tashabbuskori', $this->loyiha_tashabbuskori])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'xorij_hamkor', $this->xorij_hamkor])
            ->andFilterWhere(['like', 'horij_davlat', $this->horij_davlat])
            ->andFilterWhere(['like', 'xiz_kursatuvchi_bank', $this->xiz_kursatuvchi_bank])
            ->andFilterWhere(['like', 'ishga_tush_mud_reja', $this->ishga_tush_mud_reja])
            ->andFilterWhere(['like', 'ishga_tush_mud_asos', $this->ishga_tush_mud_asos])
            ->andFilterWhere(['like', 'loyiha_kredit_jadval_topshirish', $this->loyiha_kredit_jadval_topshirish])
            ->andFilterWhere(['like', 'loyiha_kredit_haq_topshirish', $this->loyiha_kredit_haq_topshirish])
            ->andFilterWhere(['like', 'loyiha_kredit_jadval_ajratish', $this->loyiha_kredit_jadval_ajratish])
            ->andFilterWhere(['like', 'loyiha_kredit_haq_ajratish', $this->loyiha_kredit_haq_ajratish]);

        return $dataProvider;
    }
}
