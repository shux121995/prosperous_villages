<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tables_excel}}".
 *
 * @property int $id
 * @property int $table_id
 * @property int $section_id
 * @property int $input_id
 * @property int $parent_id
 * @property int $type
 * @property int $level
 * @property int $colspan
 * @property int $rowspan
 * @property int $order
 * @property int $is_empty
 *
 * @property Sections[] $section
 */
class TablesExcel extends \yii\db\ActiveRecord
{
    const TYPE_ONLY_SECTION = 1;
    const TYPE_SECTION_INPUT = 3;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tables_excel}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['table_id'], 'required'],
            [['table_id', 'section_id', 'input_id', 'parent_id', 'type', 'level', 'colspan', 'rowspan', 'order', 'is_empty'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'table_id' => Yii::t('app', 'Table ID'),
            'section_id' => Yii::t('app', 'Section ID'),
            'input_id' => Yii::t('app', 'Input ID'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'type' => Yii::t('app', 'Type'),
            'level' => Yii::t('app', 'Level'),
            'colspan' => Yii::t('app', 'Colspan'),
            'rowspan' => Yii::t('app', 'Rowspan'),
            'order' => Yii::t('app', 'Order'),
            'is_empty' => Yii::t('app', 'Is empty'),
        ];
    }

    /**
     * Generation tables
     * @param $table_id
     * @return bool
     * @throws \yii\db\Exception
     */
    public static function GenExcelTable($table_id)
    {
        $table = Tables::findOne($table_id);
        if($table==null) return false; //пропустим если таблица нет
        //Получим только родительские секции
        $sections = Sections::find()->where(['tables_id'=>$table->id, 'parent_id'=>null])->all();
        //Команда на выполнение генерации excel table
        self::CreateExcel($sections); //генерация только секции
        self::CreateExcel($sections,1,null,self::TYPE_SECTION_INPUT); //генерация для inputs
        return true;
    }

    /**
     * Генерация секции таблиц
     * @param $sections
     * @param int $level
     * @return bool
     */
    public static function CreateExcel($sections,$level=1,$model = null,$type=self::TYPE_ONLY_SECTION)
    {
        if($sections==null) return true; //пропустим если секции нет
        $parent_id = null;

        /** @var $model TablesExcel */
        if($model!=null) $parent_id = $model->id;
        /** @var $section Sections*/
        foreach ($sections as $section){
            //Зарос на создание TablesExcel
            $model = self::CreateTE($section,$level,$type,false,$parent_id);
            //добавить td для inputs
            $inputs = Inputs::find()->where(['forms_id'=>$section->forms_id])->all();
            if($type==self::TYPE_SECTION_INPUT && $inputs!=null){
                foreach ($inputs as $input){
                    self::CreateTE($section,$level,$type,false,$parent_id,$input->id);
                }
            }elseif ($type==self::TYPE_ONLY_SECTION){ //Добавим только section_id для ONLY SECTION
                if($section->forms_id!=null) self::CreateTE($section,null,$type,false,$parent_id);
            }

            //Добавить если пустый левелы
            if($section->rows>1){
                //Добавление пустое TD
                $sublevel = self::CreateEmpty($section,$model->level,$type,$model->id,$type);
                //Генерация суб секции
                if($section->sections) self::CreateExcel($section->sections, $sublevel+1, $model,$type);
            }else{
                //Генерация суб секции
                if($section->sections) self::CreateExcel($section->sections, $level+1, $model,$type);
            }
        }
    }

    /**
     * Конечный добавление к Tables excel
     * @param $section
     * @param $level
     * @param $type
     * @param bool $isEmpty
     * @param null $parent_id
     * @param null $input_id
     * @return TablesExcel
     */
    public static function CreateTE($section,$level,$type,$isEmpty=false,$parent_id=null,$input_id=null)
    {
        /** @var $section Sections */
        $model = new TablesExcel();
        $model->table_id = $section->tables_id;
        $model->section_id = $section->id;
        $model->parent_id = $parent_id;
        $model->level = ($input_id==null)?$level:null;
        $model->type = $type;
        $model->rowspan = $section->rows;
        $model->colspan = ($type==self::TYPE_SECTION_INPUT)?$section->cols_input:$section->cols;
        if($isEmpty) $model->is_empty = 1;
        if($input_id!=null) $model->input_id = $input_id;
        $model->save();
        return $model;
    }

    /**
     * Добавить пустое td
     * @param $section
     * @param $level
     * @param $type
     * @param $parent_id
     * @return int
     */
    public static function CreateEmpty($section,$level,$type,$parent_id)
    {
        /** @var $section Sections */
        if($section->rows>1){
            for ($i = 1; $i <= $section->rows-1; $i++) {
                //Зарос на создание TablesExcel
                self::CreateTE($section,$level+$i,$type, true, $parent_id);
            }
            return $section->rows-1 + $level;
        }
        return $level;
    }

    /**
     * @return Sections|null
     */
    public function getSection()
    {
        return Sections::findOne($this->section_id);
    }

    /**
     * @param $section_id
     * @return Sections|null
     */
    public static function eSection($section_id)
    {
        return Sections::findOne($section_id);
    }

    /**
     * @param $section_id
     * @return Sections|null
     */
    public static function eInput($input_id)
    {
        return Inputs::findOne($input_id);
    }
}
