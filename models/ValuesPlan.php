<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%values_plan}}".
 *
 * @property int $id
 * @property int $plan_id
 * @property int $section_id
 * @property int $inputs_id
 * @property string $reja
 * @property string $molled
 * @property int $projects
 * @property int $qurulish_id
 * @property int $bozor_id
 *
 * @property ValuesFact[] $valuesFacts
 * @property ValuesManbaMolled[] $valuesManbaMolleds
 * @property ValuesManbaPlan[] $valuesManbaPlans
 * @property Inputs $inputs
 * @property LocalityPlans $plan
 * @property Sections $section
 */
class ValuesPlan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%values_plan}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['plan_id'], 'required'],
            [['plan_id', 'section_id', 'inputs_id', 'projects'], 'integer'],
            [['molled'], 'number'],
            [['reja'], 'string', 'max' => 45],
            [['bozor_id', 'qurulish_id'], 'safe'],
            [['inputs_id'], 'exist', 'skipOnError' => true, 'targetClass' => Inputs::className(), 'targetAttribute' => ['inputs_id' => 'id']],
            [['plan_id'], 'exist', 'skipOnError' => true, 'targetClass' => LocalityPlans::className(), 'targetAttribute' => ['plan_id' => 'id']],
            [['section_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sections::className(), 'targetAttribute' => ['section_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'plan_id' => Yii::t('app', 'Plan ID'),
            'section_id' => Yii::t('app', 'Section ID'),
            'inputs_id' => Yii::t('app', 'Inputs ID'),
            'reja' => Yii::t('app', 'Reja'),
            'molled' => Yii::t('app', 'Molled'),
            'projects' => Yii::t('app', 'Projects'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValuesFacts()
    {
        return $this->hasMany(ValuesFact::className(), ['plan_value_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValuesManbaMolleds()
    {
        return $this->hasMany(ValuesManbaMolled::className(), ['values_plan_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValuesManbaPlans()
    {
        return $this->hasMany(ValuesManbaPlan::className(), ['values_plan_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInputs()
    {
        return $this->hasOne(Inputs::className(), ['id' => 'inputs_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlan()
    {
        return $this->hasOne(LocalityPlans::className(), ['id' => 'plan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSection()
    {
        return $this->hasOne(Sections::className(), ['id' => 'section_id']);
    }

    public function  getCalc()
    {
        $v = 0;
        $values = $this->valuesManbaPlans;
        if($values==null) return 0;

        /** @var $value ValuesManbaPlan */
        foreach ($values as $value){
            $v = $v + $value->reja;
        }
        return $v;
    }
}
