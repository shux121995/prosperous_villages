<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bozor_infratuzilmasi_amalda".
 *
 * @property int $id
 * @property int $bozor_id
 * @property string $loyiha_qiy_amalda
 * @property string $mol_mab_amalda
 * @property string $moliya_kredit_amalda
 * @property string $moliya_invest_amalda
 * @property int $ish_urin_amalda
 * @property string $ishga_tush_mud_amalda
 *
 * @property BozorInfratuzilmasi $bozor
 */
class BozorInfratuzilmasiAmalda extends \yii\db\ActiveRecord
{
    public $uzlashtirildi;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bozor_infratuzilmasi_amalda';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bozor_id', 'ish_urin_amalda', 'ishga_tush_mud_amalda', 'loyiha_qiy_amalda'], 'required'],
            [['bozor_id', 'ish_urin_amalda'], 'integer'],
            [['loyiha_qiy_amalda', 'mol_mab_amalda', 'moliya_kredit_amalda', 'moliya_invest_amalda'], 'number'],
            [['ishga_tush_mud_amalda'], 'string', 'max' => 128],
            [['uzlashtirildi'], 'safe'],
            [['bozor_id'], 'exist', 'skipOnError' => true, 'targetClass' => BozorInfratuzilmasi::className(), 'targetAttribute' => ['bozor_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bozor_id' => 'Бозор инфратузилмаси объекти',
            'loyiha_qiy_amalda' => 'Лойиҳа қиймати (амалда)',
            'mol_mab_amalda' => 'Молиялаштириш манбалари (ўз маблағи)',
            'moliya_kredit_amalda' => 'Молиялаштириш манбалари (банк кредити)',
            'moliya_invest_amalda' => 'Молиялаштириш манбалари (хорижий инвестиция)',
            'ish_urin_amalda' => 'Иш ўринлари (амалда)',
            'ishga_tush_mud_amalda' => 'Ишга тушиш муддати (амалда)',
            'uzlashtirildi' => 'Ўзлаштирилди (Молиялаштириш манбалари)',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBozor()
    {
        return $this->hasOne(BozorInfratuzilmasi::className(), ['id' => 'bozor_id']);
    }
}
