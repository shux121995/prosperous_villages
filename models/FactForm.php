<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Fact form
 */
/**
 * This is the model.
 *
 * @property int $id
 * @property int $values_fact_id
 * @property int $moliya_manbai_id
 * @property float $fact
 * @property int $is_fill
 * @property int $inputs_id
 *
 */

class FactForm extends Model
{
    public $id;
    public $fact;
    public $moliya_manbai_id;
    public $values_fact_id;
    public $is_fill;
    public $inputs_id;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'moliya_manbai_id', 'values_fact_id', 'is_fill'], 'integer'],
            ['fact', 'number'],
            ['is_fill', 'boolean'],
            ['is_fill', 'checkIsfills'],
        ];
    }

    /**
     * Проверка остаток в балансе
     * @return bool
     */
    public function checkIsfills()
    {
        if ($this->is_fill && empty($this->reja)) {
            $this->addError('fact', Yii::t('app', 'Танланган манба учун фактни киритиш шарт!'));
            return false;
        }
        return true;
    }

    /**
     * Сохранить ил обновить данны
     */
    public function addFact($plan_id, $section_id, $force = false)
    {
        /** @var $valuesPlan ValuesPlan*/
        $valuesPlan = ValuesPlan::find()->where(['plan_id' => $plan_id, 'section_id' => $section_id])->one();
        if($valuesPlan==null){
            return false;
        }

        $access = FillAccess::getAccess();
        if($access==null) return false; //Пропучтим потому что нет доступ к заполнение

        $valuesFact = ValuesFact::find()->where(['access_id'=>$access->id,'plan_value_id'=>$valuesPlan->id])->one();
        if($valuesFact==null){
            $valuesFact = new ValuesFact();
            $valuesFact->access_id = $access->id;
            $valuesFact->plan_value_id = $valuesPlan->id;
            $valuesFact->user_id = Yii::$app->user->getId();
            $valuesFact->post_date = $access->year.'-'.$access->month.'-'.'01';
            $valuesFact->save(false);
        }
        //проверим на наличие добавленный
        $exist = ValuesManbaFact::findOne($this->id);
        if($exist!=null){
            $model = $exist;
            //Если пустое но ранный добавленный манба будут удален
            if(empty($this->fact)) {
                $exist->delete();
                return true;
            }
        }else{
            $model = new ValuesManbaFact();
            $model->values_fact_id = $valuesFact->id;
            $model->moliya_manba_id = $this->moliya_manbai_id;
        }
        $model->fact = $this->fact;
        //Пропустим если поля пустое
        if(empty($model->fact)) {
            return true;
        }
        //Добавляем ил обновляем
        if($model->save()){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Update reja values after adding plan manba
     * @param $plan_id
     * @param $section_id
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function calcFact($plan_id, $section_id)
    {
        /** @var $valuesPlan ValuesPlan*/
        $valuesPlan = ValuesPlan::find()->where(['plan_id' => $plan_id, 'section_id' => $section_id])->one();
        if($valuesPlan!=null){
            /** @var $valuesFact ValuesFact */
            $valuesFact = ValuesFact::find()->where(['plan_value_id' => $valuesPlan->id])->one();
            if($valuesPlan!=null) {
                $valuesFact->fact = $valuesFact->getCalc();
                $valuesFact->update(false);
            }
        }
    }
}
