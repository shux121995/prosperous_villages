<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "qurulish_amalda".
 *
 * @property int $id
 * @property int $qurulish_id
 * @property string $qiy_used
 * @property string $oh_yer
 * @property string $oh_poydevor
 * @property string $oh_gisht_terish
 * @property string $oh_tom_yopish
 * @property string $oh_demontaj
 * @property string $oh_oraliq_devor
 * @property string $oh_eshik_deraza
 * @property string $oh_ichki_pardozlash
 * @property string $oh_tashqi_pardozlash
 * @property string $date
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Qurulish $qurulish
 */
class QurulishAmalda extends \yii\db\ActiveRecord
{
    public $uzlashtirildi;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'qurulish_amalda';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['oh_yer', 'oh_poydevor', 'oh_gisht_terish', 'oh_tom_yopish', 'oh_demontaj', 'oh_oraliq_devor', 'oh_eshik_deraza', 'oh_ichki_pardozlash', 'oh_tashqi_pardozlash'], 'required'],
            [['qurulish_id', 'created_at', 'updated_at'], 'integer'],
            [['oh_yer', 'oh_poydevor', 'oh_gisht_terish', 'oh_tom_yopish', 'oh_demontaj', 'oh_oraliq_devor', 'oh_eshik_deraza', 'oh_ichki_pardozlash', 'oh_tashqi_pardozlash'], 'number'],
            [['qiy_used', 'date', 'uzlashtirildi'], 'safe'],
            [['qurulish_id'], 'exist', 'skipOnError' => true, 'targetClass' => Qurulish::className(), 'targetAttribute' => ['qurulish_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'qurulish_id' => 'Қурилиш объекти',
            'qiy_used' => 'Қиймати (ўзлаштирилди)',
            'oh_yer' => 'Объект холати (Ер ишлари)',
            'oh_poydevor' => 'Объект холати (Пойдевор ишлари)',
            'oh_gisht_terish' => 'Объект холати (Ғишт териш)',
            'oh_tom_yopish' => 'Объект холати (Том ёпмаси)',
            'oh_demontaj' => 'Объект холати (Демонтаж ишлари)',
            'oh_oraliq_devor' => 'Объект холати (Оралиқ девор ишлари)',
            'oh_eshik_deraza' => 'Объект холати (Эшик ва дераза ўрнатиш)',
            'oh_ichki_pardozlash' => 'Объект холати (Ички пардозлаш)',
            'oh_tashqi_pardozlash' => 'Объект холати (Ташқи пардозлаш)',
            'uzlashtirildi' => 'Ўзлаштирилди (Молиялаштириш манбалари)',
            'date' => 'Сана',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQurulish()
    {
        return $this->hasOne(Qurulish::className(), ['id' => 'qurulish_id']);
    }
}
