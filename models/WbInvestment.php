<?php


namespace app\models;


use yii\db\ActiveRecord;

use Yii;

/**
 * This is the model class for table "{{%aholi}}".
 *
 * @property int $id
 * @property int $region_id
 * @property int $district_id
 * @property int $locality_id
 * @property int $project_id
 * @property int $project_year
 * @property int $project_investment
 */

class WbInvestment extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%wb_investment}}';
    }

    public function rules()
    {
        return [
            [['region_id', 'district_id', 'locality_id', 'project_id', 'project_year', 'project_investment'], 'integer'],
            [['project_year', 'project_investment'], 'required', 'on' => 'add'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'locality_id' => Yii::t('app', 'Village ID'),
            'region_id' => Yii::t('app', 'Region ID'),
            'district_id' => Yii::t('app', 'District ID'),
            'project_id' => Yii::t('app', 'Project ID'),
            'project_year' => Yii::t('app', 'Project Year'),
            'project_investment' => Yii::t('app', 'Project Investment, USD'),
        ];
    }

    public function getTotalProjects($region_id, $district_id, $village_id){
            if(!empty($village_id)) {
                $projects_village = WbInvestment::find()->where(['locality_id' => $village_id])->all();
                return count($projects_village);
            } else if(!empty($district_id)) {
                $projects_district =  WbInvestment::find()->where(['district_id' => $district_id])->all();
                return count($projects_district);
            } else if(!empty($region_id)) {
                $projects_region =  WbInvestment::find()->where(['region_id' => $region_id])->all();
                return count($projects_region);
            } else if(empty($region_id) && empty($district_id) && empty($village_id)){
                $total_projects =  WbInvestment::find()->all();
                return count($total_projects);
            }
            return 0;
    }

    public function getTotalInvestments($region_id, $district_id, $village_id){
        if(!empty($village_id)) {
            return WbInvestment::find()->where(['locality_id' => $village_id])->sum('project_investment');
        } else if(!empty($district_id)) {
            return WbInvestment::find()->where(['district_id' => $district_id])->sum('project_investment');
        } else if(!empty($region_id)) {
            return WbInvestment::find()->where(['region_id' => $region_id])->sum('project_investment');
        } else if(empty($region_id) && empty($district_id) && empty($village_id)){
            return WbInvestment::find()->sum('project_investment');
        }
        return 0;
    }

    public function getSpecificProjectInfo($region_id, $district_id, $village_id, $project_id){
        if(!empty($village_id)) {
            $projects_village = WbInvestment::find()->where(['locality_id' => $village_id, 'project_id'=>$project_id])->all();
            return $projects_village;
        } else if(!empty($district_id)) {
            $projects_district =  WbInvestment::find()->where(['district_id' => $district_id, 'project_id'=>$project_id])->all();
            return $projects_district;
        } else if(!empty($region_id)) {
            $projects_region =  WbInvestment::find()->where(['region_id' => $region_id, 'project_id'=>$project_id])->all();
            return $projects_region;
        } else if(empty($region_id) && empty($district_id) && empty($village_id)){
            $total_projects =  WbInvestment::find()->where(['project_id'=>$project_id])->all();
            return $total_projects;
        }
        return 0;
    }
}