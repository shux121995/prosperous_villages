<?php

namespace app\models;

use Yii;

class WbTypes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%wb_types}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title', 'title_en', 'identifier'], 'string', 'max' => 255],
            [['icon'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'title_en' => Yii::t('app', 'Title En'),
            'icon' => Yii::t('app', 'Icon'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWbProjects()
    {
        return $this->hasMany(WbProjects::className(), ['type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWbSubTypes()
    {
        return $this->hasMany(WbSubTypes::className(), ['type_id' => 'id']);
    }

    /**
     * @param $lang
     * @return mixed|string
     * Lang helper by lang title
     */
    public function getTitleLang($lang)
    {
        if ($lang != 'uz') {
            $lang = 'title_' . $lang;
            return !empty($this->$lang) ? $this->$lang : $this->title;
        } else {
            return $this->title;
        }
    }

    public function getPlan()
    {
        $subQuery = WbSubTypes::find()->select('id')->where(['type_id'=>$this->id]);
        return WbTasks::find()->select('price_usd')->where(['in', 'sub_type_id', $subQuery])->sum('price_usd');
    }

    public function getFact()
    {
        $subQuery = WbSubTypes::find()->select('id')->where(['type_id'=>$this->id]);
        return WbTasks::find()->select('price_usd')->where(['in', 'sub_type_id', $subQuery])->andWhere(['status'=>1])->sum('price_usd');
    }

    public function getPlanResp($regionId,$typeId)
    {
        $subQuery = WbSubTypes::find()->select('id')->where(['type_id'=>$typeId]);
        $subQueryPlan = WbPlans::find()->select('id')->where(['region_id'=>$regionId]);
        $subQueryProject = WbProjects::find()->select('id')
            ->where(['type_id' => $typeId])
            ->where(['in', 'plan_id', $subQueryPlan]);

        return WbTasks::find()->select('price_usd')
            ->where(['in', 'sub_type_id', $subQuery])
            ->andWhere(['in', 'project_id', $subQueryProject])
            ->sum('price_usd');
    }

    public function getFactResp($regionId,$typeId)
    {
        $subQuery = WbSubTypes::find()->select('id')->where(['type_id'=>$typeId]);
        $subQueryPlan = WbPlans::find()->select('id')->where(['region_id'=>$regionId]);
        $subQueryProject = WbProjects::find()->select('id')
            ->where(['type_id' => $typeId])
            ->andWhere(['in', 'plan_id', $subQueryPlan]);

        return WbTasks::find()->select('price_usd')
            ->where(['in', 'sub_type_id', $subQuery])
            ->andWhere(['in', 'project_id', $subQueryProject])
            ->andWhere(['status'=>1])->sum('price_usd');
    }

    public function getPlanReg($districtId,$typeId)
    {
        $subQuery = WbSubTypes::find()->select('id')->where(['type_id'=>$typeId]);
        $subQueryPlan = WbPlans::find()->select('id')->where(['district_id'=>$districtId]);
        $subQueryProject = WbProjects::find()->select('id')
            ->where(['type_id' => $typeId])
            ->where(['in', 'plan_id', $subQueryPlan]);

        return WbTasks::find()->select('price_usd')
            ->where(['in', 'sub_type_id', $subQuery])
            ->andWhere(['in', 'project_id', $subQueryProject])
            ->sum('price_usd');
    }

    public function getFactReg($districtId,$typeId)
    {
        $subQuery = WbSubTypes::find()->select('id')->where(['type_id'=>$typeId]);
        $subQueryPlan = WbPlans::find()->select('id')->where(['district_id'=>$districtId]);
        $subQueryProject = WbProjects::find()->select('id')
            ->where(['type_id' => $typeId])
            ->andWhere(['in', 'plan_id', $subQueryPlan]);

        return WbTasks::find()->select('price_usd')
            ->where(['in', 'sub_type_id', $subQuery])
            ->andWhere(['in', 'project_id', $subQueryProject])
            ->andWhere(['status'=>1])->sum('price_usd');
    }

    public function getPlanDis($localityId,$typeId)
    {
        $subQuery = WbSubTypes::find()->select('id')->where(['type_id'=>$typeId]);
        $subQueryPlan = WbPlans::find()->select('id')->where(['locality_id'=>$localityId]);
        $subQueryProject = WbProjects::find()->select('id')
            ->where(['type_id' => $typeId])
            ->where(['in', 'plan_id', $subQueryPlan]);

        return WbTasks::find()->select('price_usd')
            ->where(['in', 'sub_type_id', $subQuery])
            ->andWhere(['in', 'project_id', $subQueryProject])
            ->sum('price_usd');
    }

    public function getFactDis($localityId,$typeId)
    {
        $subQuery = WbSubTypes::find()->select('id')->where(['type_id'=>$typeId]);
        $subQueryPlan = WbPlans::find()->select('id')->where(['locality_id'=>$localityId]);
        $subQueryProject = WbProjects::find()->select('id')
            ->where(['type_id' => $typeId])
            ->andWhere(['in', 'plan_id', $subQueryPlan]);

        return WbTasks::find()->select('price_usd')
            ->where(['in', 'sub_type_id', $subQuery])
            ->andWhere(['in', 'project_id', $subQueryProject])
            ->andWhere(['status'=>1])->sum('price_usd');
    }
}
