<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%wb_developers}}".
 *
 * @property int $id
 * @property string $title
 * @property string $title_en
 * @property string $address
 * @property string $phone
 * @property string $email
 * @property string $direktor
 */
class WbDevelopers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%wb_developers}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['address'], 'string'],
            [['title', 'title_en', 'direktor'], 'string', 'max' => 255],
            [['phone', 'email'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'title_en' => Yii::t('app', 'Title En'),
            'address' => Yii::t('app', 'Address'),
            'phone' => Yii::t('app', 'Phone'),
            'email' => Yii::t('app', 'Email'),
            'direktor' => Yii::t('app', 'Direktor'),
        ];
    }

    /**
     * @param $lang
     * @return mixed|string
     * Lang helper by lang title
     */
    public function getTitleLang($lang)
    {
        if ($lang != 'uz') {
            $lang = 'title_' . $lang;
            return !empty($this->$lang) ? $this->$lang : $this->title;
        } else {
            return $this->title;
        }
    }
}
