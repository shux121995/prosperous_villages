<?php


namespace app\models;


use yii\db\ActiveRecord;

class TotalFeedback extends ActiveRecord
{
    public static function tableName()
    {
        return 'total_feedback';
    }

    public function rules()
    {
        return [
            [['region_id', 'feedback_year', 'feedback_quantity'], 'required'],
            [['region_id'], 'string', 'max'=>2],
            [['feedback_year'], 'string', 'max' => 4],
            [['feedback_quantity'], 'string'],
        ];
    }
}