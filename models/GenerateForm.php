<?php
/**
 * Created by PhpStorm.
 * User: Muxtorov Ulugbek
 * Date: 16.04.2019
 * Time: 14:17
 */

namespace app\models;


use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use Yii;

class GenerateForm extends Component
{
    /**
     * Генератор формы
     * @param $section_id
     * @param $plan_id
     * @param bool $onlyAdmin
     * @return string
     */
    public static function genForm($section_id, $plan_id, $onlyAdmin = true)
    {
        $html = '';
        $section = Sections::findOne($section_id);
        if ($section->forms_id != null) {
            $inputs = $section->forms->inputs;
            foreach ($inputs as $input) {
                if ($onlyAdmin) {
                    if ($input->is_admin != 1) continue;
                }
                if (!$onlyAdmin) {
                    if ($input->is_admin == 1) continue;
                }
                if (Yii::$app->request->isPost) {
                    $postData = Yii::$app->request->post();
                    $value = isset($postData['section'][$section->id][$input->name]) ? $postData['section'][$section->id][$input->name] : null;
                } else {
                    $value = null;
                    if ($onlyAdmin) {
                        $getValue = ValuesPlan::find()->where(['section_id' => (int)$section_id, 'plan_id' => (int)$plan_id])->asArray()->one();
                        if ($getValue != null) $value = $getValue[$input->name];
                    }
                }

                if($input->type=='select'){
                    $html .= '<div class="form-group required">
                            <label class="control-label">' . $input->title . '</label>
                            ' . Html::dropDownList(
                            'section[' . $section->id . '][' . $input->name . ']',
                            $value,
                            ArrayHelper::map(MoliyaManbai::find()->All(),'id','title'),
                            [
                                'class' => 'form-control',
                                'prompt' => $input->title.'ни танланг'
                            ]) . '
                            <div class="help-block"></div>
                        </div>';
                }else{
                    $html .= '<div class="form-group required">
                            <label class="control-label">' . $input->title . '</label>
                            ' . Html::input(
                            $input->type,
                            'section[' . $section->id . '][' . $input->name . ']',
                            $value,
                            [
                                'class' => 'form-control',
                                'data-validation' => "number",
                                'data-validation-allowing' => "float",
                                'data-validation-optional' => "true",
                            ]) . '
                            <div class="help-block"></div>
                        </div>';
                }

                $html .= Html::input(
                    'hidden',
                    'section[' . $section->id . '][input_id]',
                    $input->id,
                    [
                        'class' => 'form-control',
                    ]);
            }
        }
        return $html;
    }

    /**
     * Генератор формы для факты
     * @param $section_id
     * @param $plan_id
     * @param bool $onlyAdmin
     * @return string
     */
    public static function genFormFact($section_id, $plan_id, $plan_status, $onlyAdmin = true)
    {
        $html = '';
        $section = Sections::findOne($section_id);
        if ($section->forms_id != null) {
            $inputs = $section->forms->inputs;
            foreach ($inputs as $input) {

                $getReja = ValuesPlan::find()->where(['section_id' => (int)$section_id, 'plan_id' => (int)$plan_id])->asArray()->one();
                if($getReja==null OR empty($getReja)) continue;

                if (Yii::$app->request->isPost) {
                   $postData = Yii::$app->request->post();
                   $value = isset($postData['section'][$section->id][$input->name]) ? $postData['section'][$section->id][$input->name] : null;
                } else {
                   $value = null;
                   $getValue = ValuesFact::find()->where(['plan_value_id' => (int)$getReja['id'], 'user_id' => Yii::$app->user->identity->id])->asArray()->one();
                   if ($getValue != null) $value = $getValue[$input->name];

                }

                if($input->name == 'fact' and $plan_status == 1) {

                   if ($input->type == 'select') {
                       $html .= '<div class="form-group required">
                            <p><b>Режа:</b> ' . $getReja['reja'] . '</p>
                            <label class="control-label">' . $input->title . '</label>
                            ' . Html::dropDownList(
                               'section[' . $section->id . '][' . $input->name . ']',
                               $value,
                               ArrayHelper::map(MoliyaManbai::find()->All(), 'id', 'title'),
                               [
                                   'class' => 'form-control',
                                   'prompt' => $input->title . 'ни танланг'
                               ]) . '
                            <div class="help-block"></div>
                        </div>';
                   } else {
                       $html .= '<div class="form-group required">
                            <p><b>Режа:</b> ' . $getReja['reja'] . '</p>
                            <label class="control-label">' . $input->title . '</label>
                            ' . Html::input(
                               $input->type,
                               'section[' . $section->id . '][' . $input->name . ']',
                               $value,
                               [
                                   'class' => 'form-control',
                                   'data-validation' => "number",
                                   'data-validation-allowing' => "float",
                                   'data-validation-optional' => "false",
                               ]) . '
                            <div class="help-block"></div>
                        </div>';
                   }

                    $html .= Html::input(
                        'hidden',
                        'section[' . $section->id . '][input_id]',
                        $input->id,
                        [
                            'class' => 'form-control',
                        ]);

                    $html .= Html::input(
                        'hidden',
                        'section[' . $section->id . '][plan_value_id]',
                        $getReja['id'],
                        [
                            'class' => 'form-control',
                        ]);
                }
                elseif($input->name == 'fact' and $plan_status != 1) {

                    if ($input->type == 'select') {
                        $html .= '<div class="form-group required">
                            <p><b>Режа:</b> ' . $getReja['reja'] . '</p>
                        </div>';
                    } else {
                        $html .= '<div class="form-group required">
                            <p><b>Режа:</b> ' . $getReja['reja'] . '</p>
                        </div>';
                    }
                }


            }
        }
        return $html;
    }
    /**
     * Генератор секции + формы для него
     * @param $sections
     * @param $plan_id
     * @return string
     */
    public static function genSections($sections, $plan_id, $plan_status = 0, $plan = 'plan', $onlyAdmin = true)
    {
        $html = '';
        $padding = 'padding_left_off';
        foreach ($sections as $sectioned){
            //Form generations
            $form = '';
            $subSections = null;
            if($plan == 'plan'){
                $form = self::genForm($sectioned->id, $plan_id, $onlyAdmin);
                $subSections = Sections::findOne($sectioned->id)->sections;
            }elseif($plan == 'fact'){
                $form = self::genFormFact($sectioned->id, $plan_id, $plan_status, $onlyAdmin);
                //$subQuery = ValuesPlan::find()->select('section_id')->where(['plan_id' => $plan_id, 'section_id' => $sectioned->id]);
                //$subSections = Sections::find()->where(['id'=>$sectioned->parent_id])->andWhere(['in', 'id', $subQuery])->all();
                $subSections = Sections::findOne($sectioned->id)->sections;
            }

            $html .= '<div class="bs-example">';
            $html .= '<label>'.$sectioned->title.'</label>';
            $html .= '<div class="'.($subSections==null?'':$padding).'">';
            $html .= $form;

            if($sectioned->sections!=null){
                $html .= self::genSections($sectioned->sections, $plan_id, $plan_status, $plan, $onlyAdmin);
            }
            $html .= '</div>';
            $html .= '</div>';
        }
        return $html;
    }/**
     * Генератор секции + формы для него
     * @param $sections
     * @param $plan_id
     * @return string
     */
    public static function genSectionsTabled($sections, $plan_id, $plan_status = 0, $plan = 'plan', $onlyAdmin = true)
    {
        $html = '<table class="table-bordered table" style="width: 100%;">';
        foreach ($sections as $sectioned){
            $subSections = null;
            $html .= '<tr>';
            $html .= '<td>'.$sectioned->title.'</td>';

            //subs and forms
            if($sectioned->sections!=null){
                $html .= '<td>';
                $html .= self::tabledSection($sectioned->sections, $plan_id, $plan_status, $plan, $onlyAdmin);
                $html .= '</td>';
            }
            if($sectioned->forms!=null){
                $html .= '<td>';
                $html .= self::tabledForm($sectioned, $plan_id, $plan_status, $plan, $onlyAdmin);
                $html .= '</td>';
            }
            $html .= '</tr>';
        }
        $html .= '</table>';
        return $html;
    }

    public static function tabledSection($sections, $plan_id, $plan_status, $plan, $onlyAdmin)
    {
        $html = '<table class="table-bordered table" style="border:0; width: 100%;">';
        if($sections!=null){

            /** @var $section Sections */
            foreach ($sections as $section){
                $html .= '<tr>';
                $html .= '<td>'.$section->title.'</td>';
                if($section->sections!=null){
                    $html .= '<td>';
                    $html .= self::tabledSection($section->sections, $plan_id, $plan_status, $plan, $onlyAdmin);
                    $html .= '</td>';
                }
                if($section->forms!=null){
                    $html .= '<td>';
                    $html .= self::getData($section->id, $plan_id, $plan);
                    $html .= '</td>';
                    $html .= '<td>';
                    $html .= self::tabledForm($section, $plan_id, $plan_status, $plan, $onlyAdmin);
                    $html .= '</td>';
                }
                $html .= '</tr>';
            }

        }
        $html .= '</table>';
        return $html;
    }

    public static function tabledForm($section, $plan_id, $plan_status, $plan, $onlyAdmin)
    {
        if($plan == 'plan'){
            $form = self::genAjaxForm($section, $plan_id, $plan_status, $onlyAdmin);
        }elseif($plan == 'fact'){
            $form = self::genAjaxFormFact($section, $plan_id, $plan_status, $onlyAdmin);
        }
        return $form;
    }

    public static function genAjaxForm($section, $plan_id, $plan_status, $onlyAdmin)
    {
        //Пропустим если актив или нет план
        if($plan_status!=0 OR $plan_status!=null) {
            return null;
        }
        $textbutton = 'Киритиш!';
        /** @var $valuesPlan ValuesPlan*/
        $valuesPlan = ValuesPlan::find()->where(['plan_id' => $plan_id, 'section_id' => $section->id])->one();
        if(isset($valuesPlan->valuesManbaPlans)){
            $textbutton = 'Узгартириш';
        }

        /** @var  $section Sections */
        $table = Tables::findOne($section->tables_id);
        return Html::a(Yii::t('app', $textbutton),
            ['add', 'plan_id' => $plan_id, 'table_id'=>$table->id, 'section_id'=>$section->id],
            [
                'title' => Yii::t('app', $section->title. 'га режа киритиш'),
                'data-toggle' => 'modal',
                'data-target' => '#modal',
                'class' => 'showModalButton btn btn-danger'
            ]
        );
    }

    public static function genAjaxFormFact($section, $plan_id, $plan_status, $onlyAdmin)
    {
        //Пропустим если не актив или нет план
        if($plan_status!=1) {
            return null;
        }

        $textbutton = 'Киритиш!';
        /** @var $valuesPlan ValuesPlan*/
        $valuesPlan = ValuesPlan::find()->where(['plan_id' => $plan_id, 'section_id' => $section->id])->one();
        if($valuesPlan==null) {
            return 'Режа йук';
        }

        if($valuesPlan->valuesFacts!=null){
            $textbutton = 'Узгартириш';
        }

        /** @var  $section Sections */
        $table = Tables::findOne($section->tables_id);
        return Html::a(Yii::t('app', $textbutton),
            ['add', 'plan_id' => $plan_id, 'table_id'=>$table->id, 'section_id'=>$section->id],
            [
                'title' => Yii::t('app', $section->title. 'га факт/амалда киритиш'),
                'data-toggle' => 'modal',
                'data-target' => '#modal',
                'class' => 'showModalButton btn btn-danger'
            ]
        );
    }

    public static function getData($section_id, $plan_id, $plan)
    {
        if($plan=='plan'){
            return self::getPlanData($section_id, $plan_id);
        }elseif ($plan=='fact'){
            return self::getFactData($section_id, $plan_id);
        }
        return null;
    }

    public static function getPlanData($section_id, $plan_id, $onlySum=false)
    {
        $html = '';
        /** @var $valuesPlan ValuesPlan*/
        $valuesPlan = ValuesPlan::find()->where(['plan_id' => $plan_id, 'section_id' => $section_id])->one();
        if($valuesPlan != null){
            $html .= 'Жами режа: <b>'.$valuesPlan->reja.'</b>';
            if(!$onlySum){
                /** @var $value ValuesManbaPlan */
                foreach ($valuesPlan->valuesManbaPlans as $value){
                    $html .= '</br>';
                    $html .= $value->moliyaManbai->title.': '.$value->reja;
                }
            }
        }
        return $html;
    }
    public static function getFactData($section_id, $plan_id, $onlySum=false)
    {
        $html = '';
        $html .= self::getPlanData($section_id,$plan_id,true);
        /** @var $valuesPlan ValuesPlan*/
        $valuesPlan = ValuesPlan::find()->where(['plan_id' => $plan_id, 'section_id' => $section_id])->one();
        if($valuesPlan != null){
            /** @var $valuesFact ValuesFact */
            $valuesFact = ValuesFact::find()->where(['plan_value_id' => $valuesPlan->id])->one();
            if($valuesFact!=null) {
                $html .= 'Жами факт: <b>'.$valuesFact->fact.'</b>';
                if(!$onlySum) {
                    /** @var $value ValuesManbaFact */
                    foreach ($valuesFact->valuesManbaFacts as $value) {
                        $html .= '</br>';
                        $html .= $value->moliyaManba->title . ': ' . $value->fact;
                    }
                }
            }

        }
        return $html;

    }

    /**
     * @param $column
     * @param $table
     * @param $status
     * @param $plan
     */

    public static function getDataInTableTdFact($column, $table, $status, $construction_object)
    {
        echo '<td>'.$table->getAttributeLabel($column).'</td>
            <td>'.$table->$column.'</td>';
        if($status != 1) {
            $button = null;
        }
        else {
            $button = '<td>'.Html::a(Yii::t('app', GenerateForm::textButton($table->$column)),
                    ['add', 'construction_object' => $construction_object, 'input'=>$column, 'id'=>$table->id],
                    [
                        'title' => Yii::t('app', $table->getAttributeLabel($column). 'га режа киритиш'),
                        'data-toggle' => 'modal',
                        'data-target' => '#modal',
                        'class' => 'showModalButton btn btn-danger'
                    ]
                ).'</td>';
        }
        echo $button;
    }

    public static function textButton($item)
    {
        if($item === null) {
            return 'Киритиш!';
        }
        else {
            return 'Узгартириш';
        }
    }
}