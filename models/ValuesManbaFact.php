<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%values_manba_fact}}".
 *
 * @property int $id
 * @property int $values_fact_id
 * @property int $moliya_manba_id
 * @property string $fact
 * @property string $post_date
 * @property int $created_at
 * @property int $updated_at
 *
 * @property ValuesFact $valuesFact
 * @property MoliyaManbai $moliyaManba
 */
class ValuesManbaFact extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%values_manba_fact}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['values_fact_id'], 'required'],
            [['values_fact_id', 'moliya_manba_id', 'created_at', 'updated_at'], 'integer'],
            [['post_date'], 'safe'],
            [['fact'], 'string', 'max' => 45],
            [['values_fact_id'], 'exist', 'skipOnError' => true, 'targetClass' => ValuesFact::className(), 'targetAttribute' => ['values_fact_id' => 'id']],
            [['moliya_manba_id'], 'exist', 'skipOnError' => true, 'targetClass' => MoliyaManbai::className(), 'targetAttribute' => ['moliya_manba_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'values_fact_id' => Yii::t('app', 'Values Fact ID'),
            'moliya_manba_id' => Yii::t('app', 'Moliya Manba ID'),
            'fact' => Yii::t('app', 'Fact'),
            'post_date' => Yii::t('app', 'Post Date'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValuesFact()
    {
        return $this->hasOne(ValuesFact::className(), ['id' => 'values_fact_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMoliyaManba()
    {
        return $this->hasOne(MoliyaManbai::className(), ['id' => 'moliya_manba_id']);
    }
}
