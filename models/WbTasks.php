<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%wb_tasks}}".
 *
 * @property int $id
 * @property int $sub_type_id
 * @property int $project_id
 * @property double $price
 * @property double $price_usd
 * @property int $status
 * @property string $date_start Дата начало выполнение задач
 * @property string $date_end Дата завершение выполнение задач
 *
 * @property WbSubTypes $subType
 * @property WbProjects $project
 */
class WbTasks extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%wb_tasks}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sub_type_id', 'project_id'], 'required'],
            [['sub_type_id', 'project_id', 'status'], 'integer'],
            [['price', 'price_usd'], 'number'],
            [['date_start', 'date_end'], 'safe'],
            [['sub_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => WbSubTypes::className(), 'targetAttribute' => ['sub_type_id' => 'id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => WbProjects::className(), 'targetAttribute' => ['project_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'sub_type_id' => Yii::t('app', 'Sub Type ID'),
            'project_id' => Yii::t('app', 'Project ID'),
            'price' => Yii::t('app', 'Price'),
            'price_usd' => Yii::t('app', 'Price Usd'),
            'status' => Yii::t('app', 'Completed'),
            'date_start' => Yii::t('app', 'Дата начало выполнение задач'),
            'date_end' => Yii::t('app', 'Дата завершение выполнение задач'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubType()
    {
        return $this->hasOne(WbSubTypes::className(), ['id' => 'sub_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(WbProjects::className(), ['id' => 'project_id']);
    }
}
