<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%values_manba_molled}}".
 *
 * @property int $id
 * @property int $values_plan_id
 * @property int $moliya_manba_id
 * @property string $molled
 * @property string $date
 *
 * @property MoliyaManbai $moliyaManba
 * @property ValuesPlan $valuesPlan
 */
class ValuesManbaMolled extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%values_manba_molled}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['values_plan_id'], 'required'],
            [['values_plan_id', 'moliya_manba_id'], 'integer'],
            [['date'], 'safe'],
            [['molled'], 'string', 'max' => 45],
            [['moliya_manba_id'], 'exist', 'skipOnError' => true, 'targetClass' => MoliyaManbai::className(), 'targetAttribute' => ['moliya_manba_id' => 'id']],
            [['values_plan_id'], 'exist', 'skipOnError' => true, 'targetClass' => ValuesPlan::className(), 'targetAttribute' => ['values_plan_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'values_plan_id' => Yii::t('app', 'Values Plan ID'),
            'moliya_manba_id' => Yii::t('app', 'Moliya Manba ID'),
            'molled' => Yii::t('app', 'Molled'),
            'date' => Yii::t('app', 'Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMoliyaManba()
    {
        return $this->hasOne(MoliyaManbai::className(), ['id' => 'moliya_manba_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValuesPlan()
    {
        return $this->hasOne(ValuesPlan::className(), ['id' => 'values_plan_id']);
    }
}
