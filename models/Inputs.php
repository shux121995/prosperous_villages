<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%inputs}}".
 *
 * @property int $id
 * @property string $title
 * @property string $title_en
 * @property string $name
 * @property int $forms_id
 * @property string $type
 * @property int $required
 * @property int $is_admin
 *
 * @property Forms $forms
 * @property ValuesFact[] $valuesFacts
 * @property ValuesPlan[] $valuesPlans
 */
class Inputs extends \yii\db\ActiveRecord
{
    const TYPE_TEXT = 'text';
    const TYPE_SELECT = 'select';
    const TYPE_CHECKBOX = 'checkbox';
    const TYPE_CHECKBOXLIST = 'checkboxlist';
    const TYPE_RADIO = 'radio';
    const TYPE_RADIOLIST = 'radiolist';

    /**
     * {@inheritdoc}
     */
    public function inputTypes()
    {
        return [
            self::TYPE_TEXT => Yii::t('app', 'Text'),
            self::TYPE_SELECT => Yii::t('app', 'Select'),
            self::TYPE_CHECKBOX => Yii::t('app', 'Checkbox'),
            self::TYPE_CHECKBOXLIST => Yii::t('app', 'Checkbox list'),
            self::TYPE_RADIO => Yii::t('app', 'Radio'),
            self::TYPE_RADIOLIST => Yii::t('app', 'Radio list'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%inputs}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'name', 'forms_id'], 'required'],
            [['forms_id', 'required', 'is_admin'], 'integer'],
            [['title', 'title_en', 'name'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 45],
            [['forms_id'], 'exist', 'skipOnError' => true, 'targetClass' => Forms::className(), 'targetAttribute' => ['forms_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'title_en' => Yii::t('app', 'Title En'),
            'name' => Yii::t('app', 'Name'),
            'forms_id' => Yii::t('app', 'Forms ID'),
            'type' => Yii::t('app', 'Type'),
            'required' => Yii::t('app', 'Required'),
            'is_admin' => Yii::t('app', 'Is Admin'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForms()
    {
        return $this->hasOne(Forms::className(), ['id' => 'forms_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValuesFacts()
    {
        return $this->hasMany(ValuesFact::className(), ['inputs_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValuesPlans()
    {
        return $this->hasMany(ValuesPlan::className(), ['inputs_id' => 'id']);
    }
}
