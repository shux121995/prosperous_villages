<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%values_fact}}".
 *
 * @property int $id
 * @property int $access_id
 * @property int $plan_value_id
 * @property int $inputs_id
 * @property int $user_id
 * @property string $fact
 * @property string $post_date
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Inputs $inputs
 * @property ValuesPlan $planValue
 * @property ValuesManbaFact[] $valuesManbaFacts
 */
class ValuesFact extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%values_fact}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['plan_value_id', 'access_id'], 'required'],
            [['plan_value_id', 'access_id' , 'inputs_id', 'user_id', 'created_at', 'updated_at'], 'integer'],
            [['post_date'], 'safe'],
            [['fact'], 'string', 'max' => 45],
            [['inputs_id'], 'exist', 'skipOnError' => true, 'targetClass' => Inputs::className(), 'targetAttribute' => ['inputs_id' => 'id']],
            [['plan_value_id'], 'exist', 'skipOnError' => true, 'targetClass' => ValuesPlan::className(), 'targetAttribute' => ['plan_value_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'access_id' => Yii::t('app', 'Access ID'),
            'plan_value_id' => Yii::t('app', 'Plan Value ID'),
            'inputs_id' => Yii::t('app', 'Inputs ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'fact' => Yii::t('app', 'Fact'),
            'post_date' => Yii::t('app', 'Post Date'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInputs()
    {
        return $this->hasOne(Inputs::className(), ['id' => 'inputs_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlanValue()
    {
        return $this->hasOne(ValuesPlan::className(), ['id' => 'plan_value_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValuesManbaFacts()
    {
        return $this->hasMany(ValuesManbaFact::className(), ['values_fact_id' => 'id']);
    }

    public function  getCalc()
    {
        $v = 0;
        $values = $this->valuesManbaFacts;
        if($values==null) return 0;

        /** @var $value ValuesManbaFact */
        foreach ($values as $value){
            $v = $v + $value->fact;
        }
        return $v;
    }

}
