<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%values_manba_plan}}".
 *
 * @property int $id
 * @property int $values_plan_id
 * @property int $moliya_manbai_id
 * @property string $reja
 *
 * @property MoliyaManbai $moliyaManbai
 * @property ValuesPlan $valuesPlan
 */
class ValuesManbaPlan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%values_manba_plan}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['values_plan_id'], 'required'],
            [['values_plan_id', 'moliya_manbai_id'], 'integer'],
            [['reja'], 'string', 'max' => 45],
            [['moliya_manbai_id'], 'exist', 'skipOnError' => true, 'targetClass' => MoliyaManbai::className(), 'targetAttribute' => ['moliya_manbai_id' => 'id']],
            [['values_plan_id'], 'exist', 'skipOnError' => true, 'targetClass' => ValuesPlan::className(), 'targetAttribute' => ['values_plan_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'values_plan_id' => Yii::t('app', 'Values Plan ID'),
            'moliya_manbai_id' => Yii::t('app', 'Moliya Manbai ID'),
            'reja' => Yii::t('app', 'Reja'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMoliyaManbai()
    {
        return $this->hasOne(MoliyaManbai::className(), ['id' => 'moliya_manbai_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValuesPlan()
    {
        return $this->hasOne(ValuesPlan::className(), ['id' => 'values_plan_id']);
    }
}
