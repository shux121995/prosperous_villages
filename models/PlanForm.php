<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Plan form
 */
/**
 * This is the model.
 *
 * @property int $id
 * @property int $values_plan_id
 * @property int $moliya_manbai_id
 * @property float $reja
 * @property int $is_fill
 * @property int $inputs_id
 * @property int $section_id
 *
 */

class PlanForm extends Model
{
    public $id;
    public $reja;
    public $moliya_manbai_id;
    public $values_plan_id;
    public $is_fill;
    public $inputs_id;
    public $section_id;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'moliya_manbai_id', 'values_plan_id', 'is_fill', 'section_id'], 'integer'],
            ['reja', 'number'],
            ['is_fill', 'boolean'],
            ['is_fill', 'checkIsfills'],
        ];
    }

    /**
     * Проверка остаток в балансе
     * @return bool
     */
    public function checkIsfills()
    {
        if ($this->is_fill && empty($this->reja)) {
            $this->addError('reja', Yii::t('app', 'Танланган манба учун режани киритиш шарт!'));
            return false;
        }
        return true;
    }

    /**
     * Сохранить ил обновить данны
     */
    public function addPlan($plan_id, $section_id, $force = false)
    {
        /** @var $valuesPlan ValuesPlan*/
        $valuesPlan = ValuesPlan::find()->where(['plan_id' => $plan_id, 'section_id' => $section_id])->one();
        if($valuesPlan==null){
            $valuesPlan = new ValuesPlan();
            $valuesPlan->plan_id = $plan_id;
            $valuesPlan->section_id = $section_id;
            $valuesPlan->save(false);
        }

        //проверим на наличие добавленный
        $exist = ValuesManbaPlan::findOne($this->id);
        if($exist!=null){
            $model = $exist;
            //Если пустое но ранный добавленный манба будут удален
            if(empty($this->reja)) {
                $exist->delete();
                return true;
            }
        }else{
            $model = new ValuesManbaPlan();
            $model->values_plan_id = $valuesPlan->id;
            $model->moliya_manbai_id = $this->moliya_manbai_id;
        }
        $model->reja = $this->reja;
        //Пропустим если поля пустое
        if(empty($model->reja)) {
            return true;
        }
        //Добавляем ил обновляем
        if($model->save()){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Update reja values after adding plan manba
     * @param $plan_id
     * @param $section_id
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function calcPlan($plan_id, $section_id)
    {
        /** @var $valuesPlan ValuesPlan*/
        $valuesPlan = ValuesPlan::find()->where(['plan_id' => $plan_id, 'section_id' => $section_id])->one();
        if($valuesPlan!=null){
            $valuesPlan->reja = $valuesPlan->getCalc();
            $valuesPlan->update(false);
        }
    }
}
