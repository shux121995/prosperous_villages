<?php


namespace app\models;


use yii\db\ActiveRecord;

class WbStatus extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%wb_status}}';
    }


    public function getTitleLang($lang)
    {
        if($lang!='uz'){
            $lang = 'title_'.$lang;
            return !empty($this->$lang)?$this->$lang:$this->title;
        }else{
            return $this->title;
        }
    }
}