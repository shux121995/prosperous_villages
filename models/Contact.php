<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;

/**
 * Contact is the model behind the contact form.
 */

class Contact extends ActiveRecord {
    public $reCaptcha; //<-- Model

    public static function tableName()
    {
        return 'contact';
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            ///*SHUKHRAT, MAYBE I WILL INCLUDE IT LATER, JUST DON'T FORGET WHAT IT STANDS FOR*/
            //[['name', 'email', 'subject', 'body'], 'required'],
            ['name', 'required', 'message' => Yii::t('app','"Full Name" is required')],
            ['email', 'required', 'message' => Yii::t('app','"Email" is required')],
            ['subject', 'required', 'message' => Yii::t('app','"Phone" is required')],
            ['body', 'required', 'message' => Yii::t('app','"Message" is required')],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
            //['verifyCode', 'captcha'],
            [['reCaptcha'], \himiklab\yii2\recaptcha\ReCaptchaValidator::className(),
                'secret' => '6Ld3Oc8UAAAAAIxsIaG9lT20HPWlJr6Git3bPgFO', // unnecessary if reСaptcha is already configured
                'uncheckedMessage' => 'Please confirm that you are not a bot.'],
            [['name'],'string', 'max' => 50],
            [['email'], 'string', 'max' => 50],
            [['subject'], 'string', 'max' => 50],
            [['body'], 'string', 'max' => 255],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Verification Code',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function contact($email)
    {
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom([$this->email => $this->name])
                ->setSubject($this->subject)
                ->setTextBody($this->body)
                ->send();

            return true;
        }
        return false;
    }

}
