<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%table_plan_end}}".
 *
 * @property int $id
 * @property int $plan_id
 * @property int $table_id
 * @property int $status
 *
 * @property LocalityPlans $plan
 * @property Tables $table
 */
class TablePlanEnd extends \yii\db\ActiveRecord
{

    const STATUS_OPEN  = null;
    const STATUS_ACTIVE  = 1;
    const STATUS_NO_PLAN  = 2;

    public static function statusLabels()
    {
        return [
            self::STATUS_OPEN => 'Open',
            self::STATUS_ACTIVE => 'Activeted',
            self::STATUS_NO_PLAN => 'No plan',
        ];
    }

    public function getStatusLabel()
    {
        return isset(self::statusLabels()[$this->status])?self::statusLabels()[$this->status]:null;
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%table_plan_end}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['plan_id', 'table_id'], 'required'],
            [['plan_id', 'table_id'], 'integer'],
            [['status'], 'integer'],
            [['plan_id'], 'exist', 'skipOnError' => true, 'targetClass' => LocalityPlans::className(), 'targetAttribute' => ['plan_id' => 'id']],
            [['table_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tables::className(), 'targetAttribute' => ['table_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'plan_id' => Yii::t('app', 'Plan ID'),
            'table_id' => Yii::t('app', 'Table ID'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlan()
    {
        return $this->hasOne(LocalityPlans::className(), ['id' => 'plan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTable()
    {
        return $this->hasOne(Tables::className(), ['id' => 'table_id']);
    }
}
