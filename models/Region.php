<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%region}}".
 *
 * @property int $id
 * @property string $title
 * @property int $order
 * @property string $map_longitude
 * @property string $map_latitude
 * @property string $map_zoom
 *
 * @property District[] $districts
 */
class Region extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%region}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['order'], 'integer'],
            [['title', 'title_en'], 'string', 'max' => 128],
            [['map_longitude', 'map_latitude', 'map_zoom'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'title_en' => Yii::t('app', 'Title En'),
            'order' => Yii::t('app', 'Order'),
            'map_longitude' => Yii::t('app', 'Долгота (longitude)'),
            'map_latitude' => Yii::t('app', 'Широта (latitude)'),
            'map_zoom' => Yii::t('app', 'Zoom'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistricts()
    {
        return $this->hasMany(District::className(), ['region_id' => 'id']);
    }

    /**
     * @param $name
     * @return int
     */
    public function idByName($name)
    {
        $region = self::find()->where(['title' => $name])->one();
        if ($region == null) {
            $region = new Region();
            $region->title = $name;
            $region->save();
        }
        return $region->id;
    }

    public function getEmployee()
    {
        $emplys = UserAccess::find()->where(['region_id'=>$this->id])->all();
        $users = [];
        if($emplys!=null){
            /** @var $u UserAccess */

            foreach ($emplys as $u){
                $users[$u->id] = isset($u->user->username)?$u->user->username:null;
            }
        }
        return $users;
    }

    /**
     * @param $lang
     * @return mixed|string
     * Lang helper by lang title
     */
    public function getTitleLang($lang)
    {
        if($lang!='uz'){
            $lang = 'title_'.$lang;
            return !empty($this->$lang)?$this->$lang:$this->title;
        }else{
            return $this->title;
        }
    }
}
