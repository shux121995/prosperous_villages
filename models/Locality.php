<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%locality}}".
 *
 * @property int $id
 * @property int $district_id
 * @property string $title
 * @property string $title_en
 * @property string $description
 * @property string $type
 * @property int $sector_reg
 * @property int $sector_dis
 * @property string $map_longitude
 * @property string $map_latitude
 * @property string $map_zoom
 * @property int $order
 *
 * @property Aholi[] $aholis
 * @property District $district
 * @property LocalityPlans[] $localityPlans
 * @property WbPlans[] $wbPlans
 */
class Locality extends \yii\db\ActiveRecord
{
    const TYPE_QISHLOQ = 'qishloq';
    const TYPE_MAHALLA = 'mahalla';
    const TYPE_WB = 'wb';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%locality}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['district_id', 'sector_reg', 'sector_dis', 'order'], 'integer'],
            [['title'], 'required'],
            [['description', 'type'], 'string'],
            [['map_longitude', 'map_latitude', 'map_zoom'], 'string', 'max' => 50],
            [['title', 'title_en'], 'string', 'max' => 128],
            [['district_id'], 'exist', 'skipOnError' => true, 'targetClass' => District::className(), 'targetAttribute' => ['district_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'district_id' => Yii::t('app', 'District ID'),
            'title' => Yii::t('app', 'Title'),
            'title_en' => Yii::t('app', 'Title En'),
            'description' => Yii::t('app', 'Description'),
            'type' => Yii::t('app', 'Type'),
            'sector_reg' => Yii::t('app', 'Sector region'),
            'sector_dis' => Yii::t('app', 'Sector district'),
            'map_longitude' => Yii::t('app', 'Долгота (longitude)'),
            'map_latitude' => Yii::t('app', 'Широта (latitude)'),
            'map_zoom' => Yii::t('app', 'Zoom'),
            'order' => Yii::t('app', 'order'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistrict()
    {
        return $this->hasOne(District::className(), ['id' => 'district_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocalityPlans()
    {
        return $this->hasMany(LocalityPlans::className(), ['locality_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAholis()
    {
        return $this->hasMany(Aholi::className(), ['locality_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWbPlans()
    {
        return $this->hasMany(WbPlans::className(), ['locality_id' => 'id']);
    }

    /**
     * @param $lang
     * @return mixed|string
     * Lang helper by lang title
     */
    public function getTitleLang($lang)
    {
        if($lang!='uz'){
            $lang = 'title_'.$lang;
            return !empty($this->$lang)?$this->$lang:$this->title;
        }else{
            return $this->title;
        }
    }

    /**
     * @return array
     */
    public static function typeLables()
    {
        return [
            self::TYPE_QISHLOQ => 'Обод қишлоқ',
            self::TYPE_MAHALLA => 'Обод маҳалла',
            self::TYPE_WB => 'Жаҳон банки',
        ];
    }

}
