<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%sections}}".
 *
 * @property int $id
 * @property string $title
 * @property int $tables_id
 * @property int $is_group Группировать поле только на этом
 * @property int $parent_id
 * @property int $forms_id
 * @property int $level
 * @property int $cols
 * @property int $cols_input colspan with inputs
 * @property int $rows
 *
 * @property Sections $parent
 * @property Sections[] $sections
 * @property Forms $forms
 * @property Tables $tables
 * @property ValuesPlan[] $valuesPlans
 */
class Sections extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%sections}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tables_id', 'is_group', 'parent_id', 'forms_id', 'level', 'cols', 'cols_input', 'rows'], 'integer'],
            [['title', 'title_en'], 'string', 'max' => 255],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sections::className(), 'targetAttribute' => ['parent_id' => 'id']],
            [['forms_id'], 'exist', 'skipOnError' => true, 'targetClass' => Forms::className(), 'targetAttribute' => ['forms_id' => 'id']],
            [['tables_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tables::className(), 'targetAttribute' => ['tables_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'title_en' => Yii::t('app', 'Title En'),
            'tables_id' => Yii::t('app', 'Tables ID'),
            'is_group' => Yii::t('app', 'Группировать поле только на этом'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'forms_id' => Yii::t('app', 'Forms ID'),
            'level' => Yii::t('app', 'Level'),
            'cols' => Yii::t('app', 'Columns'),
            'cols_input' => Yii::t('app', 'Columns with inputs'),
            'rows' => Yii::t('app', 'Row span'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Sections::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSections()
    {
        return $this->hasMany(Sections::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForms()
    {
        return $this->hasOne(Forms::className(), ['id' => 'forms_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTables()
    {
        return $this->hasOne(Tables::className(), ['id' => 'tables_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValuesPlans()
    {
        return $this->hasMany(ValuesPlan::className(), ['section_id' => 'id']);
    }

    /**
     * @param $lang
     * @return mixed|string
     * Lang helper by lang title
     */
    public function getTitleLang($lang)
    {
        if($lang!='uz'){
            $lang = 'title_'.$lang;
            return !empty($this->$lang)?$this->$lang:$this->title;
        }else{
            return $this->title;
        }
    }

}
