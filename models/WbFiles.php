<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%wb_files}}".
 *
 * @property int $id
 * @property int $project_id
 * @property string $filename
 * @property int $type до 1 и после 2
 * @property string $path
 * @property string $extension
 * @property double $filesize
 *
 * @property WbProjects $project
 */
class WbFiles extends \yii\db\ActiveRecord
{
    const TYPE_BEFORE = 1;
    const TYPE_AFTER = 2;

    public static function typeLabels()
    {
        return [
            self::TYPE_BEFORE => Yii::t('app','Before'),
            self::TYPE_AFTER => Yii::t('app','After'),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%wb_files}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['project_id', 'type'], 'integer'],
            [['filename'], 'required'],
            [['filesize'], 'number'],
            [['filename'], 'string', 'max' => 255],
            [['path'], 'string', 'max' => 512],
            [['extension'], 'string', 'max' => 5],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => WbProjects::className(), 'targetAttribute' => ['project_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'project_id' => Yii::t('app', 'Project ID'),
            'filename' => Yii::t('app', 'Filename'),
            'type' => Yii::t('app', 'до 1 и после 2'),
            'path' => Yii::t('app', 'Path'),
            'extension' => Yii::t('app', 'Extension'),
            'filesize' => Yii::t('app', 'Filesize'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(WbProjects::className(), ['id' => 'project_id']);
    }
}
