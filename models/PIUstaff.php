<?php


namespace app\models;


use yii\db\ActiveRecord;

class PIUstaff extends ActiveRecord
{
    public static function tableName()
    {
        return 'piu_staff';
    }

    public function rules()
    {
        return [
            [['full_name'], 'required'],
            [['position'], 'string'],
            [['position_uz'], 'string'],
            [['cv', 'cv_uz'], 'string', 'max' => 1023],
            [['email'], 'string', 'max' => 255],
            [['photo'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, jpeg'],
        ];
    }

    public function getCV($lang)
    {
        if ($lang == 'uz') {
            return $this->cv_uz;
        } else {
            return $this->cv;
        }
    }
    public function getPosition($lang)
    {
        if ($lang == 'uz') {
            return $this->position_uz;
        } else {
            return $this->position;
        }
    }
}