<?php

namespace app\controllers;

use Yii;
use app\models\BozorInfratuzilmasi;
use app\models\LocalityPlans;
use app\models\ValuesPlan;
use app\models\MoliyaManbai;
use app\models\ValuesManbaPlan;
use app\models\search\BozorInfratuzilmasiSearch;
use app\models\search\PlansSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BozorInfratuzilmasiController implements the CRUD actions for BozorInfratuzilmasi model.
 */
class BozorInfratuzilmasiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BozorInfratuzilmasi models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PlansSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BozorInfratuzilmasi model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionConstructionObjects($plan_id)
    {
        $plan = LocalityPlans::findOne($plan_id);
        if ($plan->locality->district->region_id != Yii::$app->access->getRegionId()) return 'error auth!';
        $searchModel = new BozorInfratuzilmasiSearch();
        $dataProvider = $searchModel->searchByPlanId(Yii::$app->request->queryParams, $plan_id);
        $page = '';
        $per_page = '';
        if(Yii::$app->request->get('page')!=null) {
            $page = Yii::$app->request->get('page');
        }
        if(Yii::$app->request->get('per-page') != null) {
            $per_page = Yii::$app->request->get('per-page');
        }
        return $this->render('construction_objects',[
            'plan'=>$plan,
            'dataProvider' => $dataProvider,
            'page' => $page,
            'per_page' => $per_page,
        ]);
    }

    public function actionFill($plan_id, $id='', $page='', $per_page='')
    {
        //Check auth&access for adding
        $plan = LocalityPlans::findOne($plan_id);
        if ($plan->locality->district->region_id != Yii::$app->access->getRegionId()) return 'error auth!';

        if(!empty($id)) {
            $values_plan = ValuesPlan::find()->where(['bozor_id'=>$id])->one();
            $model = BozorInfratuzilmasi::findOne($id);
        }
        else {
            $model = new BozorInfratuzilmasi();
            $values_plan = new ValuesPlan();
        }

        if(!empty($page) and !empty($per_page)) {
            $pagination_url = '&page='.$page.'&per-page='.$per_page;
        }
        else{
            $pagination_url = '';
        }
        if($model->status == 0 or $model->status == null) {
            $viewFile = 'fill';
        }
        else {
            $viewFile = 'view_data';
        }
        if($model->load(Yii::$app->request->post())) {
            $msg = 'Режа муваффакиятли кушилди(узгартирилди)!';
            $model->plan_id = $plan_id;
            $model->save();
            if(empty($id)) {
                $id = $model->id;
            }
            $values_plan->plan_id = $plan_id;
            $values_plan->bozor_id = $model->id;
            if(isset($model->moliya_manbai)) {
                $values_plan->reja = '' . array_sum($model->moliya_manbai) . '';
            }
            $values_plan->save();
            if(isset($model->moliya_manbai)) {
                foreach($model->moliya_manbai as $key => $value) {
                    $values_manba_plan = ValuesManbaPlan::find()->where(['values_plan_id'=>$values_plan->id, 'moliya_manbai_id'=>$key])->all();

                    if($values_manba_plan != null) {
                        foreach($values_manba_plan as $item) {
                            $item->values_plan_id = $values_plan->id;
                            $item->moliya_manbai_id = $key;
                            $item->reja = $value;
                            $item->save();
                        }
                    }
                    else {
                        $values_manba_plan = new ValuesManbaPlan();
                        $values_manba_plan->values_plan_id = $values_plan->id;
                        $values_manba_plan->moliya_manbai_id = $key;
                        $values_manba_plan->reja = $value;
                        $values_manba_plan->save();
                    }
                }
            }
            if($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app', $msg));
                return $this->redirect('/bozor-infratuzilmasi/construction-objects?plan_id='.$plan_id.$pagination_url);
            }



        }
        if(Yii::$app->request->post('status')) {
            $status = Yii::$app->request->post('status');
            if(!$model->isNewRecord) {
                $model->status = $status;
                $model->save();
                $msg_status = 'success';
            }
            else {
                $msg = 'Ушбу объект бўйича маълумотлар киритилмаган!';
                $msg_status = 'danger';
            }
            if($status == 1 and $model->save()) {
                $msg = 'Ушбу режа муваффақиятли активлаштирилди!';
            }
            elseif($status == 2 and $model->save()) {
                $msg = 'Ушбу режага "Режа йўқ" статуси мувафаққиятли белгиланди!';
            }
            Yii::$app->session->setFlash($msg_status, Yii::t('app', $msg));
            return $this->redirect('/bozor-infratuzilmasi/construction-objects?plan_id='.$plan_id.$pagination_url);
        }
        $moliya_manbai = MoliyaManbai::find()->all();

        return $this->renderAjax($viewFile, [
            'model' => $model,
            'plan' => $plan,
            'moliya_manbai' => $moliya_manbai,
            'id' => $id,
        ]);
    }

}
