<?php

namespace app\controllers;

use yii\helpers\Url;
use Yii;

class YearController extends \yii\web\Controller
{
    public function actionIndex($year=null)
    {
        if($year==null){
            $_SESSION['year'] = date('Y');
        }else{
            $_SESSION['year'] = $year;
        }

        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }

}
