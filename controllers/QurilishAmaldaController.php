<?php

namespace app\controllers;



use Yii;
use app\models\QurulishAmalda;
use app\models\Qurulish;
use app\models\LocalityPlans;
use app\models\FillAccess;
use app\models\MoliyaManbai;
use app\models\ValuesFact;
use app\models\ValuesPlan;
use app\models\ValuesManbaFact;
use app\models\search\QurulishSearch;
use app\models\search\QurulishAmaldaSearch;
use app\models\search\PlansSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * QurilishAmaldaController implements the CRUD actions for QurulishAmalda model.
 */
class QurilishAmaldaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all QurulishAmalda models.
     * @return mixed
     */
    public function actionIndex()
    {
        $table = new QurulishAmalda();
        $searchModel = new PlansSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'table' => $table,
        ]);
    }

    public function actionConstructionObjects($plan_id)
    {
        $qurulish = new QurulishSearch();
        $plan = LocalityPlans::findOne($plan_id);
        if ($plan->locality->district->region_id != Yii::$app->access->getRegionId()) return 'error auth!';
        $dataProvider = $qurulish->searchByPlanId(Yii::$app->request->queryParams, $plan_id);

        $page = '';
        $per_page = '';
        if(Yii::$app->request->get('page')!=null) {
            $page = Yii::$app->request->get('page');
        }
        if(Yii::$app->request->get('per-page') != null) {
            $per_page = Yii::$app->request->get('per-page');
        }

        return $this->render('construction-objects',[
            'dataProvider' => $dataProvider,
            'plan' => $plan,
            'page' => $page,
            'per_page' => $per_page,
        ]);
    }

    public function actionFill($plan_id, $construction_object, $page='', $per_page='')
    {
        $plan = LocalityPlans::findOne($plan_id);
        $model = QurulishAmalda::find()->where(['qurulish_id'=>$construction_object])->one();
        $qurulish = Qurulish::findOne($construction_object);
        $values_plan = ValuesPlan::find()->where(['qurulish_id'=>$construction_object])->one();
        if($model == null) {
            $model = new QurulishAmalda();
        }
        $values_fact = ValuesFact::find()->where(['plan_value_id'=>$values_plan->id])->one();
        if($values_fact == null) {
            $values_fact = new ValuesFact();
        }
        if ($plan->locality->district->region_id != Yii::$app->access->getRegionId()) return 'error auth!';

        $access = FillAccess::getAccess();
        if($access==null) return $this->renderAjax('//layouts/_modal_alert', ['message' => 'Хозирча, факт киритиш учун имкон берилмаган!']);//Пропучтим потому что нет доступ к заполнение

        //for pagination url
        if(!empty($page) and !empty($per_page)) {
            $pagination_url = '&page='.$page.'&per-page='.$per_page;
        }
        else{
            $pagination_url = '';
        }

        if($qurulish->status == 1) {
            $viewFile = 'fill';
        }
        else {
            $viewFile = 'view_data';
        }
        if($model->load(Yii::$app->request->post())) {
            $model->qurulish_id = $construction_object;
            if($model->isNewRecord) {
                $model->created_at = time();
                $model->updated_at = time();
                $model->date = date('Y-m-d');
                $values_fact->created_at = time();
                $values_fact->updated_at = time();
            }
            else {
                $model->updated_at = time();
                $values_fact->updated_at = time();
            }
            $model->save();
            $values_fact->access_id = $access->id;
            $values_fact->post_date = $access->year.'-'.$access->month.'-'.'01';
            $values_fact->user_id = Yii::$app->user->getId();
            $values_fact->plan_value_id = $values_plan->id;
            if(isset($model->uzlashtirildi)) {
                $values_fact->fact = '' . array_sum($model->uzlashtirildi) . '';
            }
            $values_fact->save();
            if(isset($model->uzlashtirildi)) {
                foreach($model->uzlashtirildi as $key => $value) {
                    $values_manba_fact = ValuesManbaFact::find()->where(['values_fact_id'=>$values_fact->id, 'moliya_manba_id'=>$key])->all();

                    if($values_manba_fact != null) {
                        foreach($values_manba_fact as $item) {
                            $item->values_fact_id = $values_fact->id;
                            $item->moliya_manba_id = $key;
                            $item->fact = $value;
                            $item->updated_at = time();
                            $item->save();
                        }
                    }
                    else {
                        $values_manba_fact = new ValuesManbaFact();
                        $values_manba_fact->values_fact_id = $values_fact->id;
                        $values_manba_fact->moliya_manba_id = $key;
                        $values_manba_fact->fact = $value;
                        $values_manba_fact->created_at = time();
                        $values_manba_fact->updated_at = time();
                        $values_manba_fact->save();
                    }
                }
            }

            $msg = 'Факт кушилди(узгартирилди)!';
            if($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app', $msg));
                return $this->redirect('/qurilish-amalda/construction-objects?plan_id='.$plan->id.$pagination_url);
            }

        }
        $moliya_manbai = MoliyaManbai::find()->all();

        return $this->renderAjax($viewFile, [
            'model' => $model,
            'plan' => $plan,
            'qurulish' => $qurulish,
            'moliya_manbai' => $moliya_manbai,
        ]);
    }

}
