<?php

namespace app\controllers;

use app\models\FactForm;
use app\models\FillAccess;
use app\models\MoliyaManbai;
use app\models\Sections;
use app\models\ValuesManbaFact;
use Yii;
use app\models\ValuesPlan;
use app\models\ValuesFact;
use app\models\Tables;
use app\models\search\PlansSearch;
use app\models\LocalityPlans;
use app\models\TablePlanEnd;
use yii\base\Model;
use yii\helpers\Json;
use yii\web\Response;

class FactsController extends \yii\web\Controller
{


    public function actionIndex($table)
{
        $table = $this->findTable($table);
        $searchModel = new PlansSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'table' => $table,
        ]);
}

    public function findTable($id)
    {
        if (($model = Tables::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * @param $plan_id
     * @param $table_id
     * @return string
     */
    public function actionFill($plan_id, $table_id)
    {
        $table = $this->findTable($table_id);

        //Check auth&access for adding
        $plan = LocalityPlans::findOne($plan_id);
        if ($plan->locality->district->region_id != Yii::$app->access->getRegionId()) return 'error auth!';
        //if (Aholi::find()->where(['plan_id' => $plan_id])->exists()) return 'error access!';

        //validate & add
        if (Yii::$app->request->post('section')) {
            $postData = Yii::$app->request->post();
            foreach ($postData['section'] as $section_id => $data) {
                $model = ValuesFact::find()->where(['plan_value_id' => (int)$data['plan_value_id']])->one();
                if($model==null) {
                    $model = new ValuesFact();
                }

                $model->inputs_id = $data['input_id'];
                $model->fact = $data['fact'];
                $model->user_id = Yii::$app->user->identity->id;
                $model->plan_value_id = $data['plan_value_id'];
                if($model->isNewRecord){ //если это новое
                    $model->created_at = time();
                    $model->updated_at = time();
                    $model->post_date = date('Y-m-d');
                    if (!empty($model->fact) OR $model->fact != null OR $model->fact != 0) {
                        $model->save();
                    }
                }else{ //если это изменение
                    $model->updated_at = time();
                    if (!empty($model->fact) OR $model->fact != null OR $model->fact != 0) {
                        $model->update();
                    }else{
                        $model->delete();
                    }
                }
            }
            Yii::$app->session->setFlash('success', Yii::t('app', 'Факт муваффакиятли кушилди(узгартирилди)!'));
        }

        $plan_status = 1;
        $getStatus = TablePlanEnd::find()->where(['plan_id' => $plan_id, 'table_id' => $table_id])->one();
        //if($getStatus!=null) $plan_status = $getStatus->status;//allow for fill alltime

        return $this->render('fill', [
            'table' => $table,
            'plan' => $plan,
            'plan_status' => $plan_status,
        ]);
    }

    public function actionAdd($plan_id, $section_id, $change = false)
    {
        //Check auth&access for adding
        $plan = LocalityPlans::findOne($plan_id);
        if ($plan->locality->district->region_id != Yii::$app->access->getRegionId()) return $this->renderAjax('//layouts/_modal_alert', ['message' => 'error auth!']);

        $access = FillAccess::getAccess();
        if($access==null) return $this->renderAjax('//layouts/_modal_alert', ['message' => 'Хозирча, факт киритиш учун имкон берилмаган!']);//Пропучтим потому что нет доступ к заполнение

        $section = Sections::findOne($section_id);
        $plan_status = null;
        $getStatus = TablePlanEnd::find()->where(['plan_id' => $plan_id, 'table_id' => $section->tables_id])->one();
        if($getStatus!=null) $plan_status = $getStatus->status;
        /** @var $valuesPlan ValuesPlan*/
        $valuesPlan = ValuesPlan::find()->where(['plan_id' => $plan_id, 'section_id' => $section_id])->one();
        $valuesFact = ValuesFact::find()->where(['access_id'=>$access->id,'plan_value_id'=>$valuesPlan->id])->one();
        $moliyas = MoliyaManbai::find()->all();
        $models = [];
        //Generation multi models
        /** @var $moliya MoliyaManbai */
        foreach($moliyas as $moliya) {
            $m = new FactForm();
            $m->moliya_manbai_id = $moliya->id;
            if($valuesFact!=null){
                /** @var $manbaFact ValuesManbaFact */
                $manbaFact = ValuesManbaFact::find()->where(['values_fact_id'=>$valuesFact->id,'moliya_manba_id'=>$moliya->id])->one();
                if($manbaFact!=null){
                    $m->id = $manbaFact->id;
                    $m->fact = $manbaFact->fact;
                }
            }
            $models[$moliya->id] = $m;
        }

        //multi models validation&saving
        if (Model::loadMultiple($models, Yii::$app->request->post()) && Model::validateMultiple($models)) {
            $nofilled = true;
            foreach ($models as $model){
                if(!empty($model->fact)) $nofilled = false;
            }
            if($nofilled) return Json::encode(array('status' => 'error', 'type' => 'danger', 'message' => 'Факт кушилмади! Хеч булмаганда бирорта манба учун фактни киритиш шарт!'));

            $success = true;
            foreach ($models as $model) {
                if(!$model->addFact($plan_id,$section_id,false)) $success = false;
            }

            //Update plan value
            (new FactForm())->calcFact($plan_id,$section_id);

            if($success){
                Yii::$app->session->setFlash('success', Yii::t('app', 'Режа кушилди!'));
                return Json::encode(array('status' => 'success', 'type' => 'success', 'message' => 'Режа кушилди!'));
            }
            Yii::$app->session->setFlash('error', Yii::t('app', 'Режа кушилмади!'));
            return Json::encode(array('status' => 'error', 'type' => 'danger', 'message' => 'Режа кушилмади!'));
        }

        return $this->renderAjax('_add', ['models' => $models, 'plan'=>$plan, 'section'=>$section, 'plan_status'=>$plan_status]);

    }

    public function actionFillValidate($plan_id, $section_id = null, $change = false)
    {
        //Check auth&access for adding
        $plan = LocalityPlans::findOne($plan_id);
        if ($plan->locality->district->region_id != Yii::$app->access->getRegionId()) return 'error auth!';

        $moliyas = MoliyaManbai::find()->all();
        $models = [];
        foreach($moliyas as $moliya) {
            $m = new FactForm();
            $m->moliya_manbai_id = $moliya->id;
            $models[$moliya->id] = $m;
        }

        if (Yii::$app->request->isAjax && Model::loadMultiple($models, Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return Model::validateMultiple($models);
        }
    }
}