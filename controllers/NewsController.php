<?php

namespace app\controllers;

use app\controllers\HomeController as Controller;
use app\models\News;
use yii\data\Pagination;

class NewsController extends Controller
{
    public $layout = '//wb';
    public function actionView($alias = null)
    {
        $this->view->params['tn_url'] = '/news/view?alias='.$alias;
        $model = $this->loadModel($alias);
        return $this->render('view', [
            'model' => $model
        ]);
    }
    
    public function actionIndex()
    {
        $this->view->params['tn_url'] = '/news';
        $query = News::find()->where(['news_language'=>(new \app\components\LangHelper())->check_lang(),'news_show'=>1])->orderBy('news_date DESC');
        $count = $query->count();
        $pagination  = new Pagination(['totalCount' => $count, 'pageSize'=>9]);
        $models = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return $this->render('index', [
            'models' => $models,
            'pages' => $pagination,
        ]);
    }

    public function loadModel($alias)
    {
        if (($model = News::find()->where(['news_language'=>(new \app\components\LangHelper())->check_lang(), 'news_alias'=>$alias])->one()) !== null) {
            return $model;
        } else {
            throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
        }
    }
}
