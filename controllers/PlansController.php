<?php

namespace app\controllers;

use app\models\LocalityPlans;
use app\models\MoliyaManbai;
use app\models\MoliyaManbaiTable;
use app\models\PlanForm;
use app\models\search\PlansSearch;
use app\models\Sections;
use app\models\Tables;
use app\models\TablesExcel;
use app\models\ValuesManbaPlan;
use app\models\ValuesPlan;
use app\models\TablePlanEnd;
use app\modules\results\models\GenerateTable;
use Yii;
use yii\base\Model;
use yii\helpers\Json;
use yii\web\Response;
use yii\widgets\ActiveForm;

class PlansController extends \yii\web\Controller
{
    /**
     * Lists all LocalityPlans models.
     * @return mixed
     */
    public function actionIndex($table)
    {
        $table = $this->findTable($table);
        $searchModel = new PlansSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'table' => $table,
        ]);
    }

    /**
     * @param $id
     * @return Tables|null
     */
    public function findTable($id)
    {
        if (($model = Tables::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionAdd($plan_id, $section_id, $change = false)
    {
        //Check auth&access for adding
        $plan = LocalityPlans::findOne($plan_id);
        if ($plan->locality->district->region_id != Yii::$app->access->getRegionId()) return 'error auth!';

        $section = Sections::findOne($section_id);
        $plan_status = null;
        $getStatus = TablePlanEnd::find()->where(['plan_id' => $plan_id, 'table_id' => $section->tables_id])->one();
        if($getStatus!=null) $plan_status = $getStatus->status;
        /** @var $valuesPlan ValuesPlan*/
        $valuesPlan = ValuesPlan::find()->where(['plan_id' => $plan_id, 'section_id' => $section_id])->one();
        $moliyas = MoliyaManbai::find()->all();
        $models = [];
        //Generation multi models
        foreach($moliyas as $moliya) {
            $m = new PlanForm();
            $m->moliya_manbai_id = $moliya->id;
            if($valuesPlan!=null){
                /** @var $manbaPlan ValuesManbaPlan*/
                $manbaPlan = ValuesManbaPlan::find()->where(['values_plan_id'=>$valuesPlan->id,'moliya_manbai_id'=>$moliya->id])->one();
                if($manbaPlan!=null){
                    $m->id = $manbaPlan->id;
                    $m->reja = $manbaPlan->reja;
                }
            }
            $models[$moliya->id] = $m;
        }

        //multi models validation&saving
        if (Model::loadMultiple($models, Yii::$app->request->post()) && Model::validateMultiple($models)) {
            $nofilled = true;
            foreach ($models as $model){
                if(!empty($model->reja)) $nofilled = false;
            }
            if($nofilled) return Json::encode(array('status' => 'error', 'type' => 'danger', 'message' => 'Режа кушилмади! Хеч булмаганда бирорта манба учун режани киритиш шарт!'));

            $success = true;
            foreach ($models as $model) {
                if(!$model->addPlan($plan_id,$section_id,false)) $success = false;
            }

            //Update plan value
            (new PlanForm())->calcPlan($plan_id,$section_id);

            if($success){
                Yii::$app->session->setFlash('success', Yii::t('app', 'Режа кушилди!'));
                return Json::encode(array('status' => 'success', 'type' => 'success', 'message' => 'Режа кушилди!'));
            }
            Yii::$app->session->setFlash('error', Yii::t('app', 'Режа кушилмади!'));
            return Json::encode(array('status' => 'error', 'type' => 'danger', 'message' => 'Режа кушилмади!'));
        }

        return $this->renderAjax('_add', ['models' => $models, 'plan'=>$plan, 'section'=>$section, 'plan_status'=>$plan_status]);

    }

    public function actionFillValidate($plan_id, $section_id = null, $change = false)
    {
        //Check auth&access for adding
        $plan = LocalityPlans::findOne($plan_id);
        if ($plan->locality->district->region_id != Yii::$app->access->getRegionId()) return 'error auth!';

        $moliyas = MoliyaManbai::find()->all();
        $models = [];
        foreach($moliyas as $moliya) {
            $m = new PlanForm();
            $m->moliya_manbai_id = $moliya->id;
            $models[$moliya->id] = $m;
        }

        if (Yii::$app->request->isAjax && Model::loadMultiple($models, Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return Model::validateMultiple($models);
        }
    }

    /**
     * @param $plan_id
     * @param $table_id
     * @return string
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionFill($plan_id, $table_id)
    {
        $table = $this->findTable($table_id);

        //Check auth&access for adding
        $plan = LocalityPlans::findOne($plan_id);
        if ($plan->locality->district->region_id != Yii::$app->access->getRegionId()) return 'error auth!';
        //if (Aholi::find()->where(['plan_id' => $plan_id])->exists()) return 'error access!';

        $index = LocalityPlans::nextOrPrev($plan_id);
        $nextID = $index['next'];
        $disableNext = ($nextID===null)?'disabled':null;
        $prevID = $index['prev'];
        $disablePrev = ($prevID===null)?'disabled':null;

        //validate & add
        if (Yii::$app->request->post('section')) {
            $postData = Yii::$app->request->post();
            foreach ($postData['section'] as $section_id => $data) {
                $model = ValuesPlan::find()->where(['section_id' => (int)$section_id, 'plan_id' => (int)$plan_id])->one();
                if($model==null) {
                    $model = new ValuesPlan();
                    $model->section_id = (int)$section_id;
                    $model->plan_id = (int)$plan_id;
                }

                $model->inputs_id = $data['input_id'];

                //разделить проекти от плани
                if(isset($data['projects'])) {
                    $model->reja = 100000001;
                }else{
                    $model->reja = $data['reja'];
                }

                if(isset($data['moliya_manbai_id'])) $model->moliya_manbai_id = $data['moliya_manbai_id'];
                if(isset($data['molled'])) $model->molled = $data['molled'];
                if(isset($data['projects'])) $model->molled = $data['projects'];

                if($model->isNewRecord){ //если это новое
                    if (!empty($model->reja) OR $model->reja != null OR $model->reja != 0 OR isset($data['projects'])) {
                        $model->save();
                    }
                }else{ //если это изменение
                    if (!empty($model->reja) OR $model->reja != null OR $model->reja != 0 OR isset($data['projects'])) {
                        $model->update();
                    }else{
                        $model->delete();
                    }
                }
            }
            Yii::$app->session->setFlash('success', Yii::t('app', 'Режа муваффакиятли кушилди(узгартирилди)!'));
        }
        if(Yii::$app->request->post('TablePlanEnd')) {
            $TablePlanEnd = new TablePlanEnd;
            $post_data = Yii::$app->request->post('TablePlanEnd');
            $TablePlanEnd->plan_id = $post_data['plan_id'];
            $TablePlanEnd->table_id = $post_data['table_id'];
            $TablePlanEnd->status = $post_data['status'];
            $TablePlanEnd->save();
            if($TablePlanEnd->status == 1) {
                $msg = 'Ушбу режа муваффақиятли активлаштирилди!';
            }
            elseif($TablePlanEnd->status == 2) {
                $msg = 'Ушбу режага "Режа йўқ" статуси мувафаққиятли белгиланди!';
            }
            Yii::$app->session->setFlash('success', Yii::t('app', $msg));
        }

        $plan_status = null;
        $getStatus = TablePlanEnd::find()->where(['plan_id' => $plan_id, 'table_id' => $table_id])->one();
        if($getStatus!=null) $plan_status = $getStatus->status;

        $relatedManbalar = MoliyaManbaiTable::find()->select('manba_id')->where(['table_id'=>$table_id]);
        $locals = MoliyaManbai::find()->where(['in','id',$relatedManbalar])->all();
        if($locals==null) $locals = MoliyaManbai::find()->all();
        $models = [];

        //forms Generation multi models
        /** @var $local MoliyaManbai */
        foreach ($locals as $local){
            $excels = GenerateTable::getExcelInputs($table->id,null,\app\models\TablesExcel::TYPE_ONLY_SECTION);
            /** @var $excel \app\models\TablesExcel*/
            foreach ($excels as $excel) {
                /** @var $valuesPlan ValuesPlan*/
                $valuesPlan = ValuesPlan::find()->where(['plan_id' => $plan_id, 'section_id' => $excel->section_id])->one();

                $m = new PlanForm();
                $m->moliya_manbai_id = $local->id;
                $m->section_id = $excel->section_id;
                if($valuesPlan!=null){
                    /** @var $manbaPlan ValuesManbaPlan*/
                    $manbaPlan = ValuesManbaPlan::find()->where(['values_plan_id'=>$valuesPlan->id,'moliya_manbai_id'=>$local->id])->one();
                    if($manbaPlan!=null){
                        $m->id = $manbaPlan->id;
                        $m->reja = $manbaPlan->reja;
                    }
                }
                $models[$excel->section_id.'_'.$local->id] = $m;
            }
        }

        return $this->render('fill', [
            'table' => $table,
            'plan' => $plan,
            'plan_status' => $plan_status,
            'header'    => GenerateTable::genTable($table->id,TablesExcel::TYPE_ONLY_SECTION, 'Манба номи'),
            'locals'    => $locals,
            'models'    => $models,

            'nextID'=>$nextID,
            'prevID'=>$prevID,
            'disableNext'=>$disableNext,
            'disablePrev'=>$disablePrev,
        ]);
    }

    public function actionAddNew($plan_id, $table_id, $change = false)
    {
        $table = $this->findTable($table_id);
        //Check auth&access for adding
        $plan = LocalityPlans::findOne($plan_id);
        if ($plan->locality->district->region_id != Yii::$app->access->getRegionId()) return 'error auth!';

        $plan_status = null;
        $getStatus = TablePlanEnd::find()->where(['plan_id' => $plan_id, 'table_id' => $table_id])->one();
        if($getStatus!=null) $plan_status = $getStatus->status;

        $relatedManbalar = MoliyaManbaiTable::find()->select('manba_id')->where(['table_id'=>$table_id]);
        $locals = MoliyaManbai::find()->where(['in','id',$relatedManbalar])->all();
        if($locals==null) $locals = MoliyaManbai::find()->all();
        $models = [];

        //forms Generation multi models
        /** @var $local MoliyaManbai */
        foreach ($locals as $local){
            $excels = GenerateTable::getExcelInputs($table->id,null,\app\models\TablesExcel::TYPE_ONLY_SECTION);
            /** @var $excel \app\models\TablesExcel*/
            foreach ($excels as $excel) {
                $m = new PlanForm();
                $m->moliya_manbai_id = $local->id;
                $m->section_id = $excel->section_id;
                $models[$excel->section_id.'_'.$local->id] = $m;
            }
        }

        //multi models validation&saving
        if (Model::loadMultiple($models, Yii::$app->request->post()) && Model::validateMultiple($models)) {
            $nofilled = true;
            foreach ($models as $model){
                if(!empty($model->reja)) $nofilled = false;
            }
            if($nofilled) return Json::encode(array('status' => 'error', 'type' => 'danger', 'message' => 'Режа кушилмади! Хеч булмаганда бирорта манба учун режани киритиш шарт!'));

            $success = true;
            foreach ($models as $model) {
                if(!$model->addPlan($plan_id,$model['section_id'],false)) $success = false;
                //Update plan value
                (new PlanForm())->calcPlan($plan_id,$model['section_id']);
            }

            if($success){
                Yii::$app->session->setFlash('success', Yii::t('app', 'Режа кушилди!'));
                return Json::encode(array('status' => 'success', 'type' => 'success', 'message' => 'Режа кушилди!'));
            }
            Yii::$app->session->setFlash('error', Yii::t('app', 'Режа кушилмади!'));
            return Json::encode(array('status' => 'error', 'type' => 'danger', 'message' => 'Режа кушилмади!'));
        }
        return 'Uuups! Error!';
    }

    /**
     * @param $plan_id
     * @param $table_id
     * @return string
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionFillOld($plan_id, $table_id)
    {
        $table = $this->findTable($table_id);

        //Check auth&access for adding
        $plan = LocalityPlans::findOne($plan_id);
        if ($plan->locality->district->region_id != Yii::$app->access->getRegionId()) return 'error auth!';
        //if (Aholi::find()->where(['plan_id' => $plan_id])->exists()) return 'error access!';

        //validate & add
        if (Yii::$app->request->post('section')) {
            $postData = Yii::$app->request->post();
            foreach ($postData['section'] as $section_id => $data) {
                $model = ValuesPlan::find()->where(['section_id' => (int)$section_id, 'plan_id' => (int)$plan_id])->one();
                if($model==null) {
                    $model = new ValuesPlan();
                    $model->section_id = (int)$section_id;
                    $model->plan_id = (int)$plan_id;
                }

                $model->inputs_id = $data['input_id'];

                //разделить проекти от плани
                if(isset($data['projects'])) {
                    $model->reja = 100000001;
                }else{
                    $model->reja = $data['reja'];
                }

                if(isset($data['moliya_manbai_id'])) $model->moliya_manbai_id = $data['moliya_manbai_id'];
                if(isset($data['molled'])) $model->molled = $data['molled'];
                if(isset($data['projects'])) $model->molled = $data['projects'];

                if($model->isNewRecord){ //если это новое
                    if (!empty($model->reja) OR $model->reja != null OR $model->reja != 0 OR isset($data['projects'])) {
                        $model->save();
                    }
                }else{ //если это изменение
                    if (!empty($model->reja) OR $model->reja != null OR $model->reja != 0 OR isset($data['projects'])) {
                        $model->update();
                    }else{
                        $model->delete();
                    }
                }
            }
            Yii::$app->session->setFlash('success', Yii::t('app', 'Режа муваффакиятли кушилди(узгартирилди)!'));
        }
        if(Yii::$app->request->post('TablePlanEnd')) {
            $TablePlanEnd = new TablePlanEnd;
            $post_data = Yii::$app->request->post('TablePlanEnd');
            $TablePlanEnd->plan_id = $post_data['plan_id'];
            $TablePlanEnd->table_id = $post_data['table_id'];
            $TablePlanEnd->status = $post_data['status'];
            $TablePlanEnd->save();
            if($TablePlanEnd->status == 1) {
                $msg = 'Ушбу режа муваффақиятли активлаштирилди!';
            }
            elseif($TablePlanEnd->status == 2) {
                $msg = 'Ушбу режага "Режа йўқ" статуси мувафаққиятли белгиланди!';
            }
            Yii::$app->session->setFlash('success', Yii::t('app', $msg));
        }

        $plan_status = null;
        $getStatus = TablePlanEnd::find()->where(['plan_id' => $plan_id, 'table_id' => $table_id])->one();
        if($getStatus!=null) $plan_status = $getStatus->status;

        return $this->render('fill-old', [
            'table' => $table,
            'plan' => $plan,
            'plan_status' => $plan_status,
        ]);
    }
}
