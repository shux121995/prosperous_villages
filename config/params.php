<?php

return [
    'adminEmail' => 'shux121995@gmail.com',
    'years' => [
        '2020' => 2020,
        '2021' => 2021,
        '2022' => 2022,
        '2023' => 2023,
        '2024' => 2024,
    ],
    'langs' => [
        'uz' => 'Узбекча',
        'ru' => 'Русские',
        'en' => 'English',
    ],
    'map_api'=>'AIzaSyBXMYbORhUC25_JG9HBIbUjcgaKGqHKJ80',//demo
    'map_api_real'=>'AIzaSyD_MaXFH_GoPDb7AT4CkFfMXiUvREVVuSI',//real
    'ymap_api_key'=>'e7984134-c0a5-42b8-b88c-3f3bfe4be053',//real
];
