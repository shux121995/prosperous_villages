<?php
$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';
require __DIR__ . '/../components/LangHelper.php';
$lang = (new \app\components\LangHelper())->check_lang();

if(!isset($_SESSION['year'])) $_SESSION['year'] = date('Y');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => $lang.'-'.strtoupper($lang), // <- здесь!
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'modules' => [
        'wbadmin' => [
            'class' => 'app\modules\wbadmin\Module',
        ],
        'wbmoderator' => [
            'class' => 'app\modules\wbmoderator\Module',
        ],
        'wbregion' => [
            'class' => 'app\modules\wbregion\Module',
        ],
        'admin' => [
            'class' => 'app\modules\admin\Module',
        ],
        'results' => [
            'class' => 'app\modules\results\Module',
        ],
        'wbresults' => [
            'class' => 'app\modules\wbresults\Module',
        ],
        'manage' => [
            'class' => 'app\modules\manage\Module',
        ],
        'wb' => [
            'class' => 'app\modules\wb\Module',
        ],
        'local' => [
            'class' => 'app\modules\local\Module',
        ],
        'gridview' => ['class' => 'kartik\grid\Module'],
        'pages' => [
            'class' => 'bupy7\pages\Module',
            'controllerMap' => [
                'manager' => [
                    'class' => 'bupy7\pages\controllers\ManagerController',
                    'as access' => [
                        'class' => yii\filters\AccessControl::className(),
                        'ruleConfig' => [
                            'class' => yii\filters\AccessRule::className(),
                        ],
                        'rules' => [
                            [
                                'allow' => true,
                                'roles' => ['admin'],
                            ],
                        ],
                    ],
                    'layout' => '//admin',
                ],
                'default' => [
                    'class' => 'app\controllers\DefaultController',
                    /*'on beforeAction' => function($event) {
                            // see content of $event
                            $event->action->controller->layout = '/contacts';
                    }*/
                ],
            ],
            'layout' => '//wb',
            'imperaviLanguage' => 'ru',
            'tableName' => '{{%article}}',
            'pathToImages' => '@webroot/images',
            'urlToImages' => '@web/images',
            'pathToFiles' => '@webroot/files',
            'urlToFiles' => '@web/files',
            'uploadImage' => true,
            'uploadFile' => true,
            'addImage' => true,
            'addFile' => true,
        ]
    ],
    'components' => [
        'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => 'himiklab\yii2\recaptcha\ReCaptchaConfig',
            'siteKeyV2' => '6LflPbgUAAAAAP0onbwkLGvfrgY05QYwzn-rG3e0',
            'secretV2' => '6LflPbgUAAAAAHpAgj7zxuB2wz2-I2mA-ep0D1Po',
            'siteKeyV3' => '6Le3PbgUAAAAAInnQkzFH-0ZDc4DuwCvWpEB6toh',
            'secretV3' => '6Le3PbgUAAAAALEfKWJPjJh-UjIhbvMGsm52moky',
            //'siteKey' => '6LflPbgUAAAAAP0onbwkLGvfrgY05QYwzn-rG3e0',
            //'secret' => '6LflPbgUAAAAAHpAgj7zxuB2wz2-I2mA-ep0D1Po',
            'siteKey' => '6Ld3Oc8UAAAAALM5-hdAc-MdWRFZ4-tbjMDPJqul',
            'secret' => '6Ld3Oc8UAAAAAIxsIaG9lT20HPWlJr6Git3bPgFO',
        ],
        'firebase' => [
            'class'=>'grptx\Firebase\Firebase',
            'credential_file'=>'wb/firebase_authentication_key/prosperous-villages-proj-2cb49-b1c382fb30ee.json',
            'database_uri'=>'https://prosperous-villages-proj-2cb49.firebaseio.com', // (optional)
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages', // if advanced application, set @frontend/messages
                    'sourceLanguage' => 'uz-UZ',
                    'fileMap' => [
                        'app' => 'app.php',
                        'news' => 'news.php',
                    ],
                ],
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\PhpManager',
            'defaultRoles' => ['admin', 'author', 'region', 'viewer'],
        ],
        'access' => [
            'class'=>'app\models\UserAccess',
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'pHojLIC21Bd3CwCBRRCthu4Pvu34HwLl',
            'baseUrl'=>'',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@app/mail',
            'useFileTransport' => true,//set this property to false to send mails to real email addresses
            //comment the following array to send mail using php's mail function
//            'transport' => [
//                'class' => 'Swift_SmtpTransport',
//                'host' => 'obodqishloq.geekart.club',
//                'username' => 'nazarov7mu',
//                'password' => 'password',
//                'port' => '587',
//                'encryption' => 'tls',
//            ],
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            //'useFileTransport' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'http://local.obod.loc' => 'local/default/index',
                'http://local.obodqishloq.uz' => 'local/default/index',
                'http://local.obod.loc/<language:(ru|uz|en)>' => 'local/default/index',
                'http://local.obodqishloq.uz/<language:(ru|uz|en)>' => 'local/default/index',

                'devadd' => 'wbadmin/developers/devadd',
                'devaddsubmit' => 'wbadmin/developers/devaddsubmit',

                'view' => 'wb/default/view',
                '/pages/manager' => 'pages/manager/index',
                '<language:(ru|uz|en)>/pages/manager' => 'pages/manager/index',

                '/pages/<page:[\w-]+>' => 'pages/default/index',
                '<language:(ru|uz|en)>/pages/<page:[\w-]+>' => 'pages/default/index',
                '<language:(ru|uz|en)>/view' => 'wb/default/view',

                '<language:(ru|uz|en)>/<module>' => '<module>',
                '<language:(ru|uz|en)>/<module>/<controller>' => '<module>/<controller>',
                '<language:(ru|uz|en)>/<module>/<controller>/<action>' => '<module>/<controller>/<action>',
                '<language:(ru|uz|en)>/<module>/<controller>/<action>/<id:\d+>' => '<module>/<controller>/<action>',

                '<language:(ru|uz|en)>/<controller>' => '<controller>',
                '<language:(ru|uz|en)>/<controller>/<action>' => '<controller>/<action>',
                '<language:(ru|uz|en)>/<controller>/<action>/<id:\d+>' => '<controller>/<action>',

                '<language:(ru|uz|en)>/' => 'wb/default/index',

                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',

                ],
        ],
        'lang' => [
            'class' => 'app\components\LangHelper',
        ],
    ],
    'defaultRoute' => 'wb',
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
