<?php

use yii\db\Migration;

/**
 * Class m190610_073818_alter_sections_cols_input
 */
class m190610_073818_alter_sections_cols_input extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE {{%sections}}
	ADD COLUMN `cols_input` INT(11) NULL DEFAULT NULL AFTER `cols`;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190610_073818_alter_sections_cols_input cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190610_073818_alter_sections_cols_input cannot be reverted.\n";

        return false;
    }
    */
}
