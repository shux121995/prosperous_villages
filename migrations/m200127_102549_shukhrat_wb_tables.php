<?php

use yii\db\Migration;

/**
 * Class m200127_102546_shukhrat_wb_tables
 */
class m200127_102549_shukhrat_wb_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $sql = "
        -- -----------------------------------------------------
        -- Table {{%community_progress_categories}}
        -- -----------------------------------------------------
        CREATE TABLE IF NOT EXISTS community_progress_categories (
        id int NOT NULL AUTO_INCREMENT,
        dpcs_and_village_selection VARCHAR(255) NOT NULL,
        outreach_and_project_introduction VARCHAR(255) NOT NULL,
        mpcs VARCHAR(255) NOT NULL,
        socio_economic_and_resource_analysis VARCHAR(255) NOT NULL,
        planning_discussions_selection VARCHAR(255) NOT NULL,
        subproject_implementation VARCHAR(255) NOT NULL,
        institutional_strengthening VARCHAR(255) NOT NULL,
        monitoring VARCHAR(255) NOT NULL,
        locality_id int,
        PRIMARY KEY (id),
        FOREIGN KEY (locality_id) REFERENCES locality(id));
        
        -- -----------------------------------------------------
        -- Table {{%contact}}
        -- -----------------------------------------------------
        CREATE TABLE IF NOT EXISTS contact (
        id int NOT NULL AUTO_INCREMENT,
        name VARCHAR(50) NOT NULL,
        email VARCHAR(50) NOT NULL,
        subject VARCHAR(50) NOT NULL,        
        body VARCHAR(255) NOT NULL,
        PRIMARY KEY (id));
        
        -- -----------------------------------------------------
        -- Table {{%feedback}}
        -- -----------------------------------------------------
        ALTER TABLE feedback ADD COLUMN IF NOT EXISTS anonymous TINYINT(1) NOT NULL;
        ALTER TABLE feedback ADD COLUMN IF NOT EXISTS region VARCHAR(50) NOT NULL;
        ALTER TABLE feedback ADD COLUMN IF NOT EXISTS district VARCHAR(50) NOT NULL;
        ALTER TABLE feedback ADD COLUMN IF NOT EXISTS locality VARCHAR(50) NOT NULL;
        
        -- -----------------------------------------------------
        -- Table {{%piu_staff}}
        -- -----------------------------------------------------
        CREATE TABLE IF NOT EXISTS piu_staff (
        id int NOT NULL AUTO_INCREMENT,
        full_name VARCHAR(50) NOT NULL,
        position VARCHAR(50) NOT NULL,
        position_uz VARCHAR(50) NOT NULL,
        cv VARCHAR(1000) NOT NULL,
        cv_uz VARCHAR(1000) NOT NULL,
        photo VARCHAR(255) NOT NULL,
        email VARCHAR(50) NOT NULL,
        PRIMARY KEY (id));
        
        -- -----------------------------------------------------
        -- Table {{%social_mobilization}}
        -- -----------------------------------------------------
        CREATE TABLE IF NOT EXISTS social_mobilization (
        id int NOT NULL AUTO_INCREMENT,
        tripartite VARCHAR(255) NOT NULL,
        household VARCHAR(255) NOT NULL,
        voters_list_election_form VARCHAR(255) NOT NULL,
        community_prof VARCHAR(255) NOT NULL,
        qdp VARCHAR(255) NOT NULL,
        social_audit VARCHAR(255) NOT NULL,
        mdu_self_assessment VARCHAR(255) NOT NULL,
        locality_id int,
        PRIMARY KEY (id),
        FOREIGN KEY (locality_id) REFERENCES locality(id));
        
        -- -----------------------------------------------------
        -- Table {{%village_selection_process}}
        -- -----------------------------------------------------
        CREATE TABLE IF NOT EXISTS village_selection_process (
        district_id int NOT NULL AUTO_INCREMENT,
        fp_name VARCHAR(255) NOT NULL,
        date_of_dpc VARCHAR(255) NOT NULL,
        dpc_members_list VARCHAR(255) NOT NULL,
        date_of_vrasm VARCHAR(255) NOT NULL,
        meeting_protocol VARCHAR(255) NOT NULL,
        final_list_of_villages_by_years VARCHAR(255) NOT NULL,
        photo_1 VARCHAR(255) NOT NULL,
        photo_2 VARCHAR(255) NOT NULL,
        photo_3 VARCHAR(255) NOT NULL,
        photo_4 VARCHAR(255) NOT NULL,
        photo_5 VARCHAR(255) NOT NULL,
        PRIMARY KEY (district_id));
        
        -- -----------------------------------------------------
        -- Table {{%landscaping_works}}
        -- -----------------------------------------------------
        CREATE TABLE IF NOT EXISTS landscaping_works (
        id int NOT NULL AUTO_INCREMENT,
        region_id int NOT NULL,
        district_id int NOT NULL,
        locality_id int NOT NULL,
        year VARCHAR(255) NOT NULL,
        aholi_soni VARCHAR(255) NOT NULL,
        honadonlar_soni VARCHAR(255) NOT NULL,
        tashkilot_nomi VARCHAR(255) NOT NULL,
        sektor_raqami VARCHAR(255) NOT NULL,
        viloyatdan_masul VARCHAR(255) NOT NULL,
        tumandan_masul VARCHAR(255) NOT NULL,
        PRIMARY KEY (id),
        FOREIGN KEY (region_id) REFERENCES region(id),
        FOREIGN KEY (district_id) REFERENCES district(id),
        FOREIGN KEY (locality_id) REFERENCES locality(id));
                      
        -- -----------------------------------------------------
        -- Table {{%construction_materials_plan_report}}
        -- -----------------------------------------------------
        CREATE TABLE IF NOT EXISTS construction_materials_plan_report (
        id int NOT NULL AUTO_INCREMENT,
        region_id int NOT NULL,
        district_id int NOT NULL,
        locality_id int NOT NULL,
        year VARCHAR(255) NOT NULL,        
        metal_prokat VARCHAR(255) NOT NULL,
        sement VARCHAR(255) NOT NULL,
        shifer VARCHAR(255) NOT NULL,
        gisht VARCHAR(255) NOT NULL,
        shagal VARCHAR(255) NOT NULL,
        qum VARCHAR(255) NOT NULL,
        yogoch VARCHAR(255) NOT NULL,
        eshik VARCHAR(255) NOT NULL,
        deraza VARCHAR(255) NOT NULL,
        boyoq_mahsulotlari VARCHAR(255) NOT NULL,        
        gipso_karton VARCHAR(255) NOT NULL,
        polietelen_truba VARCHAR(255) NOT NULL,
        tayyor_beton VARCHAR(255) NOT NULL,
        PRIMARY KEY (id),
        FOREIGN KEY (region_id) REFERENCES region(id),
        FOREIGN KEY (district_id) REFERENCES district(id),
        FOREIGN KEY (locality_id) REFERENCES locality(id));
        
        -- -----------------------------------------------------
        -- Table {{%construction_materials_financed_report}}
        -- -----------------------------------------------------
        CREATE TABLE IF NOT EXISTS construction_materials_financed_report (
        id int NOT NULL AUTO_INCREMENT,
        region_id int NOT NULL,
        district_id int NOT NULL,
        locality_id int NOT NULL,
        year VARCHAR(255) NOT NULL,        
        metal_prokat VARCHAR(255) NOT NULL,
        sement VARCHAR(255) NOT NULL,
        shifer VARCHAR(255) NOT NULL,
        gisht VARCHAR(255) NOT NULL,
        shagal VARCHAR(255) NOT NULL,
        qum VARCHAR(255) NOT NULL,
        yogoch VARCHAR(255) NOT NULL,
        eshik VARCHAR(255) NOT NULL,
        deraza VARCHAR(255) NOT NULL,
        boyoq_mahsulotlari VARCHAR(255) NOT NULL,        
        gipso_karton VARCHAR(255) NOT NULL,
        polietelen_truba VARCHAR(255) NOT NULL,
        tayyor_beton VARCHAR(255) NOT NULL,
        PRIMARY KEY (id),
        FOREIGN KEY (region_id) REFERENCES region(id),
        FOREIGN KEY (district_id) REFERENCES district(id),
        FOREIGN KEY (locality_id) REFERENCES locality(id));
        
        -- -----------------------------------------------------
        -- Table {{%construction_materials_assimilated_report}}
        -- -----------------------------------------------------
        CREATE TABLE IF NOT EXISTS construction_materials_assimilated_report (
        id int NOT NULL AUTO_INCREMENT,
        region_id int NOT NULL,
        district_id int NOT NULL,
        locality_id int NOT NULL,
        year VARCHAR(255) NOT NULL,        
        metal_prokat VARCHAR(255) NOT NULL,
        sement VARCHAR(255) NOT NULL,
        shifer VARCHAR(255) NOT NULL,
        gisht VARCHAR(255) NOT NULL,
        shagal VARCHAR(255) NOT NULL,
        qum VARCHAR(255) NOT NULL,
        yogoch VARCHAR(255) NOT NULL,
        eshik VARCHAR(255) NOT NULL,
        deraza VARCHAR(255) NOT NULL,
        boyoq_mahsulotlari VARCHAR(255) NOT NULL,        
        gipso_karton VARCHAR(255) NOT NULL,
        polietelen_truba VARCHAR(255) NOT NULL,
        tayyor_beton VARCHAR(255) NOT NULL,
        PRIMARY KEY (id),
        FOREIGN KEY (region_id) REFERENCES region(id),
        FOREIGN KEY (district_id) REFERENCES district(id),
        FOREIGN KEY (locality_id) REFERENCES locality(id));
        
        ";

        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200127_102546_shukhrat_wb_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200127_102546_shukhrat_wb_tables cannot be reverted.\n";

        return false;
    }
    */
}
