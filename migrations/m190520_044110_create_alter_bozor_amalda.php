<?php

use yii\db\Migration;

/**
 * Class m190520_044110_create_alter_bozor_amalda
 */
class m190520_044110_create_alter_bozor_amalda extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE `bozor_infratuzilmasi_amalda` ADD `date` DATE NOT NULL AFTER `ishga_tush_mud_amalda`, ADD `created_at` INT NOT NULL AFTER `date`, ADD `updated_at` INT NOT NULL AFTER `created_at`;
");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190520_044110_create_alter_bozor_amalda cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190520_044110_create_alter_bozor_amalda cannot be reverted.\n";

        return false;
    }
    */
}
