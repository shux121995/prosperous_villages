<?php

use yii\db\Migration;

class m200619_090535_mdu_members extends Migration
{
    public function safeUp()
    {
        $sql = "
        CREATE TABLE mdu_members (
        id int NOT NULL AUTO_INCREMENT,
        locality_id int NOT NULL,
        
        chair varchar(255) NULL,
        vice_chair varchar(255) NULL,
        secretary varchar(255) NULL,
        phone_number varchar(255) NULL,
        
        PRIMARY KEY (id),
        FOREIGN KEY (locality_id ) REFERENCES locality(id));
            ";
        $this->execute($sql);
    }

    public function safeDown()
    {
        $slq = "DROP TABLE mdu_members;";
        $this->execute($slq);
    }
}
