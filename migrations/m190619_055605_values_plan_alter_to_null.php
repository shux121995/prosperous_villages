<?php

use yii\db\Migration;

/**
 * Class m190619_055605_values_plan_alter_to_null
 */
class m190619_055605_values_plan_alter_to_null extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE {{%values_plan}}
	CHANGE COLUMN `qurulish_id` `qurulish_id` INT(11) NULL DEFAULT NULL AFTER `projects`,
	CHANGE COLUMN `bozor_id` `bozor_id` INT(11) NULL DEFAULT NULL AFTER `qurulish_id`;
");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190619_055605_values_plan_alter_to_null cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190619_055605_values_plan_alter_to_null cannot be reverted.\n";

        return false;
    }
    */
}
