<?php

use yii\db\Migration;

/**
 * Class m190415_074321_alter_inputs
 */
class m190415_074321_alter_inputs extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE {{%inputs}}
	ADD COLUMN `title` VARCHAR(255) NOT NULL AFTER `id`,
	ADD COLUMN `name` VARCHAR(255) NOT NULL AFTER `title`;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190415_074321_alter_inputs cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190415_074321_alter_inputs cannot be reverted.\n";

        return false;
    }
    */
}
