<?php

use yii\db\Migration;

class m200623_082844_project_design_step extends Migration
{
    public function safeUp()
    {
        $sql = "
        CREATE TABLE project_design_step(
        id int NOT NULL AUTO_INCREMENT,
    	region_id int NOT NULL,
    	district_id int NOT NULL,
    	locality_id int NOT NULL,
    
        form1_1 date,
        form1_2 date,
        form1_3 date,
        form1_4 date,
        form1_5 date,
        step_2 date,
        step_3 date,
        step_4 date,
        step_5 date,
    
        PRIMARY KEY (id),
    	FOREIGN KEY (region_id) REFERENCES region(id),
    	FOREIGN KEY (district_id) REFERENCES district(id),
    	FOREIGN KEY (locality_id ) REFERENCES locality(id)
);";
        $this->execute($sql);
    }

    public function safeDown()
    {
        $slq = "DROP TABLE project_design_step;";
        $this->execute($slq);
    }

}
