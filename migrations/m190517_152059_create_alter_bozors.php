<?php

use yii\db\Migration;

/**
 * Class m190517_152059_create_alter_bozors
 */
class m190517_152059_create_alter_bozors extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE `bozor_infratuzilmasi` CHANGE `ishga_tush_mud_reja` `ishga_tush_mud_reja` DATE NULL DEFAULT NULL;
        ALTER TABLE `bozor_infratuzilmasi_amalda` CHANGE `ishga_tush_mud_amalda` `ishga_tush_mud_amalda` DATE NULL DEFAULT NULL;
");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190517_152059_create_alter_bozors cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190517_152059_create_alter_bozors cannot be reverted.\n";

        return false;
    }
    */
}
