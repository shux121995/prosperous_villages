<?php

use yii\db\Migration;

class m200622_052934_cpm_leader extends Migration
{
    public function safeUp()
    {
        $sql = "
        CREATE TABLE cpm_leader (
        id int NOT NULL AUTO_INCREMENT,
        locality_id int NOT NULL,
        
        name varchar(255) NULL,
        phone varchar(255) NULL,
        
        PRIMARY KEY (id),
        FOREIGN KEY (locality_id ) REFERENCES locality(id));
            ";
        $this->execute($sql);
    }

    public function safeDown()
    {
        $slq = "DROP TABLE cpm_leader;";
        $this->execute($slq);
    }
}
