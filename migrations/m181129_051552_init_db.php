<?php

use yii\db\Migration;

/**
 * Class m181129_051552_init_db
 */
class m181129_051552_init_db extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS {{%region}} (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(128) NOT NULL,
  `order` INT NULL DEFAULT 100,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS {{%district}} (
  `id` INT NOT NULL AUTO_INCREMENT,
  `region_id` INT NULL,
  `title` VARCHAR(128) NOT NULL,
  `type` ENUM('district', 'city') NULL COMMENT 'это для тип района и города чтобы отличить их из одного',
  `order` INT NULL DEFAULT 100,
  PRIMARY KEY (`id`),
  INDEX `region` (`region_id` ASC),
  CONSTRAINT `fk_region`
    FOREIGN KEY (`region_id`)
    REFERENCES {{%region}} (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS {{%locality}} (
  `id` INT NOT NULL AUTO_INCREMENT,
  `district_id` INT NULL,
  `title` VARCHAR(128) NOT NULL,
  `description` TEXT(3000) NULL,
  `type` ENUM('qishloq', 'mahalla') NULL,
  PRIMARY KEY (`id`),
  INDEX `district` (`district_id` ASC),
  CONSTRAINT `district`
    FOREIGN KEY (`district_id`)
    REFERENCES {{%district}} (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS {{%locality_plans}} (
  `id` INT NOT NULL AUTO_INCREMENT,
  `year` VARCHAR(4) NOT NULL,
  `locality_id` INT NOT NULL,
  `district_id` INT NULL,
  `region_id` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `locality` (`locality_id` ASC),
  CONSTRAINT `fk_locality`
    FOREIGN KEY (`locality_id`)
    REFERENCES {{%locality}} (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;
");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181129_051552_init_db cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181129_051552_init_db cannot be reverted.\n";

        return false;
    }
    */
}
