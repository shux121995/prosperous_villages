<?php

use yii\db\Migration;

class m200622_050428_mdu_imi_score extends Migration
{
    public function safeUp()
    {
        $sql = "
        CREATE TABLE mdu_imi_score (
        id int NOT NULL AUTO_INCREMENT,
        locality_id int NOT NULL,
        
        score int(11) NULL,
        
        PRIMARY KEY (id),
        FOREIGN KEY (locality_id ) REFERENCES locality(id));
            ";
        $this->execute($sql);
    }

    public function safeDown()
    {
        $slq = "DROP TABLE mdu_imi_score;";
        $this->execute($slq);
    }
}
