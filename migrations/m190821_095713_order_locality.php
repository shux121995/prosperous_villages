<?php

use yii\db\Migration;

/**
 * Class m190821_095713_order_locality
 */
class m190821_095713_order_locality extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE {{%locality}}
	ADD COLUMN `order` INT NULL DEFAULT '999' AFTER `map_zoom`;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190821_095713_order_locality cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190821_095713_order_locality cannot be reverted.\n";

        return false;
    }
    */
}
