<?php

use yii\db\Migration;

/**
 * Class m190305_053148_user_access
 */
class m190305_053148_user_access extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS {{%user_access}} (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `region_id` INT NULL,
  `type` ENUM('admin', 'moder', 'region', 'district', 'locality') NULL DEFAULT 'district',
  `to_id` INT NULL,
  `create` INT NULL DEFAULT 0,
  `update` INT NULL DEFAULT 0,
  `view` INT NULL DEFAULT 1,
  `delete` INT NULL DEFAULT 0,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `a_user_id` (`user_id` ASC),
  CONSTRAINT `fk_access_user`
    FOREIGN KEY (`user_id`)
    REFERENCES {{%user}} (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;
");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190305_053148_user_access cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190305_053148_user_access cannot be reverted.\n";

        return false;
    }
    */
}
