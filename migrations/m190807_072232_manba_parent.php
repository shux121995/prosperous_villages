<?php

use yii\db\Migration;

/**
 * Class m190807_072232_manba_parent
 */
class m190807_072232_manba_parent extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE {{%moliya_manbai}}
	ADD COLUMN `parent_id` INT NULL DEFAULT NULL AFTER `description`;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190807_072232_manba_parent cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190807_072232_manba_parent cannot be reverted.\n";

        return false;
    }
    */
}
