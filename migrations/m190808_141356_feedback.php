<?php

use yii\db\Migration;

/**
 * Class m190808_141356_feedback
 */
class m190808_141356_feedback extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS {{%feedback}} (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(128) NOT NULL,
  `phone` VARCHAR(45) NOT NULL,
  `address` VARCHAR(255) NOT NULL,
  `locality_id` INT NULL,
  `table_id` INT NULL,
  `section_id` INT NULL,
  `type` INT NULL,
  `text` TEXT NOT NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  `status` INT NULL DEFAULT 2 COMMENT '2 - новый',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS {{%feedback_files}} (
  `id` INT NOT NULL AUTO_INCREMENT,
  `feed_id` INT NOT NULL,
  `filename` VARCHAR(128) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `feedid` (`feed_id` ASC),
  CONSTRAINT `fk_feedid`
    FOREIGN KEY (`feed_id`)
    REFERENCES {{%feedback}} (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;
");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190808_141356_feedback cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190808_141356_feedback cannot be reverted.\n";

        return false;
    }
    */
}
