<?php

use yii\db\Migration;

/**
 * Class m191028_121040_tables_oprosnik
 */
class m191028_121040_tables_oprosnik extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("-- -----------------------------------------------------
-- Table {{%wb_oprosnik_section}}
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS {{%wb_oprosnik_section}} (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NULL,
  `title_en` VARCHAR(255) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table {{%wb_oprosnik_form`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS {{%wb_oprosnik_form}} (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `title_en` VARCHAR(255) NULL,
  `op_section_id` INT NULL,
  `parent_id` INT NULL,
  `order` INT NULL,
  `is_addons` INT NULL COMMENT 'bu dop kalit bir xil tipdagi ma\'lumotlar uchun\nmisol uchun: \nmaktab[1]\nmaktab[2]\n1 va 2 lar addons hisoblanadi\nbu _values da addons ga qushimcha parametr beradi',
  `is_group` INT NULL,
  `is_table` INT NULL,
  `is_horizontal` INT NULL,
  `input_type` VARCHAR(255) NULL COMMENT 'text\nselect\ncheckbox\nradio',
  `input_data` TEXT NULL,
  `input_placeholder` VARCHAR(255) NULL,
  `level` INT NULL,
  `cols` INT NULL,
  `rows` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `op_section` (`op_section_id` ASC),
  CONSTRAINT `fk_op_section`
    FOREIGN KEY (`op_section_id`)
    REFERENCES {{%wb_oprosnik_section}} (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table {{%wb_oprosnik`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS {{%wb_oprosnik}} (
  `id` INT NOT NULL AUTO_INCREMENT,
  `locality_id` INT NOT NULL,
  `district_id` INT NULL,
  `region_id` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `op_local` (`locality_id` ASC),
  CONSTRAINT `fk_op_local`
    FOREIGN KEY (`locality_id`)
    REFERENCES {{%locality}} (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table {{%wb_oprosnik_values`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS {{%wb_oprosnik_values}} (
  `id` INT NOT NULL AUTO_INCREMENT,
  `oprosnik_id` INT NULL,
  `op_form_id` INT NULL,
  `fio_id` INT NULL,
  `addons` INT NULL,
  `value` VARCHAR(255) NULL,
  PRIMARY KEY (`id`),
  INDEX `value_oprosnik` (`oprosnik_id` ASC),
  INDEX `value_form` (`op_form_id` ASC),
  CONSTRAINT `fk_value_oprosnik`
    FOREIGN KEY (`oprosnik_id`)
    REFERENCES {{%wb_oprosnik}} (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_value_form`
    FOREIGN KEY (`op_form_id`)
    REFERENCES {{%wb_oprosnik_form}} (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table {{%wb_fio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS {{%wb_fio}} (
  `id` INT NOT NULL AUTO_INCREMENT,
  `locality_id` INT NULL,
  `fisrtname` VARCHAR(255) NULL,
  `lastname` VARCHAR(255) NULL,
  `middlename` VARCHAR(255) NULL,
  `phone` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  `birthday` DATE NULL,
  `married` INT NULL DEFAULT 1 COMMENT 'Oilaviy ahvoli\n1 Uylangan/Turmushga chiqqan\n2 Uylanmagan/Turmushga chiqmagan\n3 Ajrashgan',
  `jinsi` INT NULL COMMENT '1 Erkak\n2 Ayol',
  `manzili` VARCHAR(255) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table {{%wb_oprosnik_excel`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS {{%wb_oprosnik_excel}} (
  `id` INT NOT NULL AUTO_INCREMENT,
  `section_id` INT NULL,
  `form_id` INT NULL,
  `parent_id` INT NULL,
  `type` INT NULL COMMENT 'horizontal\nvertical',
  `level` INT NULL,
  `colspan` INT NULL,
  `rowspan` INT NULL,
  `is_empty` INT NULL,
  `is_input` INT NULL DEFAULT 0,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191028_121040_tables_oprosnik cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191028_121040_tables_oprosnik cannot be reverted.\n";

        return false;
    }
    */
}
