<?php

use yii\db\Migration;

/**
 * Class m190426_054527_create_bozor_inf_tables
 */
class m190426_054527_create_bozor_inf_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
-- -----------------------------------------------------
-- Table `bozor_infratuzilmasi`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `obod`.`bozor_infratuzilmasi` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `plan_id` INT NOT NULL,
  `loyiha_tashabbuskori` VARCHAR(128) NULL,
  `title` VARCHAR(128) NULL,
  `loyiha_yunalishi` INT NULL,
  `quvvat_ulchov_birlik` DECIMAL NULL,
  `quvvat_natural_qiy` DECIMAL NULL,
  `quvvat_ish_chiq_hajm` DECIMAL NULL,
  `yil_eksport_hajm` DECIMAL NULL,
  `yil_byudjetga_tushum_hajm` DECIMAL NULL,
  `loyiha_qiy_reja` DECIMAL NULL,
  `moliya_mab_reja` DECIMAL NULL,
  `moliya_kredit_reja` DECIMAL NULL,
  `moliya_invest_reja` DECIMAL NULL,
  `xorij_hamkor` VARCHAR(128) NULL,
  `horij_davlat` VARCHAR(128) NULL,
  `xiz_kursatuvchi_bank` VARCHAR(128) NULL,
  `ish_urin_reja` INT NULL,
  `ishga_tush_mud_reja` VARCHAR(128) NULL,
  `ishga_tush_mud_asos` VARCHAR(128) NULL,
  `loyiha_muhan_yer_holati` INT NULL,
  `loyiha_muhan_elektr_holati` INT NULL,
  `loyiha_muhan_gaz_holati` INT NULL,
  `loyiha_kredit_jadval_topshirish` VARCHAR(128) NULL,
  `loyiha_kredit_haq_topshirish` VARCHAR(128) NULL,
  `loyiha_kredit_jadval_ajratish` VARCHAR(128) NULL,
  `loyiha_kredit_haq_ajratish` VARCHAR(128) NULL,
  PRIMARY KEY (`id`),
  INDEX `bozor_infratuzilmasi_plan_id` (`plan_id` ASC),
  CONSTRAINT `fk_bozor_infratuzilmasi_plan_id`
    FOREIGN KEY (`plan_id`)
    REFERENCES `obod`.`locality_plans` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `bozor_infratuzilmasi_amalda`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `obod`.`bozor_infratuzilmasi_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `bozor_id` INT NOT NULL,
  `loyiha_qiy_amalda` DECIMAL NULL,
  `mol_mab_amalda` DECIMAL NULL,
  `moliya_kredit_amalda` DECIMAL NULL,
  `moliya_invest_amalda` DECIMAL NULL,
  `ish_urin_amalda` INT NULL,
  `ishga_tush_mud_amalda` VARCHAR(128) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_amalda_bozor_id_idx` (`bozor_id` ASC),
  CONSTRAINT `fk_amalda_bozor_id`
    FOREIGN KEY (`bozor_id`)
    REFERENCES `obod`.`bozor_infratuzilmasi` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190426_054527_create_bozor_inf_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190426_054527_create_bozor_inf_tables cannot be reverted.\n";

        return false;
    }
    */
}
