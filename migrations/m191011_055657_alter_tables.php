<?php

use yii\db\Migration;

/**
 * Class m191011_055657_alter_tables
 */
class m191011_055657_alter_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE {{%tables}}
	ADD COLUMN `project_type` VARCHAR(50) NULL DEFAULT 'qishloq' AFTER `levels`,
	ADD COLUMN `year` VARCHAR(4) NULL DEFAULT NULL AFTER `project_type`,
	ADD COLUMN `status` INT NULL DEFAULT NULL AFTER `year`;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191011_055657_alter_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191011_055657_alter_tables cannot be reverted.\n";

        return false;
    }
    */
}
