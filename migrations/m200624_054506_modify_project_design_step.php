<?php

use yii\db\Migration;

class m200624_054506_modify_project_design_step extends Migration
{
    public function safeUp()
    {
        $sql = "
        ALTER TABLE project_design_step
        ADD (
        form1_1_file varchar(255),
        form1_2_file varchar(255),
        form1_3_file varchar(255),
        form1_4_file varchar(255),
        form1_5_file varchar(255),
        step_2_file varchar(255),
        step_3_file varchar(255),
        step_4_file varchar(255),
        step_5_file varchar(255));";
        $this->execute($sql);
    }

    public function safeDown()
    {
        echo "m200624_054506_modify_project_design_step cannot be reverted.\n";

        return false;
    }
}
