<?php

use yii\db\Migration;

/**
 * Class m190517_064607_tables_excel
 */
class m190517_064607_tables_excel extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("-- -----------------------------------------------------
-- Table `tables_excel`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS {{%tables_excel}} (
  `id` INT NOT NULL AUTO_INCREMENT,
  `table_id` INT NOT NULL,
  `section_id` INT NULL,
  `input_id` INT NULL,
  `parent_id` INT NULL,
  `type` INT NULL DEFAULT 1,
  `level` INT NULL,
  `colspan` INT NULL,
  `rowspan` INT NULL,
  `order` INT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190517_064607_tables_excel cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190517_064607_tables_excel cannot be reverted.\n";

        return false;
    }
    */
}
