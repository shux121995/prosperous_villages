<?php

use yii\db\Migration;

/**
 * Class m190807_055724_razdel_manba
 */
class m190807_055724_razdel_manba extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS {{%moliya_manbai_table}} (
  `id` INT NOT NULL AUTO_INCREMENT,
  `manba_id` INT NOT NULL,
  `table_id` INT NULL,
  `other` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = 'Это указывает какое источник какому сводную таблицу принадлежит';
");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190807_055724_razdel_manba cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190807_055724_razdel_manba cannot be reverted.\n";

        return false;
    }
    */
}
