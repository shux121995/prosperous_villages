<?php

use yii\db\Migration;

class m200629_054745_infra_and_finan extends Migration
{
    public function safeUp()
    {
        $sql = "
        CREATE TABLE infra_and_finan (
    id int NOT NULL AUTO_INCREMENT,
    region_id int NOT NULL,
    district_id int NOT NULL,
    locality_id int NOT NULL,
    sub_id int NOT NULL,
    sub_work_id int NOT NULL,
    
    direct_b int NULL,
    indirect_b int NULL,
    land_allocation varchar(255) NULL,
    topograpic varchar (255) NULL,
    architectual varchar (255) NULL,
    gen_plan_des varchar (255) NULL,
    gen_plan_progress varchar (255) NULL,
    proj_des_des varchar (255) NULL,
    proj_des_progress varchar (255) NULL,
    sta_env_exper varchar (255) NULL,
    sta_min_exper varchar (255) NULL,
    res_contractor int NULL,
    constraction_start date,
    constraction_end date,
    contract_number varchar(255) NULL,
    contract_start date,
    contract_end date,
    total_contract_value varchar(255) NULL,
    total_undis_amount varchar (255) NULL,
    status_of_subproject int NULL,
    sub_progress varchar (255) NULL,
    sub_acceptance varchar (255) NULL,
    sub_before varchar (255) NULL,
    sub_during varchar (255) NULL,
    sub_after varchar (255) NULL,
      

    PRIMARY KEY (id),
    FOREIGN KEY (region_id) REFERENCES region(id),
    FOREIGN KEY (district_id) REFERENCES district(id),
    FOREIGN KEY (locality_id ) REFERENCES locality(id),
    FOREIGN KEY (sub_id) REFERENCES wb_types(id),
    FOREIGN KEY (sub_work_id) REFERENCES wb_sub_types(id),
    FOREIGN KEY (res_contractor) REFERENCES wb_developers(id),
    FOREIGN KEY (status_of_subproject) REFERENCES wb_status(id)
);
        ";
        $this->execute($sql);
    }

    public function safeDown()
    {
        $slq = "DROP TABLE infra_and_finan;";
        $this->execute($slq);
    }

}
