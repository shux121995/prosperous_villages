<?php

use yii\db\Migration;

/**
 * Class m191031_154818_alter_region_district_map
 */
class m191031_154818_alter_region_district_map extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE {{%region}}
	ADD COLUMN `map_longitude` VARCHAR(50) NULL DEFAULT NULL AFTER `order`,
	ADD COLUMN `map_latitude` VARCHAR(50) NULL DEFAULT NULL AFTER `map_longitude`,
	ADD COLUMN `map_zoom` VARCHAR(50) NULL DEFAULT NULL AFTER `map_latitude`;");

        $this->execute("ALTER TABLE {{%district}}
	ADD COLUMN `map_longitude` VARCHAR(50) NULL DEFAULT NULL AFTER `order`,
	ADD COLUMN `map_latitude` VARCHAR(50) NULL DEFAULT NULL AFTER `map_longitude`,
	ADD COLUMN `map_zoom` VARCHAR(50) NULL DEFAULT NULL AFTER `map_latitude`;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191031_154818_alter_region_district_map cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191031_154818_alter_region_district_map cannot be reverted.\n";

        return false;
    }
    */
}
