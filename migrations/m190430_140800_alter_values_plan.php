<?php

use yii\db\Migration;

/**
 * Class m190430_140800_alter_values_plan
 */
class m190430_140800_alter_values_plan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE {{%values_plan}} 
DROP FOREIGN KEY `fk_plan_values_moliya_manbai`,
DROP INDEX `plan_values_moliya_manbai`;");
        $this->execute("ALTER TABLE {{%values_plan}}
	DROP COLUMN `moliya_manbai_id`;");
        $this->execute("-- -----------------------------------------------------
-- Table `obod`.`values_manba_plan`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS {{%values_manba_plan}} (
  `id` INT NOT NULL AUTO_INCREMENT,
  `values_plan_id` INT NOT NULL,
  `moliya_manbai_id` INT NULL,
  `reja` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  INDEX `vmp_values_plan` (`values_plan_id` ASC),
  INDEX `vmp_manba` (`moliya_manbai_id` ASC),
  CONSTRAINT `fk_vmp_values_plan`
    FOREIGN KEY (`values_plan_id`)
    REFERENCES {{%values_plan}} (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_vmp_manba`
    FOREIGN KEY (`moliya_manbai_id`)
    REFERENCES {{%moliya_manbai}} (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `obod`.`values_manba_fact`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS {{%values_manba_fact}} (
  `id` INT NOT NULL AUTO_INCREMENT,
  `values_fact_id` INT NOT NULL,
  `moliya_manba_id` INT NULL,
  `fact` VARCHAR(45) NULL,
  `post_date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `vmf_fact` (`values_fact_id` ASC),
  INDEX `vmf_manba` (`moliya_manba_id` ASC),
  CONSTRAINT `fk_vmf_fact`
    FOREIGN KEY (`values_fact_id`)
    REFERENCES {{%values_fact}} (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_vmf_manba`
    FOREIGN KEY (`moliya_manba_id`)
    REFERENCES {{%moliya_manbai}} (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `obod`.`values_manba_molled`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS {{%values_manba_molled}} (
  `id` INT NOT NULL AUTO_INCREMENT,
  `values_plan_id` INT NOT NULL,
  `moliya_manba_id` INT NULL,
  `molled` VARCHAR(45) NULL,
  `date` DATE NULL,
  PRIMARY KEY (`id`),
  INDEX `vmm_plan` (`values_plan_id` ASC),
  INDEX `vmm_manba` (`moliya_manba_id` ASC),
  CONSTRAINT `fk_vmm_plan`
    FOREIGN KEY (`values_plan_id`)
    REFERENCES {{%values_plan}} (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_vmm_manba`
    FOREIGN KEY (`moliya_manba_id`)
    REFERENCES {{%moliya_manbai}} (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190430_140800_alter_values_plan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190430_140800_alter_values_plan cannot be reverted.\n";

        return false;
    }
    */
}
