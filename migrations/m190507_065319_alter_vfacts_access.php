<?php

use yii\db\Migration;

/**
 * Class m190507_065319_alter_vfacts_access
 */
class m190507_065319_alter_vfacts_access extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE {{%values_fact}}
	ADD COLUMN `access_id` INT(11) NULL DEFAULT NULL AFTER `id`;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190507_065319_alter_vfacts_access cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190507_065319_alter_vfacts_access cannot be reverted.\n";

        return false;
    }
    */
}
