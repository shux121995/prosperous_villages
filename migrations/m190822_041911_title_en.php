<?php

use yii\db\Migration;

/**
 * Class m190822_041911_title_en
 */
class m190822_041911_title_en extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE {{%tables}}
	ADD COLUMN `title_en` VARCHAR(255) NULL DEFAULT NULL AFTER `title`;");

        $this->execute("ALTER TABLE {{%tables}}
	ADD COLUMN `excel_description_en` TEXT NULL DEFAULT NULL AFTER `excel_description`;");

        $this->execute("ALTER TABLE {{%sections}}
	ADD COLUMN `title_en` VARCHAR(255) NULL DEFAULT NULL AFTER `title`;");

        $this->execute("ALTER TABLE {{%inputs}}
	ADD COLUMN `title_en` VARCHAR(255) NULL DEFAULT NULL AFTER `title`;
");

        $this->execute("ALTER TABLE {{%region}}
	ADD COLUMN `title_en` VARCHAR(128) NULL DEFAULT NULL AFTER `title`;");

        $this->execute("ALTER TABLE {{%district}}
	ADD COLUMN `title_en` VARCHAR(128) NULL DEFAULT NULL AFTER `title`;");

        $this->execute("ALTER TABLE {{%locality}}
	ADD COLUMN `title_en` VARCHAR(128) NULL DEFAULT NULL AFTER `title`;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190822_041911_title_en cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190822_041911_title_en cannot be reverted.\n";

        return false;
    }
    */
}
