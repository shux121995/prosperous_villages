<?php

use yii\db\Migration;

/**
 * Class m191031_105532_alter_feedback
 */
class m191031_105532_alter_feedback extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE {{%feedback}}
	ADD COLUMN `project_type` VARCHAR(50) NULL DEFAULT NULL AFTER `type`;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191031_105532_alter_feedback cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191031_105532_alter_feedback cannot be reverted.\n";

        return false;
    }
    */
}
