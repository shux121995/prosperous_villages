<?php

use yii\db\Migration;

/**
 * Class m200702_072627_resettlement
 */
class m200702_072627_resettlement extends Migration
{
    public function safeUp()
    {
        $sql = "
        CREATE TABLE resettlement(
        id int NOT NULL AUTO_INCREMENT,
    	region_id int NOT NULL,
    	district_id int NOT NULL,
    	locality_id int NOT NULL,
    
        form2_1 date,
        form2_1_file varchar(255),
    
        PRIMARY KEY (id),
    	FOREIGN KEY (region_id) REFERENCES region(id),
    	FOREIGN KEY (district_id) REFERENCES district(id),
    	FOREIGN KEY (locality_id ) REFERENCES locality(id)
);";
        $this->execute($sql);
    }

    public function safeDown()
    {
        $slq = "DROP TABLE resettlement;";
        $this->execute($slq);
    }

}
