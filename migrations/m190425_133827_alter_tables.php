<?php

use yii\db\Migration;

/**
 * Class m190425_133827_alter_tables
 */
class m190425_133827_alter_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE {{%tables}}
	CHANGE COLUMN `excel_name` `excel_name` VARCHAR(128) NULL DEFAULT NULL AFTER `title`;
");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190425_133827_alter_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190425_133827_alter_tables cannot be reverted.\n";

        return false;
    }
    */
}
