<?php

use yii\db\Migration;

/**
 * Class m200703_111253_alter_infrastructure_table
 */
class m200703_111253_alter_infrastructure_table extends Migration
{
    public function safeUp()
    {
        $this->execute("ALTER TABLE {{%infra_and_finan}}
	ADD COLUMN `year` INT(11) NULL DEFAULT NULL AFTER `sub_id`;");
    }

    public function safeDown()
    {
        echo "m200703_111253_alter_infrastructure_table cannot be reverted.\n";

        return false;
    }

}
