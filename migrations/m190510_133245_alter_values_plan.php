<?php

use yii\db\Migration;

/**
 * Class m190510_133245_alter_values_plan
 */
class m190510_133245_alter_values_plan extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE `values_plan` ADD `qurulish_id` INT NOT NULL AFTER `projects`, ADD `bozor_id` INT NOT NULL AFTER `qurulish_id`;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190510_133245_alter_values_plan cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190510_133245_alter_values_plan cannot be reverted.\n";

        return false;
    }
    */
}
