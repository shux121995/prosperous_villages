<?php

use yii\db\Migration;

/**
 * Class m190515_045748_create_alter_qurulish
 */
class m190515_045748_create_alter_qurulish extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE `qurulish` ADD `status` TINYINT NOT NULL AFTER `ssm`;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190515_045748_create_alter_qurulish cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190515_045748_create_alter_qurulish cannot be reverted.\n";

        return false;
    }
    */
}
