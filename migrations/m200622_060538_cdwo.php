<?php

use yii\db\Migration;

class m200622_060538_cdwo extends Migration
{
    public function safeUp()
    {
        $sql = "
        CREATE TABLE cdwo (
        id int NOT NULL AUTO_INCREMENT,
        locality_id int NOT NULL,
        
        cdwo_name varchar(255) NULL,
        director varchar(255) NULL,
        vice_director varchar(255) NULL,
        secretary varchar(255) NULL,
        phone varchar(255) NULL,
        
        
        PRIMARY KEY (id),
        FOREIGN KEY (locality_id ) REFERENCES locality(id));
            ";
        $this->execute($sql);
    }

    public function safeDown()
    {
        $slq = "DROP TABLE cdwo;";
        $this->execute($slq);
    }
}
