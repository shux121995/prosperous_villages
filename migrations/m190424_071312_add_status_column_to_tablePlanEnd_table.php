<?php

use yii\db\Migration;

/**
 * Handles adding status to table `{{%tablePlanEnd}}`.
 */
class m190424_071312_add_status_column_to_tablePlanEnd_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%table_plan_end}}', 'status', $this->tinyInteger());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%table_plan_end}}', 'status');
    }
}
