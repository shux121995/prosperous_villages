<?php

use yii\db\Migration;

/**
 * Class m190508_061535_alter_sections_level
 */
class m190508_061535_alter_sections_level extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE {{%sections}}
	ADD COLUMN `level` INT NULL DEFAULT NULL AFTER `forms_id`,
	ADD COLUMN `cols` INT NULL DEFAULT NULL AFTER `level`,
	ADD COLUMN `rows` INT NULL DEFAULT NULL AFTER `cols`;
	ALTER TABLE {{%tables}}
	ADD COLUMN `levels` INT NULL DEFAULT NULL AFTER `excel_description`;
");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190508_061535_alter_sections_level cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190508_061535_alter_sections_level cannot be reverted.\n";

        return false;
    }
    */
}
