<?php

use yii\db\Migration;

/**
 * Class m190809_054631_mapping
 */
class m190809_054631_mapping extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE {{%locality}}
	ADD COLUMN `map_longitude` VARCHAR(50) NULL DEFAULT NULL AFTER `sector_dis`,
	ADD COLUMN `map_latitude` VARCHAR(50) NULL DEFAULT NULL AFTER `map_longitude`,
	ADD COLUMN `map_zoom` VARCHAR(50) NULL DEFAULT NULL AFTER `map_latitude`;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190809_054631_mapping cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190809_054631_mapping cannot be reverted.\n";

        return false;
    }
    */
}
