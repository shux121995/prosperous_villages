<?php

use yii\db\Migration;

/**
 * Class m191031_110555_table_feedback_answer
 */
class m191031_110555_table_feedback_answer extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS {{%feedback_answers}} (
  `id` INT NOT NULL AUTO_INCREMENT,
  `feedback_id` INT NOT NULL,
  `text` TEXT NOT NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `afeedback` (`feedback_id` ASC),
  CONSTRAINT `fk_afeedback`
    FOREIGN KEY (`feedback_id`)
    REFERENCES {{%feedback}} (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191031_110555_table_feedback_answer cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191031_110555_table_feedback_answer cannot be reverted.\n";

        return false;
    }
    */
}
