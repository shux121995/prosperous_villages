<?php

use yii\db\Migration;

/**
 * Class m190529_054850_alter_is_empty
 */
class m190529_054850_alter_is_empty extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE {{%tables_excel}}
	ADD COLUMN `is_empty` INT(11) NULL DEFAULT NULL AFTER `order`;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190529_054850_alter_is_empty cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190529_054850_alter_is_empty cannot be reverted.\n";

        return false;
    }
    */
}
