<?php

use yii\db\Migration;

/**
 * Class m190424_085223_fill_access_table
 */
class m190424_085223_fill_access_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS {{%fill_access}} (
  `id` INT NOT NULL AUTO_INCREMENT,
  `year` INT NOT NULL,
  `month` INT NOT NULL,  
  `start` INT NULL,
  `end` INT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190424_085223_fill_access_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190424_085223_fill_access_table cannot be reverted.\n";

        return false;
    }
    */
}
