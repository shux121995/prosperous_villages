<?php

use yii\db\Migration;

/**
 * Class m190417_054558_planning_end
 */
class m190417_054558_planning_end extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS {{%table_plan_end}} (
  `id` INT NOT NULL AUTO_INCREMENT,
  `plan_id` INT NOT NULL,
  `table_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `table_plan_end_plan` (`plan_id` ASC),
  INDEX `table_plan_end_table` (`table_id` ASC),
  CONSTRAINT `fk_table_plan_end_plan`
    FOREIGN KEY (`plan_id`)
    REFERENCES {{%locality_plans}} (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_table_plan_end_table`
    FOREIGN KEY (`table_id`)
    REFERENCES {{%tables}} (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190417_054558_planning_end cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190417_054558_planning_end cannot be reverted.\n";

        return false;
    }
    */
}
