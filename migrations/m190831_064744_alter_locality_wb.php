<?php

use yii\db\Migration;

/**
 * Class m190831_064744_alter_locality_wb
 */
class m190831_064744_alter_locality_wb extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        return $this->execute("ALTER TABLE {{%locality}}
	CHANGE COLUMN `type` `type` ENUM('','qishloq','mahalla','wb') NULL DEFAULT NULL AFTER `description`;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190831_064744_alter_locality_wb cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190831_064744_alter_locality_wb cannot be reverted.\n";

        return false;
    }
    */
}
