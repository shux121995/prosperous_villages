<?php

use yii\db\Migration;

/**
 * Class m190411_052332_last_tables
 */
class m190411_052332_last_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `moliya_manbai` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NULL,
  `description` TEXT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `qurulish` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `plan_id` INT NULL,
  `type` INT NULL COMMENT 'maktab\nmaktabgacha talim\nkasalxona\nmalla obektlari\nboshqa ijtimoiy obektlar',
  `title` VARCHAR(255) NULL,
  `urni` INT NULL,
  `baj_ish_turi` INT NULL,
  `qiy_reja` DECIMAL NULL,
  `qiy_moliya` INT NULL,
  `qiy_molled` DECIMAL NULL,
  `ishga_tushish_reja` DATE NULL,
  `asos_akt` VARCHAR(255) NULL,
  `asos_date` DATE NULL,
  `loyiha_smeta_hujjat` INT NULL COMMENT 'Лойиҳа смета ҳужжатининг мавжудлиги\n(ишлаб чиқилган, ишлаб чиқилмаган)',
  `tarmoq_jadvali` INT NULL COMMENT 'Тармоқ жадвалининг мавжудлиги\n(тасдиқланган, ишлаб чиқилмаган)',
  `loyihachi_tashkilot` VARCHAR(255) NULL,
  `pudratchi_tashkilot` VARCHAR(255) NULL,
  `maktab_raqam` VARCHAR(45) NULL COMMENT 'Мактаб рақами \n(Х-сонли мактаб)',
  `mtm_mulkchilik_shakli` INT NULL COMMENT 'Мулкчилик шакли\n(Давлат,\nДХШ,\nХусусий)',
  `mtm_raqam` VARCHAR(45) NULL COMMENT 'МТМ рақами \n(Х-сонли МТМ)',
  `ssm` VARCHAR(255) NULL COMMENT 'Соғлиқни сақлаш муассасаси \n(ҚВП, ҚОП,\nкасалхона) номи',
  PRIMARY KEY (`id`),
  INDEX `qurulish_plan` (`plan_id` ASC),
  CONSTRAINT `gk_qurulish_plan`
    FOREIGN KEY (`plan_id`)
    REFERENCES `locality_plans` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Дастур доирасида умумий ўрта таълим мактабларини қуриш ва таъмирлаш борасида амалга оширилаётган ишлар тўғрисида\nmaktab,maktabgacha talim,kasalxona,malla obektlari,boshqa ijtimoiy obektlar';

CREATE TABLE IF NOT EXISTS `qurulish_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `qurulish_id` INT NULL,
  `qiy_used` DECIMAL NULL,
  `oh_yer` DECIMAL NULL,
  `oh_poydevor` DECIMAL NULL,
  `oh_gisht_terish` DECIMAL NULL,
  `oh_tom_yopish` DECIMAL NULL,
  `oh_demontaj` DECIMAL NULL,
  `oh_oraliq_devor` DECIMAL NULL,
  `oh_eshik_deraza` DECIMAL NULL,
  `oh_ichki_pardozlash` DECIMAL NULL,
  `oh_tashqi_pardozlash` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `qurulish_amalda` (`qurulish_id` ASC),
  CONSTRAINT `fk_qurulish_amalda`
    FOREIGN KEY (`qurulish_id`)
    REFERENCES `qurulish` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190411_052332_last_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190411_052332_last_tables cannot be reverted.\n";

        return false;
    }
    */
}
