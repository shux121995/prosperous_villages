<?php

use yii\db\Migration;

/**
 * Class m190425_151755_alter_values_plan_projects
 */
class m190425_151755_alter_values_plan_projects extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE {{%values_plan}}
	ADD COLUMN `projects` INT(11) NULL DEFAULT NULL AFTER `molled`;
");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190425_151755_alter_values_plan_projects cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190425_151755_alter_values_plan_projects cannot be reverted.\n";

        return false;
    }
    */
}
