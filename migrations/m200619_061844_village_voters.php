<?php

use yii\db\Migration;

class m200619_061844_village_voters extends Migration
{
    public function safeUp()
    {
        $sql = "
        CREATE TABLE village_voters (
    id int NOT NULL AUTO_INCREMENT,
    region_id int NOT NULL,
    district_id int NOT NULL,
    locality_id int NOT NULL,
    
    male int NOT NULL,
    female int NOT NULL,
    total int NOT NULL,
    p_male int NOT NULL,
    p_female int NOT NULL,
    p_total int NOT NULL,
    
    PRIMARY KEY (id),
    FOREIGN KEY (region_id) REFERENCES region(id),
    FOREIGN KEY (district_id) REFERENCES district(id),
    FOREIGN KEY (locality_id ) REFERENCES locality(id)
);
        ";
        $this->execute($sql);
    }

    public function safeDown()
    {
        $sql = "DROP TABLE village_voters;";
        $this->execute($sql);
    }
}
