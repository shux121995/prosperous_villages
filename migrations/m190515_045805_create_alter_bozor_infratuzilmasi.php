<?php

use yii\db\Migration;

/**
 * Class m190515_045805_create_alter_bozor_infratuzilmasi
 */
class m190515_045805_create_alter_bozor_infratuzilmasi extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE `bozor_infratuzilmasi` ADD `status` TINYINT NOT NULL AFTER `loyiha_kredit_haq_ajratish`;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190515_045805_create_alter_bozor_infratuzilmasi cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190515_045805_create_alter_bozor_infratuzilmasi cannot be reverted.\n";

        return false;
    }
    */
}
