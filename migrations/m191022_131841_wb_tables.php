<?php

use yii\db\Migration;

/**
 * Class m191022_131841_wb_tables
 */
class m191022_131841_wb_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("-- Table {{%wb_plans}}
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS {{%wb_plans}} (
  `id` INT NOT NULL AUTO_INCREMENT,
  `year` VARCHAR(4) NULL,
  `locality_id` INT NOT NULL,
  `district_id` INT NULL,
  `region_id` INT NULL,
  `dev_id` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `wbp_locality` (`locality_id` ASC),
  CONSTRAINT `fk_wbp_locality`
    FOREIGN KEY (`locality_id`)
    REFERENCES {{%locality}} (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table {{%wb_types}}
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS {{%wb_types}} (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `title_en` VARCHAR(255) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table {{%wb_projects}}
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS {{%wb_projects}} (
  `id` INT NOT NULL AUTO_INCREMENT,
  `plan_id` INT NOT NULL,
  `type_id` INT NULL,
  `price` FLOAT NULL,
  `price_usd` FLOAT NULL,
  `table_file` VARCHAR(255) NULL,
  `status` INT NULL DEFAULT 1,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `wbp_plan` (`plan_id` ASC),
  INDEX `wbp_type` (`type_id` ASC),
  CONSTRAINT `fk_wbp_plan`
    FOREIGN KEY (`plan_id`)
    REFERENCES {{%wb_plans}} (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_wbp_type`
    FOREIGN KEY (`type_id`)
    REFERENCES {{%wb_types}} (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table {{%wb_developers}}
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS {{%wb_developers}} (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `title_en` VARCHAR(255) NULL,
  `address` TEXT NULL,
  `phone` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  `direktor` VARCHAR(255) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table {{%wb_sub_types}}
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS {{%wb_sub_types}} (
  `id` INT NOT NULL AUTO_INCREMENT,
  `type_id` INT NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `title_en` VARCHAR(255) NULL,
  PRIMARY KEY (`id`),
  INDEX `stype` (`type_id` ASC),
  CONSTRAINT `fk_stype`
    FOREIGN KEY (`type_id`)
    REFERENCES {{%wb_types}} (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table {{%wb_tasks}}
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS {{%wb_tasks}} (
  `id` INT NOT NULL AUTO_INCREMENT,
  `sub_type_id` INT NULL,
  `project_id` INT NULL,
  `price` FLOAT NULL,
  `price_usd` FLOAT NULL,
  `status` INT NULL DEFAULT 1,
  `date_start` DATE NULL COMMENT 'Дата начало выполнение задач',
  `date_end` DATE NULL COMMENT 'Дата завершение выполнение задач',
  PRIMARY KEY (`id`),
  INDEX `projectid` (`project_id` ASC),
  INDEX `subtype` (`sub_type_id` ASC),
  CONSTRAINT `fk_wbp_subtype`
    FOREIGN KEY (`sub_type_id`)
    REFERENCES {{%wb_sub_types}} (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_wbp_task_project`
    FOREIGN KEY (`project_id`)
    REFERENCES {{%wb_projects}} (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table {{%wb_files}}
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS {{%wb_files}} (
  `id` INT NOT NULL AUTO_INCREMENT,
  `project_id` INT NULL,
  `filename` VARCHAR(255) NOT NULL,
  `type` INT NULL COMMENT 'до 1 и после 2',
  `path` VARCHAR(512) NULL,
  `extension` VARCHAR(5) NULL,
  `filesize` FLOAT NULL,
  PRIMARY KEY (`id`),
  INDEX `fproject` (`project_id` ASC),
  CONSTRAINT `fk_fproject`
    FOREIGN KEY (`project_id`)
    REFERENCES {{%wb_projects}} (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191022_131841_wb_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191022_131841_wb_tables cannot be reverted.\n";

        return false;
    }
    */
}
