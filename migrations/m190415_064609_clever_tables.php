<?php

use yii\db\Migration;

/**
 * Class m190415_064609_clever_tables
 */
class m190415_064609_clever_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
-- -----------------------------------------------------
-- Table `tables`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS {{%tables}} (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `excel_name` VARCHAR(45) NULL,
  `excel_description` TEXT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `forms`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS {{%forms}} (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sections`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS {{%sections}} (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NULL,
  `tables_id` INT NULL,
  `is_group` INT NULL COMMENT 'Группировать поле только на этом',
  `parent_id` INT NULL,
  `forms_id` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `sections_tables` (`tables_id` ASC),
  INDEX `sections_forms` (`forms_id` ASC),
  INDEX `sections_parents` (`parent_id` ASC),
  CONSTRAINT `fk_sections_tables`
    FOREIGN KEY (`tables_id`)
    REFERENCES {{%tables}} (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_sections_sections`
    FOREIGN KEY (`forms_id`)
    REFERENCES {{%forms}} (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_sections_parents`
    FOREIGN KEY (`parent_id`)
    REFERENCES {{%sections}} (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `inputs`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS {{%inputs}} (
  `id` INT NOT NULL AUTO_INCREMENT,
  `forms_id` INT NOT NULL,
  `type` VARCHAR(45) NULL,
  `required` INT NULL,
  `is_admin` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `inputs_forms` (`forms_id` ASC),
  CONSTRAINT `fk_inputs_forms`
    FOREIGN KEY (`forms_id`)
    REFERENCES {{%forms}} (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `values_plan`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS {{%values_plan}} (
  `id` INT NOT NULL AUTO_INCREMENT,
  `plan_id` INT NOT NULL,
  `section_id` INT NULL,
  `inputs_id` INT NULL,
  `reja` VARCHAR(45) NULL,
  `moliya_manbai_id` INT NULL,
  `molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `plan_values_plan` (`plan_id` ASC),
  INDEX `plan_values_sections` (`section_id` ASC),
  INDEX `plan_values_inputs` (`inputs_id` ASC),
  INDEX `plan_values_moliya_manbai` (`moliya_manbai_id` ASC),
  CONSTRAINT `fk_plan_values_plan`
    FOREIGN KEY (`plan_id`)
    REFERENCES {{%locality_plans}} (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_plan_values_sections`
    FOREIGN KEY (`section_id`)
    REFERENCES {{%sections}} (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_plan_values_inputs`
    FOREIGN KEY (`inputs_id`)
    REFERENCES {{%inputs}} (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_plan_values_moliya_manbai`
    FOREIGN KEY (`moliya_manbai_id`)
    REFERENCES {{%moliya_manbai}} (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `values_fact`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS {{%values_fact}} (
  `id` INT NOT NULL AUTO_INCREMENT,
  `plan_value_id` INT NOT NULL,
  `inputs_id` INT NULL,
  `user_id` INT NULL,
  `fact` VARCHAR(45) NULL,
  `post_date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `values_fact_plan_values` (`plan_value_id` ASC),
  INDEX `values_fact_inputs` (`inputs_id` ASC),
  CONSTRAINT `fk_values_fact_plan_values`
    FOREIGN KEY (`plan_value_id`)
    REFERENCES {{%values_plan}} (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_values_fact_inputs`
    FOREIGN KEY (`inputs_id`)
    REFERENCES {{%inputs}} (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;
");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190415_064609_clever_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190415_064609_clever_tables cannot be reverted.\n";

        return false;
    }
    */
}
